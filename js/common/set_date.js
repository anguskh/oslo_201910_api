function set_date(init_flag = true,change_flag = 0){
	if(init_flag)
	{
		//日期picker, 初始化
		$j(":date").dateinput({ 
			lang: 'zh-TW', 
			trigger: false, 
			format: 'yyyy-mm-dd', 
			offset: [0, 0], 
			selectors: true, 
			yearRange: [-7, 3],
			change: function(event) {
				alert(this.getInput().attr('id'));
				switch(change_flag)
				{
					case 1:
						alert(this.getValue("yyyy-mm-dd"));
						break;
					case 2:
						alert(this.getValue("yyyy-mm-dd"));
						break;
				}
			}
		});

		$j(":date").unbind('keydown');
		$j(":date").attr('maxlength','10');
		$j(":date").keydown(function(event){
			var key = event.which;
			// console.log(key);
			
			//只可輸入數字和 - / backspace(8) del(46) tab(9)
			if ((key >= 48 && key <= 57) || key == 189 || key == 191 || 
				(key >= 96 && key <= 105) || key == 109 || key == 111 || 
				key == 46 || key == 9 || key == 8) {
			}else{
				event.preventDefault();
			}
		});

		$j(":date").blur(function(){
			var val = $j(this).val();
			if(val != ''){
				val = val.replace(/\//g, "-");
				$j(this).val(val);
				//檢查日期
				if(isNaN(checkDate(val))){
					alert('日期格式不正確');
					$j(this).val('');
					$j(this).focus();
					return;
				}else{
					$j(this).val(new Date(val).format('yyyy-MM-dd'));
				}
			}
		});

		//檢查日期
		function checkDate(str) {
			var t = Date.parse(str);
			console.log(t);
			return t;
		}
		
		//日期格式轉換
		Number.prototype.pad2 =function(){   
			return this>9?this:'0'+this;   
		}   
		Date.prototype.format=function (format) {   
			var it=new Date();   
			var it=this;   
			var M=it.getMonth()+1,H=it.getHours(),m=it.getMinutes(),d=it.getDate(),s=it.getSeconds();   
			var n={ 'yyyy': it.getFullYear()   
					,'MM': M.pad2(),'M': M   
					,'dd': d.pad2(),'d': d   
					,'HH': H.pad2(),'H': H   
					,'mm': m.pad2(),'m': m   
					,'ss': s.pad2(),'s': s   
			};   
			return format.replace(/([a-zA-Z]+)/g,function (s,$1) { return n[$1]; });   
		}
	}

}