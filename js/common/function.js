var number = 10;
function LMYC() {
    var lbmc;
    for (i = 1; i <= number; i++) {
        lbmc = eval('LM' + i);
        lbmc.style.display = 'block';
    }
}
function ShowFLT(i) {
    lbmc = eval('LM' + i);
    if (lbmc.style.display == 'none') {
        LMYC();
        lbmc.style.display = '';
    } else {
        lbmc.style.display = 'none';
    }
}

//檢查瀏覽器
function detectBrowser (){
	var sAgent = navigator.userAgent.toLowerCase();
	this.isIE = (sAgent.indexOf("msie")!=-1); //IE6.0-7
	this.isFF = (sAgent.indexOf("firefox")!=-1);//firefox
	this.isSa = (sAgent.indexOf("safari")!=-1);//safari
	this.isOp = (sAgent.indexOf("opera")!=-1);//opera
	this.isNN = (sAgent.indexOf("netscape")!=-1);//netscape
	this.isCh = (sAgent.indexOf("chrome")!=-1);//chrome
	this.isMa = this.isIE;//marthon
	this.isOther = (!this.isIE && !this.isFF && !this.isSa && !this.isOp && !this.isNN && !this.isSa);//unknown Browser	
}

//判斷瀏覽器,並選擇如何開新視窗
function selectOpenType (){
	var oBrowser = new detectBrowser();
	if (oBrowser.isIE) {  //IE瀏覽器
		return '';
	}else{  //其他瀏覽器
		return 'top=0,left=0,width=500,height=500,toolbar=no,location=no,directories=no,menubar=no,scrollbars=yes,resizable=no,status=no';
	}
}

//如果選擇格式錯誤則清空選擇欄位,避免上傳錯誤格式
function abgne(){ 
	document.getElementById("img_name").outerHTML=document.getElementById("img_name").outerHTML.replace(/value=\w/g,''); 
	//再次重新載入,避免imgUpload無法再次控制
	//uploadimg();
} 

//預覽圖片
function uploadimg(img_preview, destination, id) {
    var docObj = document.getElementById(id);
    var imgObjPreview = document.getElementById(img_preview);
	var iw = $j("#"+img_preview).attr("width"); 
	var ih = $j("#"+img_preview).attr("height"); 
	var oldsrc = $j("#"+img_preview).attr("src"); 
	var imgLocalValue = docObj.value;
	var extStart=imgLocalValue.lastIndexOf("."); 
	var ext=imgLocalValue.substring(extStart,imgLocalValue.length).toUpperCase(); 
	var checkimg = true;
	var errormsg = "上傳圖片限於png,gif,jpeg,jpg格式,請重新選擇!"; 
	
	//檢查是否為圖片類型
	if( 
		ext != ".PNG" && 
		ext != ".GIF" && 
		ext != ".JPG" &&
		ext != ".JPEG" &&
		ext != ".BMP"
		){ 
		checkimg = false;
	}

	if(docObj.files &&  docObj.files[0]){	 //其他瀏覽器
		//檢查是否為圖片類型
		var fileType = docObj.files[0].type;
		
		//if(!(/^image\/.*$/i.test(fileType))){ //另一種其他瀏覽器判斷
		if(checkimg == false){
			alert(errormsg);
			//$("#img_preview").attr("src", oldsrc);  //換回原本圖檔(jquery)
			imgObjPreview.attributes["src"].value = oldsrc;
			abgne();
			return false;
		}
		
		var file =docObj.files[0]; 
		var freader = new FileReader();
		freader.readAsDataURL(file);
		freader.onload=function(e){
			// var img = '<img id="'+img_preview+'" src="'+e.target.result+'" width="'+iw+'px" height="'+ih+'px"/>';
			var img = '<img id="'+img_preview+'" src="'+e.target.result+'" class="img_preview" />';
			$j("#"+destination).empty().append(img);
		}
	}else{	//IE,使用濾鏡
		docObj.select();
		docObj.blur();
		var imgSrc = document.selection.createRange().text;
		var localImagId = document.getElementById(destination);
		
		//必須設置大小
		localImagId.style.width = iw+"px";
		localImagId.style.height = ih+"px";
		
		//防偽
		try{
			if(checkimg == false){
				alert(errormsg);
				abgne();
				return false;
			} else {
				localImagId.style.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale)";
				localImagId.filters.item("DXImageTransform.Microsoft.AlphaImageLoader").src = imgSrc;
			}
		}catch(e){
			alert("上傳圖片限於png,gif,jpeg,jpg格式,請重新選擇!");
			return false;
		}
		imgObjPreview.style.display = 'none';
		document.selection.empty();
	}
	return true;
}

//只能輸入數字
function ValidateNumber(e, pnumber)
{
    if (!/^\d+$/.test(pnumber))
    {
        $(e).val(/^\d+/.exec($(e).val()));
    }
    return false;
}

//只可輸入數字2 ,只能有數字不能有小數點
function ValidateNumber2(){
	return /[0-9]/.test(String.fromCharCode(event.keyCode));
}


//設定連動combobox =============================================================================
//arryData 傳入資料陣列變數
//sMain 主要combobox id
//sDynamic 動態combobox id
//sDefValue 動態combobox預設值
function setupCombobox(arryData, sMain, sDynamic, sDefValue){
	var main_combobox = document.getElementById(sMain);
	var dynamic_combobox = document.getElementById(sDynamic);

	custLinkCombobox(arryData, main_combobox, dynamic_combobox, sDefValue);	//預先執行, 讓資料先塞至動態combobox
	main_combobox.onchange=function(){	//觸發主combobox
		custLinkCombobox(arryData, this, dynamic_combobox);
	}
}

//連動 mainCombobox[主要combobox], target[動態combobox], defaultvalue[動態combobox預設值]
function custLinkCombobox(arryData, mainCombobox, target, defaultvalue){	//觸發主combobox
	if(typeof(defaultvalue) === 'undefined') defaultvalue = '';	//預設值
	target.innerHTML='';
	target.options.add(first());

	if(mainCombobox.selectedIndex>0){	//動態塞值至目標combobox
		for(var i in arryData[mainCombobox.value]){   
			objoption=document.createElement('option');
			objoption.text = arryData[mainCombobox.value][i];
			objoption.value = i.substring(1);
			//objoption.value = i;
			target.options.add(objoption);
		}
		
		if (defaultvalue != ''){	//選擇預設值
			target.value = defaultvalue;
			var indexid = target.options.selectedIndex;
			target.options[indexid].setAttribute("selected", "selected");
			//alert(target.options[indexid].text); 
		}
	}
}

function first(){	//重置選單
	var objoption=document.createElement('option');
	objoption.text='請選擇';
	objoption.value="";
	return objoption;
}
//==========================================================================================

