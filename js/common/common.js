//sleep
function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

//日期起訖檢查, 起日不可大於迄日
function checkDate(sdate, edate, checksdate){
	if(checksdate == undefined){
		checksdate = true;
	}
	//時間
	var startDate = $j("#"+sdate).val(); //起
	var endDate = $j("#"+edate).val(); //迄
	if(startDate != "" && endDate != ""){
		startDate = startDate.replace(/-/g, "/");
		startDate = Date.parse(startDate).valueOf();
		endDate = endDate.replace(/-/g, "/");
		endDate = Date.parse(endDate).valueOf();
		if(startDate > endDate){	//起日不可大於迄日
			alert('起日不可大於迄日!');
			return false;
		}else{
			return true;
		}
	}else if(startDate ==  "" && checksdate == true){
		alert('請選擇起日!');
		return false;
	}else{
		return true;
	}
}

//只可輸入數字2 ,只能有數字不能有小數點
function ValidateNumber2(){
	return /[0-9]/.test(String.fromCharCode(event.keyCode));
}