<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
// rework by Jaff
$route['default_controller'] = "login";
$route['404_override'] = '';

//亂碼url路由
// $route['xxx/xxx__(:num)'] = "xxx/xxx";
// $route['xxx/xxx__(:num)/index'] = "xxx/xxx";
// $route['xxx/xxx__(:num)/index/(:num)'] = "xxx/xxx/index/$2";
// $route['xxx/xxx__(:num)/(:any)'] = "xxx/xxx/$2";

$route['admin/password__(:num)'] = "admin/password";
$route['admin/password__(:num)/index'] = "admin/password";
$route['admin/password__(:num)/index/(:num)'] = "admin/password/index/$2";
$route['admin/password__(:num)/(:any)'] = "admin/password/$2";

$route['admin/config__(:num)'] = "admin/config";
$route['admin/config__(:num)/index'] = "admin/config";
$route['admin/config__(:num)/index/(:num)'] = "admin/config/index/$2";
$route['admin/config__(:num)/(:any)'] = "admin/config/$2";

$route['admin/group__(:num)'] = "admin/group";
$route['admin/group__(:num)/index'] = "admin/group";
$route['admin/group__(:num)/index/(:num)'] = "admin/group/index/$2";
$route['admin/group__(:num)/(:any)'] = "admin/group/$2";

$route['admin/menu__(:num)'] = "admin/menu";
$route['admin/menu__(:num)/index'] = "admin/menu";
$route['admin/menu__(:num)/index/(:num)'] = "admin/menu/index/$2";
$route['admin/menu__(:num)/(:any)'] = "admin/menu/$2";

$route['admin/user__(:num)'] = "admin/user";
$route['admin/user__(:num)/index'] = "admin/user";
$route['admin/user__(:num)/index/(:num)'] = "admin/user/index/$2";
$route['admin/user__(:num)/(:any)'] = "admin/user/$2";

$route['api/ecu01__(:num)'] = "api/ecu01";
$route['api/ecu01__(:num)/index'] = "api/ecu01";
$route['api/ecu01__(:num)/index/(:num)'] = "api/ecu01/index/$2";
$route['api/ecu01__(:num)/(:any)'] = "api/ecu01/$2";

$route['show_driver_info__(:num)'] = "show_driver_info";
$route['show_driver_info__(:num)/index'] = "show_driver_info";
$route['show_driver_info__(:num)/index/(:num)'] = "show_driver_info/index/$2";
$route['show_driver_info__(:num)/(:any)'] = "show_driver_info/$2";


/* End of file routes.php */
/* Location: ./application/config/routes.php */