<?
	$web = $this->config->item('base_url');
	$this->load->helper('url');
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="<?=$web?>css/tagpage.css" />
<script src="<?=$web?>js/jquery-1.8.2.min.js"></script>
<title>電動機車API測試工具</title>
</head>
<style>
#title_1{
	font-size: 24px;
	font-family: Sans-Serif;
	color:#464D57;
	line-height: 100%;
}
#title_2{
	font-size: 22px;
	font-family: Sans-Serif;
	color: #FEFFFF;
	line-height: 100%;
}
</style>
<body>
	<!-- 系統標題 Start -->
	<div align="center" id="header_1" style="background:#B9BABC;padding: 7px;">
		<table width="100%">
			<tr>
				<td width="25%">&nbsp;</td>
				<td width="50%" align="center">
					<div id="title_1">API測試工具</div>
					<div id="title_2">API Test Tool</div>
				</td>
				<td width="25%">&nbsp;</td>
			</tr>
		</table>
	</div>
	<!-- 系統標題 End -->
	<div><input type="button" id="bss01" value="監控"><input type="button" id="bss02" value="借電成功"><input type="button" id="bss03" value="還電計費"><input type="button" id="bss04" value="借電請求查詢"><input type="button" id="ecu01" value="電池回報"><input type="button" id="ecu02" value="車機回報"></div>
</body>
<script>
	$("#bss01").click(function(){
		location.href = "<?=$web?>testtool";
	});

	$("#bss02").click(function(){
		location.href = "<?=$web?>testtool/testbss02";
	});

	$("#bss02").click(function(){
		location.href = "<?=$web?>testtool/testbss03";
	});

	$("#bss02").click(function(){
		location.href = "<?=$web?>testtool/testbss04";
	});

	$("#ecu01").click(function(){
		location.href = "<?=$web?>testtool/testecu01";
	});

	$("#ecu02").click(function(){
		location.href = "<?=$web?>testtool/testecu02";
	});
</script>