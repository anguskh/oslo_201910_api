<?
	$web = $this->config->item('base_url');
	$this->load->helper('url');

?>
<div class="abgne_tab">
	<div class="abgne_tab" style="width:1000px;">
		<ul class="tabs">
			<li><a href="#tab1">交換監控表單</a></li>
			<li><a href="#tab2">發送JSON</a></li>
			<li><a href="#tab3">回覆JSON</a></li>
		</ul>
		<div class="tab_container" >
			
			<div id="tab1" class="tab_content">
			<INPUT TYPE="button" id="rank_txt" onclick="rank_txt_click()" value="亂數產生">
				<form method="POST" name="input_form" id="input_form">
					<table class="table_input2">
			        <tbody><tr>
			          <td width="16%">
			            BSS請求日期：
			          </td>
			          <td width="36%">
			            <input type="datetime-local" name="f_upload_date" id="f_upload_date" class="datetime hasDatepicker">
			          </td>
			          <td width="18%">
			            當前黑名單版本：
			          </td>
			          <td width="30%">
			            <input type="text" name="f_blacklist_ver" id="f_blacklist_ver" class="datetime2 hasDatepicker">
			          </td>
			        </tr>
			        <tr>
			          <td>
			            BSS序號：
			          </td>
			          <td colspan="3">
						<select name="f_bss_id" id="f_bss_id">
<?
						echo '<option value="">無</option>';
						if($batteryswaps->num_rows()>0){
							foreach($batteryswaps->result_array()  as $b_arr){
								echo '<option value="'.$b_arr['bss_id'].'" bss_token="'.$b_arr['bss_token'].'">'.$b_arr['bss_id'].'【'.$b_arr['top01'].'】</option>';
							}
						}
?>
						</select>
			          </td>
			        </tr>
					<tr>
						<td colspan="4">
							<div class="abgne_tab2">
									<ul class="tabs2">
										<li><a href="#track_no1">第1軌</a></li>
										<li><a href="#track_no2">第2軌</a></li>
										<li><a href="#track_no3">第3軌</a></li>
										<li><a href="#track_no4">第4軌</a></li>
										<li><a href="#track_no5">第5軌</a></li>
										<li><a href="#track_no6">第6軌</a></li>
										<li><a href="#track_no50">第50軌</a></li>
									</ul>
									<div class="tab_container2">
<?
										$start = array('1','2','3','4','5','6','7','50');
										for($t=0; $t<8; $t++){
											$no = $start[$t];
?>
										<div id="track_no<?=$no?>" class="tab_content">
											<table>
											<tr style="display:none;">
											  <td>
												軌道編號：
											  </td>
											  <td colspan="3">
												<input name="f_track_no" id="f_track_no" type="radio" value="1" checked="">第1軌&nbsp;&nbsp;
												<input name="f_track_no" id="f_track_no" type="radio" value="2">第2軌&nbsp;&nbsp;
												<input name="f_track_no" id="f_track_no" type="radio" value="3">第3軌&nbsp;&nbsp;
												<input name="f_track_no" id="f_track_no" type="radio" value="4">第4軌&nbsp;&nbsp;
												<input name="f_track_no" id="f_track_no" type="radio" value="5">第5軌&nbsp;&nbsp;
												<input name="f_track_no" id="f_track_no" type="radio" value="6">第6軌
											  </td>
											</tr>
											</div>
											<tr>
											  <td>
												軌道狀態：
											  </td>
											  <td>
												<select name="f_track_status<?=$no?>" id="f_track_status<?=$no?>" >
												  <option value="1">1.空軌且都正常</option>
												  <option value="2">2.有電池且都正常</option>
												  <option value="3">3.空軌且BCU異常</option>
												  <option value="4">4.空軌且充電器異常</option>
												  <option value="5">5.空軌且BCU和充電器都異常</option>
												  <option value="6">6.有電池且BCU異常</option>
												  <option value="7">7.有電池且充電器異常</option>
												  <option value="8">8.有電池且BCU和充電器都異常</option>
												  <option value="9">9.電池應取但未取出</option>
												</select>
											  </td>
											  <td>
												LED狀態：
											  </td>
											  <td>
												<input name="f_status_led<?=$no?>" id="f_status_led<?=$no?>" type="radio" value="1" checked="">正常&nbsp;&nbsp;
												<input name="f_status_led<?=$no?>" id="f_status_led<?=$no?>" type="radio" value="2">異常&nbsp;&nbsp;
												<input name="f_status_led<?=$no?>" id="f_status_led<?=$no?>" type="radio" value="3">停用
											  </td>
											</tr>
											<tr>
											  <td>
												Photo Sensor狀態：
											  </td>
											  <td>
												<input name="f_status_photo_sensor<?=$no?>" id="f_status_photo_sensor<?=$no?>" type="radio" value="1" checked="">正常&nbsp;&nbsp;
												<input name="f_status_photo_sensor<?=$no?>" id="f_status_photo_sensor<?=$no?>" type="radio" value="2">異常&nbsp;&nbsp;
												<input name="f_status_photo_sensor<?=$no?>" id="f_status_photo_sensor<?=$no?>" type="radio" value="3">停用
											  </td>
											  <td>
												BCU狀態：
											  </td>
											  <td>
												<input name="f_status_bcu<?=$no?>" id="f_status_bcu<?=$no?>" type="radio" value="1" checked="">正常&nbsp;&nbsp;
												<input name="f_status_bcu<?=$no?>" id="f_status_bcu<?=$no?>"type="radio" value="2">異常&nbsp;&nbsp;
												<input name="f_status_bcu<?=$no?>" id="f_status_bcu<?=$no?>"type="radio" value="3">停用
											  </td>
											</tr>
											<tr>
											  <td>
												電池置入狀態：
											  </td>
											  <td>
												<input name="f_status_battery_in_track<?=$no?>" id="f_status_battery_in_track<?=$no?>" type="radio" value="1" checked="">該軌道有電池&nbsp;&nbsp;
												<input name="f_status_battery_in_track<?=$no?>" id="f_status_battery_in_track<?=$no?>" type="radio" value="0">該軌道無電池
											  </td>
											  <td>
												軌道目前是否啟用：
											  </td>
											  <td>
												<input name="f_track_enable<?=$no?>" id="f_track_enable<?=$no?>" type="radio" value="1" checked="">啟用&nbsp;&nbsp;
												<input name="f_track_enable<?=$no?>" id="f_track_enable<?=$no?>" type="radio" value="0">未啟用
											  </td>
											</tr>
											<tr>
											  <td>
												電池序號：
											  </td>
											  <td>
												<select name="f_battery_id<?=$no?>" id="f_battery_id<?=$no?>">
						<?
												echo '<option value="">無</option>';
												if($battery->num_rows() >0){
													foreach($battery->result_array() as $b_arr){
														echo '<option value="'.$b_arr['battery_id'].'">'.$b_arr['battery_id'].'</option>';
													}
												}
						?>
												</select>

											  </td>
											  <td>
												車輛使用者序號：
											  </td>
											  <td>
												<select name="f_vehicle_user_id<?=$no?>" id="f_vehicle_user_id<?=$no?>">
						<?
												echo '<option value="">無</option>';
												if($vehicle->num_rows()>0){
													foreach($vehicle->result_array()  as $v_arr){
														echo '<option value="'.$v_arr['vehicle_user_id'].'">'.$v_arr['vehicle_code'].'【'.$v_arr['vehicle_user_id'].'】</option>';
													}
												}
						?>
												</select>
											  </td>
											</tr>
											<tr>
											  <td>
												電池電壓：
											  </td>
											  <td>
												<input type="text" name="f_battery_voltage<?=$no?>" id="f_battery_voltage<?=$no?>">伏特
											  </td>
											  <td>
												電池芯狀態：
											  </td>
											  <td>
												<input name="f_battery_cell_status<?=$no?>" id="f_battery_cell_status<?=$no?>" type="radio" value="1" checked="">Over Temp&nbsp;&nbsp;
												<input name="f_battery_cell_status<?=$no?>" id="f_battery_cell_status<?=$no?>" type="radio" value="0">N/A
												<!-- <select name="f_battery_cell_status">
												  <option value="">--</option>
												</select> -->
											  </td>
											</tr>
											<tr>
											  <td>
												電池電流：
											  </td>
											  <td>
												<input type="text" name="f_battery_amps<?=$no?>" id="f_battery_amps<?=$no?>">安培
											  </td>
											  <td>
												充電次數：
											  </td>
											  <td>
												<input type="text" name="f_charge_cycles<?=$no?>" id="f_charge_cycles<?=$no?>">次
											  </td>
											</tr>
											<tr>
											  <td>
												電池溫度：
											  </td>
											  <td>
												<input type="text" name="f_battery_temperature<?=$no?>" id="f_battery_temperature<?=$no?>">℃
											  </td>
											  <td>
												已充電時間：
											  </td>
											  <td>
												<input type="text" name="f_electrify_time<?=$no?>" id="f_electrify_time<?=$no?>">秒
											  </td>
											</tr>
											<tr>
											  <td>
												電池容量百分比：
											  </td>
											  <td>
												<input type="text" name="f_battery_capacity<?=$no?>" id="f_battery_capacity<?=$no?>">%
											  </td>
											  <td>
												電池狀態：
											  </td>
											  <td>
												<input name="f_battery_status<?=$no?>" id="f_battery_status<?=$no?>" type="radio" value="0" checked="">充電中&nbsp;&nbsp;
												<input name="f_battery_status<?=$no?>" id="f_battery_status<?=$no?>" type="radio" value="1">飽電
											  </td>
											</tr>
											</table>
										</div>
<?
										}

?>
									</div>
								</div>
						</td>
					</tr>
			        <tr>
			          <td colspan="4" align="center">
			            <input type="button" name="f_send" id="f_send" value="發送訊息" class="button">
			            &nbsp;&nbsp;&nbsp;&nbsp;
			            <input type="button" name="f_upd" id="f_upd" value="更新表單" class="button">
			          </td>
			        </tr>
			      </tbody></table>
			    </form>
			</div>
			<div id="tab2" class="tab_content">
			</div>
			<div id="tab3" class="tab_content">
				訊息回應碼：
			    <span id="irs_msg"></span>
			    <hr>
			    顯示訊息回應碼說明：<br>
			    0000=&gt;寫入DB成功<br>
			    0001=&gt;格式錯誤<br>
			    0002=&gt;寫入DB失敗<br>
			    0003=&gt;資料為空<br>
			    0004=&gt;找不到對應的電池交換站資料
			</div>
			<div id="icode_msg"></div>
		</div>
	</div>
</div>	
<script type="text/javascript"><?/* <!-- 頁籤javascript設定 --> */?>
	$(function(){
		// 預設顯示第一個 Tab
		var _showTab = 0;
		var $defaultLi = $('ul.tabs li').eq(_showTab).addClass('active');
		$($defaultLi.find('a').attr('href')).siblings().hide();
		
		// 當 li 頁籤被點擊時...
		// 若要改成滑鼠移到 li 頁籤就切換時, 把 click 改成 mouseover
		$('ul.tabs li').click(function() {
			// 找出 li 中的超連結 href(#id)
			var $this = $(this),
				_clickTab = $this.find('a').attr('href');
			// 把目前點擊到的 li 頁籤加上 .active
			// 並把兄弟元素中有 .active 的都移除 class
			$this.addClass('active').siblings('.active').removeClass('active');
			// 淡入相對應的內容並隱藏兄弟元素
			$(_clickTab).stop(false, true).fadeIn().siblings().hide();

			return false;
		}).find('a').focus(function(){
			this.blur();
		});


		var _showTab2 = 0;
		$('.abgne_tab2').each(function(){
			// 目前的頁籤區塊
			var $tab = $(this);
	 
			var $defaultLi = $('ul.tabs2 li', $tab).eq(_showTab2).addClass('active');
			$($defaultLi.find('a').attr('href')).siblings().hide();
	 
			// 當 li 頁籤被點擊時...
			// 若要改成滑鼠移到 li 頁籤就切換時, 把 click 改成 mouseover
			$('ul.tabs2 li', $tab).click(function() {
				// 找出 li 中的超連結 href(#id)
				var $this = $(this),
					_clickTab2 = $this.find('a').attr('href');
				// 把目前點擊到的 li 頁籤加上 .active
				// 並把兄弟元素中有 .active 的都移除 class
				$this.addClass('active').siblings('.active').removeClass('active');
				// 淡入相對應的內容並隱藏兄弟元素
				$(_clickTab2).stop(false, true).fadeIn().siblings().hide();
	 
				return false;
			}).find('a').focus(function(){
				this.blur();
			});
		});
	});

	$('#f_send').click(function(){
      // 結果清空
      $('#irs_msg').html('');
      $('#tab2').html('');
      var dataArr = new Array();
      var dataArr2 = new Array();
      var dataArr3 = new Array();
      var obj = new Object;
      var obj2 = new Object;
      obj.em01 = $("#f_upload_date").val();//BSS請求日期
      obj.em02 = $("#f_bss_id").val();//BSS序號
      obj.em03 = (($("#f_bss_id").attr('bss_token') == undefined)?'':$("#f_bss_id").attr('bss_token'));//交換站Token ID
	  obj.em04 = '';
      obj.em05 = $("#f_blacklist_ver").val();//當前黑名單版本
	  var start = 0;
	  var s_arr = new Array('1','2','3','4','5','6','7','50');
	  for(s=0; s<8; s++){
			var obj2 = new Object;
			var i = s_arr[s];
			obj2.em06 = i;//軌道編號
			obj2.em07 = $('#f_track_status'+i).val();//軌道狀態
			obj2.em08 = $('input[name="f_status_led'+i+'"]:checked').val();//LED狀態
			obj2.em09 = $('input[name="f_status_photo_sensor'+i+'"]:checked').val();//Photo Sensor狀態
			obj2.em10 = $('input[name="f_status_bcu'+i+'"]:checked').val();//BCU狀態
			obj2.em11 = $('input[name="f_status_battery_in_track'+i+'"]:checked').val();//電池置入狀態
			obj2.em12 = $("#f_battery_id"+i).val();//電池序號
			obj2.em13 = $("#f_vehicle_user_id"+i).val();//車輛使用者序號
			obj2.em14 = $('input[name="f_battery_status'+i+'"]:checked').val();//電池狀態
			obj2.em15 = $("#f_battery_capacity"+i).val();//電池容量百分比
			obj2.em16 = $("#f_battery_temperature"+i).val();//電池溫度
			obj2.em17 = $("#f_battery_amps"+i).val();//電池電流
			obj2.em18 = $("#f_battery_voltage"+i).val();//電池電壓
			obj2.em19 = $("#f_charge_cycles"+i).val();//充電次數
			obj2.em20 = $("#f_electrify_time"+i).val();//已充電時間
			obj2.em21 = $('input[name="f_battery_cell_status'+i+'"]:checked').val();//電池芯狀態

			dataArr2[start] = obj2;
			start++;
	  }
	  //obj.em_info = dataArr2;
	  obj.em_info = dataArr3.concat(dataArr2);;
      console.log(obj);
      var strMsg = "ajax錯誤";
      var json_text = JSON.stringify(obj);
      $.ajax({
        type:'post',
        url: '<?=$web?>api/bss01',
        data: {JSONData:json_text},
        dataType: "json",
        error: function(xhr) {
          strMsg += 'Ajax request發生錯誤[json_monitor_bg.php]:'+xhr+'\n請重試';
        },
        success: function (rs) {
          //console.log(rs);
          //alert(rs.f_code);
          //alert(rs.f_json_data);
          $('#tab2').html(json_text);
          // $('#irs_msg').html(rs.f_code);
          $("#irs_msg").html(rs.return_code);
          alert("傳送成功");
          // $("#icode_msg").dialog({
          //   title: "訊息狀態",
          //   bgiframe: true,
          //   width: 400,
          //   height: 200,
          //   modal: true,
          //   draggable: true,
          //   resizable: false,
          //   buttons: {
          //     關閉訊息:
          //     function(){
          //       $('#icode_msg').dialog( 'close' );
          //     },
          //   }
          // });
        }
      })
    })
	function rank_txt_click(){
		var strMsg = '';
		  $.ajax({
			type:'post',
			url: '<?=$web?>testtool/bss01Rank',
			data: {source:'bss01'},
			dataType: "json",
			error: function(xhr) {
			  strMsg += 'Ajax request發生錯誤\n請重試';
			},
			beforeSend:function(){
				$('#loadingIMG').show();
			},
			complete:function(){
				// alert('complete');
				$('#loadingIMG').hide();
			},			
			success: function (rs) {
				console.log(rs);
				if(rs[0] = 'Y'){
				  var s_arr = new Array('1','2','3','4','5','6','7','50');
				  for(s=0; s<8; s++){
					var i = s_arr[s];
						$.each( rs[i], function( key, value ) {
							if(i==1){
								//前3個先塞入
								$("#"+key).val(value);
							}
							if($("#"+key+i).attr('type') == 'radio'){
								$('[id='+key+i+'][value='+value+']').prop('checked',true);
							}else{
								$("#"+key+i).val(value);
							}
						});
					}
				}
			}
		})
	}
</script>
<div id="loadingIMG" style="display: none;">
	<div id="img_label">資料處理中，請稍後。</div>
</div>
