<?
	$web = $this->config->item('base_url');
	$this->load->helper('url');
?>
<div class="abgne_tab">
	<div class="abgne_tab" style="width:1000px;">
		<ul class="tabs">
			<li><a href="#tab1">交換表單</a></li>
			<li><a href="#tab2">發送JSON</a></li>
			<li><a href="#tab3">回覆JSON</a></li>
		</ul>
		<div class="tab_container" >
			<div id="tab1" class="tab_content">
			<INPUT TYPE="button" id="rank_txt" onclick="rank_txt_click()" value="亂數產生">
				<form method="POST" name="input_form" id="input_form">
					<table class="table_input2">
			        <tbody><tr>
			          <td width="18%">
			            借電站電池借出日期：
			          </td>
			          <td width="82%">
			            <input type="datetime-local" name="f_upload_date" id="f_upload_date" class="datetime hasDatepicker">
			          </td>
			        </tr>
			        <tr>
			          <td>
			            借電站Token ID：
			          </td>
			          <td>
						<select name="f_bss_id" id="f_bss_id">
<?
						echo '<option value="">無</option>';
						if($batteryswaps->num_rows()>0){
							foreach($batteryswaps->result_array()  as $b_arr){
								echo '<option value="'.$b_arr['bss_id'].'">'.$b_arr['bss_id'].'【'.$b_arr['note'].'】</option>';
							}
						}
?>
						</select>
			          </td>
			        </tr>
			        <tr>
			          <td>
			            User ID：
			          </td>
			          <td>
			            <input name="f_swap_type" id="f_swap_type" type="radio" value="0" checked="">離線交換&nbsp;&nbsp;
			            <input name="f_swap_type" id="f_swap_type" type="radio" value="1">連線交換
			          </td>
			        </tr>
			        <tr>
			          <td>
			            車輛使用者序號：
			          </td>
			          <td>
						<select name="f_vehicle_user_id1" id="f_vehicle_user_id1">
<?
 						echo '<option value="">無</option>';
						if($vehicle->num_rows()>0){
							foreach($vehicle->result_array()  as $v_arr){
								echo '<option value="'.$v_arr['vehicle_user_id'].'">'.$v_arr['vehicle_code'].'【'.$v_arr['vehicle_user_id'].'】</option>';
							}
						}
?>
						</select>
			          </td>
			        </tr>
			        <tr>
			          <td>
			            換出電池的軌道編號：
			          </td>
			          <td>
			            <input name="f_track_no1" id="f_track_no1" type="radio" value="1" checked="">第1軌&nbsp;&nbsp;
			            <input name="f_track_no1" id="f_track_no1" type="radio" value="2">第2軌&nbsp;&nbsp;
			            <input name="f_track_no1" id="f_track_no1" type="radio" value="3">第3軌&nbsp;&nbsp;
			            <input name="f_track_no1" id="f_track_no1" type="radio" value="4">第4軌&nbsp;&nbsp;
			            <input name="f_track_no1" id="f_track_no1" type="radio" value="5">第5軌&nbsp;&nbsp;
			            <input name="f_track_no1" id="f_track_no1" type="radio" value="6">第6軌
			          </td>
			        </tr>
			        <tr>
			          <td>
			            換出的電池序號：
			          </td>
			          <td>
						<select name="f_battery_id1" id="f_battery_id1">
<?
						echo '<option value="">無</option>';
						if($battery->num_rows() >0){
							foreach($battery->result_array() as $b_arr){
								echo '<option value="'.$b_arr['battery_id'].'">'.$b_arr['battery_id'].'</option>';
							}
						}
?>
						</select>
			          </td>
			        </tr>
			      </tbody></table>
			    </form>
			</div>
			<div id="tab2" class="tab_content">
			</div>
			<div id="tab3" class="tab_content">
				訊息回應碼：
			    <span id="irs_msg"></span>
			    <hr>
			    顯示訊息回應碼說明：<br>
			    0000=&gt;寫入DB成功<br>
			    0001=&gt;格式錯誤<br>
			    0002=&gt;寫入DB失敗<br>
			    0003=&gt;資料為空<br>
			    0004=&gt;找不到對應的電池交換站資料<br>
			    0005=&gt;找不到對應的車輛資料
			</div>
			<div id="icode_msg"></div>
		</div>
	</div>
</div>	
<script type="text/javascript"><?/* <!-- 頁籤javascript設定 --> */?>
	$(function(){
		// 預設顯示第一個 Tab
		var _showTab = 0;
		var $defaultLi = $('ul.tabs li').eq(_showTab).addClass('active');
		$($defaultLi.find('a').attr('href')).siblings().hide();
		
		// 當 li 頁籤被點擊時...
		// 若要改成滑鼠移到 li 頁籤就切換時, 把 click 改成 mouseover
		$('ul.tabs li').click(function() {
			// 找出 li 中的超連結 href(#id)
			var $this = $(this),
				_clickTab = $this.find('a').attr('href');
			// 把目前點擊到的 li 頁籤加上 .active
			// 並把兄弟元素中有 .active 的都移除 class
			$this.addClass('active').siblings('.active').removeClass('active');
			// 淡入相對應的內容並隱藏兄弟元素
			$(_clickTab).stop(false, true).fadeIn().siblings().hide();

			return false;
		}).find('a').focus(function(){
			this.blur();
		});
	});

	$('#f_send').click(function(){
      // 結果清空
      $('#irs_msg').html('');
      $('#tab2').html('');
      var dataArr = new Array();
      var dataArr2 = new Array();
      var obj = new Object;
      var obj2 = new Object;
      obj.upload_date = $("#f_upload_date").val();//BSS請求日期
      obj.bss_id = $("#f_bss_id").val();//BSS序號
      obj.swap_type = $('input[name="f_swap_type"]:checked').val();//交換類別
      obj.vehicle_user_id = $("#f_vehicle_user_id1").val();//車輛使用者序號
      obj.track_no1 = $('input[name="f_track_no1"]:checked').val();//換出的軌道編號
      obj.battery_id1 = $("#f_battery_id1").val();//換出的電池序號
      obj.track_no2 = $('input[name="f_track_no2"]:checked').val();//換回的軌道編號
      obj.battery_id2 = $("#f_battery_id2").val();//喚回的電池序號
      obj.vehicle_code = $("#f_vehicle_code").val();//機車編號
      obj.exchange_date = $("#f_exchange_date").val();//交換日期時間
      
      console.log(obj);
      var strMsg = "ajax錯誤";
      var json_text = JSON.stringify(obj);
      $.ajax({
        type:'post',
        url: '<?=$web?>api/bss02',
        data: {JSONData:json_text},
        dataType: "json",
        error: function(xhr) {
          strMsg += 'Ajax request發生錯誤[json_monitor_bg.php]:'+xhr+'\n請重試';
        },
        success: function (rs) {
          //console.log(rs);
          //alert(rs.f_code);
          //alert(rs.f_json_data);
          $('#tab2').html(json_text);
          // $('#irs_msg').html(rs.f_code);
          $("#irs_msg").html(rs.return_code);
          alert("傳送成功");
          // $("#icode_msg").dialog({
          //   title: "訊息狀態",
          //   bgiframe: true,
          //   width: 400,
          //   height: 200,
          //   modal: true,
          //   draggable: true,
          //   resizable: false,
          //   buttons: {
          //     關閉訊息:
          //     function(){
          //       $('#icode_msg').dialog( 'close' );
          //     },
          //   }
          // });
        }
      })
    })
	function rank_txt_click(){
		var strMsg = '';
		  $.ajax({
			type:'post',
			url: '<?=$web?>testtool/bss02Rank',
			data: {source:'bss01'},
			dataType: "json",
			error: function(xhr) {
			  strMsg += 'Ajax request發生錯誤\n請重試';
			},
			beforeSend:function(){
				$('#loadingIMG').show();
			},
			complete:function(){
				// alert('complete');
				$('#loadingIMG').hide();
			},			
			success: function (rs) {
				console.log(rs);
				if(rs[0] = 'Y'){
					console.log(rs[1]);
					$.each( rs[1], function( key, value ) {
						if($("#"+key).attr('type') == 'radio'){
							$('[id='+key+'][value='+value+']').prop('checked',true);
						}else{
							$("#"+key).val(value);
						}
					});
				}else{
					rank_txt_click();
				}
			}
		})
	}
</script>
<div id="loadingIMG" style="display: none;">
	<div id="img_label">資料處理中，請稍後。</div>
</div>
