<!DOCTYPE html>
<?php 
	$web = $this->config->item('base_url');
	$select1 = "";
	$select2 = "";
	if($this->session->userdata('comparefun')=="KW")
		$select2 = "selected";
	else
		$select1 = "selected";
?>
<form id="searchform" method="POST" action="<?=$web?>/show_driver_info/search_battery">
	<select id="comparefun" name="comparefun">
		<option value="=" <?=$select1?>>=</option>
		<option value="KW" <?=$select2?>>KW</option>
	</select>
	<input type="text" name="battery_id" id="battery_id" placeholder="請輸入要搜尋的電池序號" value="<?=$this->session->userdata('battery_id')?>"> 
	<input type="submit" name="search_button" id="search_button" value="搜尋">
</form>
<table border="1" width="4000px">
<?php
	if($page_num != 0)
		$pre_num = $page_num-1;
	else
		$pre_num = 0;
	$next_num = $page_num+1;
	$pre_url = $web."show_driver_info/index/".$pre_num;
	$next_url = $web."show_driver_info/index/".$next_num;
	foreach($driverfield as $key1 => $value1)
	{
		if($value1['Field'] != 'insert_sql')
		{
			$width = "";
			if($value1['Field'] == 'receive_data_hex')
			{
				$width = "style = 'width:1200px;'";
			}
			else if($value1['Field'] == 'allgetdata')
			{
				$width = "style = 'width:1000px;word-break: break-all;'";
			}
			echo "<td {$width}>";
			echo $value1['Comment'];
			echo "</td>";
		}
	}
	echo "<tr>";
	foreach($driverInfo as $key => $value)
	{
		echo "<tr>";
		foreach($driverfield as $key1 => $value1)
		{
			if($value1['Field'] != 'insert_sql')
			{
				$width = "";
				if($value1['Field'] == 'receive_data_hex')
				{
					$width = "style = 'width:1200px;'";
					$value[$value1['Field']] = str_replace("\n","<br>",$value[$value1['Field']]);
				}
				else if($value1['Field'] == 'allgetdata')
				{
					$width = "style = 'width:1000px;word-break: break-all;'";
				}
				echo "<td {$width}>";
				echo $value[$value1['Field']];
				echo "</td>";
			}
		}
		echo "<tr>";
	}
?>
</table>
<a href="<?=$pre_url?>">上一頁</a>&nbsp&nbsp&nbsp&nbsp<a href="<?=$next_url?>">下一頁</a>
