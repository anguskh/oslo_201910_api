<?
	$web = $this->config->item('base_url');
	$this->load->helper('url');
?>
<div class="abgne_tab">
	<div class="abgne_tab" style="width:1000px;">
		<ul class="tabs">
			<li><a href="#tab1">車機回報表單</a></li>
			<li><a href="#tab2">發送JSON</a></li>
			<li><a href="#tab3">回覆JSON</a></li>
		</ul>
		<div class="tab_container" >
			<div id="tab1" class="tab_content">
				<INPUT TYPE="button" id="rank_txt" onclick="rank_txt_click()" value="亂數產生">
				<form method="POST" name="input_form" id="input_form">
					<table class="table_input2">
			        <tbody><tr>
			          <td width="23%">
			            行車日期<br>(YYYY-MM-DD hh:mm:ss)：
			          </td>
			          <td width="25%">
			            <input type="datetime-local" name="f_driving_date" id="f_driving_date" class="datetime hasDatepicker">
			          </td>
			          <td width="25%">
			            M30 Unit ID：
			          </td>
			          <td width="27%">
						<select name="f_unit_id" id="f_unit_id" style="width:225px;">
<?
 						echo '<option value="">無</option>';
						if($vehicle->num_rows()>0){
							foreach($vehicle->result_array()  as $v_arr){
								echo '<option value="'.$v_arr['unit_id'].'">'.$v_arr['unit_id'].'</option>';
							}
						}
?>
						</select>
			          </td>
			        </tr>
			        <tr>
			          <td>
			            M30 Unit Name：
			          </td>
			          <td>
			            <input type="text" name="f_unit_name" id="f_unit_name" style="width:225px;">
			          </td>
			          <td>
			            M30狀態：
			          </td>
			          <td>
			            <input name="f_unit_status" id="f_unit_status" type="radio" value="A" checked="">Effective position&nbsp;&nbsp;
			            <input name="f_unit_status" id="f_unit_status" type="radio" value="V">Ineffective position
			          </td>
			        </tr>
			        <tr>
			          <td>
			            UTC日期(DDMMYY)：
			          </td>
			          <td>
			            <input type="text" name="f_utc_date" id="f_utc_date" style="width:225px;">
			          </td>
			          <td>
			            UTC時間(hhmmss)：
			          </td>
			          <td>
			            <input type="text" name="f_utc_time" id="f_utc_time" style="width:225px;">
			          </td>
			        </tr>
			        <tr>
			          <td>
			            GPS緯度<br>([N|S]DDMM.MMMM)：
			          </td>
			          <td>
			            <input type="text" name="f_latitude" id="f_latitude" style="width:225px;">
			          </td>
			          <td>
			            GPS經度<br>([E|W]DDDMM.MMMM)：
			          </td>
			          <td>
			            <input type="text" name="f_longitude" id="f_longitude" style="width:225px;">
			          </td>
			        </tr>
			        <tr>
			          <td>
			            時速<br>(單位(公里/小時)，小數點1位)：
			          </td>
			          <td>
			            <input type="text" name="f_speed" id="f_speed" style="width:225px;">
			          </td>
			          <td>
			            角度(0.0 ~ 359.9)：
			          </td>
			          <td>
			            <input type="text" name="f_angle" id="f_angle" style="width:225px;">
			          </td>
			        </tr>
			        <tr>
			          <td>
			            GPS衛星數(0 ~ 13)：
			          </td>
			          <td>
			            <input type="text" name="f_gps_satellite" id="f_gps_satellite" style="width:225px;">
			          </td>
			          <td>
			            事件代碼：
			          </td>
			          <td>
			          	<select name="f_event_id" id="f_event_id" style="width:225px;">
				            <option value="01">還車</option>
				            <option value="02">借車</option>
				            <option value="03">傾倒</option>
				        </select>
			          </td>
			        </tr>
			        <tr>
			          <td>
			            回報類型：
			          </td>
			          <td>
			          	<select name="f_return_type" id="f_return_type" style="width:225px;">
				            <option value="0">Normal【正常傳遞】</option>
				            <option value="1">Auto-resend【自動修復】</option>
				            <option value="2"> Manual-resend【下指令補傳】</option>
				            <option value="3">Ping【$,PING指令】</option>
				            <option value="4">POSGET-Start【軌跡修補開始】</option>
				            <option value="5">POSGET-End【軌跡修補結束】</option>
				            <option value="6">BCGET-Start【條碼修補開始】</option>
				            <option value="7">BCGET-End【條碼修補結束】</option>
				        </select>
			          </td>
			          <td>
			            租借狀態：
			          </td>
			          <td>
			            <input name="f_lease_status" id="f_lease_status" type="radio" value="S" checked="">閒置中&nbsp;&nbsp;
			            <input name="f_lease_status" id="f_lease_status" type="radio" value="R">租借中&nbsp;&nbsp;
			            <input name="f_lease_status" id="f_lease_status" type="radio" value="F">故障
			          </td>
			        </tr>
			        <tr>
			          <td>
			          	格式類別：
			          </td>
			          <td>
			          	<select name="f_format_type" id="f_format_type" style="width:225px;">
			          		<option value="1">1</option>
			          	</select>
			          </td>
			          <td>
			            啟動狀態：
			          </td>
			          <td>
			            <input name="f_engine_status" id="f_engine_status" type="radio" value="0" checked="">熄火&nbsp;&nbsp;
			            <input name="f_engine_status" id="f_engine_status" type="radio" value="1">啟動中&nbsp;&nbsp;
			          </td>
			        </tr>
			        <tr>
			          <td>
			            電池電流充放電模式<br>
			          </td>
			          <td>
			            <input name="f_battery_amps_type" id="f_battery_amps_type" type="radio" value="1" checked="">充電&nbsp;&nbsp;
			            <input name="f_battery_amps_type" id="f_battery_amps_type" type="radio" value="0">放電&nbsp;&nbsp;
			          </td>
			          <td>
			            電池電流<br>(單位(安培)，小數點3位)：
			          </td>
			          <td>
			            <input type="text" name="f_battery_amps" id="f_battery_amps" style="width:225px;">
			          </td>
			        </tr>
			        <tr>
			          <td>
			            電池電壓<br>(單位(伏特)，小數點2位)：
			          </td>
			          <td>
			            <input type="text" name="f_battery_voltage" id="f_battery_voltage" style="width:225px;">
			          </td>
			          <td>
			            電池溫度<br>(單位(攝氏°C)，小數點0位)：
			          </td>
			          <td>
			            <input type="text" name="f_battery_temperature" id="f_battery_temperature" style="width:225px;">
			          </td>
			        </tr>
			        <tr>
			          <td>
			            環境溫度<br>(單位(攝氏°C)，小數點0位)：
			          </td>
			          <td>
			            <input type="text" name="f_environment_temperature" id="f_environment_temperature" style="width:225px;">
			          </td>
			          <td>
			            電池容量百分比<br>(百分比，XXX%)：
			          </td>
			          <td>
			            <input type="text" name="f_battery_capacity" id="f_battery_capacity" style="width:225px;">
			          </td>
			        </tr>
			        <tr>
			          <!-- <td>
			            PowerBank電壓<br>(單位(伏特)，小數點1位)：
			          </td>
			          <td>
			            <input type="text" name="f_power_bank_voltage" id="f_power_bank_voltage" style="width:225px;">
			          </td> -->
			          <td>
			            I/O狀態：
			          </td>
			          <td>
			            x:<input name="io_x" id="io_x" type="radio" value="1" checked>Battery&nbsp;&nbsp;
			            <input name="io_x" id="io_x"  type="radio" value="0">關<br>
			            y:<input name="io_y" id="io_y"  type="radio" value="1" checked>MainPower&nbsp;&nbsp;
			            <input name="io_y" id="io_y"  type="radio" value="0">關<br>
			            z:<input name="io_z" id="io_z"  type="radio" value="1" checked>ACC&nbsp;&nbsp;
			            <input name="io_z" id="io_z"  type="radio" value="0">關
			          </td>
			        </tr>
			        <tr>
			          <td colspan="4" align="center">
			            <input type="button" name="f_send" id="f_send" value="發送訊息" class="button">
			            &nbsp;&nbsp;&nbsp;&nbsp;
			            <input type="button" name="f_upd" id="f_upd" value="更新表單" class="button">
			          </td>
			        </tr>
			      </tbody></table>
			    </form>
			</div>
			<div id="tab2" class="tab_content">
			</div>
			<div id="tab3" class="tab_content">
				訊息回應碼：
			    <span id="irs_msg"></span>
			    <hr>
			    顯示訊息回應碼說明：<br>
			    0000=&gt;寫入DB成功<br>
			    0001=&gt;格式錯誤<br>
			    0002=&gt;寫入DB失敗<br>
			    0003=&gt;資料為空<br>
			    0004=&gt;找不到對應的電池交換站資料
			</div>
			<div id="icode_msg"></div>
		</div>
	</div>
</div>	
<script type="text/javascript"><?/* <!-- 頁籤javascript設定 --> */?>
	$(function(){
		// 預設顯示第一個 Tab
		var _showTab = 0;
		var $defaultLi = $('ul.tabs li').eq(_showTab).addClass('active');
		$($defaultLi.find('a').attr('href')).siblings().hide();
		
		// 當 li 頁籤被點擊時...
		// 若要改成滑鼠移到 li 頁籤就切換時, 把 click 改成 mouseover
		$('ul.tabs li').click(function() {
			// 找出 li 中的超連結 href(#id)
			var $this = $(this),
				_clickTab = $this.find('a').attr('href');
			// 把目前點擊到的 li 頁籤加上 .active
			// 並把兄弟元素中有 .active 的都移除 class
			$this.addClass('active').siblings('.active').removeClass('active');
			// 淡入相對應的內容並隱藏兄弟元素
			$(_clickTab).stop(false, true).fadeIn().siblings().hide();

			return false;
		}).find('a').focus(function(){
			this.blur();
		});
	});

	$('#f_send').click(function(){
      // 結果清空
      $('#irs_msg').html('');
      $('#tab2').html('');
      var dataArr = new Array();
      var dataArr2 = new Array();
      var obj = new Object;
      var obj2 = new Object;
      obj.di01 = $("#f_driving_date").val();//ECU行車日期
      obj.di02 = $("#f_unit_id").val();//M30 Unit ID
      obj.di03 = $("#f_unit_name").val();//M30 Unit Name
      obj.di04 = $('input[name="f_unit_status"]:checked').val();//車輛使用者序號
      obj.di05 = $("#f_utc_date").val();//UTC日期
      obj.di06 = $("#f_utc_time").val();//UTC時間
      obj.di07 = $("#f_latitude").val();//GPS緯度
      obj.di08 = $("#f_longitude").val();//GPS經度
      obj.di09 = $("#f_speed").val();//時速
      obj.di10 = $("#f_angle").val();//角度
      obj.di11 = $("#f_gps_satellite").val();//GPS衛星數
      obj.di12 = $('#f_event_id').val();//事件代碼
      //obj.di12 = $('input[name="f_event_id"]:checked').val();//事件代碼
      obj.di13 = $('#f_return_type').val();//回報類型
      //obj.di13 = $('input[name="f_return_type"]:checked').val();//回報類型
      var f_battery_voltage = $("#f_battery_voltage").val().replace(".","");
      var f_battery_amps = $("#f_battery_amps").val().replace(".","");
      obj.di14 = $('#f_format_type').val()+$('input[name="f_lease_status"]:checked').val()+$('input[name="f_engine_status"]:checked').val()+f_battery_voltage+$('input[name="f_battery_amps_type"]:checked').val()+f_battery_amps+$("#f_battery_temperature").val()+$("#f_environment_temperature").val()+$("#f_battery_capacity").val()+"0000";
      obj.di15 = "0"+$('input[name="io_x"]:checked').val()+$('input[name="io_y"]:checked').val()+$('input[name="io_z"]:checked').val();//角度
      
      console.log(obj);
      var strMsg = "ajax錯誤";
      var json_text = JSON.stringify(obj);
      $.ajax({
        type:'post',
        url: 'http://47.52.28.170:8080/',
        data: {JSONData:json_text},
        dataType: "json",
        error: function(xhr) {
          strMsg += 'Ajax request發生錯誤[json_monitor_bg.php]:'+xhr+'\n請重試';
        },
        success: function (rs) {
          //console.log(rs);
          //alert(rs.f_code);
          //alert(rs.f_json_data);
          $('#tab2').html(json_text);
          // $('#irs_msg').html(rs.f_code);
          $("#irs_msg").html(rs.return_code);
          alert("傳送成功");
          // $("#icode_msg").dialog({
          //   title: "訊息狀態",
          //   bgiframe: true,
          //   width: 400,
          //   height: 200,
          //   modal: true,
          //   draggable: true,
          //   resizable: false,
          //   buttons: {
          //     關閉訊息:
          //     function(){
          //       $('#icode_msg').dialog( 'close' );
          //     },
          //   }
          // });
        }
      })
    })
	function rank_txt_click(){
		var strMsg = '';
		  $.ajax({
			type:'post',
			url: '<?=$web?>testtool/ecu02Rank',
			data: {source:'bss01'},
			dataType: "json",
			error: function(xhr) {
			  strMsg += 'Ajax request發生錯誤\n請重試';
			},
			beforeSend:function(){
				$('#loadingIMG').show();
			},
			complete:function(){
				// alert('complete');
				$('#loadingIMG').hide();
			},			
			success: function (rs) {
				console.log(rs);
				if(rs[0] = 'Y'){
					console.log(rs[1]);
					$.each( rs[1], function( key, value ) {
						if($("#"+key).attr('type') == 'radio'){
							$('[id='+key+'][value='+value+']').prop('checked',true);
						}else{
							$("#"+key).val(value);
						}
					});
				}else{
					rank_txt_click();
				}
			}
		})
	}
</script>
<div id="loadingIMG" style="display: none;">
	<div id="img_label">資料處理中，請稍後。</div>
</div>
