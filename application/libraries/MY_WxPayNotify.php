<?php defined('BASEPATH') OR exit('No direct script access allowed');

// 加载微信支付接口类
require_once APPPATH.'libraries/Payment/Wxpaylib/WxPay.Api.php';
require_once APPPATH.'libraries/Payment/Wxpaylib/WxPay.Notify.php';

/**
 * MY_WxPayNotify 自定义微信支付类
 * @author Mike Lee
 */
class MY_WxPayNotify extends WxPayNotify {

    private $_CI;

    public function  __construct(){
        // 获得 CI 超级对象 使得自定义类可以使用Controller类的方法
        $this->_CI = & get_instance();
    }

    /**
     * NotifyProcess 微信回调处理
     * @access public
     */
    public function NotifyProcess($data, &$msg){
        //echo "处理回调";
        log_message('error', json_encode($data));

        if ($data['return_code'] == 'SUCCESS' && $data['result_code'] == 'SUCCESS') {
            $this->updateOrderPay($data);
            return true;
        }

        return false;
    }

    /**
     * updateOrderPay 更新订单支付信息
     * @access private
     * @param string $order_code
     * @return mixed
     * @author Mike Lee
     */
    public function updateOrderPay($wxpay_data){
        $this->_CI->load->model('order_model');
        // 根据订单号获取订单信息
        $order_code = $wxpay_data['out_trade_no'];
        $order_info = $this->_CI->order_model->getOrderByID($order_code, true);
        if ($order_info['order_status'] == 0 && $order_info['pay_status'] != '1') {
            // 更新订单的支付状态及支付方式
            // pay_status => 1 表示支付成功
            // pay_method => 2 表示支付方式为微信支付
            $pay_info = array('pay_status' => '1', 'pay_method' => 2);
            $pay_result = $this->_CI->order_model->updateOrderPayStatus($order_code, $pay_info, true);
            // 添加订单支付信息
            // user_id 用户ID
            // order_id 订单ID
            // total_fee 支付总额
            // openid 用户微信唯一ID
            $order_pay_info = array(
                'user_id'   => $order_info['user_id'],
                'order_id'  => $order_info['id'],
                'total_fee' => $wxpay_data['cash_fee'] / 100,
                'openid'    => $wxpay_data['openid'],
                'time'      => time()
            );
            $this->_CI->order_model->addOrderWxPayLog($order_pay_info);
            // 添加用户收支日志
            // correlation_id 关联ID 这里为订单ID
            // action_type => 1 表示为商品购买事件
            // point_type => 4 表示为微信支付
            $payment_log = array(
                'user_id'        => $order_info['user_id'],
                'correlation_id' => $order_info['id'],
                'action_type'    => 1,
                'pay_type'		=> 4,
                'total_fee'  => $wxpay_data['cash_fee'] / 100,
                'remark'         => 'XXXX商品购买：微信支付'.$wxpay_data['cash_fee'] / 100,
                'action_ip'      => $_SERVER['REMOTE_ADDR'],
                'add_time'       => time()
            );
            $this->_CI->order_model->addUserPayLog($payment_log);
        }
        return true;
    }


}


/* End of file MY_WxPayNotify.php */
/* Location: ./application/libraries/MY_WxPayNotify.php */