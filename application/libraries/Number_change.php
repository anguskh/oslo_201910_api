<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Number_change {

   	public function getChineseNumber($money){

	    $ar = array("零", "壹", "貳", "參", "肆", "伍", "陸", "柒", "捌", "玖") ;
	    $cName = array("", "", "拾", "佰", "仟", "萬", "拾", "佰", "仟", "億", "拾", "佰", "仟");
	    $conver = "";
	    $cLast = "" ;
	    $cZero = 0;
	    $i = 0;
	    for ($j = strlen($money) ; $j >=1 ; $j--){  
			$cNum = intval(substr($money, $i, 1));
			$cunit = $cName[$j]; //取出位數
			if ($cNum == 0) { //判斷取出的數字是否為0,如果是0,則記錄共有幾0
			 $cZero++;
			 if (strpos($cunit,"萬億") >0 && ($cLast == "")){ // '如果取出的是萬,億,則位數以萬億來補
			  $cLast = $cunit ;
			 }      
			}else {
			if ($cZero > 0) {// '如果取出的數字0有n個,則以零代替所有的0
			  if (strpos("萬億", substr($conver, strlen($conver)-2)) >0) {
			     $conver .= $cLast; //'如果最後一位不是億,萬,則最後一位補上"億萬"
			  }
			  $conver .=  "零" ;
			  $cZero = 0;
			  $cLast = "" ;
			}
			 $conver = $conver.$ar[$cNum].$cunit; // '如果取出的數字沒有0,則是中文數字+單位          
			}
			$i++;
	    }  
	  //'判斷數字的最後一位是否為0,如果最後一位為0,則把萬億補上
	    if (strpos("萬億", substr($conver, strlen($conver)-2)) >0) {
	       $conver .=$cLast; // '如果最後一位不是億,萬,則最後一位補上"億萬"
	    }
	    return $conver;
	}
}