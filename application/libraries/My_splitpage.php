<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class My_splitpage
{
	private $_Total_rows;	// 總筆數
	private $_Page_rows;	// 每頁筆數
	private $_PageCnt;		// 總分頁數
	private $_NowPage;		// 現在頁數
	private $_StartRow;		// 現在筆數
	private $_CurrentPage;	// 現在頁面
	private $_OtherNum;	// 是否還有後面的頁碼
	private $_lang;
	private $_new_URL;
	private $_para;
	
	public function __construct( $spConfigArr )
	{
		$CI =& get_instance();
		
		if($CI->session->userdata('default_language'))
		{
			$this->_lang = $CI->session->userdata('default_language');
		}
		else {
			$this->_lang = $CI->session->userdata('display_language');
		}
		
		$this->_Page_rows = $spConfigArr["base_pageRow"];
		
		$this->reset_url();
	}
	
	public function reset_url(){
		$CI =& get_instance();
		$random_function = $CI->uri->segment(2);
		$random_function = explode('__',$random_function);
		$random_name = "";
		if(isset($random_function[1])){
			$random_name = $random_function[1];
			$this->_new_URL = base_url().$CI->router->fetch_directory().$CI->router->fetch_class().'__'.$random_name.'/index';
		}else{
			$this->_new_URL = base_url().$CI->router->fetch_directory().$CI->router->fetch_class().'/index';
		}
		//$this->_new_URL = base_url().$CI->router->fetch_directory().$CI->router->fetch_class().'/'.$CI->router->fetch_method();
		//$this->_new_URL = base_url().$CI->router->fetch_directory().$CI->router->fetch_class().'/index';
		
		$para = $CI->uri->segment(5);
		if($para != ""){
			$this->_para = $para;
		}else{
			$this->_para = "";
		}
	}

	/**
	 * 
	 */	
	public function getPageAreaArr( $totalRow, $startRow = 0, $currentPage = "")
	{
		$pageAreaArr = array() ;
		$this->_Total_rows = $totalRow ;
		$this->_StartRow = $startRow ;
		if($currentPage != ""){
			$this->_CurrentPage = $currentPage;
		}else{
			$this->_CurrentPage = 'index';
		}
		
		$pagebuttonArr = $this->makePageButton();

		$pageAreaArr["html"]  = "";
		foreach( $pagebuttonArr as $buttonStr )
		{
			$pageAreaArr["html"] .= $buttonStr ;
		}
		
		$pageAreaArr["js"] = "";

		return $pageAreaArr ;
	}
	
	private function makePageButton()
	{
		$btnArr = array();
		$this->_PageCnt = ceil($this->_Total_rows / $this->_Page_rows ) ;

		array_push( $btnArr, $this->retFirstButton() ) ;
		array_push( $btnArr, $this->retPreviousButton() ) ;
		
		for ( $i = 0; $i < $this->_PageCnt; $i++ )
		{
			$this->retPageButton( $i ) ;
			if($this->retPageButton( $i ) != ""){
				array_push( $btnArr , $this->retPageButton( $i ) ) ;
			}
		}
		
		if($this->_OtherNum == true){	//顯示有後面其他頁碼, 以...顯示
			array_push( $btnArr , '...' ) ;
		}
		
		// array_push( $btnArr, $this->retNextButton() ) ;
		// array_push( $btnArr, $this->retLastButton( $i ) ) ;
		
		
		array_push( $btnArr, $this->retLastButton( $i ) ) ;
		array_push( $btnArr, $this->retNextButton() ) ;
		array_push( $btnArr, $this->retInputPage( $i ) ) ;

		return $btnArr ;
	}
	
	private function retPageButton( $pageNum )
	{
		$showNum = $pageNum + 1;
		$moveIndex = $pageNum * $this->_Page_rows;
		//echo 'index:'.$pageNum;
		if($this->_Total_rows > $this->_Page_rows)
		{
			//分頁顯是最多筆數 共 11筆
			$min = $this->_StartRow - ($this->_Page_rows * 5);
			$max = $this->_StartRow + ($this->_Page_rows * 5);
			
			if($moveIndex >= $min && $moveIndex <= $max){ //在區間內才顯示
				if ( $this->_StartRow == $moveIndex ) {
					$this->_NowPage = $showNum;
					$strButton = "	<span class=\"disabled\">{$showNum}</span>" ;
				} else {
					if ($this->_StartRow < 1)
						$strButton = "	<a href=\"{$this->_new_URL}/{$moveIndex}/{$this->_para}\">{$showNum}</a>" ;
					else if ($pageNum < 1)
						$strButton = "	<a href=\"{$this->_new_URL}/0/{$this->_para}\">{$showNum}</a>" ;
					else
						$strButton = "	<a href=\"{$this->_new_URL}/{$moveIndex}/{$this->_para}\">{$showNum}</a>" ;
				}
				
				if(($this->_PageCnt -1) != $pageNum){	//無出現最後分頁碼
					$this->_OtherNum = true;
				}else{
					$this->_OtherNum = false;
				}
				
			}else{
				$strButton = "";
			}

			return $strButton ;
		}
	}
	
	/**
	 * 處理第一頁
	 */
	private function retFirstButton()
	{		
		$CI =& get_instance();
		$CI->lang->load("common", $this->_lang);
	
		$strButton = "";
		if($this->_StartRow == 0)
			$strButton = "" ;
		else
			
			$strButton = "	<a href=\"{$this->_new_URL}/0/{$this->_para}\">{$CI->lang->line('common_first_page')}</a>" ;

		return $strButton ;
	}
	
	/**
	 * 處理上一頁
	 */
	private function retPreviousButton()
	{
		$CI =& get_instance();
		$CI->lang->load("common", $this->_lang);
	
		$startRow = $this->_StartRow - $this->_Page_rows ;

		if ( $this->_StartRow == 0  or $this->_StartRow < 0 ) {
			$strPrevious = "";
		} else if ( $startRow <= 0 ) {
			$strPrevious = "	<a href=\"{$this->_new_URL}/0/{$this->_para}\" class=\"next\">{$CI->lang->line('common_prev_page')}</a>";
		} else if ( $startRow  > 0 ) {
			$strPrevious = "	<a href=\"{$this->_new_URL}/{$startRow}/{$this->_para}\" class=\"next\">{$CI->lang->line('common_prev_page')}</a>";
		}
		
		return $strPrevious;
	}
	
	/**
	 * 處理下一頁
	 */
	private function retNextButton()
	{		
		$CI =& get_instance();
		$CI->lang->load("common", $this->_lang);
		
		$nextNum = $this->_StartRow + $this->_Page_rows ;
		$startRow = $this->_Total_rows - $this->_StartRow ;

		$strNext = "";
		if ( $this->_StartRow == 0 ) {
			if ( $startRow > $this->_Page_rows )
			{
				$len = strlen($_SERVER['REQUEST_URI']);
				$URI_last_byte = $_SERVER['REQUEST_URI'][$len-1];
				if($URI_last_byte != '/')
					$strNext = "	<a href=\"{$this->_new_URL}/{$nextNum}/{$this->_para}\" class=\"next\">{$CI->lang->line('common_next_page')}</a>" ;
				else
					$strNext = "	<a href=\"{$this->_new_URL}/{$nextNum}/{$this->_para}\" class=\"next\">{$CI->lang->line('common_next_page')}</a>" ;
			}
		} else if ( $nextNum < $this->_Total_rows ) {
			$strNext = "	<a href=\"{$this->_new_URL}/{$nextNum}/{$this->_para}\" class=\"next\">{$CI->lang->line('common_next_page')}</a>" ;
		}

		return $strNext ;
	}
	
	/**
	 * 處理最後一頁
	 */
	private function retLastButton( $pageNum )
	{		
		$CI =& get_instance();
		$CI->lang->load("common", $this->_lang);

		$showNum = $pageNum - 1;
		$moveIndex = $showNum * $this->_Page_rows;

		if($this->_Total_rows)
		{
			$strButton = "";
			if ( $this->_StartRow != $moveIndex ) {
				if($this->_StartRow == 0)
				{
					$len = strlen($_SERVER['REQUEST_URI']);
					$URI_last_byte = $_SERVER['REQUEST_URI'][$len-1];
					if($URI_last_byte != '/')
						//$strButton = "	<a href=\"".current_url()."/..//{$moveIndex}\">{$CI->lang->line('common_last_page')}</a>" ;
						//$strButton = "	<a href=\"".current_url()."/{$moveIndex}\">{$CI->lang->line('common_last_page')}</a>" ;
						//$strButton = "	<a href=\"".current_url()."/../{$this->_CurrentPage}/{$moveIndex}\">{$CI->lang->line('common_last_page')}</a>" ;
						$strButton = "	<a href=\"{$this->_new_URL}/{$moveIndex}/{$this->_para}\">{$CI->lang->line('common_last_page')}</a>" ;
						// $strButton = "	<a href=\"{$this->_new_URL}/{$moveIndex}/{$this->_para}\">..{$this->_PageCnt}</a>" ;
					else
						//$strButton = "	<a href=\"".current_url()."/index/{$moveIndex}\">{$CI->lang->line('common_last_page')}</a>" ;
						$strButton = "	<a href=\"{$this->_new_URL}/{$moveIndex}/{$this->_para}\">{$CI->lang->line('common_last_page')}</a>" ;
						// $strButton = "	<a href=\"{$this->_new_URL}/{$moveIndex}/{$this->_para}\">..{$this->_PageCnt}</a>" ;
				}
				else
					//$strButton = "	<a href=\"".current_url()."/../{$moveIndex}\">{$CI->lang->line('common_last_page')}</a>" ;
					$strButton = "	<a href=\"{$this->_new_URL}/{$moveIndex}/{$this->_para}\">{$CI->lang->line('common_last_page')}</a>" ;
					// $strButton = "	<a href=\"{$this->_new_URL}/{$moveIndex}/{$this->_para}\">..{$this->_PageCnt}</a>" ;
			}
			return $strButton ;
		}
	}
	
	//可輸入分頁
	private function retInputPage( $pageNum ){
		$CI =& get_instance();
		$CI->lang->load("common", $this->_lang);
		
		$showNum = $pageNum - 1;
		
		$moveIndex = $showNum * $this->_Page_rows;
		
		$pagelen = strlen($this->_PageCnt);
		$setWidth = 11;
		if($pagelen <= 2){
			$inputWidth = $setWidth * 2;
		}else{
			$inputWidth = $setWidth * $pagelen;
		}
		
		if($this->_PageCnt > 1){
			$strButton = " <input style=\"width: {$inputWidth}px;\" maxlength=\"{$pagelen}\" class=\"input_page\" pagerows=\"{$this->_Page_rows}\" maxpage=\"{$this->_PageCnt}\" url=\"{$this->_new_URL}\" value=\"{$this->_NowPage}\"> / {$this->_PageCnt}{$CI->lang->line('common_page')}";
			return $strButton ;
		}	
	}
}
// END Pagination Class

/* End of file MY_Splitpage.php */
/* Location: ./application/libraries/MY_Splitpage.php */
