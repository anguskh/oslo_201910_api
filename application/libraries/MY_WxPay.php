<?php defined('BASEPATH') OR exit('No direct script access allowed');

// 加载微信支付接口类
require_once APPPATH.'libraries/Payment/Wxpaylib/WxPay.Api.php';

/**
 * MY_WxPay 自定义微信支付类
 * @author Mike Lee
 */
class MY_WxPay {

    private $_CI;

    public function  __construct(){
        // 获得 CI 超级对象 使得自定义类可以使用Controller类的方法
        $this->_CI = & get_instance();
    }

    /**
     * createPayQRcode2
     * 生成直接支付url 支付url有效期为2小时 模式二
     * @param array $params 统一下单参数组
     * @return void
     * @author Mike Lee
     */
    public function createPayQRcode2($params){
        $input = $this->getUnifiedOrderInput($params);
        if ($input->GetTrade_type() == 'NATIVE') {
            $result = WxPayApi::unifiedOrder($input);
            // 这里也可以直接返回code_url 然后直接使用demo中生成二维码的方式
            // return urlencode($result['code_url']);
            $this->_CI->load->library('MY_QRcode');
            $this->_CI->my_qrcode->createQRcode($result['code_url']);
        }
    }

    /**
     * getUnifiedOrderInput 获取统一下单输入对象
     * @access public
     * @param array $params
     * @return UnifiedOrderInput $input
     * @author Mike Lee
     */
    public function getUnifiedOrderInput($params){
        $input = new WxPayUnifiedOrder();

		$input->SetBody($params['body']);  
		//$input->SetAttach('test');    
		$input->SetOut_trade_no($params['out_trade_no']);  
		$input->SetTotal_fee($params['total_fee']); 
		$input->SetTime_start(date("YmdHis"));  
		$input->SetTime_expire(date("YmdHis", time() + 60*10));    //订单失效时间,报错可不写
		$input->SetGoods_tag("tag");       //设置商品标记，说明详见代金券或立减优惠
		$input->SetNotify_url($params['notify_url']);   //设置接收微信支付异步通知回调地址
		$input->SetTrade_type("APP");      //设置类型如下：JSAPI，NATIVE，APP
		$order_data = WxPayApi::unifiedOrder($input);  //统一下单

        return $order_data;
    }

    /**
     * wxPayOrderQuery 微信支付订单查询
     * @access public
     * @param string $order_code
     * @param bool $mode
     * @return array
     * @author Mike Lee
     */
    public function getWxPayOrderInfo($order_code, $mode = false){
        if ( ! $order_code) return false;
        $input = new WxPayOrderQuery();
        if ($mode) {
            // 微信订单号
            $input->SetTransaction_id($order_code);
        } else {
            // 商户订单号
            $input->SetOut_trade_no($order_code);
        }

        return WxPayApi::orderQuery($input);
    }

}


/* End of file MY_Wxpay.php */
/* Location: ./application/libraries/MY_WxPay.php */