<?
//產生欄位搜尋
//$colArr 為清單顯示欄位
//$searchData 為 先前搜尋的 session資料
//指定欄位類別, 提供搜尋欄位建立 array('欄位名稱'=> '欄位類型', ....)
//目前類型只有判斷date
//需要忽略的欄位array()
function create_search_box($colArr, $searchData = "", $fieldType = "", $ignoreArr = array(), $searchable = "", $twoInout = ""){
	$searchArr = array();
	if($searchData){
		foreach($searchData as $key => $value){
			$tmparr = explode('[:;]', $value);
			$field = $tmparr[0];
			$option = $tmparr[1];
			$data = $tmparr[2];
			$searchArr[$field][0] = $field;
			$searchArr[$field][1] = $option;
			$searchArr[$field][2] = $data;
			if(isset($tmparr[3])){  //迄日資料
				$searchArr[$field][3] = $tmparr[3];
			}
		}
	}
	
	//產生search
	$search_box = "";
	$i=0;
	//搜尋方式
	$optionArr = array("=", "like", "between", "KW"); 
	$search_box .= "<table id=\"search_table\">";
	foreach($colArr as $key => $value){
		if(in_array($key, $ignoreArr)){  
			continue;
		}
		$sessOption = "";
		$sessData = "";
		$sessData2 = "";  //迄日資料
		if(isset($searchArr[$key])){
			$sessOption = $searchArr[$key][1];
			$sessData = $searchArr[$key][2];
			if(isset($searchArr[$key][3])){  //迄日資料
				$sessData2 = $searchArr[$key][3];
			}
		}
		
		//欄位類型
		$searchFieldType = "";
		//特定關鍵字
		$fieldDefault = ((isset($fieldType[$key]))?$fieldType[$key]:'');
		//預設option
		$search_option = default_option($i, $optionArr, $sessOption, $fieldDefault);
		//預設input
		$search_input = default_input($i, $sessData, $fieldDefault);

		$td_colspan = 1;

		if(isset($fieldType[$key])){  //欄位類型是否有定義
			if(is_string($fieldType[$key])){  //字串, 包括日期,..等等
				if(strtolower($fieldType[$key]) == 'date' || strtolower($fieldType[$key]) == 'month'){  //日期
					$searchFieldType = strtolower($fieldType[$key]);
					$search_option = date_option($i, $optionArr);
					$search_input = date_input($i, $sessData, $sessData2, strtolower($fieldType[$key]));
				}else if($fieldType[$key] == 'range'){
					//起始批號 ...等等
					$searchFieldType = strtolower($fieldType[$key]);
					$search_option = range_option($i, $optionArr);
					$search_input = range_input($i, $sessData, $sessData2, strtolower($fieldType[$key]));
				}
				else if($fieldType[$key] == 'selectmonth')
				{
					$searchFieldType = strtolower($fieldType[$key]);
					$search_option = date_month_option($i, $optionArr);
					$search_input = date_month_input($i, $sessData, 'month');
					$td_colspan = 2;
				}
				else if($fieldType[$key] == 'checkinput')
				{
					$searchFieldType = strtolower($fieldType[$key]);
					$search_option = checkbox_option($i, $optionArr);
					$search_input = checkbox_input($i, '1', $sessData);
				}
			}else if(is_array($fieldType[$key])){  //陣列資料
				if(isset($searchable[$key])){
					$multiple = true;
					if(is_array($searchable[$key])){  //是陣列參數
						if($searchable[$key][0] == 'Y'){
							if(isset($searchable[$key][1])){
								$multiple = $searchable[$key][1];
							}
						}else{//目前沒有N的設定
						}
					}else{ //字串參數
						if($searchable[$key] == 'Y'){
							// $multiple = true;
						}else{  //目前沒有N的設定
							// $multiple = true;
						}
					}
					
					$td_colspan = 2;
					$searchFieldType = "searchable";
					$search_input = arr_input2($i, $fieldType[$key], $sessData, $key, $multiple);	
				}else if (isset($twoInout[$key])) {
					$searchFieldType = "";
					$search_input = arr_input3($i, $fieldType[$key], $sessData, $key);
				}else{
					$searchFieldType = "";
					$search_input = arr_input($i, $fieldType[$key], $sessData);
				}
			}
		}else{  //無定義, 就是一般input或者是非資料庫對應select
			//查詢該欄位有無非資料庫的對應內容, 例如 0 = 停用, 1 = 啟用
			//有則為select提供選擇, 無則是一般input
			$aliasArr = alias_colName($key);
			if($aliasArr){   //有對應, 非資料庫內容
				$search_input = alias_input($i, $aliasArr, $sessData);
			}
		}
		
		$search_box .= "<tr class=\"tr_{$key}\">";
		$search_box .= "<td class=\"td_label_{$key}\">";
		$search_box .= "	<input type=\"hidden\" name=\"searchField_{$i}\" value=\"{$key}\">";
		$search_box .= "	<input type=\"hidden\" name=\"searchFieldType_{$i}\" value=\"{$searchFieldType}\">";
		$search_box .= "	<input type=\"hidden\" name=\"searchNum\" value=\"{$i}\">";
		$search_box .= "	<label class=\"search_label\">{$value}</label>";
		$search_box .= "</td>";

		//非searchable需要 = or like
		if(!isset($searchable[$key]) && $searchFieldType != 'selectmonth' ){
			$search_box .= "<td class=\"td_option_{$key}\"  >";
			$search_box .= $search_option;
			$search_box .= "</td>";
		}
		$search_box .= "<td colspan=\"{$td_colspan}\" class=\"td_data_{$key} td_search\" >";
		$search_box .= $search_input;
		$search_box .= "</td>";
		$search_box .= "</tr>";
		$i++;
	}
	$search_box .= "</table>";
	// exit();
	//2017/01/04 加上清空按鈕 by ming
	$search_box .= '<input type="button" id="btClear" value="清空" />';

	return $search_box;
}

//日期選項
function date_option($i, $optionArr){
	$tmp = "";
	$tmp .= "	<select name=\"searchOption_{$i}\" class=\"searchOption label_inline display_none\" >";
	$tmp .= "	<option value=\"{$optionArr[2]}\" selected>{$optionArr[2]}</option>";
	$tmp .= "	</select>";
	
	$tmp .= "<div class=\"start_end_label\">起/訖</div>";
	return $tmp;
}

//月份日期選項
function date_month_option($i, $optionArr){
	$tmp = "";
	// $tmp .= "	<select name=\"searchOption_{$i}\" class=\"searchOption label_inline display_none\" >";
	// $tmp .= "	<option value=\"{$optionArr[0]}\" selected>{$optionArr[0]}</option>";
	// $tmp .= "	</select>";
	return $tmp;
}

//起始input選項
function range_option($i, $optionArr){
	$tmp = "";
	$tmp .= "	<select name=\"searchOption_{$i}\" class=\"searchOption label_inline display_none\" >";
	$tmp .= "	<option value=\"{$optionArr[2]}\" selected>{$optionArr[2]}</option>";
	$tmp .= "	</select>";
	
	$tmp .= "<div class=\"start_end_label\">起/訖</div>";
	return $tmp;
}

//預設選項
function default_option($i, $optionArr, $sessOption, $fieldType){
	if($fieldType == 'KW'){
		$sessOption = (($sessOption=='')?'KW':$sessOption);
	}
	$tmp = "";
	$tmp .= "	<select name=\"searchOption_{$i}\" class=\"searchOption label_inline\" >";
	//對應類別選擇, 目前只有 = 和 like
	foreach($optionArr as $opKey => $opValue){
		if($opKey == 2){  //略過brtween, 是日期在用的
			continue;
		}
		$selected = "";
		if($opValue == $sessOption){
			$selected = " selected";
		}
		$tmp .= "	<option value=\"{$opValue}\" {$selected}>{$opValue}</option>";
	}
	$tmp .= "	</select>";
	return $tmp;
}

//預設
function default_input($i, $sessData, $fieldType){
	if($fieldType == 'KW'){
		$tmp = "	<input type=\"text\" class=\"searchData\" name=\"searchData_{$i}\" value=\"{$sessData}\" placeholder=\"關鍵字搜尋\">";
	}else{
		$tmp = "	<input type=\"text\" class=\"searchData\" name=\"searchData_{$i}\" value=\"{$sessData}\">";
	}
	return $tmp;
}


//checkbox input
function checkbox_input($i, $sessValue, $sessData){
	$checked = "";
	if($sessData == '1'){
		$checked = "checked";
	}
	$tmp = "	<input type=\"checkbox\" class=\"searchcheckbox\" name=\"searchData_{$i}\" value=\"{$sessValue}\" {$checked}>";
	return $tmp;
}
//checkbox選項
function checkbox_option($i, $optionArr){
	$tmp = "";
	$tmp .= "	<select name=\"searchOption_{$i}\" class=\"searchOption label_inline display_none\" >";
	$tmp .= "	<option value=\"checkbox\" selected>checkbox</option>";
	$tmp .= "	</select>";
	
	return $tmp;
}

//有對應非資料庫內容
function alias_input($i, $aliasArr, $sessData){
	$tmp = "";
	$tmp .= "	<select name=\"searchData_{$i}\" class=\"searchData_sel\" >";
	foreach($aliasArr as $key => $value){
		$selected = "";
		//轉成字串
		$key = $key.'';
		if($sessData === $key){
			$selected = " selected";
		}
		$tmp .= "	<option value=\"{$key}\" {$selected}>{$value}</option>";
	}
	$tmp .= "	</select>";
	return $tmp;
}

//日期輸入
function date_input($i, $sessData, $sessData2, $format){
	$tmp = "";
	// $tmp .= "	<input type=\"{$format}\" class=\"searchData\" name=\"searchData_{$i}_0\" value=\"{$sessData}\">";
	// $tmp .= "	<input type=\"{$format}\" class=\"searchData\" name=\"searchData_{$i}_1\" value=\"{$sessData2}\">";
	$tmp .= "	<input type=\"date\" class=\"searchdateData\" name=\"searchData_{$i}_0\" value=\"{$sessData}\">";
	$tmp .= "	<input type=\"date\" class=\"searchdateData\" name=\"searchData_{$i}_1\" value=\"{$sessData2}\">";
	return $tmp;
}

//月份日期輸入
function date_month_input($i, $sessData, $format){
	$tmp = "";
	// $tmp .= "	<input type=\"{$format}\" class=\"searchData\" name=\"searchData_{$i}_0\" value=\"{$sessData}\">";
	// $tmp .= "	<input type=\"{$format}\" class=\"searchData\" name=\"searchData_{$i}_1\" value=\"{$sessData2}\">";
	$sessData = "20".substr($sessData,0,2)."-".substr($sessData,2,2);
	$tmp .= "	<input type=\"month\" id='datepicker' name=\"searchData_{$i}\" value=\"{$sessData}\">";
	return $tmp;
}

//起始輸入
function range_input($i, $sessData, $sessData2, $format){
	$tmp = "";
	// $tmp .= "	<input type=\"{$format}\" class=\"searchData\" name=\"searchData_{$i}_0\" value=\"{$sessData}\">";
	// $tmp .= "	<input type=\"{$format}\" class=\"searchData\" name=\"searchData_{$i}_1\" value=\"{$sessData2}\">";
	$tmp .= "	<input type=\"text\" class=\"searchdateData\" name=\"searchData_{$i}_0\" value=\"{$sessData}\">";
	$tmp .= "	<input type=\"text\" class=\"searchdateData\" name=\"searchData_{$i}_1\" value=\"{$sessData2}\">";
	return $tmp;
}

//陣列資料
function arr_input($i, $arrData, $sessData){
	$tmp = "";
	$tmp .= "	<select name=\"searchData_{$i}\" class=\"searchData_sel\">";
	$tmp .= "	<option value=\"\"></option>";


	foreach($arrData as $arrkey => $arrvalue){
		$arrkeys = array_keys($arrvalue);  //取第二個key, 因為通常list 陣列, 第二筆資料都是所需的資料
		//先都取第二個key, 到時再來看要怎樣改成找第一個key, 或者全部功能都一起改
		$getKey = $arrkeys[1]; 
		$value = $arrvalue[$getKey];
		$getKey = $arrkeys[1]; 
		$label = $arrvalue[$getKey];
		$selected = "";
		if($sessData === $value){
			$selected = " selected";
		}

		$tmp .= "	<option value=\"{$value}\" {$selected}>{$label}</option>";
	}
	$tmp .= "	</select>";
	return $tmp;
}

//陣列資料 搜尋下拉選單
function arr_input2($i, $arrData, $sessData, $key, $multiple = true){
	if($multiple){
		$multiple_str = "multiple=\"multiple\"";
	}else{
		$multiple_str = "";
	}
	//$sessData 用逗號區隔
	$sessArr = explode(',',$sessData);
	$tmp = "";
	$tmp .= "	<select id=\"{$key}\" name=\"searchData_{$i}[]\" {$multiple_str}>";

	foreach($arrData as $arrkey => $arrvalue){
		
		$arrkeys = array_keys($arrvalue);  //取第二個key, 因為通常list 陣列, 第二筆資料都是所需的資料
		$CodeKey = $arrkeys[0]; 
		if(isset($arrkeys[1])){  
			$getKey = $arrkeys[1]; 
			$value = $arrvalue[$CodeKey].'｜'.$arrvalue[$getKey];
		}else{  //如果找不到欄位, 就已第一個為主
			$getKey = $arrkeys[0];
			$value = $arrvalue[$CodeKey];
		}
		
		$selected = "";

		if(in_array("'".$arrvalue[$CodeKey]."'", $sessArr)) { 
			
			$selected = " selected";
		}

		$tmp .= "	<option value=\"{$arrvalue[$CodeKey]}\" {$selected}>{$value}</option>";
	}
	$tmp .= "	</select>";
	return $tmp;
}
//陣列資料
function arr_input3($i, $arrData, $sessData, $key){
	$tmp = "";
	$tmp .= "	<input type='text' id='{$key}_input' class=\"label_inline twoInout_input\" value=\"{$sessData}\">";
	$tmp .= "	<select id=\"{$key}\" name=\"searchData_{$i}\" class=\"twoInout_sel\">";
	$tmp .= "	<option value=\"\"></option>";

	foreach($arrData as $arrkey => $arrvalue){
		$arrkeys = array_keys($arrvalue);  //取第二個key, 因為通常list 陣列, 第二筆資料都是所需的資料
		//先都取第二個key, 到時再來看要怎樣改成找第一個key, 或者全部功能都一起改
		$getKey = $arrkeys[0]; 
		$value = $arrvalue[$getKey];
		$getKey = $arrkeys[1]; 
		$label = $arrvalue[$getKey];
		$selected = "";
		if($sessData === $value){
			$selected = " selected";
		}

		$attr ="";
		if ($key == "item_no") {
			$kind_no = $arrvalue["kind_no"];
			$attr =" kind_no='{$kind_no}' ";
		}

		$tmp .= " <option value=\"{$value}\" val_name=\"{$label}\" {$attr} {$selected}>{$label}</option>";
	}
	$tmp .= "	</select>";
	return $tmp;
}

?>