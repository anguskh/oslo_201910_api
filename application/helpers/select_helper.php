<?
	//產生select
	//$list 來源陣列清單
	//$keyData 傳入要對應的資料
	//$defMsg 預設顯示文字
	//$filterArr 來源其他欄位給予input 其他屬性值, 如果是二微陣列則value是要預先過濾的值
	//$defMsg 當為陣列, $defMsg[0] 為選擇是字串, $defMsg[1] 為value值
	function create_select_option($list, $keyData = "", $defMsg = "", $filterArr = array()){
		if( !empty( $list ) )
		{
			if(is_array($defMsg)){  //是陣列
				echo "<option value=\"{$defMsg[1]}\">{$defMsg[0]}　</option>\n";
			}else if($defMsg === false){ //false, 沒有option
				
			}else if($defMsg != ""){  //有字串, 沒字串就不產生文字select
				echo "<option value=\"\">{$defMsg}　</option>\n";
			}else{
				echo "<option value=\"\"></option>\n";
			}
			// print_r($list);
			foreach ($list as $key => $value) {
				$filterFlag = true;
				$keyArr = array_keys($value);
				$showValue = $value[$keyArr[0]];
				if(isset($keyArr[1])){
					$showName = $value[$keyArr[1]];
				}else{
					$showName = $value[$keyArr[0]];
				}
				
				
				$filterStr = "";
				if(count($filterArr) > 0){
					foreach($filterArr as $filterKey => $filterVal){
						$filterFieldVal = "";
						if(is_string($filterKey)){  //判斷是否為字串, 為字串代表$filterKey 為過濾欄位, $filterVal為要過濾的欄位值
							$filterFlag = false;
							$filterField = $filterKey;
							$filterFieldVal = $filterVal;
						}else{  //非字串, 則$filterVal為過濾欄位, $filterKey只是但純陣列索引
							$filterField = $filterVal;
						}
						$filterStr .= $filterField."=\"{$value[$filterField]}\" "; 
						
						if($filterFieldVal != "" && $value[$filterField] == $filterFieldVal){
							$filterFlag = true;
						}
					}
				}

				if( $keyData == $showValue && $filterFlag){
					$selected = " selected";
				}else{
					$selected = "";
				}
					//echo "<option value=\"{$showValue}\" {$filterStr} {$selected}>[{$showValue}] {$showName}</option>\n";
					echo "<option value=\"{$showValue}\" {$filterStr} {$selected} val_name=\"{$showName}\">{$showName}</option>\n";
			}
		}
		else
		{
			echo "<option value=\"\">{$defMsg}　</option>\n";
		}
	}

	
	//用於先過濾類別, 再產生資料
	function create_select_option_2($list, $keyData = "", $defMsg = "", $filterArr = array()){
		$str = "";
		if( !empty( $list ) )
		{
			if(count($filterArr) > 0){
				$first_key = array_shift($filterArr);
				if(is_string($first_key)){
					//$kind = $filterArr[$first_key];
					$list = $list[$first_key];
				}
			}
			$str .= "<option value=\"\">{$defMsg}　</option>\n";
			foreach ($list as $key => $value) {
				$filterFlag = true;
				$keyArr = array_keys($value);
				$showValue = $value[$keyArr[0]];
				$showName = $value[$keyArr[1]];
				
				$filterStr = "";
				if(count($filterArr) > 0){
					foreach($filterArr as $filterKey => $filterVal){
						$filterFieldVal = "";
						if(is_string($filterKey)){  //判斷是否為字串, 為字串代表$filterKey 為過濾欄位, $filterVal為要過濾的欄位值
							$filterFlag = false;
							$filterField = $filterKey;
							$filterFieldVal = $filterVal;
						}else{  //非字串, 則$filterVal為過濾欄位, $filterKey只是但純陣列索引
							$filterField = $filterVal;
						}
						$filterStr .= $filterField."=\"{$value[$filterField]}\" "; 
						
						if($filterFieldVal != "" && $value[$filterField] == $filterFieldVal){
							$filterFlag = true;
						}
					}
				}

				if( $keyData == $showValue && $filterFlag)
					$selected = " selected";
				else
					$selected = "";
					//echo "<option value=\"{$showValue}\" {$filterStr} {$selected}>[{$showValue}] {$showName}</option>\n";
					$str .= "<option value=\"{$showValue}\" {$filterStr} {$selected} val_name=\"{$showName}\">{$showName}</option>\n";
			}
		}
		else
		{
			$str .= "<option value=\"\">{$defMsg}　</option>\n";
		}
		return $str;
	}

?>