<?
//接收搜尋資料, 轉為SQL語法
//$searchData 清單搜尋的條件
//$defAlias 預設欄位主別名 如 {$defAlias}.Fields
//$setAlias 自訂欄位主別名 如 $setAlias = array( "Fields"=> "Alias" );, 可多組	
function setSearchData($searchData = "", $defAlias = "",$setAlias = array()){
	if($searchData != ""){
		$searchStr = "";
		$searchArr = "";
		foreach($searchData as $key => $value){
			$tmparr = explode('[:;]', $value);
			$field = $tmparr[0];
			$option = $tmparr[1];
			$data = $tmparr[2];
			
			//SQL別名
			if(isset($setAlias[$field])){
				if(strpos($setAlias[$field], '.') !== false){  //有.代表有別名+欄位名稱, 用來取代原先設定的別名
					$alias = "";
					$field = $setAlias[$field];
				}else{  //沒有., 就直接別名+欄位名稱
					$alias = $setAlias[$field].'.';
				}
				
			}else if($defAlias != ""){
				$alias = $defAlias.'.';
			}else{
				$alias = "";
			}
			
			//搜尋類別
			$like = "";
			if($option == "like"){
				$like = "%";
				$searchArr[] = "{$alias}{$field} {$option} '{$data}{$like}'";
			}else if($option == "KW"){
				$option = 'like';
				$like = "%";
				$searchArr[] = "{$alias}{$field} {$option} '{$like}{$data}{$like}'";
			}else if($option == "between"){  //日期
				$data2 = $tmparr[3];  //$data 就是起日, 第四筆資料是迄日
				if($data != "" && $data2 != ""){  //有起訖日
					$searchArr[] = "{$alias}{$field} {$option} '{$data}' and '{$data2}'";
				}else if($data != ""){
					$searchArr[] = "{$alias}{$field} = '{$data}'";
				}else if($data2 != ""){
					$searchArr[] = "{$alias}{$field} = '{$data2}'";
				}
			}else if($option == "IN"){ //多筆
				$searchArr[] = "{$alias}{$field} {$option} ({$data})";
			}else if($option == ""){ //checkbox
				$option = "=";
				$searchArr[] = "{$alias}{$field} {$option} ({$data})";
			}else{
				$searchArr[] = "{$alias}{$field} {$option} '{$data}{$like}'";
			}
		}
		$searchStr = join(' and ', $searchArr);
		$searchStr = " and ({$searchStr})";
		
		// echo($searchStr);
		// exit();
		return $searchStr;
	}else{
		return;
	}
}

//將清單送出的搜尋資料轉成陣列
function setSearch2Arr($postData){
	$searchArr = array();
	$searchNum = $postData["searchNum"];
	
	for($i = 0; $i <= $searchNum; $i++){
		//欄位類別
		$searchFieldType = $postData["searchFieldType_{$i}"];
		$searchField = $postData["searchField_{$i}"];
		$searchOption = isset($postData["searchOption_{$i}"])?$postData["searchOption_{$i}"]:'';
		if(strtolower($searchFieldType) == 'date' || strtolower($searchFieldType) == 'month'){  //為日期
			for($k = 0; $k <= 1; $k++){  //1起日, 2迄日
				${"searchData_".$k} = addslashes($postData["searchData_{$i}_{$k}"]);
				if(${"searchData_".$k} != ""){
					${"searchData_".$k} = str_replace("/", "-", ${"searchData_".$k});
				}
			}
			if($searchData_0 != "" || $searchData_1 != ""){
				$searchArr[] = "{$searchField}[:;]{$searchOption}[:;]{$searchData_0}[:;]{$searchData_1}";
			}
		}else if(strtolower($searchFieldType) == 'range'){  //起始input
			for($k = 0; $k <= 1; $k++){  //1開始, 2結束
				${"searchData_".$k} = addslashes($postData["searchData_{$i}_{$k}"]);
				if(${"searchData_".$k} != ""){
					${"searchData_".$k} = str_replace("/", "-", ${"searchData_".$k});
				}
			}
			if($searchData_0 != "" || $searchData_1 != ""){
				$searchArr[] = "{$searchField}[:;]{$searchOption}[:;]{$searchData_0}[:;]{$searchData_1}";
			}
		}else if(strtolower($searchFieldType) == 'searchable'){  //searchable套件
		
			if(isset($postData["searchData_{$i}"]) ){
				$ckbSelArr = '';
				if(count($postData["searchData_{$i}"])>0){

					$ckbSelArr = $postData["searchData_{$i}"];
					$IdStr = join( "','", $ckbSelArr );
					$IdStr = "'".$IdStr."'";

					$searchArr[] = "{$searchField}[:;]IN[:;]{$IdStr}";

					//remark[:;]like[:;]123
				}
			}
		}else if(strtolower($searchFieldType) == 'selectmonth'){  //選擇月
			$searchData_0 = $postData["searchData_0"];
			if($searchData_0 != ""){
				$searchData_0 = substr($searchData_0,2,2).substr($searchData_0,5,2);
				$searchArr[] = "{$searchField}[:;]=[:;]{$searchData_0}";
			}
		}else if(strtolower($searchFieldType) == 'checkinput'){  //checkinput
			if(isset($postData["searchData_{$i}"])){
				$searchData = addslashes($postData["searchData_{$i}"]);
				$searchArr[] = "{$searchField}[:;]=[:;]{$searchData}";
			}
		}else{  //其他字串搜尋
			if(isset($postData["searchData_{$i}"])){
				$searchData = addslashes($postData["searchData_{$i}"]);
				if($searchData != ""){
					$searchArr[] = "{$searchField}[:;]{$searchOption}[:;]{$searchData}";
				}
			}
		}

	}
	return $searchArr;
}

//join對應, 依照搜尋條件來加入join查詢, 僅SQL count 語法使用
function leftJoinSet($search, $joinMap){
	$joinStr = "";
	if(!empty( $search)){
		foreach($search as $key => $value){
			$tmparr = explode('[:;]', $value);
			$field = $tmparr[0];  //欄位
			if(isset($joinMap[$field])){  //有找到
				$joinStr .= $joinMap[$field];
			}else{  //沒找到
				foreach($joinMap as $key2 => $value2){  //找joiMap的key值是否有可以分割的key
					$arrExp = explode(",", $key2);
					if(count($arrExp) >= 2){  //分割
						if(in_array($field, $arrExp)){  //有找到
							$joinStr .= $value2;
							unset($joinMap[$key2]); //移除此筆對應, 避免又重複join
						}
						break;
					}
				}
			}
		}
	}
	return $joinStr;
}

?>