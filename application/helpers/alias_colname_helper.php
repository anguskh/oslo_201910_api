<?php
//對應表, [case 對應必須為大寫英文]
function alias_colName($colName, $data = ""){
	$colArr = array();
	$CI =& get_instance();
	$fetch_class = $CI->router->fetch_class();
	switch(strtoupper($colName)){
		//狀態
		case 'STATUS':
			if($fetch_class == 'user'){
				$colArr = array (
					"" 			=> "{$CI->lang->line('search_all')}", 
					"1"			=> "{$CI->lang->line('enable')}", 
					"0"			=> "{$CI->lang->line('disable')}"
				);
			}else if($fetch_class == 'log_operating'){
				$colArr = array (
					"" 			=> "{$CI->lang->line('search_all')}", 
					"1"			=> "{$CI->lang->line('success_action')}", 
					"0"			=> "{$CI->lang->line('failed_action')}"
				);
			}else{
				$colArr = array (
					"" 			=> "{$CI->lang->line('search_all')}", 
					"1"			=> "{$CI->lang->line('enable')}",
					"0"			=> "{$CI->lang->line('disable')}"
					//"D"			=> "{$CI->lang->line('delete')}"
				);
			}
			break;
		//前後台登入
		case 'LOGIN_SIDE_DISPLAY':
			$colArr = array (
				"" 			=> "{$CI->lang->line('search_all')}", 
				"0"			=> "{$CI->lang->line('login_side_0')}",
				"1"			=> "{$CI->lang->line('login_side_1')}",
				"2"			=> "{$CI->lang->line('login_side_2')}"
			);
			break;
		case 'ISP_YES':
		case 'SHIP_YES':
		case 'STOCK_YES':
		case 'INSTALL_YES':
			$colArr = array (
				"" 			=> "{$CI->lang->line('search_all')}", 
				"1"			=> "{$CI->lang->line('enable')}", 
				"0"			=> "{$CI->lang->line('disable')}"
			);
			break;
		case 'AIR_MARK':
		case 'USE_MARK':
			$colArr = array (
				"" 			=> "{$CI->lang->line('search_all')}", 
				"1"			=> "{$CI->lang->line('yes')}", 
				"0"			=> "{$CI->lang->line('no')}"
			);
			break;
		case 'MODE':
			$colArr = array (
				"" 			=> "{$CI->lang->line('search_all')}", 
				"0"			=> "{$CI->lang->line('browser')}", 
				"1"			=> "{$CI->lang->line('add')}", 
				"2"			=> "{$CI->lang->line('edit')}", 
				"3"			=> "{$CI->lang->line('delete')}", 
			);
			break;
		default:
			break;
	}
	return $colArr;
}
?>