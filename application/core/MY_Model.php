<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11
class MY_Model extends CI_Model {
/**
 * DB的相關資料
 * http://codeigniter.com/user_guide//database/examples.html
 * http://www.codeigniter.org.tw/user_guide/database/helpers.html
 * http://www.codeigniter.org.tw/user_guide/database/active_record.html
 */
//-------------------------------------------------------------------------------------------------
	/**
	 * @var Model_user 使用者帳號
	 */
	var $model_user;
//-------------------------------------------------------------------------------------------------

    var $_db ;
    
	/**
	 * 建構子
	 */
    function __construct()
    {
        parent::__construct();
		$this->load->library("common_function") ;
        // load database，(這種寫法是應付一個系統會LOAD很多個DB Connecting)預先會load那些db連線，先建立起來
        $this->_db['default'] = $this->load->database('default', TRUE);
		//$this->_db['airlink'] = $this->load->database('airlink', TRUE);  //連線多個資料庫
		//$this->_db['mssql'] = $this->load->database('mssql', TRUE);  //連線多個資料庫, 改至model_transaction再進行連線
        // $this->_db['hm_db'] = $this->load->database('hm_db',TRUE);
        
        // $this->_db['default'] = $this->load->database(); // 只會load default setting的DB，參考./application/config/database.php
    }
	
	//取得連線driver
	//來源:$db_name 連線名稱
	public function getDBdriver($db_name = '') {
        if (empty($db_name)) {
            $db_name = "default";
        }
		$db = $this->getDb($db_name);
		$retrun = "";
		$retrun = $db->dbdriver;
		return $retrun;
	}
	
	//取得連線DB name
	//來源:$db_name 連線名稱
	public function getDBname() {
        $db_name = "default";
		$db = $this->getDb($db_name);
		$retrun = "";
		$retrun = $db->database;
		return $retrun;
	}
    
	/**
	 * 直接傳入SQLCmd
	 * @param SQLCmd SQLCmd - 字串
	 * @param db_name 資料庫名稱 - 字串
	 */
	public function db_query($SQLCmd = '', $db_name = '')
    {
		if ( empty( $SQLCmd ) ) {
			return array();
		}
		if ( empty( $db_name ) ) {
			$db_name = "default";
		}

		$db = $this->getDb($db_name);
		
		if (empty($db)) {
			return "沒有連結到DB";
		} else {
			//$this->session->set_userdata('sql_cmd', "111");
			//紀錄SQL
			// $this->sql_start_record($SQLCmd);
			if ( strpos( strtoupper( "  ".$SQLCmd ), "INSERT INTO" ) > 0 || strpos( strtoupper( "  ".$SQLCmd ), "UPDATE" ) > 0 || strpos( strtoupper( "  ".$SQLCmd ), "DELETE FROM" ) > 0) {
				// do INSERT or UPDATE or DELETE cmd
				return $db->query($SQLCmd) ;
			} else {
				// do other cmd
				return $db->query($SQLCmd)->result_array() ;
			}
		}
    }

	/**
	 * 還在測 不能用
	 */
	public function db_quert_where( $tableName = '', $whereArr = "", $db_name = "" )
	{
		if ( empty( $tableName ) or empty( $whereArr )) {
			return array();
		}
		if ( empty( $db_name ) ) {
			$db_name = "default";
		}

		$db = $this->getDb($db_name);

		if (empty($db)) {
			return "沒有連結到DB";
		} else {
			return $db->get_where( $tableName, $whereArr )->result_array();
			// return $db->query($SQLCmd)->result_array();
		}
		
	}
	
	/**
	 * 傳入table name, 欄位資料陣列，即可進行新增
	 * @param tableName 資料表名稱 - 字串
	 * @param colDataArr 欄位陣列 - 陣列
	 * @param db_name 資料庫名稱 - 字串
	 */
	public function db_insert( $tableName = '', $colDataArr = '', $db_name = '' )
	{
        if ( empty( $tableName ) or empty( $colDataArr )) {
            return array();
        }
        if ( empty( $db_name ) ) {
            $db_name = "default";
        }

        $db = $this->getDb($db_name);

        if (empty($db)) {
            return "沒有連結到DB";
        } else {
        	if (in_array("now()", $colDataArr)) {
        		$SQLCmd = $db->insert_string($tableName, $colDataArr) ; // 產生新增的SQLCmd
        		$SQLCmd = str_replace("'now()'", "now()", $SQLCmd) ;
    //     		if($tableName == 'log_driving_info')
				// {
				// 	$colDataArr['insert_sql'] = $SQLCmd;
				// 	$SQLCmd = $db->insert_string($tableName, $colDataArr) ; // 產生新增的SQLCmd
    //     			$SQLCmd = str_replace("'now()'", "now()", $SQLCmd) ;
				// }
				//紀錄SQL
				// $this->sql_start_record($SQLCmd);
				// $this->data_record($colDataArr);
				$db->query($SQLCmd) ;
				return $db->affected_rows() ;
			} else {
				return $db->insert($tableName, $colDataArr) ; // 直接新增進db
			}
			
        }		
	}
	
	/**
	 * 傳入table name, 欄位資料陣列, 條件陣列，即可進行更新
	 * @param tableName 資料表名稱 - 字串
	 * @param colDataArr 欄位陣列 - 陣列
	 * @param whereDataStr 條件字串 - 字串
	 * @param db_name 資料庫名稱 - 字串
	 */
	public function db_update( $tableName = '', $colDataArr = '', $whereDataStr = '', $db_name = '' )
	{
        if ( empty( $tableName ) or empty( $colDataArr ) or empty( $whereDataStr )) {
            return array();
        }
        if ( empty( $db_name ) ) {
            $db_name = "default";
        }
        $db = $this->getDb($db_name);

        if (empty($db)) {
            return "沒有連結到DB";
			
        } else {
        	if ( in_array( "now()", $colDataArr ) ) {
        		$SQLCmd = $db->update_string( $tableName, $colDataArr, $whereDataStr ) ; // 產生新增的SQLCmd
        		$SQLCmd = str_replace("'now()'", "now()", $SQLCmd) ;
				//紀錄SQL
				// $this->sql_start_record($SQLCmd);
				// $this->data_record($colDataArr);
				$db->query($SQLCmd) ;
				return $db->affected_rows() ;
			} else {
				$SQLCmd = $db->update_string( $tableName, $colDataArr, $whereDataStr ) ; // 產生新增的SQLCmd
				//紀錄SQL
				// $this->sql_start_record($SQLCmd);
				// $this->data_record($colDataArr);
				$db->query($SQLCmd) ;
				return $db->affected_rows() ;
			}
			
        }		
	}


	
	/**
	 * 這沒有真的刪除，只是更換flag
	 * 傳入table name, 欄位資料陣列, 條件陣列，即可進行更新
	 * @param tableName 資料表名稱 - 字串
	 * @param colDataArr 欄位陣列 - 陣列
	 * @param whereDataStr 條件字串 - 字串
	 * @param db_name 資料庫名稱 - 字串
	 */
	public function db_delete($tableName = '', $colDataArr = '', $whereDataStr = '', $db_name = '')
	{
        if ( empty( $tableName ) or empty( $colDataArr ) or empty( $whereDataStr )) {
            return array();
        }
        if ( empty( $db_name ) ) {
            $db_name = "default";
        }

        $db = $this->getDb($db_name);

        if (empty($db)) {
            return "沒有連結到DB";
        } else {
        	if ( in_array( "now()", $colDataArr ) ) {
        		$SQLCmd = $db->update_string( $tableName, $colDataArr, $whereDataStr ) ; // 產生新增的SQLCmd
        		$SQLCmd = str_replace("'now()'", "now()", $SQLCmd) ;
				//紀錄SQL
				// $this->sql_start_record($SQLCmd);
				// $this->data_record($colDataArr);
				$db->query($SQLCmd) ;
				return $db->affected_rows() ;
			} else {
				return $db->update( $tableName, $colDataArr, $whereDataStr ) ; // 直接新增進db
			}
			
        }		
	}
	
    public function getDb($db_name){
        return $this->_db[$db_name];
    }
	
	//判斷是否要記錄SQL
	public function sql_start_record($SQLCmd){
		if($this->session->userdata("sql_start") == true){
			$this->session->set_userdata("sql_cmd", $SQLCmd);
		}
	}
	
	//記錄新增, 修改或刪除欄位值
	public function data_record($colDataArr){
		$this->session->set_userdata("desc", $colDataArr);
	}
	
// 轉址================================================================
	function redirect_alert( $url, $msg ) {
		if($this->session->userdata('PageStartRow') > 0)
			$url = $url . "/".$this->session->userdata('PageStartRow');
			//$url = $url . "index/".$this->session->userdata('PageStartRow');

echo <<<SCRIPT
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<Script Language="JavaScript">
	alert("$msg");
	location.href="$url";
</SCRIPT>

SCRIPT
;
	}
	
	function redirect( $url ) {
		if($this->session->userdata('PageStartRow') > 0)
			$url = $url . "index/".$this->session->userdata('PageStartRow');

echo <<<SCRIPT
<Script Language="JavaScript">
	location.href="$url";
</SCRIPT>

SCRIPT
;
	}
	function redirect_question_alert( $url1, $url2, $msg ) {
		if($this->session->userdata('PageStartRow') > 0)
			$url1 = $url1 . "/".$this->session->userdata('PageStartRow');
			//$url = $url . "index/".$this->session->userdata('PageStartRow');

echo <<<SCRIPT
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<Script Language="JavaScript">
	if(confirm("$msg"))
	{
		window.open("$url2",'_blank');
		location.href="$url1";
	}
	else
	{
		location.href="$url1";
	}
</SCRIPT>

SCRIPT
;
	}
	
// 轉址================================================================

	//判斷是否為Json格式
	public function is_json($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}

	//遞回創建目錄
	public function create_dirs($dir, $mode=0777)
	{
       if(!is_dir($dir)){
              $this->create_dirs(dirname($dir), $mode);
              mkdir($dir, $mode);
       }
       return true;
	}

	//寫入檔案
	//輸入:$PATH 路徑, $dataArr 陣列資料
	public function createtxt($PATH, $setname, $dataArr){
		$sFile = $setname;
		//創建與開啟檔案
		$fp= fopen($PATH.$sFile,"w");
		
		//將資料寫入檔案中
		$break = "\r\n";
		
		foreach($dataArr as $key => $value){
			$value = str_replace(' ', ' ',$value);//此處兩種空白不同，因輸出結果有錯誤，所以以一般空白取代特殊空白
			$value = trim($value);//去除前後空白
			$encoding = mb_detect_encoding($value, array('ASCII','EUC-CN','BIG-5','UTF-8'));
			// echo $encoding;
			if ($encoding == 'UTF-8') {
				// $value = iconv("UTF-8","big5",$value);//將UTF-8轉為BIG5
				$value = mb_convert_encoding($value, "BIG5", "UTF-8");//將UTF-8轉為BIG5
			}
			
			if(trim($value)!="")
			{
				if(trim($value)=="&nbsp;")
				{
					$value="";
				}
				$data = $value . $break; 
				fputs($fp,$data);
			}
		}
		//修改檔案權限
		chmod($PATH.$sFile,0664);
		fclose($fp);//關閉檔案
	}

	//讀取:$PATH 路徑
	public function readfile($PATH){
		$dataArr = array();
		if(file_exists($PATH))
		{
			$txt = fopen($PATH,"r");
			$filecontent = fread($txt,10240);
			$filecontent = iconv("BIG5","UTF-8",$filecontent);
			$dataArr = explode("\r\n",$filecontent);
		}
		return $dataArr;
	}
}

/* End of file my_model.php */
/* Location: ./application/core/my_model.php */