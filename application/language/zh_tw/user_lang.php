<?php
//其他
$lang['user_management'] = '帳號管理';
$lang['employee_management'] = '員工基本資料';
$lang['user_input_account'] = '請輸入登入帳號';
$lang['user_input_name'] = '請輸入姓名';
$lang['user_password'] = '密碼';
$lang['user_input_password'] = '請輸入登入密碼';
$lang['user_confirm_password'] = '確認密碼';
$lang['user_input_confirm_password'] = '請輸入相同登入密碼';
$lang['user_input_mobile'] = '請輸入行動電話';
$lang['user_input_phone'] = '請輸入電話';
$lang['user_input_email'] = '請輸入電子郵件信箱';
$lang['user_bank'] = '選擇銀行';
$lang['user_bank_help'] = '-- 請選擇銀行 --&nbsp;';
$lang['user_merchant'] = '選擇商店';
$lang['user_merchant_help'] = '-- 請選擇商店 --&nbsp;';
$lang['user_select_group'] = '請選擇使用者群組';
$lang['user_status2'] = '使用者狀態';
$lang['user_force_change_password'] = '強制更改密碼';
$lang['user_repassword'] = '回復密碼';
$lang['user_log_change_password'] = '<font color=blue>[更改密碼]</font>';
$lang['user_log_target'] = '<font color=blue>[目標]</font>';
$lang['user_log_content'] = '<font color=blue>[內容]</font>';
$lang['user_log_user_id'] = '帳號:';
$lang['user_log_password'] = '密碼:';
$lang['user_label_basic'] = '基本資料';
$lang['user_label_company'] = '公司相關';
$lang['user_label_group_access'] = '群組與權限';

$lang['user_label_emp_ename'] = '英文姓名';
$lang['user_label_dept_no'] = '部門';
$lang['user_input_dept_no'] = '請輸入部門';
$lang['user_label_emp_id'] = '員工編號';
$lang['user_label_birth_date'] = '出生日期';
$lang['user_label_hire_date'] = '雇用日期';
$lang['user_label_location'] = '辦公室地點';
$lang['user_label_fax'] = '傳真';
$lang['user_label_home_tel'] = '住家電話';
$lang['user_label_district'] = '國家地區';
$lang['user_label_city'] = '鄉鎮縣市';
$lang['user_label_zip_code'] = '郵遞區號';
$lang['user_label_address'] = '地址';
$lang['user_label_urgent_name'] = '緊急連絡人';
$lang['user_label_urgent_tel'] = '緊急連絡電話';
$lang['user_label_ID_No'] = '身分證字號';
$lang['user_label_mobile2'] = '行動電話2';
$lang['user_label_position'] = '職位';
$lang['user_label_charge_area'] = '負責區域';
$lang['user_label_extension'] = '分機';
$lang['user_label_status'] = '狀態';
$lang['user_label_fulltime'] = '正職';
$lang['user_label_onduty'] = '在職';
$lang['user_label_install_per'] = '裝機人員';
$lang['user_label_isp_per'] = '採購人員';
$lang['user_label_stock_per'] = '倉管人員';
$lang['user_label_sale_per'] = '銷售人員';
$lang['user_label_contract_per'] = '委外簽約人員';
$lang['user_label_contract_per_no'] = '委外代號';
$lang['user_label_remark'] = '備註';


//欄位
$lang['user_user_id'] = '帳號';
$lang['user_user_name'] = '使用者';
$lang['user_sex'] = '性別';
$lang['user_mobile'] = '行動電話';
$lang['user_phone'] = '電話';
$lang['user_email'] = '電子郵件';
$lang['user_group_sn'] = '使用者群組';
$lang['user_comment_notes'] = '銀行';
$lang['user_merchant_name_chinese'] = '商店';
$lang['user_status'] = '狀態';
$lang['user_access'] = '權限控管';


/* End of file user_lang.php */
/* Location: ./system/language/zh_tw/user_lang.php */
