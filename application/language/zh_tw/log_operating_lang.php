<?php
//其他
$lang['log_operating_inquiry'] = '歷程記錄查詢';

//欄位
$lang['log_operating_operating_date'] = '操作時間';
$lang['log_operating_user_sn'] = '使用者名稱';
$lang['log_operating_mode'] = '操作類型';
$lang['log_operating_desc'] = '操作後';
$lang['log_operating_sql'] = '指令';
$lang['log_operating_MERCHANT_ID'] = '商店代號';
$lang['log_operating_TERMINAL_ID'] = '端末機代號';
$lang['log_operating_function_name'] = '功能名稱';
$lang['log_operating_before_desc'] = '操作前';
$lang['log_operating_ip_address'] = 'IP位址';
$lang['log_operating_status'] = '狀態';



/* End of file reader_initial_lang.php */
/* Location: ./system/language/zh_tw/reader_initial_lang.php */
