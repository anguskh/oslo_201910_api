<?php
//其他
$lang['config_management'] = '參數管理';
$lang['config_please_input'] = '請輸入';
$lang['config_allow_ip_segment_msg'] = '當不輸入任何網段, 預設為任何IP都可進入!';
$lang['config_allow_pc_name'] = '當不輸入, 預設可進入任何登入頁面!';
$lang['config_P'] = '正式參數';
$lang['config_T'] = '測試參數';

//欄位
$lang['config_config_sn'] = '參數流水號';
$lang['config_config_desc'] = '參數名稱';
$lang['config_config_set'] = '參數值';
$lang['config_after_desc'] = '參數單位';


/* End of file group_lang.php */
/* Location: ./system/language/zh_tw/group_lang.php */
