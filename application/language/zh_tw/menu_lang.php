<?php
//其他
$lang['menu_management'] = '選單管理';
$lang['menu_parent_name'] = '上層選單名稱';
$lang['menu_select_parent_name'] = '請選擇上層選單名稱';
$lang['menu_select_second_name'] = '請選擇第二層選單名稱';
$lang['menu_input_name'] = '請輸入選單中文名稱';
$lang['menu_input_en_name'] = '請輸入選單英文名稱';
$lang['menu_input_directory'] = '請輸入選單路徑';
$lang['menu_input_image'] = '請輸入選單圖檔名稱';
$lang['menu_input_sequence'] = '請輸入順序';
$lang['menu_status2'] = '選單狀態';

//欄位
$lang['menu_menu_sn'] = '選單編號';
$lang['menu_parent_menu_sn'] = '上層選單編號';
$lang['menu_second_menu_sn'] = '第二層選單名稱';
$lang['menu_name'] = '選單中文名稱';
$lang['menu_menu_name_english'] = '選單英文名稱';
$lang['menu_menu_name'] = '選單名稱';
$lang['menu_menu_directory'] = '選單路徑';
$lang['menu_menu_image'] = '選單圖檔名稱';
$lang['menu_menu_sequence'] = '選單排列順序';
$lang['menu_login_side_display'] = '登入顯示';
$lang['menu_access_control'] = '預設功能';
$lang['menu_status'] = '狀態';

//控制權限
$lang['menu_access_control'] = '操作權限';


/* End of file menu_lang.php */
/* Location: ./system/language/zh_tw/menu_lang.php */
