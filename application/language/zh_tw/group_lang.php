<?php
//其他
$lang['group_management'] = '群組管理';
$lang['group_input_name'] = '請輸入群組名稱';
$lang['group_name_duplicate'] = '群組名稱不可重複';
$lang['group_in_use'] = '有使用者正在使用';
$lang['group_login_side'] = '登入權限';
$lang['group_select_user_group'] = '請勾選使用者群組';
$lang['group_msg'] = '使用者管理需勾選使用者群組!';

//log
$lang['group_user_group'] = '使用者群組';
$lang['group_menu_sn'] = '選單編號';
$lang['group_group_sn'] = '群組編號';


//欄位
$lang['group_group_name'] = '群組名稱';
$lang['group_status'] = '群組狀態';
$lang['group_approve_terminal'] = '覆核端末機';


/* End of file group_lang.php */
/* Location: ./system/language/zh_tw/group_lang.php */
