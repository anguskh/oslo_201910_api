<?php

$lang['password_current'] = '舊密碼';
$lang['password_input_current'] = '請輸入舊密碼';
$lang['password_current_error'] = '舊密碼輸入錯誤';
$lang['password_change'] = '更改密碼';
$lang['password_new'] = '新密碼';
$lang['password_input_new'] = '請輸入新密碼';
$lang['password_new_confirm'] = '確認新密碼';
$lang['password_input_new_confirm'] = '請輸入相同的新密碼';
$lang['password_first_login'] = '首次登入請更換密碼';


/* End of file password_lang.php */
/* Location: ./system/language/zh_tw/password_lang.php */
