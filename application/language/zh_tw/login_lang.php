<?php
//其他
$lang['login_user_title'] = '電動機車管理系統';
$lang['login_admin_title'] = '電動機車管理系統';
$lang['login_advise'] = '建議您使用 <a href="http://www.google.com/chrome?hl=zh-TW&amp;brand=CHMI">Google Chrome</a> 以取得較佳的螢幕視野、較親和的使用者介面、較快速的內容呈視。';
$lang['login_web_foreground'] = '電動機車管理系統-前台';
$lang['login_web_background'] = '電動機車管理系統';
$lang['login_id'] = '帳號';
$lang['login_password'] = '密碼';
$lang['login_button'] = '登入';
$lang['submit_button'] = '送出';
$lang['security_Input'] = '驗證碼';
$lang['try_another'] = '換一個';
$lang['forget_password'] = '忘記密碼';
$lang['forget_email'] = '輸入Email';
$lang['return_to_login'] = '回登入頁';
$lang['login_send'] = '送出';
$lang['login_email_wrong_email_address'] = '信箱不正確, 請重新輸入!';
$lang['login_email_right_email_address'] = '新密碼已送至信箱！';

$lang['login_email_subject'] = "密碼查詢";
$lang['login_email_error1'] = "信件無法寄送, 請聯絡";
$lang['login_email_error2'] = "系統管理員, 確認信箱設定";
$lang['login_email_body1'] = "已傳送新密碼，請登入後修改密碼!";
$lang['login_email_body2'] = "帳號";
$lang['login_email_body3'] = "新密碼";
$lang['login_email_body4'] = "網址";
$lang['login_account_disabled'] = "此帳號已被停用!!";
$lang['login_verify_code_error'] = "驗證碼錯誤!!";
$lang['login_ip_error'] = "登入IP錯誤!!";
$lang['login_account_password_error'] = "帳號或密碼錯誤!!";
$lang['login_account_lock_out'] = "此帳號已無法再次登入, 詳情請洽管理員!";
$lang['login_remain'] = "剩餘嘗試次數";
$lang['login_count'] = "次";

$lang['error_account_cant_login_again'] = '帳號或密碼錯誤!!\n此帳號已無法再次登入, 詳情請洽管理員!';
$lang['error_account_disabled'] = '此帳號被停用!!';
$lang['error_account'] = '帳號或密碼錯誤!!';
$lang['error_ip'] = 'IP不符合網段設定!!';
$lang['count_retry_password'] = '嘗試次數 ';


/* End of file bank_lang.php */
/* Location: ./system/language/zh_tw/bank_lang.php */
