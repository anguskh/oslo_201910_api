<?php
//一般通用
$lang['airlink'] = '科技股份有限公司';
$lang['airlink_eng'] = 'test Technology Co.,Ltd.';
$lang['airlink_addr'] = '無地址';
$lang['airlink_tel'] = 'TEL:(02)1111-1111 FAX:(02)1111-1111';

$lang['logout'] = '登出';
$lang['select'] = '選擇';
$lang['browser'] = '瀏覽/查詢';
$lang['view'] = '查詢';
$lang['add'] = '新增';
$lang['edit'] = '修改';
$lang['delete'] = '刪除';
$lang['save'] = '存檔';
$lang['cancel'] = '取消';
$lang['print'] = '預覽列印';

$lang['print_ok_vender'] = '合格廠商一覽表';
$lang['print_item_all'] = '料品明細表';
$lang['print_item_new_old'] = '料品新舊對照表';
$lang['trans_inv'] = '前月轉入';
$lang['recount_inv'] = '重新計算庫存';
$lang['data_search'] = '搜尋';
$lang['export_xml'] = '匯出XML';
$lang['export_excel'] = '匯出EXCEL';
$lang['batch_report'] = '批次回報';

$lang['resetkey'] = '回收';
$lang['resetphone'] = '清除匹配';
$lang['change_readersn'] = '維修換機';
$lang['up'] = '上移';
$lang['down'] = '下移';
$lang['copy'] = '複製';
$lang['reload'] = '重選';
$lang['search'] = '搜尋';
$lang['search_button'] = '搜 尋';
$lang['import_img'] = '匯入圖檔';
$lang['select_img'] = '選擇圖檔';
$lang['img_format'] = '僅支援jpg,png格式,大小:175*320';
$lang['prev_page'] = '上一頁';
$lang['paring_merchant'] = '配對商店';
$lang['favor_tip'] = '此區域可自訂捷徑, 如需增加請點選子選單前的星號!';

$lang['enable'] = '啟用';
$lang['disable'] = '停用';
$lang['bypass'] = 'ByPass HASH check';
$lang['bypass2'] = 'ByPass';
$lang['nolimits'] = '新版本更新通知 (無期限)';
$lang['nolimits2'] = '無期限';
$lang['limits'] = '版本將於截止日後停用 (有限期)';
$lang['limits2'] = '有限期';
$lang['disable_2'] = '未啟用';
$lang['disableuse'] = '暫停使用';
$lang['lockup'] = '鎖機';
$lang['approve'] = '通過';
$lang['not_approve'] = '未通過';
$lang['yes'] = '是';
$lang['no'] = '否';
$lang['sucess'] = '成功';
$lang['fail'] = '失敗';
$lang['login_side_0'] = '前台';
$lang['login_side_1'] = '後台';
$lang['login_side_2'] = '全部';
$lang['search_all'] = '全部';
$lang['merchant_event'] = '商店事件';
$lang['system_event'] = '系統事件';

$lang['male'] = '男';
$lang['female'] = '女';

$lang['currently_online_users'] = '目前正在線上的人員';
$lang['last_10_rows_of_unsettlement'] = '最近未結帳明細';
$lang['last_10_rows_of_modify_merchant'] = '最近異動商店';
$lang['last_10_rows_of_modify_merchant_data'] = '最近異動資料';
$lang['last_10_rows_of_event_log'] = '最近事件日誌';


$lang['add_successfully'] = '新增成功';
$lang['edit_successfully'] = '修改成功';
$lang['edit_failed'] = '修改失敗';
$lang['add_failed'] = '新增失敗';
$lang['copy_successfully'] = '複製成功';
$lang['reset_successfully'] = '清空成功';
$lang['disable_merchant'] = '\n, 目前已無擁有端末機, 將自動停用此商店和相關使用者帳號!';
$lang['change_successfully'] = '換機成功';
$lang['delete_successfully'] = '刪除成功';
$lang['delete_failed'] = '刪除失敗';
$lang['recover_successfully'] = '復原資料成功';
$lang['recover_fail'] = '復原資料失敗';

$lang['confirm_delete'] = '是否確認要刪除？';
$lang['cnat_delete'] = '無法刪除!';
$lang['confirm_resetkey'] = '是否確認要清空XAC E50序號和key值？\n此動作將會清除重要欄位,\n如(BKLK_KEK...)';
$lang['confirm_resetphone'] = '是否確認要清空手機匹配？';
$lang['ajax_request_an_error_occurred'] = 'Ajax request 發生錯誤！\n請重試！';
$lang['search_not_found'] = '查無資料';
$lang['process_failed'] = '無資料可刪除';
$lang['delete_img'] = '刪除圖檔';


$lang['confirm'] = '確認執行';
$lang['next'] = '下一步';
$lang['resend'] = '重新發送';
$lang['space'] = '　';
$lang['space1_9'] = '　';
$lang['space2'] = '　';
$lang['space6'] = '　　　　　';

$lang['home'] = '首頁';
$lang['all'] = '全部列表';
$lang['all_not_approve'] = '全部未覆核列表';
$lang['bank'] = '銀行：';
$lang['bank2'] = '銀行';
$lang['kind'] = '類別：';
$lang['merchant'] = '商店：';
$lang['terminal'] = '端末機：';
$lang['project'] = '專案：';
$lang['user'] = '使用者 : ';

$lang['select_bank'] = '請選擇銀行';
$lang['select_bank_help'] = '-- 請選擇欲維護專案的銀行 --&nbsp;';
$lang['select_supplier'] = '請選擇刷卡機廠商：';
$lang['select_supplier_help'] = '-- 請選擇欲維護專案的刷卡機廠商 --&nbsp;';
$lang['select_merchant'] = '請選擇商店';
$lang['select_pattern'] = '請選擇樣版';
$lang['select_merchant_help'] = '-- 請選擇欲維護的商店 --&nbsp;';
$lang['select_terminal'] = '請選擇端末機：';
$lang['select_terminal_help'] = '-- 請選擇欲維護的端末機 --&nbsp;';
$lang['select_project'] = '請選擇專案：';
$lang['select_project_help'] = '-- 請選擇欲維護的專案 --&nbsp;';
$lang['select_user'] = '請選擇使用者：';
$lang['select_user_help'] = '-- 請選擇欲維護的使用者 --&nbsp;';
$lang['select_choose_a_date'] = '請選擇日期區間';
$lang['select_choose_close_date'] = '請選擇結帳日期';
$lang['select_choose_statistical'] = '請選擇統計日期<br />(無選擇結束日期, 預設取起日前三個月資料)';
$lang['select_event_type'] = '請選擇事件';
$lang['select_event_type_help'] = '-- 請選擇事件 --　';
$lang['select_system_event'] = '系統事件';
$lang['select_merchant_event'] = '商店事件';
$lang['select_all'] = '全部';
$lang['select_all_bank_no'] = '全部銀行';
$lang['select_all_kind_no'] = '全部類別';
$lang['select_all_isp_class'] = '全部分類';
$lang['select_all_ship_class'] = '全部分類';
$lang['select_all_stock_class'] = '全部分類';
$lang['select_city'] = '請選擇城市';
$lang['select_dept'] = '請選擇部門';
$lang['select_location'] = '請選擇辦公室地點';
$lang['select_district'] = '請選擇地區';
$lang['select_machine'] = '請選擇機型';
$lang['select_machineap'] = '請選擇POSAP版本';
$lang['select_kernel'] = '請選擇kernel版本';
$lang['select_status'] = '請選擇狀態';
$lang['select_flag'] = '請選擇完成結果';
$lang['select_unfinish'] = '請選擇未完成原因';
$lang['select_onsite'] = '請選擇回收方式';
$lang['select_maintain_onsite'] = '請選擇處理方式';
$lang['select_cause_no'] = '請選擇不良原因';
$lang['select_air_cause_no'] = '請選擇公司不良原因';
$lang['select_fixway_no'] = '請選擇完成維修';
$lang['select_use_situation'] = '請選擇卡機處理';
$lang['select_position'] = '請選擇職位';
$lang['select_charge_area'] = '請選擇負責區域';
$lang['select_bank_kind'] = '請選擇客戶類別';
$lang['select_per_no'] = '請選擇業務負責人';
$lang['select_per_no2'] = '請輸入或選擇人員';
$lang['select_request_per_no'] = '請選擇請購人員';
$lang['select_request_ord_per'] = '請選擇訂購人員';
$lang['select_request_isp_per'] = '請選擇驗收人員';
$lang['select_check_per'] = '請選擇盤點人員';
$lang['select_isp_per'] = '請選擇申請人員';
$lang['select_stock_per'] = '請選擇倉管人員';
$lang['select_stock_per2'] = '請選擇驗收主管';
$lang['select_ship_per'] = '請選擇發貨人員';
$lang['select_ship_per2'] = '請選擇請款人員';
$lang['select_ship_per3'] = '請選擇申請人員';
$lang['select_t_manager'] = '請選擇部門主管';
$lang['select_check_per'] = '請選擇盤點人員';
$lang['select_stock_manager'] = '請選擇倉管主管';
$lang['select_ship_way'] = '請選擇交貨方式';
$lang['select_sales_kind'] = '請選擇交貨類別';
$lang['select_machine_type_per_no'] = '請選擇廠商員工';
$lang['select_account_close_date'] = '請選擇結帳日';
$lang['select_payway'] = '請選擇付款方式';
$lang['select_paydate'] = '請選擇付款日';
$lang['select_invoice_way'] = '請選擇發票種類';
$lang['select_paygetway'] = '請選擇取款方式';
$lang['select_bank_no'] = '請選擇銀行';
$lang['select_bank_no2'] = '請選擇客戶編號';
$lang['select_bank_no3'] = '請選擇客戶名稱';
$lang['select_four_dbc'] = '請選擇4DBC';
$lang['select_kind_no'] = '請選擇料號類別';
$lang['select_item_no'] = '請選擇料號';
$lang['select_isp_class'] = '請選擇物品分類';
$lang['select_ship_class'] = '請選擇物品分類';
$lang['select_use_mark'] = '請選擇用途';
$lang['select_request_status'] = '請選擇報比價';
$lang['select_vnd_no'] = '請選擇廠商';
$lang['select_warranty_way'] = '請選擇保固條件';
$lang['select_paytxt'] = '請選擇付款條件';
$lang['select_remark'] = '請選擇備註';
$lang['select_stock_class_2'] = '請選擇出庫類別';
$lang['select_stock_class_3'] = '請選擇移倉類別';
$lang['select_tra_reason'] = '請選擇移倉原因';
$lang['select_reason'] = '請選擇差異原因';
$lang['select_version'] = '請選擇銀行版本';
$lang['select_user2'] = '請選擇使用者';
$lang['select_notice_no'] = '請選擇通知方式';

$lang['select_release_per'] = '請選擇申請人員';
$lang['select_stock_per'] = '請選擇倉管人員';
$lang['select_rel_sub_per'] = '請選擇代申請人員';
$lang['select_stock_class'] = '請選擇領用類別';
$lang['select_instock_class'] = '請選擇入庫類別';
$lang['select_stock_kind'] = '請選擇領用大類';
$lang['select_warehouse_no'] = '請選擇出庫倉庫';
$lang['select_warehouse_no2'] = '請選擇倉庫';
$lang['select_warehouse_no3'] = '請輸入或選擇倉庫';
$lang['select_new_warehouse_no'] = '請選擇入庫倉庫';
$lang['select_release_reason'] = '請選擇出庫原因';
$lang['select_ost_sub_per_no'] = '請選擇代出人員';
$lang['select_ret_sub_per'] = '請選擇代入人員';
$lang['select_ins_per_no'] = '請選擇安裝人員';
$lang['select_mai_per_no'] = '請選擇維護人員';
$lang['select_sha_per_no'] = '請選擇共用人員';
$lang['select_rec_per_no'] = '請選擇回收人員';
$lang['select_rep_per'] = '請選擇記錄人員';
$lang['select_area_no'] = '請選擇儲位';
$lang['select_emsType'] = '請選擇類別代號';
$lang['select_contlsUFlag'] = '請選擇含U卡功能';
$lang['select_contlsVFlag'] = '請選擇含VISA功能';
$lang['select_contlsMFlag'] = '請選擇含MASTER功能';
$lang['select_contlsJFlag'] = '請選擇含JBC功能';
$lang['select_contlsAEFlag'] = '請選擇含AE功能';

$lang['select_return_reason'] = '請選擇入庫原因';
$lang['select_ret_warehouse_no'] = '請選擇入庫倉庫';
$lang['select_check_result'] = '請選擇檢查結果';
$lang['select_problem_reason'] = '請選擇問題件原因';

$lang['select_zip_Code'] = '請選擇郵遞區號';
$lang['select_pay_bank'] = '請選擇匯款銀行';
$lang['select_mcht_district'] = '請選擇裝機地區';
$lang['select_industry_code'] = '請選擇行業別';
$lang['select_pay_branch'] = '請選擇分行代號';

$lang['select_import_excel'] = '請選擇匯入的EXCEL檔案';
$lang['select_import_caption_excel2_error'] = '匯入過程失敗!, 請聯絡管理員!';


$lang['select_all_help'] = '-- 選擇全部 --&nbsp;';
$lang['select_all_mer_help'] = '-- 全部商店 --&nbsp;';
$lang['select_stock_help'] = '-- 選擇庫存端末機 --&nbsp;';
$lang['all_mer'] = '全部商店';

$lang['select_machine_type_no'] = '請選擇機型';
$lang['select_machine_type_no2'] = '請選擇機型編號';
$lang['select_all_machine_type_no'] = '全部機型';

$lang['please_input_merchant_id'] = '請輸入商店代號';
$lang['please_input_terminal_id'] = '請輸入端末機代號';
$lang['please_input'] = '請輸入';

$lang['label_include'] = '含序號';
$lang['report_no_data'] = '無資料';

//訊息
$lang['check_url_error'] = '沒有權限存取此頁面，\n將自動跳轉到登入頁面';
$lang['success_action'] = '成功';
$lang['failed_action'] = '失敗';
$lang['out_max_no'] = '超過自動編號, 請聯絡管理員!';
$lang['timeout_msg'] = '系統閒置已登出!';

//分頁
$lang['common_first_page'] = '第一頁';
$lang['common_last_page'] = '最後一頁';
$lang['common_prev_page'] = '上一頁';
$lang['common_next_page'] = '下一頁';
$lang['common_page'] = '頁';

$lang['copy_bank_project_help'] = '此複製功能僅限於將所有資料，完整複製一份至下方選擇的銀行專案裡！！！';
$lang['copy_bank_project_error'] = '所選擇的銀行專案已經有資料了，所以不予許複製！！！';
$lang['select_import_caption_xml'] = '請選取欲匯入的派工檔案（XML格式）';
$lang['select_import_caption_excel'] = '請選取欲匯入的讀卡機配對檔案（EXCEL格式）';
$lang['select_upload_file'] = '選擇上傳檔案';

$lang['login_user_title'] = '電動機車管理系統';
$lang['login_admin_title'] = '電動機車管理系統';
$lang['html_title'] = '電動機車';
$lang['btn_mno'] = '序號明細';
$lang['btn_delete'] = '刪除資料';

//log
$lang['searchData'] = '搜尋';
$lang['acquire_bank_id'] = '銀行編號';
$lang['merchant_id'] = '商店編號';
$lang['terminal_id'] = '商店編號';
$lang['create_date'] = '建檔日期';
$lang['create_user'] = '建檔人員';
$lang['update_user'] = '修改人員';
$lang['update_date'] = '修改日期';
$lang['UPDATE_DATETIME'] = '修改日期';
$lang['start_date'] = '起日';
$lang['end_date'] = '迄日';
$lang['FILE_DATE_Start'] = '起日';
$lang['FILE_DATE_End'] = '迄日';
$lang['INITIALIZE_DATE_Start'] = '起日';
$lang['INITIALIZE_DATE_End'] = '迄日';
$lang['ACT'] = '動作';
$lang['RETURN'] = '回傳';
$lang['SN'] = '編號';
$lang['BANK_ID'] = '銀行編號';
$lang['error_date'] = '起日不可大於迄日!';
$lang['error_date2'] = '請選擇日期!!';


$lang['report_sign_General_Manager'] = '總經理';
$lang['report_sign_Vice_President'] = '副總經理';
$lang['report_sign_Unit_Manager'] = '單位主管';
$lang['report_sign_Lister'] = '單位主管';

$lang['total_count'] = '總筆數:';

/* End of file common_lang.php */
/* Location: ./system/language/zh_tw/common_lang.php */
