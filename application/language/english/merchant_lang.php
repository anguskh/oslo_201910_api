<?php
//其他
$lang['merchant_management'] = 'Merchant Management';
$lang['merchant_input_merchant_id'] = 'Please Input Merchant ID';
$lang['merchant_input_chinese_name'] = 'Please Input Merchant Chinese Name';
$lang['merchant_input_english_name'] = 'Please Input Merchant English Name';
$lang['merchant_input_invoice_no'] = 'Please Input Invoice No';
$lang['merchant_city'] = 'City';
$lang['merchant_input_city'] = 'Please Input City';
$lang['merchant_tel'] = 'Telephone';
$lang['merchant_input_tel'] = 'Please Input Telephone No';
$lang['merchant_CONTACT'] = 'Linkman';
$lang['merchant_input_boss'] = 'Please Input Name of Boss';
$lang['merchant_address1'] = 'Address';
$lang['merchant_input_address1'] = 'Please Input Address';
$lang['merchant_address2'] = 'Address 2';
$lang['merchant_input_address2'] = 'Please Input Address 2';
$lang['merchant_address3'] = 'Address 3';
$lang['merchant_input_address3'] = 'Please Input Address 3';
$lang['merchant_address4'] = 'Address 4';
$lang['merchant_input_address4'] = 'Please Input Address 4';
$lang['merchant_mcc_code'] = 'MCC Code';
$lang['merchant_input_mcc_code'] = 'Please Input Address MCC Code';
$lang['merchant_ip_address'] = 'IP Address';
$lang['merchant_input_ip_address'] = 'Please Input IP Address';
$lang['merchant_visa_card'] = 'VISA CARD';
$lang['merchant_master_card'] = 'MASTER CARD';
$lang['merchant_jcb_card'] = 'JCB CARD';
$lang['merchant_u_card'] = 'U CARD';
$lang['merchant_AGREE'] = 'Singing Date';
$lang['merchant_DISAGREE'] = 'Termination Date';
$lang['merchant_SETTLE_TIME'] = 'Auto Settlement Time';
$lang['merchant_input_settlement_time'] = 'Please Select Settlement Time' ;
$lang['merchant_max_amout'] = 'Max Amount';
$lang['merchant_input_max_amout'] = 'Please Input Max Amount';
$lang['merchant_SETTLE_TYPE'] = 'Auto Settlement';
$lang['merchant_id_duplicate'] = 'Merchant ID duplicate!!!';
$lang['merchant_acquire'] = 'Acquire';
$lang['merchant_no_acquire'] = 'No Acquire';
$lang['merchant_authorize_without_settlement'] = 'Authorize Without Settlement';
$lang['merchant_authorize_with_settlement'] = 'Authorize With Settlement';
$lang['merchant_authorize_only'] = 'Authorize Only';

//欄位
$lang['merchant_MERCHANT_ID'] = 'Merchant ID';
$lang['merchant_ACQUIRE_BANK_ID'] = 'Bank ID';
$lang['merchant_COMMENT_NOTES'] = 'Bank Name';
$lang['merchant_merchant_name_chinese'] = 'Merchant Chinese Name';
$lang['merchant_merchant_name_english'] = 'Merchant English Name';
$lang['merchant_invoice_no'] = 'Invoice No';
$lang['merchant_STATUS'] = 'Status';

$lang['merchant_TOTAL'] = 'Total';
/* End of file merchant_lang.php */
/* Location: ./system/language/zh_tw/merchant_lang.php */
