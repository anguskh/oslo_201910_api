<?php
//一般通用
$lang['logout'] = 'Logout';
$lang['select'] = 'Select';
$lang['browser'] = 'Browser/Search';
$lang['add'] = 'Add';
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';
$lang['print'] = 'Print';
$lang['resetkey'] = 'recycle';
$lang['resetphone'] = 'reset phone';
$lang['change_readersn'] = 'change reader';
$lang['up'] = 'Up';
$lang['down'] = 'Down';
$lang['copy'] = 'Copy';
$lang['reload'] = 'Reload';
$lang['refund'] = 'Refund';
$lang['search'] = 'Search';
$lang['search_button'] = 'Search';
$lang['import_img'] = 'import image';
$lang['select_img'] = 'select image';
$lang['img_format'] = 'only support jpg, png format,size:175*320';
$lang['prev_page'] = 'Prev';
$lang['paring_merchant'] = 'Paring Merchant';
$lang['favor_tip'] = 'This area can customize the shortcut, please click submenu additional increase before the asterisk!';

$lang['enable'] = 'Enable';
$lang['disable'] = 'Disable';
$lang['bypass'] = '忽略檢查HASH(ByPass HASH check)';
$lang['bypass2'] = '忽略檢查';
$lang['nolimits'] = '新版本更新通知 (無期限)';
$lang['nolimits2'] = '無期限';
$lang['limits'] = '版本將於截止日後停用 (有限期)';
$lang['limits2'] = '有限期';
$lang['disable_2'] = 'not Enable';
$lang['disableuse'] = 'Disable';
$lang['lockup'] = 'Block';
$lang['approve'] = 'Approve';
$lang['not_approve'] = 'Not Approve';
$lang['yes'] = 'Yes';
$lang['no'] = 'No';
$lang['sucess'] = 'sucess';
$lang['fail'] = 'fail';
$lang['login_side_0'] = 'Foreground';
$lang['login_side_1'] = 'Background';
$lang['login_side_2'] = 'All';
$lang['merchant_event'] = 'Merchant Event';
$lang['system_event'] = 'System Event';

$lang['male'] = 'Male';
$lang['female'] = 'Female';

$lang['currently_online_users'] = 'Currently online users';
$lang['last_10_rows_of_unsettlement'] = 'Last rows of unsettlement';
$lang['last_10_rows_of_modify_merchant'] = 'Last rows of modify merchant';
$lang['last_10_rows_of_modify_merchant_data'] = 'Last rows of modify data';
$lang['last_10_rows_of_event_log'] = 'Last rows of event log';

$lang['add_successfully'] = 'Successfully added';
$lang['edit_successfully'] = 'Successfully modified';
$lang['edit_failed'] = 'Modified Failed';
$lang['copy_successfully'] = 'Successfully copied';
$lang['reset_successfully'] = 'Successfully reset';
$lang['disable_merchant'] = '\n, not have available Terminal,it will auto disable this Merchant and User';
$lang['change_successfully'] = 'Successfully change';
$lang['delete_successfully'] = 'Successfully delete';

$lang['delete_failed'] = 'Delete Failed';

$lang['recover_successfully'] = 'Successfully recover';
$lang['recover_fail'] = 'fail recover';

$lang['confirm_delete'] = 'Confirm that you want to delete?';
$lang['cnat_delete'] = 'Can not Delete!';
$lang['confirm_resetkey'] = 'Confirm that you want to reset key?\nwill be reset important data\nabout(BKLK_KEK...)';
$lang['confirm_resetphone'] = 'Confirm that you want to reset check phone?';
$lang['ajax_request_an_error_occurred'] = 'Ajax request an error occurred!\nPlease try again!';
$lang['search_not_found'] = 'Search not found';
$lang['process_failed'] = 'No data to Delete';
$lang['delete_img'] = 'Delete Img';

$lang['confirm'] = 'Confirm';
$lang['next'] = 'Next';
$lang['resend'] = 'Resend';
$lang['space'] = '&nbsp;&nbsp;';
$lang['space1_9'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
$lang['space2'] = '&nbsp;&nbsp;&nbsp;&nbsp;';
$lang['space6'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

$lang['home'] = 'Home';
$lang['all'] = 'All List';
$lang['all_not_approve'] = 'Non Approved List';
$lang['bank'] = 'Bank : ';
$lang['merchant'] = 'Merchant : ';
$lang['terminal'] = 'Terminal : ';
$lang['project'] = 'Project : ';
$lang['user'] = 'User : ';

$lang['select_bank'] = 'Please select bank name :';
$lang['select_bank_help'] = '-- Please select the edit projects bank --&nbsp;';
$lang['select_supplier'] = 'Please select supplier name :';
$lang['select_supplier_help'] = '-- Please select the edit projects supplier --&nbsp;';
$lang['select_merchant'] = 'Please select merchant name :';
$lang['select_merchant_help'] = '-- Please select the edit merchant --&nbsp;';
$lang['select_terminal'] = 'Please Select Terminal：';
$lang['select_terminal_help'] = '-- Please Select Terminal --&nbsp;';
$lang['select_project'] = 'Please select project name :';
$lang['select_project_help'] = '-- Please select project --&nbsp;';
$lang['select_user'] = 'Please select user name :';
$lang['select_user_help'] = '-- Please select user --&nbsp;';
$lang['select_choose_a_date'] = 'Please choose a date';
$lang['select_choose_close_date'] = 'Please choose close date';
$lang['select_choose_statistical'] = 'Please choose statistical date';
$lang['select_event_type'] = 'Please select event type';
$lang['select_event_type_help'] = '-- Please select event type --&nbsp;';
$lang['select_system_event'] = 'System Event';
$lang['select_merchant_event'] = 'Merchant Event';
$lang['select_all'] = 'All';
$lang['select_city'] = '-- 請選擇城市 --&nbsp;';

$lang['select_all_help'] = '-- Select All --&nbsp;';
$lang['select_all_mer_help'] = '-- Select All Merchant--&nbsp;';
$lang['select_stock_help'] = '-- Stock Terminal --&nbsp;';
$lang['all_mer'] = 'All Merchant';

$lang['please_input_merchant_id'] = 'Please Input Merchant ID';
$lang['please_input_terminal_id'] = 'Please Input Terminal ID';

//訊息
$lang['check_url_error'] = 'Do not have permission to access this page,\nit will return to the login.';
$lang['success_action'] = 'Success';
$lang['failed_action'] = 'Failed';

//分頁
$lang['common_first_page'] = 'First';
$lang['common_last_page'] = 'Last';
$lang['common_prev_page'] = 'Prev';
$lang['common_next_page'] = 'Next';

$lang['copy_bank_project_help'] = 'This copy feature is limited to all data, a complete copy of the project to the inside bottom of the selected bank!';
$lang['copy_bank_project_error'] = 'The selected projects have data banks, so will not permit copying!';
$lang['select_import_caption_xml'] = 'Please select the file（XML format）';
$lang['select_import_caption_excel'] = 'Please select the file（EXCEL format）';
$lang['select_upload_file'] = 'select upload file';

$lang['login_user_title'] = 'National Credit Card Center Gateway Solution Management System';
$lang['login_admin_title'] = 'Gateway Solution Management System';

//log
$lang['searchData'] = 'Search';
$lang['acquire_bank_id'] = 'Bank ID';
$lang['merchant_id'] = 'Merchant ID';
$lang['terminal_id'] = 'Terminal ID';
$lang['create_date'] = 'Create Date';
$lang['create_user'] = 'Create User';
$lang['update_user'] = 'Update User';
$lang['update_date'] = 'Update Date';
$lang['UPDATE_DATETIME'] = 'Update Date';
$lang['start_date'] = 'Start Date';
$lang['end_date'] = 'End Date';
$lang['FILE_DATE_Start'] = 'Start Date';
$lang['FILE_DATE_End'] = 'End Date';
$lang['INITIALIZE_DATE_Start'] = 'Start Date';
$lang['INITIALIZE_DATE_End'] = 'End Date';
$lang['ACT'] = 'Action';
$lang['RETURN'] = 'Return';
$lang['SN'] = 'SN';
$lang['BANK_ID'] = 'Bank ID';
/* End of file common_lang.php */
/* Location: ./system/language/english/common_lang.php */
