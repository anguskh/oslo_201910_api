<?php
//其他
$lang['location_management'] = '辦公室地點';
$lang['location_label_location_no'] = '辦公室代號';
$lang['location_input_location_no'] = '請輸入辦公室代號';
$lang['location_label_location_name'] = '辦公室地點';
$lang['location_input_location_name'] = '請輸入辦公室地點';


//欄位
$lang['location_location_no'] = '辦公室代號';
$lang['location_location_name'] = '辦公室地點';

/* End of file user_lang.php */
/* Location: ./system/language/zh_tw/user_lang.php */
