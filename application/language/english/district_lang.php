<?php
//其他
$lang['district_management'] = '地區基本資料';
$lang['district_label_district_no'] = '地區代號';
$lang['district_input_district_no'] = '請輸入地區代號';
$lang['district_label_district_name'] = '地區名稱';
$lang['district_input_district_name'] = '請輸入地區名稱';
$lang['district_label_NCCC_no'] = 'NCCC代號';
$lang['district_input_NCCC_no'] = '請輸入NCCC代號';
$lang['district_label_class_no'] = '類別代號';
$lang['district_input_class_no'] = '請輸入類別代號';
$lang['district_label_city_no'] = '城市代號';
$lang['district_input_city_no'] = '請輸入城市代號';



//欄位
$lang['district_district_no'] = '地區代號';
$lang['district_district_name'] = '地區名稱';
$lang['district_NCCC_no'] = 'NCCC代號';
$lang['district_class_no'] = '類別代號';
$lang['district_city_no'] = '城市代號';
$lang['district_city_name'] = '城市名稱';

/* End of file user_lang.php */
/* Location: ./system/language/zh_tw/user_lang.php */
