<?php
//其他
$lang['position_management'] = '部門基本資料';
$lang['position_label_position_no'] = '職稱代號';
$lang['position_input_position_no'] = '請輸入職稱代號';
$lang['position_label_position_name'] = '職稱';
$lang['position_input_position_name'] = '請輸入職稱';


//欄位
$lang['position_position_no'] = '職稱代號';
$lang['position_position_name'] = '職稱';

/* End of file user_lang.php */
/* Location: ./system/language/zh_tw/user_lang.php */
