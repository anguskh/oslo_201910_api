<?php
//其他
$lang['department_management'] = '部門基本資料';
$lang['department_label_dept_no'] = '部門代號';
$lang['department_input_dept_no'] = '請輸入部門代號';
$lang['department_label_dept_name'] = '部門名稱';
$lang['department_input_dept_name'] = '請輸入部門名稱';
$lang['department_label_isp_yes'] = '採購';
$lang['department_label_ship_yes'] = '銷售';
$lang['department_label_stock_yes'] = '庫存';
$lang['department_label_install_yes'] = '服務';
$lang['department_label_mark'] = '狀態';

//欄位
$lang['department_dept_no'] = '部門代號';
$lang['department_dept_name'] = '部門名稱';
$lang['department_isp_yes'] = '採購';
$lang['department_ship_yes'] = '銷售';
$lang['department_stock_yes'] = '庫存';
$lang['department_install_yes'] = '服務';
$lang['department_mark'] = '狀態';

/* End of file user_lang.php */
/* Location: ./system/language/zh_tw/user_lang.php */
