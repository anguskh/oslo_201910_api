<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_driver_info extends MY_Model {
	
    function __construct() 
	{
        parent::__construct();
		// echo "in Model_column";
	}

	public function getdriverinfo($page_num = 0)
	{
		$start = $page_num*20;
		$end = 20;
		$battery_id = $this->session->userdata("battery_id");
		$comparefun = $this->session->userdata("comparefun");
		$whereStr = "";
		if($battery_id!="")
		{
			if($comparefun=="KW")
				$whereStr = " WHERE unit_id LIKE '%{$battery_id}%'";
			else if($comparefun=="=")
				$whereStr = " WHERE unit_id = '{$battery_id}'";
		}
		$SQLCmd = "SELECT * FROM log_driving_info {$whereStr} ORDER BY s_num DESC limit {$start},{$end}";
		$rs = $this->db_query($SQLCmd);
		return $rs; 
	}

	public function getdriverfield()
	{
		$SQLCmd = "SHOW FULL FIELDS FROM log_driving_info";
		$rs = $this->db_query($SQLCmd);
		return $rs; 
	}
}

/* End of file Model_driver_info.php */
