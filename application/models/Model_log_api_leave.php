<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_log_api_leave extends MY_Model {
	
    function __construct() 
	{
        parent::__construct();
		// echo "in Model_column";
		define('PI',3.1415926535898);
		define('EARTH_RADIUS',6378.137);
	}

	public function get_log(){
		$SQLCmd = "SELECT receive_data FROM log_api_history WHERE api_name = 'c2af4999'";
		$rs = $this->db_query($SQLCmd);
		if($rs)
		{
			foreach($rs as $key => $value)
			{
				$postjson = $value['receive_data'];
				$postjson = str_replace("'","",$postjson);
				$postjson = str_replace("JSONData=","",$postjson);
				if($this->is_json($postjson))
				{
					if($postjson!="")
					{
						$data_json_de = json_decode($postjson,true);
						if(isset($data_json_de['bs02']))
						{
							if($data_json_de['bs02']!="")
							{
								$bss_token = $data_json_de['bs02'];
								$SQLCmdBS = "SELECT s_num FROM tbl_battery_swap_station WHERE bss_token = '{$bss_token}' AND status <> 'D'";
								$rsBS = $this->db_query($SQLCmdBS);
								if($rsBS)
								{
									$sb_num = $rsBS[0]['s_num'];
									$SQLCmdBS = "UPDATE tbl_battery_swap_station SET exchange_num = exchange_num+1 WHERE s_num = '{$sb_num}' AND status <> 'D'";
									$this->db_query($SQLCmdBS); 
									// $leave_date = $data_json_de['bs01'];
									// $track_no = $data_json_de['bs04'];
									// $battery_id = $data_json_de['bs05'];;//電池序號
									// $whereStr = "leave_date = '{$leave_date}' AND leave_track_no = '{$track_no}' AND leave_battery_id = '{$battery_id}'";
									// $dataArr['leave_sb_num'] = $sb_num;
									// if($this->db_update('log_battery_leave_return',$dataArr,$whereStr))
									// {
									// 	echo "電池{$battery_id} 借出時間{$leave_date} 更新成功<br>";
									// }
								}
							}
						}
					}
				}
			}
		}
	}

	public function fill_log($limit1="",$date="")
	{
		$limit = "";
		$limit1 = urldecode($limit1);
		$date = urldecode($date);
		if($limit1!="")
			$limit = " limit {$limit1}"; 
		$SQLCmd = "SELECT s_num,allgetdata,system_log_date FROM log_driving_info WHERE log_type='B' AND system_log_date >= '{$date}' {$limit}";
		echo $SQLCmd;
		echo "<br>";
		$rs = $this->db_query($SQLCmd);
		if($rs)
		{
			foreach($rs as $key=>$value)
			{
				$postjson = $value['allgetdata'];
				$postjson = str_replace("'","",$postjson);
				$postjson = str_replace("JSONData=","",$postjson);
				if($this->is_json($postjson))
				{
					if($postjson != "")
					{
						$whereStr = "s_num = {$value['s_num']}";
						$data_json_de = json_decode($postjson,true);
						if(isset($data_json_de['di01']))
						{
							$dataArr['unit_id'] = $data_json_de['di01'];//battery id
							$user_id = addslashes($data_json_de['di02']);//會員代號
							if($user_id != ''){
								$SQLCmdM = "SELECT s_num FROM tbl_member WHERE user_id = '{$user_id}' AND status <> 'D'";
								$rsM = $this->db_query($SQLCmdM);
								if(count($rsM)>0)
								{
									$dataArr['tm_num'] = $rsM[0]['s_num'];
								}
							}
							$dataArr['utc_date'] = addslashes($data_json_de['di03']);//UTC日期
							$dataArr['utc_time'] = addslashes($data_json_de['di04']);//UTC時間
							$dataArr['latitude'] = addslashes($data_json_de['di05']);//GPS緯度
							$dataArr['longitude'] = addslashes($data_json_de['di06']);//GPS經度
							$dataArr['battery_capacity'] = addslashes($data_json_de['di07']);//battery_capacity
							$dataArr['battery_temperature'] = addslashes(substr($data_json_de['di08'],-2));//電池溫度
							$dataArr['battery_voltage'] = addslashes($data_json_de['di09']);//電池電壓
							$dataArr['battery_amps_type'] = addslashes($data_json_de['di10']);//電池電流充放模式
							$dataArr['battery_amps'] = addslashes($data_json_de['di11']);//電池電流
							$this->db_update('log_driving_info',$dataArr,$whereStr);
							$system_log_date = $value['system_log_date'];
							$SQLCmdbl = "SELECT s_num,gps_path_track,distance FROM log_battery_leave_return WHERE leave_date<='{$value['system_log_date']}' AND (return_date>='{$value['system_log_date']}' OR return_date IS NULL) AND leave_battery_id = '{$dataArr['unit_id']}' AND leave_date IS NOT NULL ORDER BY leave_date DESC limit 1";
							$rsbl = $this->db_query($SQLCmdbl);
							if($rsbl)
							{
								echo $SQLCmdbl;
								echo "<br>";
								if($dataArr['latitude']!=0 && $dataArr['longitude']!=0)
								{
									if($rsbl[0]['gps_path_track']=="")
									{
										$dataArrBL['gps_path_track'] = $dataArr['latitude'].",".$dataArr['longitude'];
									}
									else
									{
										$gpsArr = explode(";",$rsbl[0]['gps_path_track']);
										$gpsArr2 = explode(",",$gpsArr[count($gpsArr)-1]);
										$lat1 = $gpsArr2[0];
										$lng1 = $gpsArr2[1];
										$dataArrBL['distance'] = $rsbl[0]['distance']+$this->GetDistance($lat1,$lng1,$dataArr['latitude'],$dataArr['longitude']);
										$dataArrBL['gps_path_track'] = $rsbl[0]['gps_path_track'].";".$dataArr['latitude'].",".$dataArr['longitude'];
									}
									$whereStr1 = "s_num = {$rsbl[0]['s_num']}";
									$this->db_update("log_battery_leave_return",$dataArrBL,$whereStr1);
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	* 計算兩組經緯度座標 之間的距離
	* params ：lat1 緯度1； lng1 經度1； lat2 緯度2； lng2 經度2； len_type （1:m or 2:km);
	* return m or km
	*/
	public function GetDistance($lat1, $lng1, $lat2, $lng2, $len_type = 2, $decimal = 2) {
	    $radLat1 = $lat1 * PI / 180.0;
	    $radLat2 = $lat2 * PI / 180.0;
	    $a = $radLat1 - $radLat2;
	    $b = ($lng1 * PI / 180.0) - ($lng2 * PI / 180.0);
	    $s = 2 * asin(sqrt(pow(sin($a/2),2) + cos($radLat1) * cos($radLat2) * pow(sin($b/2),2)));
	    $s = $s * EARTH_RADIUS;
	    $s = round($s * 1000);
	    if ($len_type > 1)
	    {
	    $s /= 1000;
	    }
	    return round($s, $decimal);
	}
}

?>