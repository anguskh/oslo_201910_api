<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_common extends MY_Model {
	//寫入API收到的JSON資料
	public function insert_api_log($receive_data,$api_name="",$api_chinese_name="",$hexdata="")
	{
		//寫入收到的資料到歷史紀錄
		$output = implode(', ', array_map(
		    function ($v, $k) { return sprintf("%s='%s'", $k, $v); },
		    $receive_data,
		    array_keys($receive_data)
		));
		$dataArr = array();
		date_default_timezone_set('Asia/Taipei');
		$logdataArr['api_chinese_name'] = $api_chinese_name;
		$logdataArr['api_name'] = $api_name;
		$logdataArr['receive_data'] = $output;
		$logdataArr['receive_ip'] = $_SERVER['REMOTE_ADDR'];
		$logdataArr['receive_date'] = "now()";
		// $logdataArr['api_server_ip'] = $_SERVER["SERVER_ADDR"];
		// $logdataArr['api_server_receive_date'] = date("Y-m-d H:i:s");
		if($hexdata != "")
		{
			$logdataArr['receive_data_hex'] = $hexdata;
		}
		$this->db_insert("log_api_history",$logdataArr);

		$SQLCmd = "SELECT LAST_INSERT_ID() as api_log_sn";
		$rs = $this->db_query($SQLCmd);
		return $rs[0]['api_log_sn'];
	}

	//更新API回傳的JSON資料
	public function update_api_log($api_log_sn,$response_data,$otherdataArr="")
	{
		$whereStr = "api_history_sn = {$api_log_sn}";
		//更新回應的資料到歷史紀錄
		// $output = implode(', ', array_map(
		//     function ($v, $k) { return sprintf("%s='%s'", $k, $v); },
		//     $response_data,
		//     array_keys($response_data)
		// ));
		$dataArr = array();
		date_default_timezone_set('Asia/Taipei');
		$logdataArr['response_data'] = $response_data;
		if($otherdataArr!="")
		{
			$logdataArr['response_date'] = "now()";
			// $logdataArr['api_server_response_date'] = date("Y-m-d H:i:s");
			foreach($otherdataArr as $key => $value)
			{
				$logdataArr[$key] = $value;
			}
			// $logdataArr['sys_spend_time'] = $otherdataArr['sys_spend_time'];
		}
		$this->db_update("log_api_history",$logdataArr,$whereStr);

		
	}

	//寫入實名驗證傳送資料
	public function insert_person_verify_log($senddata)
	{
		$logdataArr = array();
		date_default_timezone_set('Asia/Taipei');
		$logdataArr['send_date'] = "now()";
		$logdataArr['send_data'] = $senddata;
		$this->db_insert("log_person_verification",$logdataArr);

		$SQLCmd = "SELECT LAST_INSERT_ID() as log_sn";
		$rs = $this->db_query($SQLCmd);
		return $rs[0]['log_sn'];
	}

	//更新實名驗證回傳的JSON資料
	public function update_person_verify_log($log_sn,$response_data)
	{
		$whereStr = "s_num = {$log_sn}";
		$output = $response_data;
		$logdataArr = array();
		date_default_timezone_set('Asia/Taipei');
		$logdataArr['receive_data'] = $output;
		$logdataArr['receive_date'] = "now()";
		$this->db_update("log_person_verification",$logdataArr,$whereStr);
	}

	//判斷是否為Json格式
	public function is_json($string) {
		json_decode($string);
		return (json_last_error() == JSON_ERROR_NONE);
	}

	public function getstatistics()
	{
		$SQLCmd1 = "SELECT count(*) cnt FROM tbl_battery_swap_station WHERE status <> 'D'";
		$rs1 = $this->db_query($SQLCmd1);
		header("Content-Type:text/html; charset=utf-8");
		echo "交換站：{$rs1[0]['cnt']} <br>";
		$statino_num = $rs1[0]['cnt'];

		$SQLCmd2 = "SELECT SUM(exchange_num)/(DATEDIFF(MAX(log_date),MIN(log_date))) as avgNum FROM log_bss_info";
		$rs2 = $this->db_query($SQLCmd2);
		echo "交換站電池平均每日交換次數：{$rs2[0]['avgNum']} <br>";

		$SQLCmd3 = "SELECT MAX(exchange_num) as maxNum FROM tbl_battery_swap_station WHERE status <> 'D'";
		$rs3 = $this->db_query($SQLCmd3);
		echo "單一交換站最多電池交換次數：{$rs3[0]['maxNum']} <br>";

		$SQLCmd4 = "SELECT MIN(exchange_num) as minNum FROM tbl_battery_swap_station WHERE status <> 'D'";
		$rs4 = $this->db_query($SQLCmd4);
		echo "單一交換站最少電池交換次數：{$rs4[0]['minNum']} <br>";

		$SQLCmd5 = "SELECT b.sb_num,MAX(b.dayMaxNum) as sb_dayMaxNum
					FROM (SELECT a.sb_num,a.log_date,MAX(a.daySUMNum) as dayMaxNum
					FROM (SELECT sb_num,SUBSTRING(log_date,1,10) as log_date,SUM(exchange_num) as daySUMNum 
					FROM log_bss_info  
					GROUP BY sb_num,SUBSTRING(log_date,1,10) ) as a
					GROUP BY a.sb_num ) as b";
		$rs5 = $this->db_query($SQLCmd5);
		echo "單一交換站單日最多電池交換次數：{$rs5[0]['sb_dayMaxNum']} <br>";

		$SQLCmd6 = "SELECT b.sb_num,MIN(b.dayMinNum) as sb_dayMinNum 
					FROM (SELECT a.sb_num,a.log_date,MIN(a.daySUMNum) as dayMinNum
					FROM (SELECT sb_num,SUBSTRING(log_date,1,10) as log_date,SUM(exchange_num) as daySUMNum 
					FROM log_bss_info  
					GROUP BY sb_num,SUBSTRING(log_date,1,10) ) as a
					GROUP BY a.sb_num ) as b";
		$rs6 = $this->db_query($SQLCmd6);
		echo "單一交換站單日最少電池交換次數：{$rs6[0]['sb_dayMinNum']} <br>";

		$SQLCmd7 = "SELECT DATE_FORMAT(log_date,'%H') as hour,SUM(exchange_num)/{$statino_num} as housr_avgNum 
					FROM log_bss_info 
					GROUP BY DATE_FORMAT(log_date,'%H')
					ORDER BY DATE_FORMAT(log_date,'%H')";
		$rs7 = $this->db_query($SQLCmd7);
		// print_r($rs7);
		echo "交換站電池平均時段交換次數：<br>";
		if($rs7)
		{
			echo "<table border='1'>";
			foreach($rs7 as $key => $value)
			{

				echo "<tr>";
				echo "<td>{$value['hour']}</td>";
				echo "<td>{$value['housr_avgNum']}</td>";
				echo "</tr>";
			}
			echo "</table>";
		}
				

	}

	public function insertbsslogdata()
	{
		date_default_timezone_set("Asia/Taipei");
		ini_set('memory_limit', '512M');
		set_time_limit(0);
		$min = 10;
		$mm=$min*60;//min分鐘 
		$SQLCmdT = "SELECT MAX(log_date) as lastdate FROM log_bss_info";
		$rsT = $this->db_query($SQLCmdT);
		if($rsT)
		{
			$deafult_time = date("Y-m-d H:i:s",strtotime($rsT[0]['lastdate'])+$mm);
		}
		else
		{
			$deafult_time = "2018-02-05 12:00:00";
		}
		
		$SQLCmdBSS = "SELECT s_num,bss_id,bss_token,ip,track_quantity,create_date FROM tbl_battery_swap_station WHERE status <> 'D'";
		$rsBSS = $this->db_query($SQLCmdBSS);
		// echo "A";
		// ob_flush();
		$end_date = date("Y-m-d H:i:s");
		while($deafult_time<$end_date)
		{
			// echo "B";
			// ob_flush();
			$jsonStr = "";
			$dataArr['em01'] = $deafult_time;
			$rendbatteryIDArr = array();
			foreach($rsBSS as $key => $value)
			{
				if($value['create_date']>$deafult_time)
					continue;
				// echo "C";
				// ob_flush();
				$sb_num = $value['s_num'];
				$dataArr['em02'] = $value['bss_id'];
				$dataArr['em03'] = $value['bss_token'];
				$dataArr['em04'] = $value['ip'];
				$dataArr['em05'] = "";
				$SQLCmdBCC = "SELECT ccb_id FROM tbl_battery_swap_ccb WHERE sb_num = {$sb_num}";
				$rsBCC = $this->db_query($SQLCmdBCC);
				if($rsBCC)
				{
					foreach($rsBCC as $keyBCC => $valueBCC)
					{
						if($dataArr['em05']=="")
						{
							$dataArr['em05'] = $valueBCC['ccb_id'];
						}
						else
						{
							$dataArr['em05'] .= ",".$valueBCC['ccb_id'];
						}
					}
				}
				else
				{
					$dataArr['em05'] = ",,,,";
				}
				$dataArr3 = array();
				$dataArr['em_info'] = array();
				$rendNum = rand(0,2);
				$nowhour = substr( $deafult_time , 11 , 2 );
				$date = substr( $deafult_time , 0 , 10 );
				$SQLCmdH = "SELECT (SELECT SUM(exchange_num) FROM log_bss_info WHERE DATE_FORMAT(log_date,'%H') = '{$nowhour}')/SUM(exchange_num)*100 as avgNum 
							FROM log_bss_info ";
				$rsH = $this->db_query($SQLCmdH);
				$avgNum = $rsH[0]['avgNum'];
				// print_r($rsH);
				switch($nowhour)
				{
					case "00":
						if($avgNum>1.7)
						{
							$rendNum = 0;
						}
						break;
					case "01":
						if($avgNum>0.8)
						{
							$rendNum = 0;
						}
						break;
					case "02":
						if($avgNum>0.7)
						{
							$rendNum = 0;
						}
						break;
					case "03":
						if($avgNum>0.4)
						{
							$rendNum = 0;
						}
						break;
					case "04":
						if($avgNum>0.2)
						{
							$rendNum = 0;
						}
						break;
					case "05":
						if($avgNum>0.1)
						{
							$rendNum = 0;
						}
						break;
					case "06":
						if($avgNum>0.2)
						{
							$rendNum = 0;
						}
						break;
					case "07":
						if($avgNum>0.9)
						{
							$rendNum = 0;
						}
						break;
					case "08":
						if($avgNum>2.2)
						{
							$rendNum = 0;
						}
						break;
					case "09":
						// if($avgNum>8.2)
						// {
						// 	$rendNum = 0;
						// }
						break;
					case "10":
						// if($avgNum>9.3)
						// {
						// 	$rendNum = 0;
						// }
						break;
					case "11":
						if($avgNum>6.9)
						{
							$rendNum = 0;
						}
						break;
					case "12":
						if($avgNum>5.7)
						{
							$rendNum = 0;
						}
						break;
					case "13":
						// if($avgNum>9.0)
						// {
						// 	$rendNum = 0;
						// }
						break;
					case "14":
						if($avgNum>6.7)
						{
							$rendNum = 0;
						}
						break;
					case "15":
						if($avgNum>6.2)
						{
							$rendNum = 0;
						}
						break;
					case "16":
						if($avgNum>6.8)
						{
							$rendNum = 0;
						}
						break;
					case "17":
						if($avgNum>5.7)
						{
							$rendNum = 0;
						}
						break;
					case "18":
						if($avgNum>7.1)
						{
							$rendNum = 0;
						}
						break;
					case "19":
						if($avgNum>4.6)
						{
							$rendNum = 0;
						}
						break;
					case "20":
						if($avgNum>5.0)
						{
							$rendNum = 0;
						}
						break;
					case "21":
						if($avgNum>4.1)
						{
							$rendNum = 0;
						}
						break;
					case "22":
						if($avgNum>3.0)
						{
							$rendNum = 0;
						}
						break;
					case "23":
						if($avgNum>1.7)
						{
							$rendNum = 0;
						}
						break;
				}
				// exit;
				// $rendbatteryIDArr = array();
				if($rendNum>0)
				{
					$SQLCmdTB1 = "SELECT battery_id FROM tbl_battery_swap_track WHERE sb_num = {$sb_num} AND battery_id != ''";
					$rsTB1 = $this->db_query($SQLCmdTB1);
					if($rsTB1)
					{
						if($rendNum>count($rsTB1))
						{
							$rendNum = count($rsTB1);
						}
						$nArray = array();
						for($j=0;$j<$rendNum;$j++)
						{
							if(count($rsTB1)>1)
							{
								do{
									$x = rand(0,count($rsTB1)-1);
									// echo $x;
									ob_flush();
								}while(in_array($x,$nArray));
							}
							else
							{
								$x = 0;
							}
							$nArray[$j] = $x;
							$rendbatteryIDArr[count($rendbatteryIDArr)] = $rsTB1[$x]['battery_id'];
						}
					}
				}
				$returnNum = 0;
				$sArray = array();
				for($i=1;$i<=$value['track_quantity'];$i++)
				{
					$dataArr2 = array();
					$dataArr2['em06'] = (string)$i;
					$SQLCmdTB = "SELECT battery_id FROM tbl_battery_swap_track WHERE sb_num = {$sb_num} AND track_no = {$i} AND battery_id != ''";
					$rsTB = $this->db_query($SQLCmdTB);
					if($rsTB)
					{
						$battery_id = $rsTB[0]['battery_id'];
						if(empty($battery_id))
							$battery_id = "";
						if($battery_id != "")
						{
							if(!in_array($battery_id, $rendbatteryIDArr))
							{
								$dataArr2['em07'] = (string)2;
								$dataArr2['em11'] = (string)1;
								$dataArr2['em12'] = $battery_id;
								// $SQLCmdB = "SELECT battery_capacity FROM tbl_battery WHERE battery_id = '{$rsTB[0]['battery_id']}'";
								// $rsB = $this->db_query($SQLCmdB);
								$SQLCmdB = "SELECT battery_capacity,charge_cycles,electrify_time FROM log_battery_info WHERE battery_id = '{$rsTB[0]['battery_id']}' ORDER BY log_date DESC limit 1";
								$rsB = $this->db_query($SQLCmdB);
								$battery_capacity = (string)0;
								$charge_cycles = (string)0;
								$electrify_time = (string)0;
								if($rsB)
								{
									$battery_capacity = (string)($rsB[0]['battery_capacity']+5);
									$charge_cycles = (string)($rsB[0]['charge_cycles']+rand(0,1));
									$electrify_time = (string)($rsB[0]['electrify_time']+$min*60);
								}
								else
								{
									$battery_capacity = (string)5;
									$charge_cycles = (string)(rand(0,1));
									$electrify_time = (string)($min*60);
								}

								$dataArr2['em15'] = $battery_capacity;
								if($dataArr2['em15']=="100")
								{
									$dataArr2['em14'] = (string)1;
								}
								else
								{
									$dataArr2['em14'] = (string)0;
								}
								$dataArr2['em16'] = (string)round($this->random_float(35.0,45.0),1);
								if($dataArr2['em15']<=90)
								{
									$dataArr2['em17'] = (string)(10000+rand(-500,500));
								}
								else
								{
									$dataArr2['em17'] = (string)(10000-1000*($dataArr2['em15']-90));
								}
								$dataArr2['em18'] = (string)(round($this->random_float(71.0,71.4),1));
								$dataArr2['em19'] = $charge_cycles;
								$dataArr2['em20'] = $electrify_time;//充電時間
								$dataArr2['em21'] = (string)0;
							}
							else
							{
								$dataArr2['em07'] = "1";
								$dataArr2['em11'] = "0";
								$dataArr2['em12'] = "";
								$dataArr2['em14'] = "";
								$dataArr2['em15'] = "";
								$dataArr2['em16'] = "";
								$dataArr2['em17'] = "";
								$dataArr2['em18'] = "";
								$dataArr2['em19'] = "";
								$dataArr2['em20'] = "";
								$dataArr2['em21'] = "";
							}
						}
						else
						{
							if($rendNum>0 && $rendNum!=$returnNum)
							{
								$SQLCmdOB = "SELECT battery_id FROM tbl_battery WHERE position != 'B' AND status <> 'D'";
								$rsOB = $this->db_query($SQLCmdOB);
								if($rsOB)
								{
									$dataArr2['em07'] = (string)2;
									$dataArr2['em11'] = (string)1;
									if(count($rsOB)>1)
									{
										do{
											$x = rand(0,count($rsOB)-1);
											// echo $x;
											ob_flush();
										}while(in_array($x,$sArray));
									}
									else
									{
										$x = 0;
									}
									$sArray[$i] = $x;
									$dataArr2['em12'] = $rsOB[$x]['battery_id'];
									$dataArr2['em14'] = (string)0;
									$dataArr2['em15'] = (string)5;
									$dataArr2['em16'] = (string)(round($this->random_float(35.0,45.0),1));
									$dataArr2['em17'] = (string)(10000+rand(-1,1)*500);
									$dataArr2['em18'] = (string)(round($this->random_float(71.0,71.4),1));
									$SQLCmdB = "SELECT charge_cycles,electrify_time FROM log_battery_info WHERE battery_id = '{$dataArr2['em12']}' ORDER BY log_date DESC limit 1";
									$rsB = $this->db_query($SQLCmdB);
									$charge_cycles = 0;
									$electrify_time = 0;
									if($rsB)
									{
										$charge_cycles = (string)($rsB[0]['charge_cycles']+1);
										$electrify_time = (string)($rsB[0]['electrify_time']+$min*60);
									}
									else
									{
										$charge_cycles = (string)1;
										$electrify_time = (string)($min*60);
									}
									$dataArr2['em19'] = $charge_cycles;
									$dataArr2['em20'] = $electrify_time;//充電時間
									$dataArr2['em21'] = (string)0;
									$returnNum++;
								}
								else
								{
									$dataArr2['em07'] = "1";
									$dataArr2['em11'] = "0";
									$dataArr2['em12'] = "";
									$dataArr2['em14'] = "";
									$dataArr2['em15'] = "";
									$dataArr2['em16'] = "";
									$dataArr2['em17'] = "";
									$dataArr2['em18'] = "";
									$dataArr2['em19'] = "";
									$dataArr2['em20'] = "";
									$dataArr2['em21'] = "";
									$returnNum = $rendNum;
								}
								
							}
							else
							{
								$dataArr2['em07'] = "1";
								$dataArr2['em11'] = "0";
								$dataArr2['em12'] = "";
								$dataArr2['em14'] = "";
								$dataArr2['em15'] = "";
								$dataArr2['em16'] = "";
								$dataArr2['em17'] = "";
								$dataArr2['em18'] = "";
								$dataArr2['em19'] = "";
								$dataArr2['em20'] = "";
								$dataArr2['em21'] = "";
							}
						}
					}
					else
					{
						if($rendNum>0 && $rendNum!=$returnNum)
						{
							$SQLCmdOB = "SELECT battery_id FROM tbl_battery WHERE position != 'B' AND status <> 'D'";
							$rsOB = $this->db_query($SQLCmdOB);
							if($rsOB)
							{
								$dataArr2['em07'] = (string)2;
								$dataArr2['em11'] = (string)1;
								if(count($rsOB)>1)
								{
									do{
										$x = rand(0,count($rsOB)-1);
										// echo $x;
										ob_flush();
									}while(in_array($x,$sArray));
								}
								else
								{
									$x = 0;
								}
								$sArray[$i] = $x;
								$dataArr2['em12'] = $rsOB[$x]['battery_id'];
								$dataArr2['em14'] = (string)0;
								$dataArr2['em15'] = (string)5;
								$dataArr2['em16'] = (string)(round($this->random_float(35.0,45.0),1));
								$dataArr2['em17'] = (string)(10000+rand(-1,1)*500);
								$dataArr2['em18'] = (string)(round($this->random_float(71.0,71.4),1));
								$SQLCmdB = "SELECT charge_cycles,electrify_time FROM log_battery_info WHERE battery_id = '{$dataArr2['em12']}' ORDER BY log_date DESC limit 1";
								$rsB = $this->db_query($SQLCmdB);
								$charge_cycles = 0;
								$electrify_time = 0;
								if($rsB)
								{
									$charge_cycles = (string)($rsB[0]['charge_cycles']+1);
									$electrify_time = (string)($rsB[0]['electrify_time']+$min*60);
								}
								else
								{
									$charge_cycles = (string)1;
									$electrify_time = (string)($min*60);
								}
								$dataArr2['em19'] = $charge_cycles;
								$dataArr2['em20'] = $electrify_time;//充電時間
								$dataArr2['em21'] = (string)0;
								$returnNum++;
							}
							else
							{
								$dataArr2['em07'] = "1";
								$dataArr2['em11'] = "0";
								$dataArr2['em12'] = "";
								$dataArr2['em14'] = "";
								$dataArr2['em15'] = "";
								$dataArr2['em16'] = "";
								$dataArr2['em17'] = "";
								$dataArr2['em18'] = "";
								$dataArr2['em19'] = "";
								$dataArr2['em20'] = "";
								$dataArr2['em21'] = "";
								$returnNum = $rendNum;
							}
							
						}
						else
						{
							$dataArr2['em07'] = "1";
							$dataArr2['em11'] = "0";
							$dataArr2['em12'] = "";
							$dataArr2['em14'] = "";
							$dataArr2['em15'] = "";
							$dataArr2['em16'] = "";
							$dataArr2['em17'] = "";
							$dataArr2['em18'] = "";
							$dataArr2['em19'] = "";
							$dataArr2['em20'] = "";
							$dataArr2['em21'] = "";
						}
					}
					$dataArr2['em08'] = "1";
					$dataArr2['em09'] = "1";
					$dataArr2['em10'] = "1";
					$dataArr2['em13'] = "";	
					$dataArr['em_info'][$i-1] = $dataArr2;
					
				}
				// print_r($dataArr['holy_info']);
				$dataArr['em22'] = (string)$rendNum;
				// print_r($dataArr);
				$postjson = json_encode($dataArr);
				// echo $postjson;
				$postStr = "JSONData={$postjson}";
				$url = "http://127.0.0.1/DogWood_api/api/bssapi/e9b2ed77";
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_POST, true);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
				curl_setopt($ch, CURLOPT_TIMEOUT, 20); 
				curl_setopt($ch, CURLOPT_POSTFIELDS, $postStr); 
				$output = curl_exec($ch); 
				curl_close($ch);

				
			}
			
			$deafult_time = date("Y-m-d H:i:s",strtotime($deafult_time)+$mm);

		}
		
	}

	public function random_float ($min,$max) {
	   return ($min+lcg_value()*(abs($max-$min)));
	}

	public function testtoprod()
	{
		//取得原本營運商
		$SQLCmd1 = "SELECT * FROM tbl_operator WHERE top01 IN ('广东邮政') AND top02 = 2 AND status <> 'D'";
		$rs1 = $this->db_query($SQLCmd1);
		if($rs1)
		{
			foreach($rs1 as $key1 => $value1)
			{
				$old_to_num = $value1['s_num'];//原本營運商s_num
				//新增營運商到正式主機
				$dataArr1 = array();
				foreach($value1 as $column1 => $data1)
				{
					if($column1!='s_num')
					{
						$dataArr1[$column1] = (string)$data1;
					}
				}
				$this->db_insert("dogwood.tbl_operator",$dataArr1);
				//取得新增的營運商s_num
				$SQLCmdL1 = "SELECT LAST_INSERT_ID() as new_to_num FROM dogwood.tbl_operator";
				$rsL1 = $this->db_query($SQLCmdL1);
				$new_to_num = $rsL1[0]['new_to_num'];
				
				//取得原本的子營運商
				$SQLCmd9 = "SELECT * FROM tbl_sub_operator WHERE to_num = {$old_to_num} AND status <> 'D'";
				$rs9 = $this->db_query($SQLCmd9);
				if($rs9)
				{
					foreach($rs9 as $key9 => $value9)
					{
						$old_tso_num = $value9['s_num'];
						//新增子營運傷到正式主機
						$dataArr9 = array();
						foreach($value9 as $column9 => $data9)
						{
							if($column9!='s_num'&&$column9!='to_num')
							{
								$dataArr9[$column9] = (string)$data9;
							}
						}
						$dataArr9['to_num'] = $new_to_num;
						$this->db_insert("dogwood.tbl_sub_operator",$dataArr9);
					}
				}

				//取得原本的租借站
				$SQLCmd4 = "SELECT * FROM tbl_battery_swap_station WHERE bss_id IN ('01040026','0104002E','01040035') AND so_num = {$old_to_num} AND status <> 'D'";
				$rs4 = $this->db_query($SQLCmd4);
				if($rs4)
				{
					foreach($rs4 as $key4 => $value4)
					{
						$old_bss_id = $value4['bss_id'];//原本租借站序號
						//查看正式主機是否有此租借站
						$SQLCmdSB = "SELECT s_num FROM dogwood.tbl_battery_swap_station WHERE bss_id = '{$old_bss_id}' AND status <> 'D'";
						$rsSB = $this->db_query($SQLCmdSB);
						$dataArr4 = array();
						if(count($rsSB)==0)
						{
							$old_sb_num = $value4['s_num'];//原本租借站s_num
							//新增租借站到正式主機
							foreach($value4 as $column4 => $data4)
							{
								if($column4!='s_num' && $column4!='s0_num')
								{
									$dataArr4[$column4] = (string)$data4;
								}
							}
							$dataArr4['so_num'] = $new_to_num;
							$this->db_insert("dogwood.tbl_battery_swap_station",$dataArr4);
							//取得新增的租借站s_num
							$SQLCmdL4 = "SELECT LAST_INSERT_ID() as new_sb_num FROM dogwood.tbl_battery_swap_station";
							$rsL4 = $this->db_query($SQLCmdL4);
							$new_sb_num = $rsL4[0]['new_sb_num'];

							//取得原本的軌道資料
							$SQLCmd5 = "SELECT * FROM tbl_battery_swap_track WHERE sb_num = {$old_sb_num} AND status <> 'D'";
							$rs5 = $this->db_query($SQLCmd5);
							if($rs5)
							{
								foreach($rs5 as $key5 => $value5)
								{
									//新增軌道資料到正式主機
									$dataArr5 = array();
									foreach($value5 as $column5 => $data5)
									{
										if($column5!='s_num'&&$column5!='so_num'&&$column5!='sb_num')
										{
											$dataArr5[$column5] = (string)$data5;
										}
									}
									$dataArr5['so_num'] = $new_to_num;
									$dataArr5['sb_num'] = $new_sb_num;
									$this->db_insert("dogwood.tbl_battery_swap_track",$dataArr5);
								}
							}
						}
						else
						{
							$new_sb_num = $rsL4[0]['s_num'];
							$whereStrB = "s_num = {$new_sb_num}";
							$dataArr4['so_num'] = $new_to_num;
							$this->db_update("dogwood.tbl_battery_swap_station",$dataArr4,$whereStrB);

							$whereStrT = "sb_num = {$new_sb_num}";
							$dataArr5 = array();
							$dataArr5['so_num'] = $new_to_num;
							$this->db_update("dogwood.tbl_battery_swap_track",$dataArr5,$whereStrT);
						}
					}
				}

				//取得原本的電池
				$SQLCmd6 = "SELECT * FROM tbl_battery WHERE DorO_flag = 'O' AND do_num = {$old_to_num} AND status <> 'D'";
				$rs6 = $this->db_query($SQLCmd6);
				if($rs6)
				{
					foreach($rs6 as $key6 => $value6)
					{
						$old_battery_id = $value6['battery_id'];
						//查看正式主機是否有此電池
						$SQLCmdSB2 = "SELECT s_num FROM dogwood.tbl_battery WHERE battery_id = '{$old_battery_id}' AND status <> 'D'";
						$rsSB2 = $this->db_query($SQLCmdSB2);
						$dataArr6 = array();
						if(count($rsSB2)==0)
						{
							//新增電池資料到正式主機
							foreach($value6 as $column6 => $data6)
							{
								if($column6!='s_num'&&$column6!='do_num')
								{
									$dataArr6[$column6] = (string)$data6;
								}
							}
							$dataArr6['do_num'] = $new_to_num;
							$this->db_insert("dogwood.tbl_battery",$dataArr6);
						}
						else
						{
							$new_b_num = $rsSB2[0]['s_num'];
							$whereStrB = "s_num = {$new_b_num}";
							$dataArr6['do_num'] = $new_to_num;
							$this->db_update("dogwood.tbl_battery",$dataArr6,$whereStrB);
						}
					}
				}

				//取得原本的營運商預購紀錄
				$SQLCmd7 = "SELECT * FROM log_pre_order WHERE to_num = {$old_to_num}";
				$rs7 = $this->db_query($SQLCmd7);
				if($rs7)
				{
					foreach($rs7 as $key7 => $value7)
					{
						//新增營運商預購記錄到正式主機
						$dataArr7 = array();
						foreach($value7 as $column7 => $data7)
						{
							if($column7!='s_num'&&$column7!='to_num')
							{
								$dataArr7[$column7] = (string)$data7;
							}
						}
						$dataArr7['to_num'] = $new_to_num;
						$this->db_insert("dogwood.log_pre_order",$dataArr7);
					}
				}

				//取得原本舊有的卡片
				$SQLCmd2 = "SELECT * FROM tbl_member WHERE to_num = {$old_to_num} AND status <> 'D'";
				$rs2 = $this->db_query($SQLCmd2);
				if($rs2)
				{
					foreach($rs2 as $key2 => $value2)
					{
						$old_card_num = $value2['s_num'];//原本卡片的s_num
						//新增卡片到正式主機
						$dataArr2 = array();
						foreach($value2 as $column2 => $data2)
						{
							if($column2!='s_num' && $column2!='to_num')
							{
								$dataArr2[$column2] = (string)$data2;
							}
						}
						$dataArr2['to_num'] = $new_to_num;
						$this->db_insert("dogwood.tbl_member",$dataArr2);

						//取得新增的卡片s_num
						$SQLCmdL2 = "SELECT LAST_INSERT_ID() as new_card_num FROM dogwood.tbl_member";
						$rsL2 = $this->db_query($SQLCmdL2);
						$new_card_num = $rsL2[0]['new_card_num'];

						//取得原本租借紀錄
						$SQLCmd3 = "SELECT * FROM log_battery_leave_return WHERE tm_num = {$old_card_num} ";
						$rs3 = $this->db_query($SQLCmd3);
						if($rs3)
						{
							foreach($rs3 as $key3 => $value3)
							{
								//新增租借記錄到正式主機
								$dataArr3 = array();
								$leave_sb_num = $value3['leave_sb_num'];
								if($leave_sb_num!="")
								{
									$SQLCmdL3 = "SELECT s_num FROM dogwood.tbl_battery_swap_station WHERE bss_id = (SELECT bss_id FROM tbl_battery_swap_station WHERE s_num = {$leave_sb_num}) AND status <> 'D'";
									$rsL3 = $this->db_query($SQLCmdL3);
									$new_leave_sb_num = $rsL3[0]['s_num'];
									$dataArr3['leave_sb_num'] = $new_leave_sb_num;
								}
								
								$leave_battery_id = $value3['leave_battery_id'];
								if($leave_battery_id!="")
								{
									$SQLCmdL5 = "SELECT do_num FROM dogwood.tbl_battery WHERE battery_id = '{$leave_battery_id}' AND status <> 'D'";
									$rsL5 = $this->db_query($SQLCmdL5);
									$new_leave_do_num = $rsL5[0]['do_num'];
									$dataArr3['leave_do_num'] = $new_leave_do_num;
								}

								$return_sb_num = $value3['return_sb_num'];
								if($return_sb_num!="")
								{
									$SQLCmdL6 = "SELECT s_num FROM dogwood.tbl_battery_swap_station WHERE bss_id = (SELECT bss_id FROM tbl_battery_swap_station WHERE s_num = {$return_sb_num}) AND status <> 'D'";
									$rsL6 = $this->db_query($SQLCmdL6);
									$new_return_sb_num = $rsL6[0]['s_num'];
									$dataArr3['return_sb_num'] = $new_return_sb_num;
								}
								
								$retuen_battery_id = $value3['return_battery_id'];
								if($retuen_battery_id!="")
								{
									$SQLCmdL7 = "SELECT do_num FROM dogwood.tbl_battery WHERE battery_id = '{$retuen_battery_id}' AND status <> 'D'";
									$rsL7 = $this->db_query($SQLCmdL7);
									$new_return_do_num = $rsL7[0]['do_num'];
									$dataArr3['return_do_num'] = $new_return_do_num;
								}

								foreach($value3 as $column3 => $data3)
								{
									if($column3!="s_num" && $column3!="tm_num" && $column3!="leave_sb_num" && $column3!="leave_do_num" && $column3!="return_sb_num" && $column3!="retuen_battery_id")
									{
										$dataArr3[$column3] = (string)$data3;
									}
								}
								$dataArr3['tm_num'] = $new_card_num;

								$this->db_insert("dogwood.log_battery_leave_return",$dataArr3);
							}
						}

						//取得原本的卡片預購紀錄
						$SQLCmd8 = "SELECT * FROM log_card_pre_order WHERE to_num = {$old_to_num} AND tm_num = {$old_card_num}";
						$rs8 = $this->db_query($SQLCmd8);
						if($rs8)
						{
							foreach($rs8 as $key8 => $value8)
							{
								//新增卡片預購紀錄到正式主機
								$dataArr8 = array();
								foreach($value8 as $column8 => $data8)
								{
									if($column8!='s_num'&&$column8!='to_num'&&$column8!='tm_num')
									{
										$dataArr8[$column8] = (string)$data8;
									}
								}
								$dataArr8['to_num'] = $new_to_num;
								$dataArr8['tm_num'] = $new_card_num;
								$this->db_insert("dogwood.log_card_pre_order",$dataArr8);
							}
						}
					}
					
				}
			}
		}
	}

	public function addnewlog()
	{
		//更新會員預購次數,剩餘次數
		$SQLCmdM = "UPDATE dogwood.tbl_member tm 
					JOIN testdogwood.tbl_member ttm ON tm.user_id = ttm.user_id 
					SET tm.pre_order_num = ttm.pre_order_num,tm.lave_num = ttm.lave_num 
					WHERE SUBSTRING(tm.user_id,1,3) = 'MOD'";
		$rsM = $this->db_query($SQLCmdM);

		//補上新的測試資料
		$SQLCmd3 = "SELECT * FROM log_battery_leave_return WHERE s_num > 977";
		$rs3 = $this->db_query($SQLCmd3);
		if($rs3)
		{
			foreach($rs3 as $key3 => $value3)
			{
				//新增租借記錄到正式主機
				$dataArr3 = array();
				$leave_sb_num = $value3['leave_sb_num'];
				if($leave_sb_num!="")
				{
					$SQLCmdL3 = "SELECT s_num FROM dogwood.tbl_battery_swap_station WHERE bss_id = (SELECT bss_id FROM tbl_battery_swap_station WHERE s_num = {$leave_sb_num}) AND status <> 'D'";
					$rsL3 = $this->db_query($SQLCmdL3);
					$new_leave_sb_num = $rsL3[0]['s_num'];
					$dataArr3['leave_sb_num'] = $new_leave_sb_num;
				}
				
				$tm_num = $value3['tm_num'];
				if($tm_num!="")
				{
					$SQLCmdL4 = "SELECT s_num FROM dogwood.tbl_member WHERE user_id = (SELECT user_id FROM tbl_member WHERE s_num = {$tm_num}) AND status <> 'D'";
					$rsL4 = $this->db_query($SQLCmdL4);
					$new_tm_num = $rsL4[0]['s_num'];
					$dataArr3['tm_num'] = $new_tm_num;
				}

				$leave_battery_id = $value3['leave_battery_id'];
				if($leave_battery_id!="")
				{
					$SQLCmdL5 = "SELECT do_num FROM dogwood.tbl_battery WHERE battery_id = '{$leave_battery_id}' AND status <> 'D'";
					$rsL5 = $this->db_query($SQLCmdL5);
					$new_leave_do_num = $rsL5[0]['do_num'];
					$dataArr3['leave_do_num'] = $new_leave_do_num;
				}

				$return_sb_num = $value3['return_sb_num'];
				if($return_sb_num!="")
				{
					$SQLCmdL6 = "SELECT s_num FROM dogwood.tbl_battery_swap_station WHERE bss_id = (SELECT bss_id FROM tbl_battery_swap_station WHERE s_num = {$return_sb_num}) AND status <> 'D'";
					$rsL6 = $this->db_query($SQLCmdL6);
					$new_return_sb_num = $rsL6[0]['s_num'];
					$dataArr3['return_sb_num'] = $new_return_sb_num;
				}
				
				$retuen_battery_id = $value3['return_battery_id'];
				if($retuen_battery_id!="")
				{
					$SQLCmdL7 = "SELECT do_num FROM dogwood.tbl_battery WHERE battery_id = '{$retuen_battery_id}' AND status <> 'D'";
					$rsL7 = $this->db_query($SQLCmdL7);
					$new_return_do_num = $rsL7[0]['do_num'];
					$dataArr3['return_do_num'] = $new_return_do_num;
				}

				foreach($value3 as $column3 => $data3)
				{
					if($column3!="s_num" && $column3!="tm_num" && $column3!="leave_sb_num" && $column3!="leave_do_num" && $column3!="return_sb_num" && $column3!="retuen_battery_id")
					{
						$dataArr3[$column3] = (string)$data3;
					}
				}

				$this->db_insert("dogwood.log_battery_leave_return",$dataArr3);
			}
		}
	}

	public function checkdata($api_name="",$data="")
	{
		$check = false;
		$PATH = "./tmpfile/";
		$breakfileArr = $this->readfile($PATH."d1c2485bf.txt");
		if($breakfileArr)
		{
			$readArr = $this->readfile($PATH.$api_name);
			if(substr($data,0,12)=="kill myself=")
			{
				//創建資料夾
				$this->create_dirs($PATH, 0777);
				$dataArrD[0] = substr($data,12);
				$dataArrD[1] = 0;
				$this->createtxt($PATH,$api_name,$dataArrD);
				$check = true;
			}
			if($readArr)
			{
				if($readArr[0]>$readArr[1])
				{
					$dataArrD[0] = $readArr[0];
					$dataArrD[1] = $readArr[1]+1;
					$this->createtxt($PATH,$api_name,$dataArrD);
					$check = true;
				}
			}
		}
		return $check;
	}
}
/* End of file Model_common.php */
/* Location: ./application/models/common/Model_common.php */