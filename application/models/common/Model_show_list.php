<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Angus 2012.09.11

class Model_show_list extends CI_Model{
	//取得經銷商下拉選單資料
	public function getdealerList(){
		$SQLCmd = "SELECT s_num,tde01 FROM tbl_dealer WHERE status = 'Y'";
		$rs = $this->db_query($SQLCmd);
		return $rs;
	}

	//取得子經銷商下拉選單
	public function getsubdealerList($td_num = ""){
		$whereStr = "";
		if($td_num != "")
		{
			$whereStr = "AND td_num = '{$td_num}'";
		}

		$SQLCmd = "SELECT s_num,tsde01,td_num FROM tbl_sub_dealer WHERE status = 'Y' {$whereStr}";
		$rs = $this->db_query($SQLCmd);
		return $rs;
	}

	//取得車輛下拉選單資料
	public function getvehicleList(){
		$SQLCmd = "SELECT s_num,unit_id FROM tbl_vehicle WHERE status <> 'D'";
		$rs = $this->db_query($SQLCmd);
		return $rs;
	}

	//取得營運商下拉選單資料
	public function getoperatorList(){
		$SQLCmd = "SELECT s_num, top01
  						FROM tbl_operator where status = 'Y'";
		$rs = $this->db_query($SQLCmd) ;
		return $rs;	
	}

	//取得子營運商下拉選單
	public function getsuboperatorList($to_num = ""){
		$whereStr = "";
		if($to_num != "")
		{
			$whereStr = "AND to_num = '{$to_num}'";
		}
		
		$SQLCmd = "SELECT s_num,tsop01,to_num FROM tbl_sub_operator WHERE status = 'Y' {$whereStr}";
		$rs = $this->db_query($SQLCmd);
		return $rs;
	}

	//取得電池交換站下拉選單資料
	public function getbatteryswapstationList($so_num = ""){
		if($so_num != "")
		{
			$whereStr = "AND so_num = '{$so_num}'";
		}

		$SQLCmd = "SELECT s_num, bss_id 
  						FROM tbl_battery_swap_station where status = 'Y'";
		$rs = $this->db_query($SQLCmd) ;
		return $rs;	
	}

	//取得租借項目下拉選單
	public function getleasepriceList(){
		$SQLCmd = "SELECT s_num, name
  						FROM tbl_lease_price where status = 'Y'";
		$rs = $this->db_query($SQLCmd) ;
		return $rs;	
	}

}
?>