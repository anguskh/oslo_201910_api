<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_background extends MY_Model {
	
	//取目前使用的資料庫名稱
	public function getDefDBname(){
		return $this->getDBname();
	}
	
	public function displayLanguage(){
		$SQLCmd = " SELECT config_set FROM sys_config 
					where config_code='display_language' " ;
		$rs = $this->db_query($SQLCmd) ;
		if(count($rs) > 0){	
			return $rs[0]['config_set'];
		}else{	//找不到直接給予預設
			return 'zh_tw';
		}
	}
	
}
/* End of file model_background.php */
/* Location: ./application/models/common/model_background.php */
