<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_exception extends MY_Model {
	
	//取目前使用的资料库名称
	public function getDefDBname(){
		return $this->getDBname();
	}
	
	//解除电池绑定
	public function return_battery(){
		//計算執行時間
		$time_start = microtime(true);
		//寫入收到的資料到歷史紀錄
		$receive_data = $this->input->post();
		$api_name = 'd8bdf6c3';
		$api_chinese_name = '解除电池绑定';
		$api_log_sn = $this->model_common->insert_api_log($receive_data,$api_name,$api_chinese_name);

		$postjson = $this->input->post("JSONData");
		//先判斷是否為Json格式
		if($this->is_json($postjson))
		{
			$returnArr = array();
			//收到空資料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//資料為空
				$returnArr["rt_msg"] = "資料為空";//回應訊息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$return_date = addslashes($data_json_de['br01']);//借電站電池歸還日期
				$bss_id = addslashes($data_json_de['br02']);//借電站序號
				if(trim($data_json_de['br03'])!="")
					$User_ID = str_pad(addslashes($data_json_de['br03']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
				else
					$User_ID = "";
				$trace_no = addslashes($data_json_de['br04']);//歸還電池的軌道編號
				$battery_id = addslashes($data_json_de['br05']);//電池序號
				$otherdataArr['user_id'] = $User_ID;
				$otherdataArr['battery_id'] = $battery_id;
				// $SQLCmd = "SELECT DorO_flag,do_num FROM tbl_battery WHERE battery_id = '{$battery_id}' AND status <> 'D'";
				// $rs = $this->db_query($SQLCmd);
				// // echo $SQLCmd;
				// if($rs)
				// {
				// 	$dataArr['return_DorO_flag'] = $rs[0]['DorO_flag'];
				// 	$dataArr['return_do_num'] = $rs[0]['do_num'];
				// }
				// else
				// {
				// 	$returnArr["rt_cd"] = "0004";//無法對應到電池資料
				// 	$returnArr["rt_msg"] = "無法對應到電池資料";
				// 	echo json_encode($returnArr);
				// 	$time_end = microtime(true);
				// 	$time = $time_end - $time_start;
				// 	$otherdataArr['sys_spend_time'] = $time;
				// 	$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
				// 	return;
				// }

				$whereStrL = " AND lblr.leave_battery_id = '{$battery_id}'";
				$leave_date = "";
				if($User_ID!="")
				{
					$SQLCmdL = "SELECT lblr.s_num as lblr_num,lblr.system_log_date,lblr.leave_date,lblr.battery_user_id,lblr.leave_sb_num,lblr.return_date,lblr.return_track_no,lblr.return_battery_id,lblr.return_status,lblr.usage_time  
								FROM log_battery_leave_return lblr 
								WHERE lblr.battery_user_id = '{$User_ID}' 
								AND lblr.leave_date IS NOT NULL 
								{$whereStrL} 
								ORDER BY lblr.s_num DESC 
								limit 1";
				}
				else
				{
					$SQLCmdL = "SELECT lblr.s_num as lblr_num,lblr.system_log_date,lblr.leave_date,lblr.battery_user_id,lblr.leave_sb_num,lblr.return_date,lblr.return_track_no,lblr.return_battery_id,lblr.return_status,lblr.usage_time  
								FROM log_battery_leave_return lblr 
								WHERE lblr.leave_date IS NOT NULL 
								{$whereStrL} 
								ORDER BY lblr.s_num DESC 
								limit 1";
				}

				$rsL = $this->db_query($SQLCmdL);
				if($rsL)
				{
					if($rsL[0]['return_date']!="")
					{
						if($rsL[0]['return_date']>$rsL[0]['system_log_date'])
						{
							if($rsL[0]['return_battery_id']==$battery_id)
							{
								// $SQLCmdL = "SELECT lblr.s_num as lblr_num,lblr.system_log_date,lblr.leave_date,lblr.battery_user_id,lblr.leave_sb_num,lblr.return_date,lblr.return_track_no,lblr.return_battery_id,lblr.return_status,lblr.usage_time  
								// 			FROM log_battery_leave_return lblr 
								// 			WHERE lblr.leave_date IS NOT NULL 
								// 			{$whereStrL} 
								// 			AND lblr.battery_user_id = '{$User_ID}' 
								// 			ORDER BY lblr.s_num DESC 
								// 			limit 1";
								// $rsL = $this->db_query($SQLCmdL);
								// if($rsL)
								// {
								// 	if($rsL[0]['return_date']!="" && $rsL[0]['return_date']>$rsL[0]['system_log_date'] && $rsL[0]['return_battery_id']==$battery_id)
								// 	{
								if($rsL[0]['return_status']==0 || $rsL[0]['return_status']=='A' || $rsL[0]['return_status']=='W' || $rsL[0]['return_status']=='F')
								{
									$returnArr["rt_cd"] = "0010";//無須解除綁定
									$returnArr["rt_msg"] = "無須解除綁定";
									echo json_encode($returnArr);
									$time_end = microtime(true);
									$time = $time_end - $time_start;
									$otherdataArr['sys_spend_time'] = $time;
									$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
									return;
								}		
									// }
								// }
							}
						}
						
					}
					$User_ID = $rsL[0]['battery_user_id'];
					$leave_date = $rsL[0]['leave_date'];
				}
				else
				{
					// if($rsL)
					// {
						
					// }
					// else
					// {
						$returnArr["rt_cd"] = "0010";//無須解除綁定
						$returnArr["rt_msg"] = "無須解除綁定";
						echo json_encode($returnArr);
						$time_end = microtime(true);
						$time = $time_end - $time_start;
						$otherdataArr['sys_spend_time'] = $time;
						$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
						return;
					// }
					// $returnArr["rt_cd"] = "0005";//無法對應到電池資料
					// $returnArr["rt_msg"] = "無法取得租借紀錄";
					// echo json_encode($returnArr);
					// $time_end = microtime(true);
					// $time = $time_end - $time_start;
					// $otherdataArr['sys_spend_time'] = $time;
					// $this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
					// return;
				}

				if($bss_id!="")
				{
					$SQLCmdB = "SELECT s_num,location FROM tbl_battery_swap_station WHERE bss_id = '{$bss_id}' AND status = 'Y'";
					$rsB = $this->db_query($SQLCmdB);
					if($rsB)
					{
						$dataArr['return_sb_num'] = $rsB[0]['s_num'];
						$battery_swap_location = $rsB[0]['location'];
					}
					else
					{

						$returnArr["rt_cd"] = "0007";//借电站序號有误
						$returnArr["rt_msg"] = "借电站序號有误";
						echo json_encode($returnArr);
						$time_end = microtime(true);
						$time = $time_end - $time_start;
						$otherdataArr['sys_spend_time'] = $time;
						$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
						return;
					}
				}
				else
				{
					$dataArr['return_sb_num'] = $rsL[0]['leave_sb_num'];
					// $returnArr["rt_cd"] = "0007";//借电站序號有误
					// $returnArr["rt_msg"] = "借电站序號有误";
					// echo json_encode($returnArr);
					// $time_end = microtime(true);
					// $time = $time_end - $time_start;
					// $otherdataArr['sys_spend_time'] = $time;
					// $this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
					// return;
				}

				$usage_time = $this->minDiff($leave_date,$return_date);//使用時間
				// $charge_amount = ceil($usage_time/30)*2.5;//扣款金額

				$SQLCmdM = "SELECT * FROM tbl_member WHERE user_id = '{$User_ID}'";
				$rsM = $this->db_query($SQLCmdM);
				if($rsM)
				{
					$dataArrM = array();

					//更新會員儲值餘額欄位
					// $whereStrM = "s_num = {$rsM[0]['s_num']}";
					
					// $dataArrM['lease_status'] = 'N';
					// $this->db_update('tbl_member',$dataArrM,$whereStrM);

					$whereStrB = "battery_id = '{$battery_id}' AND status <> 'D'";
					$dataArrB['sv_num'] = NULL;
					$dataArrB['position'] = 'O';
					$dataArrB['locked_flag'] = '0';
					$this->db_update('tbl_battery',$dataArrB,$whereStrB);
				}
				else
				{
					// $dataArrM = array();

					// //更新會員儲值餘額欄位
					// $whereStrM = "s_num = {$rsM[0]['s_num']}";
					
					// $dataArrM['lease_status'] = 'N';
					// $this->db_update('tbl_member',$dataArrM,$whereStrM);

					// $whereStrB = "battery_id = '{$battery_id}' AND status <> 'D'";
					// $dataArrB['sv_num'] = NULL;
					// $dataArrB['position'] = 'B';
					// $this->db_update('tbl_battery',$dataArrB,$whereStrB);

					$returnArr["rt_cd"] = "0006";//更新失敗
					$returnArr["rt_msg"] = "会员ID有误";//
					echo json_encode($returnArr);
					$time_end = microtime(true);
					$time = $time_end - $time_start;
					$otherdataArr['sys_spend_time'] = $time;
					$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
					return;
				}
				

				$dataArr['return_request_date'] = $return_date;
				$dataArr['return_date'] = $return_date;
				$dataArr['return_track_no'] = $trace_no;
				$dataArr['return_battery_id'] = $battery_id;
				$dataArr['usage_time'] = $this->shi_jian_cha($return_date,$leave_date);
				$dataArr['return_status'] = "A";//使用wechat APP歸還
				// $dataArr['charge_amount'] = $charge_amount;
				// if($rsL[0]['return_date'] && $rsL[0]['return_date']>$rsL[0]['system_log_date'] && $rsL[0]['return_battery_id']==$battery_id && $User_ID !="")
				// 	$whereStr = "s_num = {$rsL[0]['lblr_num']}";
				// else
					$whereStr = "s_num = {$rsL[0]['lblr_num']}";

				//更新電池租借歸還記錄表
				if($this->db_update('log_battery_leave_return',$dataArr,$whereStr))
				{
					//寫入APP歸還紀錄
					$dataArrL['lblr_num'] = $rsL[0]['lblr_num'];
					$dataArrL['return_before'] = "return_date = ".$rsL[0]['return_date'].", return_track_no = ".$rsL[0]['return_track_no'].", return_battery_id = ".$rsL[0]['return_battery_id'].", usage_time = ".$rsL[0]['usage_time'].", return_status = ".$rsL[0]['return_status'];
					$dataArrL['return_after'] = "return_date = ".$return_date.", return_track_no = ".$trace_no.", return_battery_id = ".$battery_id.", usage_time = ".$dataArr['usage_time'].", return_status = A";
					$dataArrL['retuen_user_id'] = $User_ID;
					$this->db_insert('log_app_return_battery',$dataArrL);

					$returnArr["rt_cd"] = "0000";//回應碼
					$returnArr["rt_msg"] = "成功";//回應訊息
					// $returnArr["rt_01"] = $leave_date;//電池租借日期
					// $returnArr["rt_02"] = $return_date;//電池歸還日期
					// $returnArr["rt_03"] = $dataArr['usage_time'];//此次租借時間
				}
				else
				{
					$returnArr["rt_cd"] = "0002";//更新失敗
					$returnArr["rt_msg"] = "更新失敗";//更新失敗
				}

			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式錯誤
			$returnArr["rt_msg"] = "格式錯誤";//格式錯誤
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//計算時間差
	public function minDiff($startTime, $endTime) {
	    $start = strtotime($startTime);
	    $end = strtotime($endTime);
	    $timeDiff = $end - $start;
	    return ceil($timeDiff / 60);
	    // return floor($timeDiff / 60);
	}

	//計算時間差
	public function shi_jian_cha($d,$d1)
	{ 
		$time = strtotime($d) - strtotime($d1); 
		$n_time = str_pad(floor($time%(24*3600)/3600),2,0,STR_PAD_LEFT ).":".str_pad(floor($time%3600/60),2,0,STR_PAD_LEFT).":".str_pad($time%3600%60, 2, 0, STR_PAD_LEFT)."秒";
		return $n_time;
	}
}
/* End of file Model_exception.php */
/* Location: ./application/models/wechat/Model_exception.php */
