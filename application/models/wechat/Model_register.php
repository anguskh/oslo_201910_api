<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// edit by Jaff 2012.09.11

class Model_register extends MY_Model {
	
	//取目前使用的资料库名称
	public function getDefDBname(){
		return $this->getDBname();
	}
	
	public function insertDB(){
		$postdata = $this->input->post("postdata");
		
		/**
		 * 新增资料库
		 */
		$dataArr = array();
		foreach($postdata as $key => $value)
		{
			switch($key){
				default:
					$dataArr[$key] = $value;
					break;
			}
		}
		$card_expired_month = $this->input->post("card_expired_month");
		$card_expired_year = $this->input->post("card_expired_year");
		$dataArr['card_expired_date'] = $card_expired_year.'-'.$card_expired_month;

		$account_no1 = $this->input->post("card_no1");
		$account_no2 = $this->input->post("card_no2");
		$account_no3 = $this->input->post("card_no3");
		$account_no4 = $this->input->post("card_no4");
		$dataArr['account_no'] = $account_no1.$account_no2.$account_no3.$account_no4;
		//加密设定
		$encryption_arr = array('personal_id', 'birthday', 'account_no', 'card_expired_date', 'card_cvv');
		$key = md5('ebike'.$dataArr['mobile']);
		$key = substr($key, 0, 24);
		for($i=0; $i<count($encryption_arr); $i++){
			if($dataArr[$encryption_arr[$i]]  != ''){
				$string = $dataArr[$encryption_arr[$i]] ;
				//加密
				$dataArr[$encryption_arr[$i]]  = $this->encrypt($key, $string);
			}
		}
		
		$SQLCmd = "SELECT count(*) cnt FROM tbl_member WHERE mobile = '{$dataArr['mobile']}' AND status <> 'D'";
		$rs = $this->db_query($SQLCmd);
		if($rs)
		{
			if($rs[0]['cnt'] > 0)
			{
				$this->redirect_alert("../login", "您已经注册过会员,请利用重新登入来登入您的会员帐号") ;
				return;
			}
		}
		//处理图片
		$img_array = array('pic_id_front','pic_driver_front');
		for($i=0; $i<count($img_array); $i++){
			if(isset($_FILES[$img_array[$i]]['tmp_name']) && $_FILES[$img_array[$i]]['tmp_name']!=''){
				$im = $_FILES[$img_array[$i]]['tmp_name']; //上传图片资源
		        $maxwidth="275"; //设置图片的最大宽度
		        $maxheight="175"; //设置图片的最大高度
		        $filetype=$_FILES[$img_array[$i]]["type"];//图片类型
		        $result = $this->thumbImage($im,$maxwidth,$maxheight,$filetype);
				$file = fopen($result, "rb");
				// 读入图片档资料
				$fileContents = fread($file, filesize($result)); 
				//关闭图片档
				fclose($file);
				// 图片档案资料编码
				$dataArr[$img_array[$i]]  =$fileContents;
			}
		}
		$dataArr['to_num'] = 1;
		$dataArr['tso_num'] = 1;
		$dataArr['pay_type'] = 1;
		$dataArr['lease_status'] = 'N';
		$dataArr['status'] = 'R';
		$dataArr['create_date'] = "now()";
		$dataArr['create_ip'] = $this->input->ip_address();

		if($this->db_insert('tbl_member',$dataArr))
		{
			return $dataArr['personal_id'];
		}
		else
		{
			return "";
		}
	}
	
	public function insertDB1(){
		$postdata = $this->input->post("postdata");

		/**
		 * 新增资料库
		 */
		$dataArr = array();
		foreach($postdata as $key => $value)
		{
			switch($key){
				default:
					$dataArr[$key] = $value;
					break;
			}
		}

		$dataArr['personal_id'] = strtoupper($dataArr['personal_id']);
		// $card_expired_month = $this->input->post("card_expired_month");
		// $card_expired_year = $this->input->post("card_expired_year");
		// $dataArr['card_expired_date'] = $card_expired_year.'-'.$card_expired_month;

		// $account_no1 = $this->input->post("card_no1");
		// $account_no2 = $this->input->post("card_no2");
		// $account_no3 = $this->input->post("card_no3");
		// $account_no4 = $this->input->post("card_no4");
		// $dataArr['account_no'] = $account_no1.$account_no2.$account_no3.$account_no4;
		//加密设定
		$encryption_arr = array('personal_id', 'birthday');
		$key = md5('ebike'.$dataArr['mobile']);
		$key = substr($key, 0, 24);
		for($i=0; $i<count($encryption_arr); $i++){
			if($dataArr[$encryption_arr[$i]]  != ''){
				$string = $dataArr[$encryption_arr[$i]] ;
				//加密
				$dataArr[$encryption_arr[$i]]  = $this->encrypt($key, $string);
			}
		}
		
		$SQLCmd = "SELECT count(*) cnt FROM tbl_member WHERE mobile = '{$dataArr['mobile']}' AND status <> 'D'";
		$rs = $this->db_query($SQLCmd);
		if($rs)
		{
			if($rs[0]['cnt'] > 0)
			{
				echo "N";
				exit;
			}
		}
		// //处理图片
		$img_array = array('pic_id_front','pic_driver_front');
		for($i=0; $i<count($img_array); $i++){
			if(isset($_FILES[$img_array[$i]]['tmp_name']) && $_FILES[$img_array[$i]]['tmp_name']!=''){
			$file = fopen($_FILES[$img_array[$i]]["tmp_name"], "rb");
			// 读入图片档资料
			$fileContents = fread($file, filesize($_FILES[$img_array[$i]]["tmp_name"])); 
			//关闭图片档
			fclose($file);
			// 图片档案资料编码
			$dataArr[$img_array[$i]]  =$fileContents;
				// $im = $_FILES[$img_array[$i]]['tmp_name']; //上传图片资源
		  //       $maxwidth="275"; //设置图片的最大宽度
		  //       $maxheight="175"; //设置图片的最大高度
		  //       $filetype=$_FILES[$img_array[$i]]["type"];//图片类型
		  //       $result = $this->thumbImage($im,$maxwidth,$maxheight,$filetype);
				// $file = fopen($result, "rb");
				// // 读入图片档资料
				// $fileContents = fread($file, filesize($result)); 
				// //关闭图片档
				// fclose($file);
				// // 图片档案资料编码
				// $dataArr[$img_array[$i]]  =$fileContents;
			}
		}

		$dataArr['member_register'] = 'P';
		$dataArr['to_num'] = 1;
		$dataArr['tso_num'] = 1;
		$dataArr['pay_type'] = 'W';
		$dataArr['lease_status'] = 'N';
		$dataArr['status'] = 'Y';
		$dataArr['create_date'] = "now()";
		$dataArr['create_ip'] = $this->input->ip_address();

		// print_r($dataArr);
		// exit;
		if($this->db_insert('tbl_member',$dataArr))
		{
			echo  $dataArr['personal_id'];
		}
		else
		{
			echo "F";
		}
	}

	public function encrypt($source,$toencrypt){  
		//加密用的key   

		$key = $source;  

		//使用3DES方法加密   

		$encryptMethod = MCRYPT_TRIPLEDES; 

		//初始化向量来增加安全性

		$iv = mcrypt_create_iv(mcrypt_get_iv_size($encryptMethod,MCRYPT_MODE_ECB), MCRYPT_RAND);  

		//使用mcrypt_encrypt函数加密，MCRYPT_MODE_ECB表示使用ECB模式

		$encrypted_toencrypt = mcrypt_encrypt($encryptMethod, $key, $toencrypt, MCRYPT_MODE_ECB,$iv);   

		//回传解密后字串

		return base64_encode($encrypted_toencrypt);  

	}

	//压缩图片
    public function thumbImage($im,$maxwidth,$maxheight,$filetype)
    {
        switch ($filetype) {     
            case 'image/pjpeg':     
            case 'image/jpeg':     
                $im = imagecreatefromjpeg($im);    //PHP图片处理系统函数
                break;     
            case 'image/gif':     
                $im = imagecreatefromgif($im);    
                break;     
            case 'image/png':     
                $im = imagecreatefrompng($im);    
                break;
            case 'image/wbmp':     
                $im = imagecreatefromwbmp($im);    
                break;             
        }  


        $resizewidth_tag = $resizeheight_tag = false;
        $pic_width = imagesx($im);
        $pic_height = imagesy($im);


        if(($maxwidth && $pic_width > $maxwidth) || ($maxheight && $pic_height > $maxheight))
        {
			$resizewidth_tag = $resizeheight_tag = false;

            if($maxwidth && $pic_width>$maxwidth)
            {
                $widthratio = $maxwidth / $pic_width;
                $resizewidth_tag = true;
            }


            if($maxheight && $pic_height>$maxheight)
            {
                $heightratio = $maxheight / $pic_height;
                $resizeheight_tag = true;
            }


            if($resizewidth_tag && $resizeheight_tag)
            {
                if($widthratio < $heightratio)
                 $ratio = $widthratio;
                else
                 $ratio = $heightratio;
            }


            if($resizewidth_tag && !$resizeheight_tag)
            $ratio = $widthratio;


            if($resizeheight_tag && !$resizewidth_tag)
            $ratio = $heightratio;


            $newwidth = $pic_width * $ratio;
            $newheight = $pic_height * $ratio;
                        


            if(function_exists("imagecopyresampled"))
            {
                $newim = imagecreatetruecolor($newwidth,$newheight);//PHP图片处理系统函数
                imagecopyresampled($newim,$im,0,0,0,0,$newwidth,$newheight,$pic_width,$pic_height);//PHP图片处理系统函数
            }
            else
            {
                $newim = imagecreate($newwidth,$newheight);
                imagecopyresized($newim,$im,0,0,0,0,$newwidth,$newheight,$pic_width,$pic_height);
            }

            $result = $newim;
            // switch ($filetype) {     
            //     case 'image/pjpeg' :     
            //     case 'image/jpeg' :     
            //         $result = imagejpeg($newim,$name);   
            //         break;     
            //     case 'image/gif' :     
            //         $result = imagegif($newim,$name);   
            //         break;     
            //     case 'image/png' :     
            //         $result = imagepng($newim,$name);    
            //         break;
            //     case 'image/wbmp' :     
            //         $result = imagewbmp($newim,$name);    
            //         break;             
            // } 
            // imagedestroy($newim);
        }
        else
        {
        	$result = $im;
            // switch ($filetype) {     
            //     case 'image/pjpeg' :     
            //     case 'image/jpeg' :     
            //         $result = imagejpeg($im,$name);   
            //         break;     
            //     case 'image/gif' :     
            //         $result = imagegif($im,$name);   
            //         break;     
            //     case 'image/png' :     
            //         $result = imagepng($im,$name);    
            //         break;
            //     case 'image/wbmp' :     
            //         $result = imagewbmp($im,$name);    
            //         break;             
            // }
        }
        return $result; //返回结果
    }

    public function register_member($wechatInfoArr = ""){
    	if($wechatInfoArr != "")
		{
			$memberArr = array();
			$memberArr['user_id'] =$wechatInfoArr['user_id'];
			if(strlen($memberArr['user_id'])>32)
			{
				return "会员注册失败,会员User ID不可大于32码";
				exit;
			}

			$SQLCmdM = "SELECT s_num FROM tbl_member WHERE user_id = '{$memberArr['user_id']}' AND status <> 'D'";
			$rsM = $this->db_query($SQLCmdM);
			// echo $SQLCmdM;
			// echo $rsM[0]['cnt'];
			
			if(count($rsM)>0)
			{
				if($wechatInfoArr['wechat_op_id']!="")
				{
					$SQLCmdO = "SELECT s_num FROM tbl_operator WHERE top11 = '{$wechatInfoArr['wechat_op_id']}'";
					$rsO = $this->db_query($SQLCmdO);
					if($rsO)
					{
						$whereStr = "s_num = {$rsM[0]['s_num']}";
						$memberArr['to_num'] = $rsO[0]['s_num'];
						$this->db_update('tbl_member',$memberArr,$whereStr);
					}
				}
				return true;
				// return "会员注册失败,会员User ID重复";
				exit;
			}

			if($wechatInfoArr['member_type']!="")
				$memberArr['member_type'] = $wechatInfoArr['member_type'];
			else
				$memberArr['member_type'] = 'B';

			if($wechatInfoArr['openid']!="")
			{
				$memberArr['member_register'] = "W";
			}
			else
			{
				$memberArr['member_register'] = "P";
			}
			$memberArr['name'] = $wechatInfoArr['nickname'];
			$memberArr['personal_id'] = $wechatInfoArr['personal_id'];
			// $memberArr['user_id'] = MD5($memberArr['personal_id']);
			
			$memberArr['pay_type'] = 'W';
			$memberArr['stored_type'] = 'T';
			$memberArr['lease_status'] = 'N';
			$memberArr['status'] = 'Y';
			$memberArr['create_date'] = $wechatInfoArr['create_date'];
			$memberArr['create_ip'] = $this->input->ip_address();
			if($wechatInfoArr['sex'] == 1)
			{
				$memberArr['gender'] = "M";
			}
			else if($wechatInfoArr['sex'] == 2)
			{
				$memberArr['gender'] = "F";
			}
			$memberArr['province'] = $wechatInfoArr['province'];
			$memberArr['city'] = $wechatInfoArr['city'];

			$memberArr['mobile'] =$wechatInfoArr['mobile'];
			$memberArr['deposit'] = $wechatInfoArr['deposit'];
			$memberArr['deposit_status'] = $wechatInfoArr['deposit_status'];
			
			// $SQLCmd = "SELECT * FROM tbl_member WHERE personal_id = '{$wechatInfoArr['openid']}'";
			// $rs = $this->db_query($SQLCmd);
			// if(count($rs) == 0)
			// {
			$wechatArr = array();
			if($this->db_insert("tbl_member",$memberArr))
			{
				if($wechatInfoArr['openid']!="")
				{
					$SQLCmdL = "SELECT LAST_INSERT_ID() as tm_num FROM tbl_member";
					$rsL = $this->db_query($SQLCmdL);
					$wechatArr['tm_num'] = $rsL[0]['tm_num'];
					$wechatArr['access_token'] = $wechatInfoArr['access_token'];
					$wechatArr['openid'] = $wechatInfoArr['openid'];
					$wechatArr['scope'] = $wechatInfoArr['scope'];
					$wechatArr['nickname'] = $wechatInfoArr['nickname'];
					$wechatArr['sex'] = $wechatInfoArr['sex'];
					$wechatArr['province'] = $wechatInfoArr['province'];
					$wechatArr['city'] = $wechatInfoArr['city'];
					$wechatArr['country'] = $wechatInfoArr['country'];
					$wechatArr['headimgurl'] = $wechatInfoArr['headimgurl'];
					$output = "";
					if($wechatInfoArr['privilege']!="")
					{
						$output = implode(', ', array_map(
						    function ($v, $k) { return sprintf("%s='%s'", $k, $v); },
						    $wechatInfoArr['privilege'],
						    array_keys($wechatInfoArr['privilege'])
						));
					}
					$wechatArr['privilege'] = $output;
					$wechatArr['unionid'] = $wechatInfoArr['unionid'];
					$wechatArr['create_date'] = "now()";
					return $this->db_insert("tbl_member_wechat_info",$wechatArr);
				}
				else
				{
					return true;
				}
			}
			else
			{
				return "会员注册失败";
			}
				
			// }
			// else 
			// {
			// 	if($rs[0]['status'] == 'Y')
			// 	{
			// 		$whereStr = "openid = '{$wechatInfoArr['openid']}'";
			// 		$wechatArr['nickname'] = $wechatInfoArr['nickname'];
			// 		$wechatArr['sex'] = $wechatInfoArr['sex'];
			// 		$wechatArr['province'] = $wechatInfoArr['province'];
			// 		$wechatArr['city'] = $wechatInfoArr['city'];
			// 		$wechatArr['country'] = $wechatInfoArr['country'];
			// 		$wechatArr['headimgurl'] = $wechatInfoArr['headimgurl'];
			// 		$this->db_update("tbl_member_wechat_info",$wechatArr,$whereStr);
			// 		return true;
			// 	}
			// 	else
			// 	{
			// 		return "此帐号已经被停用，请您洽询客服!";
			// 	}
			// }
			
		}
		else
		{
			return "会员注册失败";
		}
    }

    //電池綁定通知
    public function bind_battery($dataArr,$bss_id="",$bind_type="",$return_battery="")
    {
    	$User_ID = $dataArr['battery_user_id'];
    	$leave_battery_id = $dataArr['leave_battery_id'];
    	//先查詢最後一筆還電紀錄是不是沒有借電紀錄
    	$SQLCmdLR1 = "SELECT s_num,leave_date,leave_battery_id,leave_status,return_sb_num,return_request_date,return_date,return_battery_id,return_status FROM log_battery_leave_return 
    				WHERE return_battery_id = '{$leave_battery_id}' ORDER BY s_num DESC limit 1";
    	$rsR1 = $this->db_query($SQLCmdLR1);
    	// echo $SQLCmdLR1;
    	if($rsR1)
    	{
    		if($rsR1[0]['leave_date']=="" && $rsR1[0]['leave_battery_id']=="" && $rsR1[0]['leave_status']=="")
    		{
    			$SQLCmdR2 = "SELECT count(*) cnt FROM log_battery_leave_return WHERE return_sb_num = '{$rsR1[0]['return_sb_num']}' AND return_request_date = '{$rsR1[0]['return_request_date']}' AND return_battery_id = '{$rsR1[0]['return_battery_id']}' AND return_status = '{$rsR1[0]['return_status']}'";
    			// echo $SQLCmdR2;
    			$rsR2 = $this->db_query($SQLCmdR2);
    			if($rsR2)
    			{
    				if($rsR2[0]['cnt']>=2)
    				{
    					$SQLCmdD = "DELETE FROM log_battery_leave_return WHERE s_num = '{$rsR1[0]['s_num']}'";
    					$rsD = $this->db_query($SQLCmdD);
    				}
    			}
    		}
    	}
    	// exit;
    	$sb_num = $dataArr['leave_sb_num'];

    	$SQLCmdL1 = "SELECT system_log_date,leave_date FROM log_battery_leave_return 
    					WHERE leave_battery_id = '{$leave_battery_id}' AND (leave_status = '' OR leave_status IS NULL OR leave_status='0' OR leave_status='A' OR leave_status='F' OR leave_status='W') ORDER BY leave_date DESC,system_log_date DESC limit 1";
	    $rsL1 = $this->db_query($SQLCmdL1);

	    $SQLCmdL2 = "SELECT return_date FROM log_battery_leave_return 
	    			WHERE return_battery_id = '{$leave_battery_id}' AND (return_status = '' OR return_status IS NULL OR return_status='0' OR return_status='A' OR return_status='F' OR return_status='W') ORDER BY return_date DESC,system_log_date DESC limit 1";
	    $rsL2 = $this->db_query($SQLCmdL2);


	    if($rsL1 && $rsL2)
	    {
	    	if($rsL2[0]['return_date'] <= $rsL1[0]['system_log_date'])
			{
				$returnArr['rt_cd'] = "0007";
				$returnArr['rt_msg'] = "此电池已经被借出,无法再绑定";
				return $returnArr;
			}
	    }
	    else if($rsL1 && !$rsL2)
	    {
	    	$returnArr['rt_cd'] = "0007";
			$returnArr['rt_msg'] = "此电池已经被借出,无法再绑定";
			return $returnArr;
	    }

	    date_default_timezone_set("Asia/Taipei");
	    $now_return_date = date("Y-m-d H:i:s");
	    $now_leave_date = date('Y-m-d H-i-s');

    	if($bind_type!="")
    	{
    		if($bind_type==1)
    		{
    			if($dataArr['member_type']!="F")
    			{
    				$SQLCmdL = "SELECT * FROM log_battery_leave_return WHERE battery_user_id = '{$dataArr['battery_user_id']}' AND (leave_date IS NOT NULL OR leave_date !='') AND (leave_status = '' OR leave_status IS NULL OR leave_status = '0' OR leave_status='A' OR leave_status='F' OR leave_status='W') ORDER BY system_log_date DESC,s_num DESC limit 1";
	    			$rsL = $this->db_query($SQLCmdL);
	    			if(count($rsL)>0)
	    			{
	    				if($rsL[0]['return_date']=="")
	    				{
	    					$returnArr['rt_cd'] = "0013";
					    	$returnArr['rt_msg'] = "已经绑定过电池";
					    	return $returnArr;
	    				}
	    			}
    			}
    		}
    		else if($bind_type==2)
    		{
    			if($return_battery=="")
    			{
    				$returnArr['rt_cd'] = "0012";
			    	$returnArr['rt_msg'] = "归还电池序号有误";
			    	return $returnArr;
    			}
    			else
    			{
    				$SQLCmdR = "SELECT * FROM log_battery_leave_return WHERE leave_battery_id = '{$return_battery}' AND battery_user_id = '{$dataArr['battery_user_id']}' AND return_date IS NULL ORDER BY system_log_date DESC,s_num DESC";
    				$rsR = $this->db_query($SQLCmdR);
    				if(count($rsR)>0)
    				{
    					$return_s_num = $rsR[0]['s_num'];
    					$whereStrR = " s_num = {$return_s_num}";
    					$return_date = $now_return_date;
    					$dataArrR = array();
		    			$dataArrR['return_DorO_flag'] = $rsR[0]['leave_DorO_flag'];
						$dataArrR['return_do_num'] = $rsR[0]['leave_do_num'];
						$dataArrR['return_battery_id'] = $rsR[0]['leave_battery_id'];
		    			$dataArrR['return_request_date'] = $return_date;
						$dataArrR['return_date'] = $return_date;
						$dataArrR['return_status'] = 'F';
    					$this->db_update('log_battery_leave_return',$dataArrR,$whereStrR);
    					$SQLCmdB = "UPDATE tbl_battery SET position = 'O',exchange_count = exchange_count+1 WHERE battery_id = '{$return_battery}' AND status <> 'D'";
						$this->db_query($SQLCmdB);
    				}
    				else
    				{
    					$SQLCmdR = "SELECT * FROM log_battery_leave_return WHERE leave_battery_id = '' AND battery_user_id = '{$dataArr['battery_user_id']}' AND return_date IS NULL ORDER BY system_log_date DESC,s_num DESC";
    					$rsR = $this->db_query($SQLCmdR);
    					if($rsR1)
    					{
    						$return_s_num = $rsR[0]['s_num'];
	    					$whereStrR = " s_num = {$return_s_num}";
	    					$return_date = $now_return_date;
	    					$dataArrR = array();
			    			$dataArrR['return_DorO_flag'] = $rsR[0]['leave_DorO_flag'];
							$dataArrR['return_do_num'] = $rsR[0]['leave_do_num'];
							$dataArrR['return_battery_id'] = $rsR[0]['leave_battery_id'];
			    			$dataArrR['return_request_date'] = $return_date;
							$dataArrR['return_date'] = $return_date;
							$dataArrR['return_status'] = 'F';
	    					$this->db_update('log_battery_leave_return',$dataArrR,$whereStrR);
	    					$SQLCmdB = "UPDATE tbl_battery SET position = 'O',exchange_count = exchange_count+1 WHERE battery_id = '{$return_battery}' AND status <> 'D'";
							$this->db_query($SQLCmdB);
    					}
    					else
    					{
    						$returnArr['rt_cd'] = "0014";
					    	$returnArr['rt_msg'] = "还电记录有误";
					    	return $returnArr;
    					}
    				}
    			}
    		}
    		else
    		{
    			$returnArr['rt_cd'] = "0011";
		    	$returnArr['rt_msg'] = "绑定类别有误";
		    	return $returnArr;
    		}
    		$dataArr['system_log_date'] = $now_leave_date;
	    	$dataArr['leave_status'] = "F";//狀態為APP綁定
	    	unset($dataArr['member_type']);
    		$this->db_insert('log_battery_leave_return',$dataArr);
    		$returnArr['rt_cd'] = "0000";
	    	$returnArr['rt_msg'] = "成功";
    	}
    	else
    	{
	    	$SQLCmdL = "SELECT * FROM log_battery_leave_return WHERE battery_user_id = '{$dataArr['battery_user_id']}' ORDER BY system_log_date DESC,s_num DESC";
	    	$rsL = $this->db_query($SQLCmdL);
	    	// echo $SQLCmdL;
	    	if(count($rsL)==0)
	    	{
	    		$dataArr['system_log_date'] = $now_leave_date;
	    		unset($dataArr['member_type']);
	    		$this->db_insert('log_battery_leave_return',$dataArr);
	    		$returnArr['rt_cd'] = "0000";
	    		$returnArr['rt_msg'] = "成功";
	    	}
	    	else
	    	{
	    		// if($dataArr['member_type']=="F")
	    		// {
	    		// 	$type = "i";
	    		// }
	    		// else
	    		// {
	    			$type = "";
		    		if($rsL[0]['return_date']=="" && $rsL[0]['return_status']!='0')
		    		{
		    			$whereStr = " s_num = {$rsL[0]['s_num']}";
		    			$leave_date = $rsL[0]['leave_date'];
		    			$return_date = $now_return_date;
		    			$dataArrR['return_DorO_flag'] = $rsL[0]['leave_DorO_flag'];
						$dataArrR['return_do_num'] = $rsL[0]['leave_do_num'];
						$dataArrR['return_battery_id'] = $rsL[0]['leave_battery_id'];
		    			$dataArrR['return_request_date'] = $return_date;
						$dataArrR['return_date'] = $return_date;
						if($rsL[0]['return_status']=="")
							$dataArrR['return_status'] = 'F';
						$dataArrR['usage_time'] = $this->shi_jian_cha($return_date,$leave_date);
						$this->db_update('log_battery_leave_return',$dataArrR,$whereStr);
						$type = "i";
		    		}
		    		else
		    		{
		    			if(count($rsL)>2)
		    			{
		    				if($rsL[1]['return_date']=="" && $rsL[1]['return_status']!='0')
				    		{
				    			$whereStr = " s_num = {$rsL[1]['s_num']}";
				    			$leave_date = $rsL[1]['leave_date'];
				    			$return_date = $now_return_date;
				    			$dataArrR['return_DorO_flag'] = $rsL[1]['leave_DorO_flag'];
								$dataArrR['return_do_num'] = $rsL[1]['leave_do_num'];
								$dataArrR['return_battery_id'] = $rsL[1]['leave_battery_id'];
				    			$dataArrR['return_request_date'] = $return_date;
								$dataArrR['return_date'] = $return_date;
								if($rsL[1]['return_status']=="")
									$dataArrR['return_status'] = 'F';
								$dataArrR['usage_time'] = $this->shi_jian_cha($return_date,$leave_date);
								$this->db_update('log_battery_leave_return',$dataArrR,$whereStr);
								$type = "u";
				    		}
				    		else
				    		{
								$type = "i";
				    		}
		    			} 
			    		else
			    		{
							$type = "i";
			    		}
		    		}
	    		// }
	    		
	    // 		else if($rsL[1]['return_date']=="" && $rsL[1]['return_status']!='0')
	    // 		{
	    // 			$whereStr = " s_num = {$rsL[1]['s_num']}";
	    // 			$leave_date = $rsL[1]['leave_date'];
	    // 			$return_date = $now_return_date;
	    // 			$dataArrR['return_DorO_flag'] = $rsL[1]['leave_DorO_flag'];
					// $dataArrR['return_do_num'] = $rsL[1]['leave_do_num'];
					// $dataArrR['return_battery_id'] = $rsL[1]['leave_battery_id'];
	    // 			$dataArrR['return_request_date'] = $return_date;
					// $dataArrR['return_date'] = $return_date;
					// if($rsL[1]['return_status']=="")
					// 	$dataArrR['return_status'] = 'F';
					// $dataArrR['usage_time'] = $this->shi_jian_cha($return_date,$leave_date);
					// $this->db_update('log_battery_leave_return',$dataArrR,$whereStr);
					// $type = "u";
	    // 		}
	    // 		else
	    // 		{
					// $type = "i";
	    // 		}

	    		$SQLCmdL = "SELECT * FROM log_battery_leave_return WHERE battery_user_id = '{$dataArr['battery_user_id']}' ORDER BY system_log_date DESC,s_num DESC";
	    		$rsL = $this->db_query($SQLCmdL);
	    		$return_date = $rsL[0]['return_date'];
	    		$return_track_no = $rsL[0]['return_track_no'];
	    		$return_battery_id = $rsL[0]['return_battery_id'];
	    		$leave_sb_num = $dataArr['leave_sb_num'];
	    		$return_sb_num = $rsL[0]['return_sb_num'];
	    		$leave_location = "";
	    		$return_location = "";

	    		if($leave_sb_num!="")
	    		{
					$SQLCmdS = "SELECT latitude,longitude,location FROM tbl_battery_swap_station WHERE s_num = {$leave_sb_num} AND status <> 'D'";
					$rsS = $this->db_query($SQLCmdS);
					$leave_gps = "";
					if($rsS)
					{
						$leave_gps = $rsS[0]['latitude'].",".$rsS[0]['longitude'];
						$leave_location = $rsS[0]['location'];
					}
	    		}
	    		if($return_sb_num!="")
	    		{
	    			$SQLCmdBS = "SELECT latitude,longitude,location FROM tbl_battery_swap_station WHERE s_num = {$return_sb_num} AND status <> 'D'";
					$rsBS = $this->db_query($SQLCmdBS);
					$return_gps = "";
					if($rsBS)
					{
						$return_gps = $rsBS[0]['latitude'].",".$rsBS[0]['longitude'];
						$return_location = $rsBS[0]['location'];
					}
	    		}

	    		$dataArr['system_log_date'] = $now_leave_date;
	    		$dataArr['leave_status'] = "F";//狀態為二次綁定
	    		unset($dataArr['member_type']);
	    		if($type == "u")
	    		{
	    			$whereStrL = " s_num = {$rsL[0]['s_num']} AND leave_status!=0"; 
	    			$this->db_update("log_battery_leave_return",$dataArr,$whereStrL);
	    		}
	    		else if($type == "i")
	    		{
	    			$this->db_insert('log_battery_leave_return',$dataArr);
	    		}
	    		else
	    		{
	    			$returnArr['rt_cd'] = "0009";
	    			$returnArr['rt_msg'] = "綁定失敗";
	    			return $returnArr;;
	    		}
	    		$returnArr['rt_cd'] = "0000";
	    		$returnArr['rt_msg'] = "成功";
	    		// $returnArr['rt_cd'] = "0005";
	    		// $returnArr['rt_msg'] = "此会员已经榜定过电池";

	    		$brrow_date = $dataArr['leave_date'];
	    		
	   //  		$postArr = array();
				// $postArr['action'] = "actionPostCabinetCallback";
				// $postArr['rentTime'] = $brrow_date;
				// $postArr['returnTime'] = (string)$return_date;
				// $postArr['location'] = $leave_location;
				// $postArr['returnLocation'] = (string)$return_location;
				// $postArr['export'] = "";
				// $postArr['access'] = (string)$return_track_no;
				// $postArr['rentBatteryNumber'] = $leave_battery_id;
				// $postArr['returnBatteryNumber'] = (string)$return_battery_id;
				// $postArr['cabinetNumber'] = (string)$bss_id;
				// $postArr['userId'] = ltrim($User_ID,"0");
				// $this->wechat_callback($postArr);
	    	}
    	}
    	

    	$SQLCmdM = "UPDATE tbl_member SET lease_status = 'L',change_count = change_count+1 WHERE user_id = '{$User_ID}' AND status <> 'D'";
		$this->db_query($SQLCmdM); 

		$SQLCmdB = "UPDATE tbl_battery SET position = 'V',exchange_count = exchange_count+1 WHERE battery_id = '{$leave_battery_id}' AND status <> 'D'";
		$this->db_query($SQLCmdB); 

		if($sb_num!="")
		{
			$SQLCmdBS = "UPDATE tbl_battery_swap_station SET exchange_num = exchange_num+1 WHERE s_num = '{$sb_num}' AND status <> 'D'";
			$this->db_query($SQLCmdBS);
		}
		$returnArr['rt_cd'] = "0000";
	    $returnArr['rt_msg'] = "成功";
    	return $returnArr;
    }

    //計算時間差
	public function shi_jian_cha($d,$d1)
	{ 
		$time = strtotime($d) - strtotime($d1); 
		$n_time = str_pad(floor($time%(24*3600)/3600),2,0,STR_PAD_LEFT ).":".str_pad(floor($time%3600/60),2,0,STR_PAD_LEFT).":".str_pad($time%3600%60, 2, 0, STR_PAD_LEFT)."秒";
		return $n_time;
	}

	//微信回調
	public function wechat_callback($postArr=array())
	{
		//取得微信的網址參數
		$SQLCmdC = "SELECT config_set FROM sys_config WHERE config_code ='wechat_callback_url' AND status = 1";
		$rsC = $this->db_query($SQLCmdC);
		if($rsC)
		{
			$url = $rsC[0]['config_set']."cabinetcallback";
		}
		else
		{
			$url = "";
		}

		$postStr = http_build_query($postArr);

		$dataArrLog = array();
		$dataArrLog['api_name'] = "actionPostCabinetCallback";
		$dataArrLog['api_chinese_name'] = "机柜回调接口";
		$dataArrLog['send_date'] = $postArr['rentTime'];
		$dataArrLog['send_data'] = $postStr;

		$this->db_insert("log_send_wechat_api",$dataArrLog);
		$SQLCmdLa = "select LAST_INSERT_ID() as log_sn FROM log_send_wechat_api";
		$rsLa = $this->db_query($SQLCmdLa);
		$log_sn = $rsLa[0]['log_sn'];
		$whereStrLog = " s_num = {$log_sn}";
		// $postStr = http_build_query($postArr);
		// $postStr = http_build_query(array("Book"=>1));
		// $postStr = '{"Book":"1"}';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postStr); 
		$output = curl_exec($ch);

		$dataArrLog = array();
		$dataArrLog['receive_date'] = "now()";
		$dataArrLog['receive_data'] = $output;
		$this->db_update("log_send_wechat_api",$dataArrLog,$whereStrLog);
	}

	//預約電池
	public function reserverBattery($user_id="",$battery_id="")
	{
		$whereStr = " battery_id = '{$battery_id}' AND status <> 'D'";
		$dataArr['reserveUserID'] = $user_id;
		$dataArr['reserveDate'] = "now()";
		return $this->db_update("tbl_battery",$dataArr,$whereStr);
	}

	//鎖定電池
	public function lockbattery($battery_id="",$locked_status="")
	{
		$whereStr = "battery_id = '{$battery_id}' AND status <> 'D'";
		$dataArr['locked_flag'] = $locked_status;
		return $this->db_update("tbl_battery",$dataArr,$whereStr);
	}

	//同步營運商
	public function syncop($operatorArr = array())
	{
		if($operatorArr)
		{
			$SQLCmdO = "SELECT * FROM tbl_operator WHERE status <> 'D'";
			$rsO = $this->db_query($SQLCmdO);
			$opnameArr = array();
			if($rsO)
			{
				foreach ($rsO as $keyO => $valueO) {
					$opnameArr[$keyO] = $valueO['top11'];
				}
			}
			$msg = "";
			foreach($operatorArr as $key => $value)
			{
				if($value['op06']!="")
				{
					$insertflag = true;
					if($opnameArr)
					{
						if(in_array($value['op06'],$opnameArr))
						{
							$insertflag = false;
						}
					}

					if($insertflag)
					{
						$dataArr['top01'] = $value['op02'];//營運商名稱
						$dataArr['top02'] = 1;//營運商類型(1:直營,2:加盟,3:其他)
						$dataArr['top03'] = $value['op03'];//營運商聯絡人姓名
						$dataArr['top04'] = $value['op04'];//營運商聯絡電話
						if(isset($value['op05']))
							$dataArr['top12'] = $value['op05'];//是否為車電一體配合廠商(Y:是,N:否)
						$dataArr['top11'] = $value['op06'];//企業ID
						$dataArr['status'] = 'Y';//狀態
						$dataArr['create_date'] = "now()";//建檔時間
						$this->db_insert("tbl_operator",$dataArr);
					}
					else
					{
						$whereStr = "top11 = '{$value['op06']}'";
						$dataArr['top01'] = $value['op02'];//營運商名稱
						$dataArr['top03'] = $value['op03'];//營運商聯絡人姓名
						$dataArr['top04'] = $value['op04'];//營運商聯絡電話
						if(isset($value['op05']))
							$dataArr['top12'] = $value['op05'];//是否為車電一體配合廠商(Y:是,N:否)
						$dataArr['status'] = 'Y';//狀態
						$dataArr['update_date'] = "now()";
						$this->db_update("tbl_operator",$dataArr,$whereStr);
					}
				}
				else
				{
					$msg = "企业名称不得为空";
				}
			}
			if(trim($msg)=='')
			{
				$returnArr['rt_cd'] = "0000";
				$returnArr['rt_msg'] = "成功";
			}
			else
			{
				$returnArr['rt_cd'] = "0004";
				$returnArr['rt_msg'] = $msg;
			}
		}
		else
		{
			$returnArr['rt_cd'] = "0002";
			$returnArr['rt_msg'] = "资料错误";
		}
		return $returnArr;
	}

	//同步付費方案
	public function synclp($lperatorArr = array())
	{
		if($lperatorArr)
		{
			$SQLCmdO = "SELECT * FROM tbl_wechat_lease_price WHERE status <> 'D'";
			$rsO = $this->db_query($SQLCmdO);
			$lpIDArr = array();
			if($rsO)
			{
				foreach ($rsO as $keyO => $valueO) {
					$lpIDArr[$keyO] = $valueO['lease_plan_id'];
				}
			}
			$msg = "";
			foreach($lperatorArr as $key => $value)
			{
				if(count($value)!=4)
				{
					$returnArr['rt_cd'] = "0002";
					$returnArr['rt_msg'] = "资料错误";
					return $returnArr;
				}
				if($value['lp02']!="" && $value['lp03']!="")
				{
					$insertflag = true;
					if($lpIDArr)
					{
						if(in_array($value['lp02'],$lpIDArr))
						{
							$insertflag = false;
						}
					}

					if($insertflag)
					{
						$dataArr['lease_plan_id'] = $value['lp02'];//租借方案ID
						$dataArr['lease_plan_name'] = $value['lp03'];//租借方案名稱
						if(is_array($value['lp04']))
						{
							if(isset($value['lp04']['pledge_money']))
								$dataArr['pledge_money'] = $value['lp04']['pledge_money'];
							if(isset($value['lp04']['is_pledge']))
								$dataArr['is_pledge'] = $value['lp04']['is_pledge'];
							if(isset($value['lp04']['how_battery']))
								$dataArr['how_battery'] = $value['lp04']['how_battery'];
							if(isset($value['lp04']['is_bottom']))
								$dataArr['is_bottom'] = $value['lp04']['is_bottom'];
							if(isset($value['lp04']['bottom_cost']))
								$dataArr['bottom_cost'] = $value['lp04']['bottom_cost'];
							if(isset($value['lp04']['pay_type']) && isset($value['lp04']['card_categories']))
							{
								$dataArr['pay_type'] = $value['lp04']['pay_type'];
								if(trim($value['lp04']['pay_type'])=='8')
								{
									$dataArr['month_categories'] = $this->arrayToString($value['lp04']['card_categories']);
								}
								else
								{
									$dataArr['month_categories'] = $value['lp04']['card_categories'];
								}
							}
							if(isset($value['lp04']['vip_money']))
								$dataArr['vip_money'] = $value['lp04']['vip_money'];
							if(isset($value['lp04']['charge_rule']))
								$dataArr['charge_rule'] = $value['lp04']['charge_rule'];
							if(isset($value['lp04']['constraint_card']))
								$dataArr['constraint_card'] = $value['lp04']['constraint_card'];
							if(isset($value['lp04']['prepay']))
								$dataArr['prepay'] = $value['lp04']['prepay'];
							$value['lp04'] = $this->arrayToString($value['lp04']);
						}
						$dataArr['lease_plan_content'] = $value['lp04'];//租借方案內容
						$dataArr['w_operator_id'] = $value['lp05'];//微信企業ID
						$dataArr['status'] = 'Y';//狀態
						$dataArr['create_date'] = "now()";//建檔時間
						$this->db_insert("tbl_wechat_lease_price",$dataArr);
					}
					else
					{
						$whereStr = "lease_plan_id = '{$value['lp02']}'";
						$dataArr['lease_plan_name'] = $value['lp03'];//租借方案名稱
						if(is_array($value['lp04']))
						{
							if(isset($value['lp04']['pledge_money']))
								$dataArr['pledge_money'] = $value['lp04']['pledge_money'];
							if(isset($value['lp04']['is_pledge']))
								$dataArr['is_pledge'] = $value['lp04']['is_pledge'];
							if(isset($value['lp04']['how_battery']))
								$dataArr['how_battery'] = $value['lp04']['how_battery'];
							if(isset($value['lp04']['is_bottom']))
								$dataArr['is_bottom'] = $value['lp04']['is_bottom'];
							if(isset($value['lp04']['bottom_cost']))
								$dataArr['bottom_cost'] = $value['lp04']['bottom_cost'];
							if(isset($value['lp04']['pay_type']) && isset($value['lp04']['card_categories']))
							{
								$dataArr['pay_type'] = $value['lp04']['pay_type'];
								if(trim($value['lp04']['pay_type'])=='8')
								{
									$dataArr['month_categories'] = $this->arrayToString($value['lp04']['card_categories']);
								}
								else
								{
									$dataArr['month_categories'] = $value['lp04']['card_categories'];
								}
							}
							if(isset($value['lp04']['vip_money']))
								$dataArr['vip_money'] = $value['lp04']['vip_money'];
							if(isset($value['lp04']['charge_rule']))
								$dataArr['charge_rule'] = $value['lp04']['charge_rule'];
							if(isset($value['lp04']['constraint_card']))
								$dataArr['constraint_card'] = $value['lp04']['constraint_card'];
							if(isset($value['lp04']['prepay']))
								$dataArr['prepay'] = $value['lp04']['prepay'];
							$value['lp04'] = $this->arrayToString($value['lp04']);
						}
						$dataArr['lease_plan_content'] = $value['lp04'];//租借方案內容
						$dataArr['w_operator_id'] = $value['lp05'];//微信企業ID
						$dataArr['status'] = 'Y';//狀態
						$dataArr['update_date'] = "now()";//修改時間
						$this->db_update("tbl_wechat_lease_price",$dataArr,$whereStr);
					}
				}
				else
				{
					$msg = "租借方案ID与名称不得为空";
				}
			}
			if(trim($msg)=='')
			{
				$returnArr['rt_cd'] = "0000";
				$returnArr['rt_msg'] = "成功";
			}
			else
			{
				$returnArr['rt_cd'] = "0004";
				$returnArr['rt_msg'] = $msg;
			}
		}
		else
		{
			$returnArr['rt_cd'] = "0002";
			$returnArr['rt_msg'] = "资料错误";
		}
		return $returnArr;
	}

	//同步付費方案(新)
	public function synclp_new($lperatorArr = array())
	{
		if($lperatorArr)
		{
			$SQLCmdO = "SELECT * FROM tbl_new_lease_price WHERE status <> 'D'";
			$rsO = $this->db_query($SQLCmdO);
			$lpIDArr = array();
			if($rsO)
			{
				foreach ($rsO as $keyO => $valueO) {
					$lpIDArr[$keyO] = $valueO['wechat_plan_no'];
				}
			}
			$msg = "";
			foreach($lperatorArr as $key => $value)
			{
				if(count($value)!=4)
				{
					$returnArr['rt_cd'] = "0002";
					$returnArr['rt_msg'] = "资料错误";
					return $returnArr;
				}
				if($value['lp02']!="" && $value['lp03']!="")
				{
					$insertflag = true;
					if($lpIDArr)
					{
						if(in_array($value['lp02'],$lpIDArr))
						{
							$insertflag = false;
						}
					}
					$dataArr = array();
					
					$dataArr['lease_plan_name'] = $value['lp03'];//租借方案名稱
					if(is_array($value['lp04']))
					{
						if(isset($value['lp04']['pledge_money']))
						{
							$dataArrBD['battery_no'] = 1;
							$dataArrBD['amount'] = $value['lp04']['pledge_money'];//押金
						}
						if(isset($value['lp04']['is_pledge']))
							$dataArr['deposit_rule'] = $value['lp04']['is_pledge'];//押金支付規則 0企业支付，1用户支付，2资金池
						if(isset($value['lp04']['how_battery']))//綁定多顆電池
						{
							if($value['lp04']['how_battery']==0)
								$dataArr['muti_battery'] = 'N';
							else if($value['lp04']['how_battery']==1)
								$dataArr['muti_battery'] = 'Y';
						}
						if(isset($value['lp04']['is_bottom']))
							$dataArr['bottom_rule'] = $value['lp04']['is_bottom'];//底托支付规则:0企业支付，1用户支付
						if(isset($value['lp04']['bottom_cost']))
							$dataArr['bottom_amount'] = $value['lp04']['bottom_cost'];//底托支付费用
						if(isset($value['lp04']['pay_type']) && isset($value['lp04']['card_categories']))
						{
							$dataArr['pay_type'] = trim($value['lp04']['pay_type']);
							switch($dataArr['pay_type'])
							{
								case 2://月費
									$dataArr['month_fee'] = $value['lp04']['vip_money'];
									$dataArr['moth_day'] = 30;
									break;
								case 4://季費
									$dataArr['quarter_fee'] = $value['lp04']['vip_money'];
									break;
								case 5://年費
									$dataArr['year_fee'] = $value['lp04']['vip_money'];
									break;
								case 8://卡種
									if(is_array($value['lp04']['card_categories']))
									{
										foreach($value['lp04']['card_categories'] as $key2 => $value2)
										{
											switch($value2['card_type'])
											{
												case 1:
													$dataArr['year_fee'] = $value2['money'];
													break;
												case 2:
													$dataArr['quarter_fee'] = $value2['money'];
													break;
												case 3:
													$dataArr['month_fee'] = $value2['money'];
													break;
												case 4:
													$dataArr['halfyear_fee'] = $value2['money'];
													break;
											}
										}
									}
									break;
							}
							// if(trim($value['lp04']['pay_type'])=='8')
							// {
							// 	$dataArr['month_categories'] = $this->arrayToString($value['lp04']['card_categories']);
							// }
							// else
							// {
							// 	$dataArr['month_categories'] = $value['lp04']['card_categories'];
							// }
						}
						// if(isset($value['lp04']['vip_money']))
						// 	$dataArr['vip_money'] = $value['lp04']['vip_money'];
						if(isset($value['lp04']['charge_rule']))
						{
							$SQLCmdON = "SELECT s_num FROM tbl_once_plan WHERE leave_one = {$value['lp04']['charge_rule']} AND leave_two IS NULL AND leave_three IS NULL AND leave_four IS NULL";
							$rsON = $this->db_query($SQLCmdON);
							if(count($rsON)==0)
							{
								$dataArrON['leave_one'] = $value['lp04']['charge_rule'];
								$dataArrON['status'] = 'Y';
								$this->db_insert('tbl_once_plan',$dataArrON);
								$SQLCmdON1 = "SELECT LAST_INSERT_ID() as s_num";
								$rsON1 = $this->db_query($SQLCmdON1);
								$dataArr['once_plan'] = $rsON1[0]['s_num'];
							}
							else
							{
								$dataArr['once_plan'] = $rsON[0]['s_num'];
							}
						}

						if(isset($value['lp04']['constraint_card']))
						{
							if($value['lp04']['constraint_card']==0)
								$dataArr['force_mode'] = "N";
							else if($value['lp04']['constraint_card']==1)
								$dataArr['force_mode'] = "M";
							else
								$dataArr['force_mode'] = $value['lp04']['constraint_card'];
						}
						if(isset($value['lp04']['prepay']))
							$dataArr['prepay'] = $value['lp04']['prepay'];
						$value['lp04'] = $this->arrayToString($value['lp04']);
					}
					// $dataArr['lease_plan_content'] = $value['lp04'];//租借方案內容
					// $dataArr['w_operator_id'] = $value['lp05'];//微信企業ID

					$dataArr['status'] = 'Y';//狀態
					
					if($insertflag)
					{
						$dataArr['wechat_plan_no'] = $value['lp02'];//租借方案ID
						$dataArr['create_date'] = "now()";//建檔時間
						$this->db_insert("tbl_new_lease_price",$dataArr);
						$SQLCmd = "SELECT LAST_INSERT_ID() as s_num";
						$rs = $this->db_query($SQLCmd);
						$dataArrBD['tnlp_num'] = $rs[0]['s_num'];
						$dataArrBD['status'] = 'Y';
						$this->db_insert("tbl_battery_deposit",$dataArrBD);
						$w_operator_id = $value['lp05'];//微信企業ID
						$whereStr = "top11 = {$w_operator_id}";
						$dataArrOP['top10'] = $rs[0]['s_num'];
						$this->db_update('tbl_operator',$dataArrOP,$whereStr);
					}
					else
					{
						$whereStr = "wechat_plan_no = '{$value['lp02']}'";
						$dataArr['update_date'] = "now()";//修改時間
						$this->db_update("tbl_new_lease_price",$dataArr,$whereStr);
						$SQLCmd1 = "SELECT s_num FROM tbl_new_lease_price WHERE {$whereStr}";
						$rs1 = $this->db_query($SQLCmd1);
						if($rs1)
						{
							$dataArrBD['tnlp_num'] = $rs1[0]['s_num'];
							$dataArrBD['status'] = 'Y';
							$whereStr2 = " tnlp_num = {$dataArrBD['tnlp_num']}";
							$SQLCmd2 = "SELECT count(*) cnt FROM tbl_battery_deposit WHERE {$whereStr2}";
							$rs2 = $this->db_query($SQLCmd2);
							if($rs2[0]['cnt']>0)
							{
								$this->db_update('tbl_battery_deposit',$dataArrBD,$whereStr2);
							}
							else
							{
								$this->db_insert("tbl_battery_deposit",$dataArrBD);
							}
						}
					}
				}
				else
				{
					$msg = "租借方案ID与名称不得为空";
				}
			}
			if(trim($msg)=='')
			{
				$returnArr['rt_cd'] = "0000";
				$returnArr['rt_msg'] = "成功";
			}
			else
			{
				$returnArr['rt_cd'] = "0004";
				$returnArr['rt_msg'] = $msg;
			}
		}
		else
		{
			$returnArr['rt_cd'] = "0002";
			$returnArr['rt_msg'] = "资料错误";
		}
		return $returnArr;
	}

	//註冊會員
	public function register_member1($mobile="")
	{
		$returnArr = array();
		if($mobile!="")
		{
			$dataArr['mobile'] = $mobile;
			$postArr = array();
			$dataArr = array();
			$postArr['action'] = "actionPostGetId";
			$postArr['key'] = "gzmod.com.cn";
			$wechatjson = $this->wechat_callback2($postArr,"获取id接口");
			if($this->model_common->is_json($wechatjson))
			{
				$userArr = json_decode($wechatjson,true);
				if($userArr['status']=='ok')
				{
					$dataArr['user_id'] = str_pad(addslashes($userArr['data']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼;
					$dataArr['mobile'] = $mobile;
					$dataArr['verify_name'] = 'N';
					$dataArr['lease_status'] = 'N';
					$dataArr['stored_type'] = 'A';
					$dataArr['deposit_status'] = 'N';
					$dataArr['status'] = 'Y';
					$SQLCmd = "SELECT s_num FROM tbl_operator WHERE top01 = '魔力營運商' AND status = 'Y'";
					$rs = $this->db_query($SQLCmd);
					if($rs)
					{
						$dataArr['to_num'] = $rs[0]['s_num'];
					}
					if($this->db_insert("tbl_member",$dataArr))
					{
						$returnArr["rt_cd"] = "0000";
						$returnArr["rt_msg"] = "成功";
						$returnArr["rt_01"] = "Y";
						$returnArr["rt_02"] = str_pad(addslashes($userArr['data']),8,"0",STR_PAD_LEFT);//user_id,左補0補滿8碼
						$returnArr["rt_03"] = "魔力營運商";
						$returnArr["rt_04"] = "N";
						$returnArr["rt_05"] = "N";
					}
					else
					{
						$returnArr["rt_cd"] = "0004";//登入失败
						$returnArr["rt_msg"] = "登入失败";
					}
					
				}
				else
				{
					$returnArr["rt_cd"] = $userArr['code'];
					$returnArr["rt_msg"] = $userArr['msg'];
				}
			}
			else
			{
				$returnArr["rt_cd"] = "0004";//登入失败
				$returnArr["rt_msg"] = "登入失败";
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0004";//登入失败
			$returnArr["rt_msg"] = "登入失败";
		}
		return $returnArr;
	}

	public function editmember($user_id="",$memberInfo = array())
	{
		if(count($memberInfo)>0 && $memberInfo!="")
		{
			$whereStr = "user_id = '{$user_id}'";
			if($this->db_update("tbl_member",$memberInfo,$whereStr))
			{
				$returnArr["rt_cd"] = "0000";//成功
				$returnArr["rt_msg"] = "成功";
			}
			else
			{
				$returnArr["rt_cd"] = "0006";//更新失败
				$returnArr["rt_msg"] = "更新失败";
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0005";//验证失败
			$returnArr["rt_msg"] = "验证失败";
		}
		return $returnArr;
	}

	public function wechat_callback2($postArr=array(),$api_chinese_name="")
	{
		//取得微信的網址參數
		$SQLCmdC = "SELECT config_set FROM sys_config WHERE config_code ='wechat_callback_url' AND status = 1";
		$rsC = $this->db_query($SQLCmdC);
		if($rsC)
		{
			$url = $rsC[0]['config_set']."synchrouser";
		}
		else
		{
			$url = "";
		}

		$postStr = http_build_query($postArr);

		date_default_timezone_set("Asia/Taipei");
		$dataArrLog = array();
		$dataArrLog['sys_date'] = date("Y-m-d H:i:s");
		$dataArrLog['api_name'] = $postArr['action'];
		$dataArrLog['api_chinese_name'] = $api_chinese_name;
		$dataArrLog['send_date'] = "now()";
		$dataArrLog['send_data'] = $postStr;

		$this->db_insert("log_send_wechat_api",$dataArrLog);
		// $SQLCmdLa = "select LAST_INSERT_ID() as log_sn FROM log_send_wechat_api";
		$SQLCmdLa = "select s_num as log_sn FROM log_send_wechat_api WHERE sys_date = '{$dataArrLog['sys_date']}'";
		$rsLa = $this->db_query($SQLCmdLa);
		$log_sn = $rsLa[0]['log_sn'];
		// $log_sn = mysql_insert_id();
		$whereStrLog = " s_num = {$log_sn}";
		// $postStr = http_build_query($postArr);
		// $postStr = http_build_query(array("Book"=>1));
		// $postStr = '{"Book":"1"}';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postStr); 
		$output = curl_exec($ch);
		curl_close($ch);
		
		$output = trim($output);
		if(@strpos($output,"502 Bad Gateway")!==false)
		{
			$output = "502 Bad Gateway";
		}
		else if(!$this->is_json($output))
		{
			$output = "格式錯誤";
		}

		$dataArrLog = array();
		$dataArrLog['receive_date'] = "now()";
		$dataArrLog['receive_data'] = $output;
		$this->db_update("log_send_wechat_api",$dataArrLog,$whereStrLog);
		return $output;
	}

	public function arrayToString($arr) {
		if (is_array($arr)){
			return implode(',', array_map(array($this, 'arrayToString'), $arr));
		}
		return $arr;
	}
	public function multi2array($array) {
		static $result_array = array();
		foreach ($array as $key => $value) {
			if (is_array($value)) {
				array_multi2array($value);
			}
			else
				$result_array[$key] = $value;
		}
		return $result_array;
	}

	public function saveVerificateCode($user_id="",$bss_id="",$verificateCode="",$quiry_date="")
	{
		if($bss_id!="" && $verificateCode!="")
		{
			$dataArr = array();
			$dataArr['user_id'] = $user_id;
			$dataArr['bss_id'] = $bss_id;
			$dataArr['verificateCode'] = $verificateCode;
			$dataArr['verificateCode'] = $verificateCode;
			$dataArr['create_verificationCode_datetime'] = $quiry_date;
			return $this->db_insert('log_verification_code',$dataArr);
		}
		else
		{
			return false;
		}
	}
}
/* End of file Model_login.php */
/* Location: ./application/models/Model_login.php */
