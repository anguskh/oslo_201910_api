<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_bss02 extends MY_Model {
	
    function __construct() 
	{
        parent::__construct();
		// echo "in Model_column";
	}

	public function borrow_battery_success(){
		//計算執行時間
		$time_start = microtime(true);
		//寫入收到的資料到歷史紀錄
		$receive_data = $this->input->post();
		$api_name = 'c2af4999';
		$api_chinese_name = '借電成功';
		$api_log_sn = $this->model_common->insert_api_log($receive_data,$api_name,$api_chinese_name);
		// $output = implode(', ', array_map(
		//     function ($v, $k) { return sprintf("%s='%s'", $k, $v); },
		//     $this->input->post(),
		//     array_keys($this->input->post())
		// ));
		// $dataArr = array();
		// date_default_timezone_set('Asia/Taipei');
		// $logdataArr['api_name'] = 'bss02';
		// $logdataArr['receive_data'] = $output;
		// $logdataArr['receive_ip'] = $_SERVER['REMOTE_ADDR'];
		// $logdataArr['receive_date'] = "now()";
		// $this->db_insert("log_api_history",$logdataArr);

		$postjson = $this->input->post("JSONData");
		$returnArr = array();
		//先判斷是否為Json格式
		if($this->is_json($postjson))
		{
			//收到空資料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//資料為空
				$returnArr["rt_msg"] = "資料為空";//資料為空
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson);
				$brrow_date = addslashes($data_json_de->bs01);//借電站電池借出日期
				$bss_tokenID = addslashes($data_json_de->bs02);//借電站tokenID
				$User_ID = addslashes($data_json_de->bs03);//User_ID
				$trace_no = addslashes($data_json_de->bs04);//取出電池的軌道編號
				$battery_id = addslashes($data_json_de->bs05);//電池序號
				$SQLCmdM = "SELECT count(*) cnt  FROM tbl_member WHERE user_id = '{$User_ID}'";
				$rsM = $this->db_query($SQLCmdM);
				if($rsM[0]['cnt'] == 0 || $User_ID=="")
				{
					// $dataArrM['member_type'] = 'B';
					// $dataArrM['member_register'] = 'P';
					// $dataArrM['name'] = '系統自動寫入';
					// $dataArrM['mobile'] = $this->randString(10,'0123456789');
					// $key = md5('ebike'.$dataArrM['mobile']);
					// $key = substr($key, 0, 24);
					// $string = $this->randString(10);
					// //加密
					// $dataArrM['personal_id'] = $this->encrypt($key, $string);
					// $dataArrM['user_id'] = $User_ID;
					// $dataArrM['gender'] = $this->randString(1,'MF');
					// $dataArrM['nationality'] = '158';
					// $dataArrM['pay_type'] = 'W';
					// $dataArrM['stored_type'] = 'N';
					// $dataArrM['lease_status'] = 'N';
					// $dataArrM['status'] = 'Y';
					// $dataArrM['create_user'] = '1';
					// $dataArrM['create_date'] = 'now()';
					// $dataArrM['create_ip'] = '127.0.0.1';
					// $this->db_insert('tbl_member',$dataArrM);
					if(substr($User_ID,0,3)=='MOD')
					{
						$returnArr["rt_cd"] = "0007";//更新失敗
						$returnArr["rt_msg"] = "卡片QR有误";//
					}
					else
					{
						$returnArr["rt_cd"] = "0006";//更新失敗
						$returnArr["rt_msg"] = "会员ID有误";//
					}
					echo json_encode($returnArr);
					$time_end = microtime(true);
					$time = $time_end - $time_start;
					$otherdataArr['sys_spend_time'] = $time;
					$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
					return;
				}
				$otherdataArr['user_id'] = $User_ID;
				$otherdataArr['battery_id'] = $battery_id;
				$SQLCmd = "SELECT DorO_flag,do_num FROM tbl_battery WHERE battery_id = '{$battery_id}'";
				$rs = $this->db_query($SQLCmd);
				if($rs)
				{
					$dataArr['leave_DorO_flag'] = $rs[0]['DorO_flag'];
					$dataArr['leave_do_num'] = $rs[0]['do_num'];
				}
				else
				{
					$returnArr["rt_cd"] = "0004";//無法對應到電池資料
					$returnArr["rt_msg"] = "無法對應到電池資料";
				}

				$SQLCmdS = "SELECT s_num FROM tbl_battery_swap_station WHERE bss_token = '{$bss_tokenID}' AND status <> 'D'";
				$rsS = $this->db_query($SQLCmdS);
				if($rsS)
				{
					$SQLCmdM = "SELECT s_num,lease_type FROM tbl_member WHERE user_id = '{$User_ID}' AND status = 'Y'";
					$rsM = $this->db_query($SQLCmdM);
					$sb_num = $rsS[0]['s_num'];
					$tm_num = $rsM[0]['s_num'];
					$dataArr['tm_num'] = $tm_num;
					$dataArr['system_log_date'] = "now()";
					$dataArr['leave_sb_num'] = $sb_num;
					$dataArr['battery_user_id'] = $User_ID;
					$dataArr['leave_date'] = $brrow_date;
					$dataArr['leave_track_no'] = $trace_no;
					$dataArr['leave_battery_id'] = $battery_id;
					// $whereStr = "s_num = ( SELECT a.s_num FROM 
					// 			(SELECT lblr.s_num 
					// 			FROM log_battery_leave_return lblr 
					// 			LEFT JOIN tbl_battery_swap_station tbss ON lblr.leave_sb_num = tbss.s_num 
					// 			WHERE tbss.bss_token = '{$bss_tokenID}' 
					// 			AND lblr.battery_user_id = '{$User_ID}' 
					// 			AND leave_date IS NULL 
					// 			ORDER BY lblr.s_num DESC 
					// 			limit 1) a)";
					if($this->db_insert('log_battery_leave_return',$dataArr))
					{
						if(substr($User_ID,0,3)=='MOD')
						{
							if($rsM[0]['lease_type']==1)
								$SQLCmdM = "UPDATE tbl_member SET lease_status = 'L',change_count = change_count+1,lave_num = lave_num-1 WHERE user_id = '{$User_ID}' AND status <> 'D'";
							else
								$SQLCmdM = "UPDATE tbl_member SET lease_status = 'L',change_count = change_count+1 WHERE user_id = '{$User_ID}' AND status <> 'D'";
						}
						else
						{
							$SQLCmdM = "UPDATE tbl_member SET lease_status = 'L',change_count = change_count+1 WHERE user_id = '{$User_ID}' AND status <> 'D'";
						}
						
						$this->db_query($SQLCmdM); 
						if($rs)
						{
							$SQLCmdB = "UPDATE tbl_battery SET position = 'V',exchange_count = exchange_count+1 WHERE battery_id = '{$battery_id}' AND status <> 'D'";
							$this->db_query($SQLCmdB); 
						}
						$SQLCmdBS = "UPDATE tbl_battery_swap_station SET exchange_num = exchange_num+1 WHERE s_num = '{$sb_num}' AND status <> 'D'";
						$this->db_query($SQLCmdBS); 
						$returnArr["rt_cd"] = "0000";//成功
						$returnArr["rt_msg"] = "成功";//成功
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//更新失敗
						$returnArr["rt_msg"] = "更新失敗";//更新失敗
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//借電站token有誤
					$returnArr["rt_msg"] = "借電站token有誤";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式錯誤
			$returnArr["rt_msg"] = "格式錯誤";//格式錯誤
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	public function encrypt($source,$toencrypt){  
		//加密用的key   

		$key = $source;  

		//使用3DES方法加密   

		$encryptMethod = MCRYPT_TRIPLEDES; 

		//初始化向量来增加安全性

		$iv = mcrypt_create_iv(mcrypt_get_iv_size($encryptMethod,MCRYPT_MODE_ECB), MCRYPT_RAND);  

		//使用mcrypt_encrypt函数加密，MCRYPT_MODE_ECB表示使用ECB模式

		$encrypted_toencrypt = mcrypt_encrypt($encryptMethod, $key, $toencrypt, MCRYPT_MODE_ECB,$iv);   

		//回传解密后字串

		return base64_encode($encrypted_toencrypt);  

	}

	function randString($length, $charset='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789')
	{
		$str = '';
		$count = strlen($charset);
		while ($length--) {
			$str .= $charset[mt_rand(0, $count-1)];
		}
		return $str;
	}

	//比对借电电池ID API
	public function compare_leave_battery_id()
	{
		//計算執行時間
		$time_start = microtime(true);
		//寫入收到的資料到歷史紀錄
		$receive_data = $this->input->post();
		$api_name = 'c6be119b';
		$api_chinese_name = '比对借电电池ID';
		$api_log_sn = $this->model_common->insert_api_log($receive_data,$api_name,$api_chinese_name);
		// $output = implode(', ', array_map(
		//     function ($v, $k) { return sprintf("%s='%s'", $k, $v); },
		//     $this->input->post(),
		//     array_keys($this->input->post())
		// ));
		// $dataArr = array();
		// date_default_timezone_set('Asia/Taipei');
		// $logdataArr['api_name'] = 'bss02';
		// $logdataArr['receive_data'] = $output;
		// $logdataArr['receive_ip'] = $_SERVER['REMOTE_ADDR'];
		// $logdataArr['receive_date'] = "now()";
		// $this->db_insert("log_api_history",$logdataArr);

		$postjson = $this->input->post("JSONData");
		$returnArr = array();
		//先判斷是否為Json格式
		if($this->is_json($postjson))
		{
			//收到空資料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//資料為空
				$returnArr["rt_msg"] = "資料為空";//資料為空
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$bss_send_date = $data_json_de['cb01'];//租借站發送時間
				$bss_id = $data_json_de['cb02'];//bss序號
				$user_id = str_pad($data_json_de['cb03'],32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
				$battery_id = $data_json_de['cb04'];//電池序號
				$otherdataArr['user_id'] = $user_id;
				$otherdataArr['battery_id'] = $battery_id;
				$SQLCmdBS = "SELECT count(*) cnt FROM tbl_battery_swap_station WHERE bss_id = '{$bss_id}' AND status <> 'D'";
				$rsBs = $this->db_query($SQLCmdBS);
				if($rsBs[0]['cnt']!=0)
				{
					$SQLCmdM = "SELECT eng_staff FROM tbl_member WHERE user_id = '{$user_id}' AND status = 'Y'";
					$rsM = $this->db_query($SQLCmdM);
					if(count($rsM)!= 0)
					{
						$SQLCmd = "SELECT * FROM log_battery_leave_return WHERE battery_user_id = '{$user_id}' ORDER BY system_log_date DESC limit 1";
						$rs = $this->db_query($SQLCmd);
						if($rs)
						{
							if($rs[0]['return_date']!="")
							{
								//工程人員回覆E
								if($rsM[0]['eng_staff']==1)
								{
									$returnArr["rt_cd"] = "0000";//无法取得租借纪录
									$returnArr["rt_msg"] = "成功";
									$returnArr["rt_01"] = "E";
								}
								else
								{
									$returnArr["rt_cd"] = "0000";//无法取得租借纪录
									$returnArr["rt_msg"] = "成功";
									$returnArr["rt_01"] = "N";
								}
								
							}
							else
							{
								$leave_battery_id = $rs[0]['leave_battery_id'];
								if($leave_battery_id==$battery_id)
								{
									$returnArr["rt_cd"] = "0000";//成功
									$returnArr["rt_msg"] = "成功";//成功
									$returnArr["rt_01"] = "Y";//電池對應正確
								}
								else
								{
									$returnArr["rt_cd"] = "0000";//电池对应错误
									$returnArr["rt_msg"] = "成功";
									$returnArr["rt_01"] = "N";//電池對應錯誤
								}
							}
						}
						else
						{
							$returnArr["rt_cd"] = "0000";//第一次還電
							$returnArr["rt_msg"] = "成功";
							$returnArr["rt_01"] = "1";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0004";//借电站bssid有误
					$returnArr["rt_msg"] = "借电站bssid有误";
				}
				
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式錯誤
			$returnArr["rt_msg"] = "格式錯誤";//格式錯誤
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}
}

/* End of file Model_bss02.php */
