<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_ecu02 extends MY_Model {
	
    function __construct() 
	{
        parent::__construct();
		// echo "in Model_column";
	}

	public function savedrivingInfo(){
		//計算執行時間
		$time_start = microtime(true);
		//寫入收到的資料到歷史紀錄
		//將接收資料轉為16進制
		$receive_data = file_get_contents('php://input');
		// print_r($receive_data);
		// echo "<br><br>";
		$packArr = unpack("H*", $receive_data);
		$hexdata = $packArr[1];
		$hexdataStr = "";
		$mod = 0;
		$length = strlen($hexdata);
		$num = floor($length/64);
		// echo $hexdata."<br>";
		if($length%64 > 0)
		{
			$num = $num+1;
			$mod = 1;
		}
		
		for($i=0;$i<$num;$i++)
		{
			if($num!=0)
			{
				$hexdataStr .= chunk_split(substr($hexdata, $i*64,64),2," ")."\n";
			}
			
			if($i==$num)
			{
				if($mod == 1)
				{
					$hexdataStr .= chunk_split(substr($hexdata, ($i+1)*64,$length-($i+1)*64),2," ")."\n";
				}
			}
		}
		// echo $hexdataStr;
		// exit;
		//寫入收到的資料到歷史紀錄
		$output = implode(', ', array_map(
		    function ($v, $k) { return sprintf("%s='%s'", $k, $v); },
		    $this->input->post(),
		    array_keys($this->input->post())
		));
		$dataArr = array();
		date_default_timezone_set('Asia/Taipei');
		// $logdataArr['api_chinese_name'] = '车机回报';
		// $logdataArr['api_name'] = 'ecu02';
		// $logdataArr['receive_data'] = $output;
		// $logdataArr['receive_data_hex'] = $hexdataStr;
		// $logdataArr['receive_ip'] = $_SERVER['REMOTE_ADDR'];
		// $logdataArr['receive_date'] = "now()";

		$api_name = 'ecu02';
		$api_chinese_name = '车机回报';
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),$api_name,$api_chinese_name,$hexdataStr);

		// $this->db_insert("log_api_history",$logdataArr);

		$postjson = $this->input->post("JSONData");
		$dataArr['system_log_date'] = "now()";//系統時間
		$dataArr['allgetdata'] = $output;//傳送之全部資料
		$dataArr['receive_data_hex'] = $hexdataStr;
		//先判斷是否為Json格式
		$time = date("H:i:s");
		if($this->is_json($postjson))
		{
			//收到空資料
			$returnArr = array();
			$returnArr["time"] = $time;//系統時間
			if($postjson == "")
			{
				$this->db_insert("log_driving_info",$dataArr);
				$returnArr["return_code"] = "0003";//接收到空資料
				$returnArr["return_msg"]  = "資料為空";
				
			}
			else
			{
				$data_json_de = json_decode($postjson);

				$dataArr['driving_date'] = $data_json_de->di01;//ECU行車日期
				$dataArr['unit_id'] = $data_json_de->di02;//battery id
				$dataArr['latitude'] = addslashes($data_json_de->di07);//GPS緯度
				$dataArr['longitude'] = addslashes($data_json_de->di08);//GPS經度
				if(trim($dataArr['unit_id'])!="")
				{
					$SQLCmdB = "SELECT s_num FROM tbl_battery WHERE battery_id = '{$dataArr['unit_id']}' AND status <> 'D'";
					$rsB = $this->db_query($SQLCmdB);
					if(count($rsB)==0)
					{
						$this->db_insert("log_driving_info",$dataArr);
						$returnArr["return_code"] = "0005";//找不到對應的車輛資料
						$returnArr["return_msg"]  = "Battery ID 資料錯誤";
						echo json_encode($returnArr);
						$time_end = microtime(true);
						$time = $time_end - $time_start;
						$otherdataArr['sys_spend_time'] = $time;
						$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
						exit;
					}

					addslashes(substr($data_json_de->di14,0,1));//格式類別
					$dataArr['lease_status'] = addslashes(substr($data_json_de->di14,1,1));//租借狀態
					$dataArr['engine_status'] = addslashes(substr($data_json_de->di14,2,1));//啟動狀態
					$dataArr['battery_voltage'] = addslashes(substr($data_json_de->di14,3,4));//電池電壓
					$battery_amps_type = addslashes(substr($data_json_de->di14,7,1));//電池電流充放模式
					// if($battery_amps_type == "+")
					// {
					// 	$dataArr['battery_amps_type'] = "1";//充電
					// }
					// else
					// {
					// 	$dataArr['battery_amps_type'] = "0";//放電
					// }
					$dataArr['battery_amps_type'] = $battery_amps_type;
					$dataArr['battery_amps'] = addslashes(substr($data_json_de->di14,8,5));//電池電流
					$dataArr['battery_temperature'] = addslashes(substr($data_json_de->di14,21,2));//電池溫度
					$dataArr['environment_temperature'] = addslashes(substr($data_json_de->di14,23,2));//環境溫度
					$dataArr['battery_capacity'] = addslashes(substr($data_json_de->di14,25,3));//電池容量百分比
					// $dataArr['power_bank_voltage'] = addslashes(substr($data_json_de->di14,20,3));//PowerBank電壓
					addslashes(substr($data_json_de->di14,28,2));//保留欄位1
					addslashes(substr($data_json_de->di14,30,2));//保留欄位2

				}
				

				$dataArr['unit_name'] = addslashes($data_json_de->di03);//車機編號
				$SQLCmdV = "SELECT s_num,DorO_flag,do_num,dso_num,status FROM tbl_vehicle WHERE car_machine_id = '{$dataArr['unit_name']}' AND status <> 'D'";
				$rsV = $this->db_query($SQLCmdV);
				if(count($rsV)==0)
				{
					$this->db_insert("log_driving_info",$dataArr);
					$returnArr["return_code"] = "0004";//找不到對應的車輛資料
					$returnArr["return_msg"]  = "車機編號資料錯誤";
					echo json_encode($returnArr);
					$time_end = microtime(true);
					$time = $time_end - $time_start;
					$otherdataArr['sys_spend_time'] = $time;
					$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
					exit;
				}
				else
				{
					if(trim($dataArr['unit_id'])!="")
					{
						//更新電池資訊
						$dataArrB = array();
						$whereStrB = "";
						$whereStrB = "battery_id = '{$dataArr['unit_id']}' AND status <> 'D'";
						$dataArrB['sv_num'] = $rsV[0]['s_num'];
						$dataArrB['position'] = 'V';
						$dataArrB['battery_capacity'] = $dataArr['battery_capacity'];
						$dataArrB['latitude'] = $dataArr['latitude'];
						$dataArrB['longitude'] = $dataArr['longitude'];
						$dataArrB['update_date'] = "now()";
						$this->db_update('tbl_battery',$dataArrB,$whereStrB);
					}
				}

				// if($rsV[0]['status'] == 2)
				// {
				// 	$dataArr['DorO_flag'] = 'D';
				// }
				// else if($rsV[0]['status'] == 4)
				// {
				// 	$dataArr['DorO_flag'] = 'O';
				// }
				$dataArr['DorO_flag'] = $rsV[0]['DorO_flag'];
				$dataArr['do_num'] = $rsV[0]['do_num'];
				$dataArr['dso_num'] = $rsV[0]['dso_num'];
				$dataArr['tv_num'] = $rsV[0]['s_num'];

				$dataArr['unit_status'] = addslashes($data_json_de->di04);//M30狀態
				$dataArr['utc_date'] = addslashes($data_json_de->di05);//UTC日期
				$dataArr['utc_time'] = addslashes($data_json_de->di06);//UTC時間
				
				//更新車子經緯度
				$whereStr = "s_num = '{$dataArr['tv_num']}'";
				$dataArrB = array();
				$dataArrB['latitude'] = $dataArr['latitude'];
				$dataArrB['longitude'] = $dataArr['longitude'];
				$this->db_update("tbl_vehicle",$dataArrB,$whereStr);

				$dataArr['speed'] = addslashes($data_json_de->di09);//時速
				$dataArr['angle'] = addslashes($data_json_de->di10);//角度
				$dataArr['gps_satellite'] = addslashes($data_json_de->di11);//GPS衛星數
				$dataArr['event_id'] = addslashes($data_json_de->di12);//事件代碼
				$dataArr['return_type'] = addslashes($data_json_de->di13);//回報類型
				$dataArr['in_out_status'] = addslashes($data_json_de->di15);//I/O狀態
				$dataArr['log_type'] = 'D';//紀錄類型
				if($this->db_insert("log_driving_info",$dataArr))
				{
					$returnArr["return_code"] = "0000";//寫入DB成功
					$returnArr["return_msg"]  = "回報成功";
				}
				else
				{
					$returnArr["return_code"] = "0002";//寫入DB失敗
					$returnArr["return_msg"]  = "寫入DB失敗";
				}
			}
		}
		else
		{
			$this->db_insert("log_driving_info",$dataArr);
			$returnArr["return_code"] = "0001";//格式錯誤
			$returnArr["return_msg"] = "格式錯誤";
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	public function insertcar()
	{
		for($i=0;$i<=500;$i++)
		{
			$dataArr = array();
			$dataArr['status'] = '3';

			$car_machine_id = 'A0101'.str_pad(strtoupper(dechex($i)),5,'0',STR_PAD_LEFT);

			$dataArr['unit_id'] = $car_machine_id;
			$dataArr['vehicle_code'] = 'A0101'.str_pad($i,5,'0',STR_PAD_LEFT);
			$dataArr['manufacture_date'] = '2017-09-28';
			$dataArr['license_plate_number'] = 'ABC-'.str_pad($i,3,'0',STR_PAD_LEFT);
			$dataArr['car_machine_id'] = $car_machine_id;
			$dataArr['vehicle_type'] = 'B';
			$dataArr['use_type'] = 'N';
			$dataArr['vehicle_charge'] = 'S';
			$dataArr['charge_count'] = '0';
			$dataArr['create_date'] = '2017-09-28 10:00:12';
			$dataArr['create_ip'] = '127.0.0.1';
			$this->db_insert("tbl_vehicle",$dataArr);
			echo 'car_machine_id = '.$car_machine_id."<br>";
		}
		echo "新增成功";
	}
}

/* End of file Model_ecu01.php */
