<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_bss04 extends MY_Model {
	
    function __construct() 
	{
        parent::__construct();
		// echo "in Model_column";
	}

	public function save_battery_exchange_log(){
		//計算執行時間
		$time_start = microtime(true);
		//寫入收到的資料到歷史紀錄
		$receive_data = $this->input->post();
		$api_name = 'd7be6c29';
		$api_chinese_name = '借電請求查詢';
		$api_log_sn = $this->model_common->insert_api_log($receive_data,$api_name,$api_chinese_name);
		// $output = implode(', ', array_map(
		//     function ($v, $k) { return sprintf("%s='%s'", $k, $v); },
		//     $this->input->post(),
		//     array_keys($this->input->post())
		// ));
		// $dataArr = array();
		// date_default_timezone_set('Asia/Taipei');
		// $logdataArr['api_name'] = 'bss04';
		// $logdataArr['receive_data'] = $output;
		// $logdataArr['receive_ip'] = $_SERVER['REMOTE_ADDR'];
		// $logdataArr['receive_date'] = "now()";
		// $this->db_insert("log_api_history",$logdataArr);

		$postjson = $this->input->post("JSONData");
		//先判斷是否為Json格式
		if($this->is_json($postjson))
		{
			$returnArr = array();
			//收到空資料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//資料為空
				$returnArr['rt_msg'] = "資料為空";
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$upload_date = addslashes($data_json_de['bq01']);//交換站查詢日期
				$token_id = addslashes($data_json_de['bq02']);//Qrcode Token
				$bss_token_id = addslashes($data_json_de['bq03']);//BSS Token ID
				$no_battery = "";
				if(isset($data_json_de['bq04']))
				{
					$no_battery = addslashes($data_json_de['bq04']);
				}
				// $SQLCmd = "SELECT lblr.User_ID as User_ID,tm.name  
				// 			FROM take_battery_Info lblr 
				// 			LEFT JOIN tbl_member tm ON lblr.User_ID = tm.personal_id 
				// 			WHERE lblr.bss_token = '{$token_id}' AND lblr.notify IS NULL ORDER BY take_battery_sn DESC limit 1";
				// $rs = $this->db_query($SQLCmd);
				$SQLCmd = "SELECT tm.user_id,tm.name,lblr.s_num,tm.remaining_time,tm.lease_status   
							FROM tbl_member tm 
							LEFT JOIN log_battery_leave_return lblr ON tm.s_num = lblr.tm_num 
							WHERE lblr.qrcode_token = '{$token_id}' AND lblr.leave_date IS NULL";
				$rs = $this->db_query($SQLCmd);
				if($rs)
				{
					if($rs[0]['remaining_time'] >0)
					{
						//目前只能租借一顆電池,所以必須判斷租借狀態
						if($rs[0]['lease_status'] != 'L')
						{
							if($bss_token_id!="")
							{
								$SQLCmdB = "SELECT s_num FROM tbl_battery_swap_station WHERE bss_token = '{$bss_token_id}'";
								$rsB = $this->db_query($SQLCmdB);
								if($rsB)
								{
									$whereStr = "s_num = '{$rs[0]['s_num']}'";
									$dataArr['leave_sb_num'] = $rsB[0]['s_num'];
									$dataArr['no_battery'] = $no_battery;
									$this->db_update('log_battery_leave_return',$dataArr,$whereStr);
									$returnArr["rt_cd"] = "0000";//查詢成功
									$returnArr['rt_msg'] = "成功";
									if($no_battery == 1 || $no_battery == "")
									{
										$returnArr['br_01'] = $rs[0]['user_id'];
										$returnArr['br_02'] = $rs[0]['name'];
									}
								}
								else
								{
									$returnArr["rt_cd"] = "0004";//租借站token有誤
									$returnArr['rt_msg'] = "租借站token有誤";

								}
							}
							else
							{
								$returnArr["rt_cd"] = "0004";//租借站token有誤
								$returnArr['rt_msg'] = "租借站token有誤";
							}
						}
						else
						{
							$returnArr["rt_cd"] = "0006";//已經租借中
							$returnArr['rt_msg'] = "您已经租借中,不可再借!";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0005";//剩餘時間不足
						$returnArr["rt_msg"] = "剩余时间不足,请储值后再继续使用!";
					}
					
				}
				else
				{
					$returnArr["rt_cd"] = "0007";//查無資料
					$returnArr['rt_msg'] = "查無租借資料";
				}

			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式錯誤
			$returnArr['rt_msg'] = "格式錯誤";
		}

		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);

		if($returnArr["rt_cd"] == "0000")
		{
			$dataArr = array();
			$whereStr = "bss_token = '{$bss_token_id}' AND notify IS NULL";
			$dataArr['notify'] = "Y";
			$dataArr['notify_date'] = $upload_date;
			$this->db_update("take_battery_Info",$dataArr,$whereStr);
		}
	}

	//卡片次數查詢
	public function get_card_lave_num(){
		//計算執行時間
		$time_start = microtime(true);
		//寫入收到的資料到歷史紀錄
		$receive_data = $this->input->post();
		$api_name = 'e94335f6';
		$api_chinese_name = '卡片次數查詢';
		$api_log_sn = $this->model_common->insert_api_log($receive_data,$api_name,$api_chinese_name);
		// $output = implode(', ', array_map(
		//     function ($v, $k) { return sprintf("%s='%s'", $k, $v); },
		//     $this->input->post(),
		//     array_keys($this->input->post())
		// ));
		// $dataArr = array();
		// date_default_timezone_set('Asia/Taipei');
		// $logdataArr['api_name'] = 'bss04';
		// $logdataArr['receive_data'] = $output;
		// $logdataArr['receive_ip'] = $_SERVER['REMOTE_ADDR'];
		// $logdataArr['receive_date'] = "now()";
		// $this->db_insert("log_api_history",$logdataArr);

		$postjson = $this->input->post("JSONData");
		//先判斷是否為Json格式
		if($this->is_json($postjson))
		{
			$returnArr = array();
			//收到空資料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//資料為空
				$returnArr['rt_msg'] = "資料為空";
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$upload_date = addslashes($data_json_de['bc01']);//交換站查詢日期
				$qrcode = addslashes($data_json_de['bc02']);//Qrcode Token
				$bss_token_id = addslashes($data_json_de['bc03']);//BSS Token ID

				if(substr($qrcode,0,3)!='MOD')
				{
					$returnArr["rt_cd"] = "0007";
					$returnArr["rt_msg"] = "卡片QR有误";
					echo json_encode($returnArr);
					$time_end = microtime(true);
					$time = $time_end - $time_start;
					$otherdataArr['sys_spend_time'] = $time;
					$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
					exit;
				}

				$SQLCmd = "SELECT lave_num,lease_type     
							FROM tbl_member  
							WHERE user_id = '{$qrcode}' AND status = 'Y'";
				$rs = $this->db_query($SQLCmd);
				if($rs)
				{
					//計費方式,1:預購,2:結算
					if($rs[0]['lease_type']!=2)
					{
						if($rs[0]['lave_num'] >0)
						{
							if($bss_token_id!="")
							{
								$SQLCmdB = "SELECT s_num FROM tbl_battery_swap_station WHERE bss_token = '{$bss_token_id}' AND status <> 'D'";
								$rsB = $this->db_query($SQLCmdB);
								if($rsB)
								{
									$returnArr["rt_cd"] = "0000";//查詢成功
									$returnArr['rt_msg'] = "成功";
									$returnArr['cr_01'] = $rs[0]['lave_num'];
								}
								else
								{
									$returnArr["rt_cd"] = "0004";//租借站token有誤
									$returnArr['rt_msg'] = "租借站token有誤";

								}
							}
							else
							{
								$returnArr["rt_cd"] = "0004";//租借站token有誤
								$returnArr['rt_msg'] = "租借站token有誤";
							}
						}
						else
						{
							$returnArr["rt_cd"] = "0005";//剩餘時間不足
							$returnArr["rt_msg"] = "剩余次数不足,请加购后再继续使用!";
						}
					}
					else 
					{
						$returnArr["rt_cd"] = "0000";//查詢成功
						$returnArr['rt_msg'] = "成功";
						$returnArr['cr_01'] = 0;
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0007";//更新失敗
					$returnArr["rt_msg"] = "卡片QR有误";//
				}

			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式錯誤
			$returnArr['rt_msg'] = "格式錯誤";
		}

		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//取得BSS广告网址
	public function get_ad_url(){
		//計算執行時間
		$time_start = microtime(true);
		//寫入收到的資料到歷史紀錄
		$receive_data = $this->input->post();
		$api_name = 'e87dd345';
		$api_chinese_name = '取得BSS广告网址';
		$api_log_sn = $this->model_common->insert_api_log($receive_data,$api_name,$api_chinese_name);
		// $output = implode(', ', array_map(
		//     function ($v, $k) { return sprintf("%s='%s'", $k, $v); },
		//     $this->input->post(),
		//     array_keys($this->input->post())
		// ));
		// $dataArr = array();
		// date_default_timezone_set('Asia/Taipei');
		// $logdataArr['api_name'] = 'bss04';
		// $logdataArr['receive_data'] = $output;
		// $logdataArr['receive_ip'] = $_SERVER['REMOTE_ADDR'];
		// $logdataArr['receive_date'] = "now()";
		// $this->db_insert("log_api_history",$logdataArr);

		$postjson = $this->input->post("JSONData");
		//先判斷是否為Json格式
		if($this->is_json($postjson))
		{
			$returnArr = array();
			//收到空資料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//資料為空
				$returnArr['rt_msg'] = "資料為空";
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$send_date = addslashes($data_json_de['as01']);//交換站查詢日期
				$bss_token_id = addslashes($data_json_de['as02']);//BSS Token ID

				$SQLCmdB = "SELECT s_num FROM tbl_battery_swap_station WHERE bss_token = '{$bss_token_id}' AND status <> 'D'";
				$rsB = $this->db_query($SQLCmdB);
				if($rsB)
				{
					$returnArr["rt_cd"] = "0000";//查詢成功
					$returnArr['rt_msg'] = "成功";
					$sb_num = $rsB[0]['s_num'];
					$now_date = date("Y-m-d");
					$SQLCmdA = "SELECT tba.advertising_address,tbam.s_num,tbam.notify_date   
								FROM tbl_bss_advertising tba 
								LEFT JOIN tbl_bss_advertising_match tbam ON tba.s_num = tbam.advertising_num 
								WHERE tbam.sb_num = {$sb_num} AND '{$now_date}' >= tba.advertising_updating_date 
								ORDER BY tba.s_num DESC";
					$rsA = $this->db_query($SQLCmdA);
					if($rsA)
					{
						$returnArr['ar_01'] = $rsA[0]['advertising_address'];
						if($rsA[0]['notify_date']=="")
						{
							$match_num = $rsA[0]['s_num'];
							//更新通知時間
							$whereStrA = " s_num = {$match_num}";
							$dataArrA['notify_date'] = $send_date;
							$this->db_update("tbl_bss_advertising_match",$dataArrA,$whereStrA);
						}
					}
					else
					{
						$returnArr['ar_01'] = "";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0004";//租借站token有誤
					$returnArr['rt_msg'] = "租借站token有誤";

				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式錯誤
			$returnArr['rt_msg'] = "格式錯誤";
		}

		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}
}

/* End of file Model_bss02.php */
