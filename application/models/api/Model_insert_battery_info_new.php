<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_insert_battery_info_new extends MY_Model {
	
    function __construct() 
	{
        parent::__construct();
		// echo "in Model_column";
	}

	public function insert_battery_log(){
		//計算執行時間
		$time_start = microtime(true);
		//寫入收到的資料到歷史紀錄
		$SQLCmdA = "SELECT receive_data FROM log_api_history WHERE api_name = 'e9b2ed77'";
		$rsA = $this->db_query($SQLCmdA);
		// print_r($rsA);
		// exit;
		if($rsA)
		{
			foreach($rsA as $keyA => $valueA)
			{
				// echo $valueA['receive_data'];
				// echo "<br>";
				$receive_data = substr( $valueA['receive_data'] , 10,-1);
				// $jsonStr = json_decode($receive_data);
				// echo $receive_data;
				// echo "<br>";
				// var_dump($jsonStr);
				$api_name = "e9b2ed77";
				$api_chinese_name = '監控';
				// $api_log_sn = $this->model_common->insert_api_log($receive_data,$api_name,$api_chinese_name);
				// $output = implode(', ', array_map(
				//     function ($v, $k) { return sprintf("%s='%s'", $k, $v); },
				//     $this->input->post(),
				//     array_keys($this->input->post())
				// ));
				// $dataArr = array();
				// date_default_timezone_set('Asia/Taipei');
				// $logdataArr['api_name'] = 'bss01';
				// $logdataArr['receive_data'] = $output;
				// $logdataArr['receive_ip'] = $_SERVER['REMOTE_ADDR'];
				// $logdataArr['receive_date'] = "now()";
				// $this->db_insert("log_api_history",$logdataArr);

				// $anydata = $this->input->post();
				// print_r($anydata);
				$this->insertDB($receive_data);
			}
		}
		
	}

	public function insertbattery()
	{
		for($i=1;$i<=50;$i++)
		{
			$dataArr = array();
			$dataArr['DorO_flag'] = 'D';
			$dataArr['do_num'] = '3';
			if($i<10)
			{
				$battery_id = '0U0204680000'.$i;
			}
			else if($i<100)
			{
				$battery_id = '0U020468000'.$i;
			}
			$dataArr['battery_id'] = $battery_id;
			$dataArr['manufacture_date'] = '2017-09-01';
			$dataArr['position'] = 'B';
			$dataArr['exchange_count'] = 0;
			$dataArr['status'] = '0';
			$dataArr['create_user'] = 1;
			$dataArr['create_date'] = '2017-09-01 14:17:44';
			$dataArr['create_ip'] = '127.0.0.1';
			// $this->db_insert("tbl_battery",$dataArr);
			echo 'battery_id = '.$battery_id."<br>";
		}
		echo "新增成功";
	}

	public function checkccb($bccArr,$rsB,$time_start,$api_log_sn)
	{
		foreach($bccArr as $keyn => $valuen)
		{	
			if(trim($valuen) == "")
			{
				$returnArr["rt_cd"] = "3003";//雲端CCB ID與BSS不符
				$returnArr["rt_msg"] = "雲端CCB ID與BSS不符";
				echo json_encode($returnArr);
				$time_end = microtime(true);
				$time = $time_end - $time_start;
				$otherdataArr['sys_spend_time'] = $time;
				// $this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
				return false;
				exit;
			}
		}

		
		if(count($rsB)>0)
		{
			$dif_flag = false;
			if(count($rsB)!=count($bccArr))
			{
				$dif_flag = true;
			}
			else
			{
				foreach($rsB as $keybc => $valuebc)
				{
					if(!in_array($valuebc['ccb_id'], $bccArr) )
					{
						$dif_flag = true;
					}
				}
			}
			
			if($dif_flag)
			{
				$returnArr["rt_cd"] = "3003";//雲端CCB ID與BSS不符
				$returnArr["rt_msg"] = "雲端CCB ID與BSS不符";
				echo json_encode($returnArr);
				$time_end = microtime(true);
				$time = $time_end - $time_start;
				$otherdataArr['sys_spend_time'] = $time;
				// $this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
				return false;
				exit;
			}
		}
		else
		{
			$CBBStr = "'".implode("','",$bccArr)."'";
			$SQLCmdBC = "SELECT count(*) cnt FROM tbl_battery_swap_ccb WHERE ccb_id in ({$CBBStr}) AND status = 'Y'";
			$rsBC = $this->db_query($SQLCmdBC);
			if($rsBC[0]['cnt']>0)
			{
				$returnArr["rt_cd"] = "3003";//雲端CCB ID與BSS不符
				$returnArr["rt_msg"] = "雲端CCB ID與BSS不符";
				echo json_encode($returnArr);
				$time_end = microtime(true);
				$time = $time_end - $time_start;
				$otherdataArr['sys_spend_time'] = $time;
				// $this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
				return false;
				exit;
			}
			foreach($bccArr as $keyb => $valueb)
			{
				$bccdataArr = array();
				$bccdataArr['so_num'] = $so_num;
				$bccdataArr['sb_num'] = $sb_num;
				$bccdataArr['ccb_id'] = $valueb;
				$bccdataArr['status'] = 'Y';
				$bccdataArr['create_date'] = "now()";
				$bccdataArr['create_ip'] = $bss_ip;
				// $this->db_insert("tbl_battery_swap_ccb",$bccdataArr);
			}
		}
		return true;
	}

	public function insertDB($receive_data)
	{
		$postjson = $receive_data;
		//先判斷是否為Json格式
		if($this->is_json($postjson))
		{
			$returnArr = array();
			//收到空資料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//資料為空
				$returnArr["rt_msg"] = "資料為空";
			}
			else
			{
				$dataArr = array();
				$trackdataArr = array();
				$bssinfodataArr = array();
				$data_json_de = json_decode($postjson,true);
				$send_date = $data_json_de['em01'];//交換站發送日期
				$bss_id = $data_json_de['em02'];//BSS序號
				$bss_token_id = $data_json_de['em03']; //交換站Token ID
				$bss_ip = $data_json_de['em04']; //當前交換站IP
				$SQLCmd = "SELECT s_num, so_num, bss_token,exchange_num  FROM tbl_battery_swap_station WHERE bss_id = '{$bss_id}' and status = 'Y'";
				$rs = $this->db_query($SQLCmd);
				if(count($rs)==0)
				{
					$returnArr["rt_cd"] = "0004";//找不到對應的電池交換站資料
					$returnArr["rt_msg"] = "找不到對應的電池交換站資料";
					echo json_encode($returnArr);
					// $time_end = microtime(true);
					// $time = $time_end - $time_start;
					// $otherdataArr['sys_spend_time'] = $time;
					// $this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
					return;
				}
				$sb_num = $rs[0]['s_num'];
				$so_num = $rs[0]['so_num'];
				$bss_token = $rs[0]['bss_token'];
				$total_exchange_num = $rs[0]['exchange_num'];
				$new_bss_token = '';
				if($bss_token == ''){
					if($bss_token_id == ''){
						$new_bss_token = 'HolyCan'.$bss_id;
						$new_bss_token = md5($new_bss_token);
						$bss_token_id = $new_bss_token;
					}else{
						$returnArr["rt_cd"] = "3001";//Token ID不符合
						$returnArr["rt_msg"] = "Token ID不符合";
						echo json_encode($returnArr);
						// $time_end = microtime(true);
						// $time = $time_end - $time_start;
						// $otherdataArr['sys_spend_time'] = $time;
						// $this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
						return;
					}
				}else{
					if($bss_token_id == '')
					{
						$returnArr["bss_token"] = $bss_token;
					}
					else if($bss_token != $bss_token_id)
					{
						$returnArr["rt_cd"] = "3001";//Token ID不符合
						$returnArr["rt_msg"] = "Token ID不符合";
						echo json_encode($returnArr);
						// $time_end = microtime(true);
						// $time = $time_end - $time_start;
						// $otherdataArr['sys_spend_time'] = $time;
						// $this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
						return;
					}
				}
				
				// $blacklist_ver = $data_json_de->em05;//當前黑名單版本
				$bcc_id = $data_json_de['em05'];//借電站bcc序號

				$bccArr = explode(",",$bcc_id);
				// if(count($bccArr)<5)
				// {
				// 	$returnArr["rt_cd"] = "3003";//雲端CCB ID與BSS不符
				// 	$returnArr["rt_msg"] = "雲端CCB ID與BSS不符";
				// 	echo json_encode($returnArr);
				// 	$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE));
				// 	exit;
				// }

				$SQLCmdB = "SELECT ccb_id FROM tbl_battery_swap_ccb WHERE sb_num = {$sb_num} AND status = 'Y'";
				$rsB = $this->db_query($SQLCmdB);

				$SQLCmdCo = "SELECT config_set FROM sys_config WHERE config_code = 'check_ccb' AND status = '1'";
				$rsCo = $this->db_query($SQLCmdCo);
				if($rsCo)
				{
					if($rsCo[0]['config_set']=='N')
					{
						if(count($rsB)==0)
						{
							foreach($bccArr as $keyb => $valueb)
							{
								$bccdataArr = array();
								$bccdataArr['so_num'] = $so_num;
								$bccdataArr['sb_num'] = $sb_num;
								$bccdataArr['ccb_id'] = $valueb;
								$bccdataArr['status'] = 'Y';
								$bccdataArr['create_date'] = "now()";
								$bccdataArr['create_ip'] = $bss_ip;
								// $this->db_insert("tbl_battery_swap_ccb",$bccdataArr);
							}
						}
					}
					else
					{
						if(!$this->checkccb($bccArr,$rsB,$time_start,$api_log_sn))
						{
							return;
						}
					}
				}
				else
				{
					if(!$this->checkccb($bccArr,$rsB,$time_start,$api_log_sn))
					{
						return;
					}
				}
					

				$em_infoArr = $data_json_de['em_info'];

				// print_r($em_infoArr);
				// $bssinfodataArr['blacklist_ver'] = $blacklist_ver;
				$bssinfodataArr['log_date'] = $send_date;
				$bssinfodataArr['bss_token_id'] = $bss_token_id;
				$bssinfodataArr['bss_ip'] = $bss_ip;
				$bssinfodataArr['sb_num'] = $sb_num;
				$bssinfodataArr['so_num'] = $so_num;
				if(isset($data_json_de['em22']))
					$bssinfodataArr['exchange_num'] = $data_json_de['em22'];
				//寫入交換站資訊記錄表
				// $this->db_insert("log_bss_info_new",$bssinfodataArr);

				foreach($em_infoArr as $key => $data_json_de1)
				{
					// print_r($jsonvalue);
					$batteryInfoArr = array();
					$trackInfoArr = array();
					$trackdata = array();
					$batteryInfoArr['log_date'] = $send_date;//記錄時間
					$trackInfoArr['log_date'] = $send_date;//記錄時間
					$batteryInfoArr['track_no'] = addslashes($data_json_de1['em06']);//軌道編號
					$trackInfoArr['track_no'] = $batteryInfoArr['track_no'];//軌道編號
					$SQLCmd = "SELECT s_num,so_num,sb_num,status FROM tbl_battery_swap_track WHERE track_no = '{$trackInfoArr['track_no']}' and sb_num = '{$sb_num}' and status = 'Y'";
					$rs = $this->db_query($SQLCmd);
					if(count($rs) != 0)
					{
						$batteryInfoArr['DorO_flag'] = 'O';//營運商類型
						$batteryInfoArr['do_num'] = $rs[0]['so_num'];//營運商s_num
						$batteryInfoArr['sb_num'] = $rs[0]['sb_num'];//電池交換站s_num
						$trackInfoArr['so_num'] = $rs[0]['so_num'];//營運商s_num
						$trackInfoArr['sb_num'] = $rs[0]['sb_num'];//電池交換站s_num
						$trackInfoArr['status_track'] = addslashes($data_json_de1['em06']);//軌道狀態
						//電池交換軌道狀態不為停用時更新狀態
						if($rs[0]['status'] != 'N')
						{
							$trackstatusArr = explode(",", $trackInfoArr['status_track']);
							if(count($trackstatusArr) == 1 )
							{
								switch($trackInfoArr['status_track'])
								{
									case 1:
									case 2:
										$trackdata['status'] = 'Y';//啟用
										break;
									case 3:
									case 4:
									case 5:
									case 6:
									case 7:
									case 8:
									case 9:
										$trackdata['status'] = 'E';//故障
										break;
								}
							}
							else
							{
								$trackdata['status'] = 'E';//故障
							}
							
						}
						
						$trackInfoArr['status_led'] = addslashes($data_json_de1['em08']);//LED狀態
						$trackInfoArr['status_photo_sensor'] = addslashes($data_json_de1['em09']);//Photo Sensor狀態
						$trackInfoArr['status_bcu'] = addslashes($data_json_de1['em10']);//BCU狀態
						$trackInfoArr['status_battery_in_track'] = addslashes($data_json_de1['em11']);//電池置入狀態
						switch($trackInfoArr['status_battery_in_track'])
						{
							case 0:
								$trackdata['column_park'] = 'N';//沒電池
								break;
							case 1:
								$trackdata['column_park'] = 'Y';//有電池
								break;
						}
						$batteryInfoArr['battery_id'] = addslashes($data_json_de1['em12']);//電池序號
						$trackdata['battery_id'] = $batteryInfoArr['battery_id'];//電池序號
						$batteryInfoArr['vehicle_user_id'] = addslashes($data_json_de1['em13']);//車輛使用者序號
						$batteryInfoArr['status'] = addslashes($data_json_de1['em14']);//電池狀態
						//先判斷軌道狀態若為E則電池狀態為故障,接著判斷電池置入狀態若為0則狀態為無電池,最後才依照電池狀態填入
						if(isset($trackdata['status']))
						{
							switch($trackdata['status'])
							{
								case 'Y':
									{
										switch($trackInfoArr['status_battery_in_track'])
										{
											case 0://無電池
												$trackdata['column_charge'] = 'S';//無電池
												break;
											case 1://有電池
											{
												switch($batteryInfoArr['status'])
												{
													case 0://充電中
														$trackdata['column_charge'] = 'Y';//電池充電中
														break;
													case 1://飽電
														$trackdata['column_charge'] = 'N';//電池飽電
														break;
												}

												//有電池的情況下,將電池位置更改為在借電站
												$whereStrB = "battery_id = '{$trackdata['battery_id']}' AND status <> 'D'";
												$dataArrB['sv_num'] = NULL;
												$dataArrB['position'] = 'B';
												// $this->db_update('tbl_battery',$dataArrB,$whereStrB);
											}
												break;
										}
									}
									break;
								case 'E':
									$trackdata['column_charge'] = 'F';//電池故障
									break;
							}
						}
						$batteryInfoArr['battery_capacity'] = addslashes($data_json_de1['em15']);//電池容量百分比
						$batteryInfoArr['battery_temperature'] = addslashes($data_json_de1['em16']);//電池溫度
						$batteryInfoArr['battery_amps'] = addslashes($data_json_de1['em17']);//電池電流
						$batteryInfoArr['battery_voltage'] = addslashes($data_json_de1['em18']);//電池電壓
						$batteryInfoArr['charge_cycles'] = addslashes($data_json_de1['em19']);//充電次數
						$batteryInfoArr['electrify_time'] = addslashes($data_json_de1['em20']);//已充電時間
						$batteryInfoArr['battery_cell_status'] = addslashes($data_json_de1['em21']);//電池芯狀態

						if($this->db_insert("log_battery_info_new",$batteryInfoArr))
						{
							
							//更新電池交換軌道資料
							$whereStr = "s_num = {$rs[0]['s_num']}";
							$trackdata['update_date'] = "now()";
							// $this->db_update("tbl_battery_swap_track",$trackdata,$whereStr);

							$bss_array = array();
							$whereStr = "s_num = {$sb_num}";
							
							//更新交換站bss_token $sb_num
							if($new_bss_token != ''){
								$bss_array['bss_token'] = $new_bss_token;
							}

							if(isset($bssinfodataArr['exchange_num']))
								$bss_array['exchange_num'] = $total_exchange_num+$bssinfodataArr['exchange_num'];

							// print_r($bss_array);
							if($bss_array)
							{
								$bss_array['update_date'] = "now()";
								// $this->db_update("tbl_battery_swap_station",$bss_array,$whereStr);
							}
							
							//寫入軌道編號資訊
							// $this->db_insert("log_track_info",$trackInfoArr);
							$returnArr["rt_cd"] = "0000";//寫入DB成功
							$returnArr["rt_msg"] = "成功";
						}
						else
						{
							$returnArr["rt_cd"] = "0002";//寫入DB失敗
							$returnArr["rt_msg"] = "失敗";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0005";//格式錯誤
						$returnArr["rt_msg"] = "資料錯誤";
					}
				}

				if($returnArr["rt_cd"] == "0000"){
					if($new_bss_token!=''){
						$returnArr["bss_token"] = $new_bss_token;
					}
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式錯誤
			$returnArr["rt_msg"] = "格式錯誤";
		}

		echo json_encode($returnArr);
		// $time_end = microtime(true);
		// $time = $time_end - $time_start;
		// $otherdataArr['sys_spend_time'] = $time;
		// $this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	public function showdata()
	{
		$SQLCmd = "SELECT tbss.bss_id,lbi.log_date,lbin.battery_id,lbin.status,lbin.battery_capacity,lbin.battery_temperature,lbin.battery_amps,lbin.battery_voltage,lbin.charge_cycles,lbi.exchange_num
					FROM log_bss_info lbi 
					LEFT JOIN tbl_battery_swap_station tbss ON tbss.s_num = lbi.sb_num 
					LEFT JOIN log_battery_info_new lbin ON lbin.log_date =lbi.log_date 
					WHERE lbin.battery_id IS NOT NULL";
		$rs = $this->db_query($SQLCmd);
		print_r($rs);
	}
}

/* End of file Model_bss01.php */
