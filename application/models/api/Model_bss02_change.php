<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_bss02 extends MY_Model {
	
    function __construct() 
	{
        parent::__construct();
		// echo "in Model_column";
	}

	public function save_battery_exchange_log(){
		$postjson = $this->input->post("JSONData");
		//先判斷是否為Json格式
		if($this->is_json($postjson))
		{
			$returnArr = array();
			//收到空資料
			if($postjson == "")
			{
				$returnArr["return_code"] = "0003";//資料為空
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson);
				$upload_date = addslashes($data_json_de->upload_date);//BSS請求日期
				$bss_id = addslashes($data_json_de->bss_id);//BSS序號
				$SQLCmd = "SELECT s_num,so_num FROM tbl_battery_swap_station WHERE bss_id = '{$bss_id}'";
				$rs = $this->db_query($SQLCmd);
				if(count($rs)==0)
				{
					$returnArr["return_code"] = "0004";//找不到對應的電池交換站資料
					echo json_encode($returnArr);
					exit;
				}
				$dataArr['sb_num'] = $rs[0]['s_num'];
				$dataArr['so_num'] = $rs[0]['so_num'];
				$dataArr['exchange_type'] = addslashes($data_json_de->swap_type);//交換類別
				$dataArr['vehicle_user_id'] = addslashes($data_json_de->vehicle_user_id);//車輛使用者序號
				$dataArr['yes_track_no'] = addslashes($data_json_de->track_no1);//換出的軌道編號
				$dataArr['yes_battery_pack_sn'] = addslashes($data_json_de->battery_id1);//換出的電池序號
				$dataArr['no_track_no'] = addslashes($data_json_de->track_no2);//換回的軌道編號
				$dataArr['no_battery_pack_sn'] = addslashes($data_json_de->battery_id2);//換回的電池序號
				$dataArr['vehicle_code'] = addslashes($data_json_de->vehicle_code);//機車編號
				$dataArr['exchange_date'] = addslashes($data_json_de->exchange_date);//交換日期時間
				$whereStr = '';
				if($dataArr['vehicle_user_id'] != "")
				{
					$whereStr = "AND vehicle_user_id = '{$dataArr['vehicle_user_id']}'";
				}
				$SQLCmdD = "SELECT s_num FROM tbl_vehicle WHERE vehicle_code = '{$dataArr['vehicle_code']}' {$whereStr}";
				$rsD = $this->db_query($SQLCmdD);
				if(count($rsD)==0)
				{
					$returnArr["return_code"] = "0005";//找不到對應的車輛資料
					echo json_encode($returnArr);
					exit;
				}
				$dataArr['sv_num'] = $rsD[0]['s_num'];
				if($this->db_insert("log_battery_exchange",$dataArr))
				{
					$returnArr["return_code"] = "0000";//寫入成功
				}
				else
				{
					$returnArr["return_code"] = "0002";//寫入資料庫錯誤
				}
			}
		}
		else
		{
			$returnArr["return_code"] = "0001";//格式錯誤
		}
		echo json_encode($returnArr);
	}
}

/* End of file Model_bss02.php */
