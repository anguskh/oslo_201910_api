<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_clear_leavereturn_data extends MY_Model {
	
    function __construct() 
	{
        parent::__construct();
		// echo "in Model_column";
	}

	public function getmemberdata()
	{
		$SQLCmd = "SELECT user_id,name FROM tbl_member WHERE status <> 'D'";
		$rs = $this->db_query($SQLCmd);
		return $rs;
	}

	public function cleardata()
	{
		$SQLCmd = "DELETE FROM log_battery_leave_return WHERE battery_user_id = '4cd58d0f128a2d0ad2171f3a889095fa'";
		$this->db_query($SQLCmd);

		$whereStr = "user_id = '4cd58d0f128a2d0ad2171f3a889095fa'";
		$dataArr['lease_status'] = 'N';
		return $this->db_update("tbl_member",$dataArr,$whereStr);
	}

	public function clearTeodata()
	{
		$SQLCmd = "DELETE FROM log_battery_leave_return WHERE battery_user_id = '9824174ea670abcf19ff7be187f28c96'";
		$this->db_query($SQLCmd);

		$whereStr = "user_id = '9824174ea670abcf19ff7be187f28c96'";
		$dataArr['lease_status'] = 'N';
		return $this->db_update("tbl_member",$dataArr,$whereStr);
	}

	public function cleardataanydata()
	{
		$ckbSelArr = $this->input->post("ckbSelArr") ;
		$delIdStr = join( "','", $ckbSelArr ) ;
		// echo "delIdStr : $delIdStr" ;
		// exit;
		$whereStr = " battery_user_id in ('{$delIdStr}') ";
		$SQLCmd = "DELETE FROM log_battery_leave_return WHERE {$whereStr}";
		$this->db_query($SQLCmd);

		$whereStr = " user_id in ('{$delIdStr}') ";
		$dataArr['lease_status'] = 'N';
		$this->db_update("tbl_member",$dataArr,$whereStr);

		$this->redirect_alert("./", "清除成功") ;
	}
}

/* End of file Model_clear_leavereturn_data.php */
