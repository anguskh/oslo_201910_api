<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_bss08 extends MY_Model {
	
    function __construct() 
	{
        parent::__construct();
		// echo "in Model_column";
	}

	//建立交易 API
	public function create_transaction()
	{
		//計算執行時間
		$time_start = microtime(true);
		//寫入收到的資料到歷史紀錄
		$receive_data = $this->input->post();
		$api_name = 'c6be119b';
		$api_chinese_name = '建立交易';
		$api_log_sn = $this->model_common->insert_api_log($receive_data,$api_name,$api_chinese_name);
		// $output = implode(', ', array_map(
		//     function ($v, $k) { return sprintf("%s='%s'", $k, $v); },
		//     $this->input->post(),
		//     array_keys($this->input->post())
		// ));
		// $dataArr = array();
		// date_default_timezone_set('Asia/Taipei');
		// $logdataArr['api_name'] = 'bss02';
		// $logdataArr['receive_data'] = $output;
		// $logdataArr['receive_ip'] = $_SERVER['REMOTE_ADDR'];
		// $logdataArr['receive_date'] = "now()";
		// $this->db_insert("log_api_history",$logdataArr);

		$postjson = $this->input->post("JSONData");
		$returnArr = array();
		//先判斷是否為Json格式
		if($this->is_json($postjson))
		{
			//收到空資料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//資料為空
				$returnArr["rt_msg"] = "資料為空";//資料為空
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$bss_send_date = $data_json_de['ct01'];//租借站發送時間
				$bss_id = $data_json_de['ct02'];//bss序號
				if($data_json_de['ct03']!="")
					$user_id = str_pad($data_json_de['ct03'],32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
				else
					$user_id = "";
				$trans_token = $data_json_de['ct04'];//交易token
				$otherdataArr['user_id'] = $user_id;
				$SQLCmdBS = "SELECT count(*) cnt FROM tbl_battery_swap_station WHERE bss_id = '{$bss_id}' AND status <> 'D'";
				$rsBs = $this->db_query($SQLCmdBS);
				if($rsBs[0]['cnt']!=0)
				{
					$SQLCmdM = "SELECT s_num,eng_staff,member_type,stored_type,stored_value,payment_plan FROM tbl_member WHERE user_id = '{$user_id}' AND status = 'Y'";
					$rsM = $this->db_query($SQLCmdM);
					if(count($rsM)!= 0)
					{
						$whereStr = "";
						if($rsM[0]['payment_plan']!="")
						{
							if($rsM[0]['payment_plan']=="M")
							{
								$returnArr["rt_cd"] = "0000";//成功
								$returnArr["rt_msg"] = "成功";
								$returnArr["rt_01"] = "M";//會員類別(月費)
							}
							else
							{
								$returnArr["rt_cd"] = "0000";//成功
								$returnArr["rt_msg"] = "成功";
								$returnArr["rt_01"] = "O";//會員類別(非月費)
								$SQLCmdN = "SELECT count(*) time FROM log_battery_leave_return WHERE battery_user_id = '{$user_id}' AND return_date IS NOT NULL AND SUBSTRING(return_date,1,10) = CURDATE()";
								$rsN = $this->db_query($SQLCmdN);
								if($rsN)
								{
									$returnArr["rt_02"] = $rsN[0]['time'];//今日租借次數
									switch($rsN[0]['time'])
									{
										case 1:
											$amount = 5;
											break;
										case 2:
											$amount = 3;
											break;
										case 3:
											$amount = 2;
											break;
										default:
											$amount = 0;
											break;
									}
									$SQLCmdL = "SELECT s_num FROM log_battery_leave_return WHERE return_date IS NOT NULL ORDER BY return_date DESC";
									$rsL = $this->db_query($SQLCmdL);
									if($rsL)
									{
										$whereStrL = "s_num = {$rsL[0]['s_num']}";
										$dataArrL['charge_amount'] = $amount;
										$this->db_update("log_battery_leave_return",$dataArrL,$whereStrL);
									}
									if($rsM[0]['stored_type']=='A' && $rsM[0]['stored_value']>0 && $amount>0)
									{
										$whereStrM = "s_num = {$rsM[0]['s_num']}";
										$dataArrM['stored_value'] = (int)$rsM[0]['stored_value'] - $amount;
										$dataArrM['update_date'] = "now()";
										$this->db_update('tbl_member',$dataArrM,$whereStrM);
										$amount = 0;
									}
									$returnArr["rt_03"] = $amount;//需付款金額
								}
								else
								{
									$returnArr["rt_cd"] = "0007";//查无会员资料
									$returnArr["rt_msg"] = "查无上次租借纪录";
								}
							}
							// else
							// {
							// 	$returnArr["rt_cd"] = "0006";//查无会员资料
							// 	$returnArr["rt_msg"] = "会员类别错误";
							// }
						}
						else
						{
							$returnArr["rt_cd"] = "0005";//查无会员资料
							$returnArr["rt_msg"] = "查无会员类别";
						}

					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0004";//借电站bssid有误
					$returnArr["rt_msg"] = "借电站bssid有误";
				}
				
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式錯誤
			$returnArr["rt_msg"] = "格式錯誤";//格式錯誤
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

}

/* End of file Model_bss02.php */
