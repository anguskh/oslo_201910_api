<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_bss04 extends MY_Model {
	
    function __construct() 
	{
        parent::__construct();
		// echo "in Model_column";
	}

	public function save_battery_exchange_log(){
		//計算執行時間
		$time_start = microtime(true);
		//寫入收到的資料到歷史紀錄
		$receive_data = $this->input->post();
		$api_name = 'd7be6c29';
		$api_chinese_name = '借電請求查詢';
		$api_log_sn = $this->model_common->insert_api_log($receive_data,$api_name,$api_chinese_name);
		// $output = implode(', ', array_map(
		//     function ($v, $k) { return sprintf("%s='%s'", $k, $v); },
		//     $this->input->post(),
		//     array_keys($this->input->post())
		// ));
		// $dataArr = array();
		// date_default_timezone_set('Asia/Taipei');
		// $logdataArr['api_name'] = 'bss04';
		// $logdataArr['receive_data'] = $output;
		// $logdataArr['receive_ip'] = $_SERVER['REMOTE_ADDR'];
		// $logdataArr['receive_date'] = "now()";
		// $this->db_insert("log_api_history",$logdataArr);

		$postjson = $this->input->post("JSONData");
		//先判斷是否為Json格式
		if($this->is_json($postjson))
		{
			$returnArr = array();
			//收到空資料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//資料為空
				$returnArr['rt_msg'] = "資料為空";
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$upload_date = addslashes($data_json_de['bq01']);//交換站查詢日期
				$token_id = addslashes($data_json_de['bq02']);//Qrcode Token
				$bss_token_id = addslashes($data_json_de['bq03']);//BSS Token ID
				$no_battery = "";
				if(isset($data_json_de['bq04']))
				{
					$no_battery = addslashes($data_json_de['bq04']);
				}
				// $SQLCmd = "SELECT lblr.User_ID as User_ID,tm.name  
				// 			FROM take_battery_Info lblr 
				// 			LEFT JOIN tbl_member tm ON lblr.User_ID = tm.personal_id 
				// 			WHERE lblr.bss_token = '{$token_id}' AND lblr.notify IS NULL ORDER BY take_battery_sn DESC limit 1";
				// $rs = $this->db_query($SQLCmd);
				$SQLCmd = "SELECT tm.user_id,tm.name,lblr.s_num,tm.remaining_time,tm.lease_status   
							FROM tbl_member tm 
							LEFT JOIN log_battery_leave_return lblr ON tm.s_num = lblr.tm_num 
							WHERE lblr.qrcode_token = '{$token_id}' AND lblr.leave_date IS NULL";
				$rs = $this->db_query($SQLCmd);
				if($rs)
				{
					if($rs[0]['remaining_time'] >0)
					{
						//目前只能租借一顆電池,所以必須判斷租借狀態
						if($rs[0]['lease_status'] != 'L')
						{
							if($bss_token_id!="")
							{
								$SQLCmdB = "SELECT s_num FROM tbl_battery_swap_station WHERE bss_token = '{$bss_token_id}'";
								$rsB = $this->db_query($SQLCmdB);
								if($rsB)
								{
									$whereStr = "s_num = '{$rs[0]['s_num']}'";
									$dataArr['leave_sb_num'] = $rsB[0]['s_num'];
									$dataArr['no_battery'] = $no_battery;
									$this->db_update('log_battery_leave_return',$dataArr,$whereStr);
									$returnArr["rt_cd"] = "0000";//查詢成功
									$returnArr['rt_msg'] = "成功";
									if($no_battery == 1 || $no_battery == "")
									{
										$returnArr['br_01'] = $rs[0]['user_id'];
										$returnArr['br_02'] = $rs[0]['name'];
									}
								}
								else
								{
									$returnArr["rt_cd"] = "0004";//租借站token有誤
									$returnArr['rt_msg'] = "租借站token有誤";

								}
							}
							else
							{
								$returnArr["rt_cd"] = "0004";//租借站token有誤
								$returnArr['rt_msg'] = "租借站token有誤";
							}
						}
						else
						{
							$returnArr["rt_cd"] = "0006";//已經租借中
							$returnArr['rt_msg'] = "您已经租借中,不可再借!";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0005";//剩餘時間不足
						$returnArr["rt_msg"] = "剩余时间不足,请储值后再继续使用!";
					}
					
				}
				else
				{
					$returnArr["rt_cd"] = "0007";//查無資料
					$returnArr['rt_msg'] = "查無租借資料";
				}

			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式錯誤
			$returnArr['rt_msg'] = "格式錯誤";
		}

		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);

		if($returnArr["rt_cd"] == "0000")
		{
			$dataArr = array();
			$whereStr = "bss_token = '{$bss_token_id}' AND notify IS NULL";
			$dataArr['notify'] = "Y";
			$dataArr['notify_date'] = $upload_date;
			$this->db_update("take_battery_Info",$dataArr,$whereStr);
		}
	}

	public function save_battery_exchange_log2(){
		//計算執行時間
		$time_start = microtime(true);
		//寫入收到的資料到歷史紀錄
		$receive_data = $this->input->post();
		$api_name = 'd6f6d2bc';
		$api_chinese_name = '付款確認查詢';
		$api_log_sn = $this->model_common->insert_api_log($receive_data,$api_name,$api_chinese_name);
		// $output = implode(', ', array_map(
		//     function ($v, $k) { return sprintf("%s='%s'", $k, $v); },
		//     $this->input->post(),
		//     array_keys($this->input->post())
		// ));
		// $dataArr = array();
		// date_default_timezone_set('Asia/Taipei');
		// $logdataArr['api_name'] = 'bss04';
		// $logdataArr['receive_data'] = $output;
		// $logdataArr['receive_ip'] = $_SERVER['REMOTE_ADDR'];
		// $logdataArr['receive_date'] = "now()";
		// $this->db_insert("log_api_history",$logdataArr);

		$postjson = $this->input->post("JSONData");
		$take_payment_sn = "";
		//先判斷是否為Json格式
		if($this->is_json($postjson))
		{
			$returnArr = array();
			//收到空資料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//資料為空
				$returnArr['rt_msg'] = "資料為空";
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$upload_date = addslashes($data_json_de['bq01']);//交換站查詢日期
				$bss_token_id = addslashes($data_json_de['bq02']);//BSS Token ID
				$qrcode_url = addslashes($data_json_de['bq03']);//QR Code URL
				$mdtoken = addslashes($data_json_de['bq04']);//魔力token
				
				if($bss_token_id!="")
				{
					// $SQLCmdB = "SELECT take_payment_sn,user_id,amount FROM take_payment_Info WHERE bss_token = '{$bss_token_id}' AND notify!=1";
					// $rsB = $this->db_query($SQLCmdB);
					// if(!$rsB)
					// {
					// 	//暫時先模擬已經付款成功
					// 	$dataArrT['payment_date'] = "now()";
					// 	$dataArrT['user_id'] = "00000000000000000000000000000616";
					// 	$dataArrT['bss_token'] = $bss_token_id;
					// 	$dataArrT['url_Info'] = $qrcode_url;
					// 	// $dataArrT['tm_num'] = 38;
					// 	$dataArrT['amount'] = "100";
					// 	$dataArrT['gps_location'] = "30.905373,121.817284";
					// 	$dataArrT['notify'] = 0;
					// 	$this->db_insert("take_payment_Info",$dataArrT);
					// }
					
					$SQLCmdB = "SELECT take_payment_sn,user_id,amount,order_status FROM take_payment_Info WHERE bss_token = '{$bss_token_id}' AND url_Info = '{$qrcode_url}' AND notify IS NULL AND mdtoken = '{$mdtoken}' ORDER BY payment_date DESC";
					$rsB = $this->db_query($SQLCmdB);
					if($rsB)
					{
						$user_id = $rsB[0]['user_id'];
						$SQLCmdM = "SELECT user_id,name FROM tbl_member WHERE user_id = '{$user_id}' AND status <> 'D'";
						$rsM = $this->db_query($SQLCmdM);
						if($rsM)
						{
							$SQLCmdB2 = "SELECT wechat_orderSn FROM log_battery_leave_return WHERE battery_user_id = '{$user_id}' ORDER BY system_log_date DESC";
							$rsB2 = $this->db_query($SQLCmdB2);
							//查詢是否已經有租借紀錄
							if(count($rsB2)>1)//原本已經有過還電紀錄的情況
							{
								$wechat_orderSn = $rsB2[1]['wechat_orderSn'];
								$SQLCmdB3 = "SELECT take_payment_sn,user_id,amount,order_status FROM take_payment_Info WHERE user_id = '{$user_id}' AND orderSn = '{$wechat_orderSn}' ORDER BY payment_date DESC";
								$rsB3 = $this->db_query($SQLCmdB3);
								if($rsB3)
								{
									$take_payment_sn = $rsB3[0]['take_payment_sn'];
									$whereStr = "take_payment_sn = {$take_payment_sn}";
									$dataArr['notify'] = 1;
									$dataArr['notify_date'] = $upload_date;
									$this->db_update('take_payment_Info',$dataArr,$whereStr);
									// if($rsB3[0]['amount']!="" && $rsB3[0]['order_status']==2)
									// {
	
										$returnArr["rt_cd"] = "0000";//查詢成功
										$returnArr['rt_msg'] = "成功";

										$returnArr['br_01'] = substr($rsM[0]['user_id'], -8);
										$returnArr['br_02'] = $rsM[0]['name'];
									// }
									// else
									// {
										//將查詢為尚未視為離線交易,不用付款
										// $returnArr["rt_cd"] = "0000";//查詢成功
										// $returnArr['rt_msg'] = "成功";

										// $returnArr['br_01'] = substr($rsM[0]['user_id'], -8);
										// $returnArr['br_02'] = $rsM[0]['name'];
										// $returnArr["rt_cd"] = "0002";//尚未付款
										// $returnArr['rt_msg'] = "尚未付款";
									// }
								}
								else
								{
									//將查詢不到付款紀錄視為離線交易,不用付款
									$returnArr["rt_cd"] = "0000";//查詢成功
									$returnArr['rt_msg'] = "成功";

									$returnArr['br_01'] = substr($rsM[0]['user_id'], -8);
									$returnArr['br_02'] = $rsM[0]['name'];
									// $returnArr["rt_cd"] = "0004";//查詢不到付款紀錄
									// $returnArr['rt_msg'] = "查询不到付款纪录";
								}
							}
							else
							{
								$SQLCmdM = "SELECT user_id,name FROM tbl_member WHERE user_id = '{$user_id}' AND status <> 'D'";
								$rsM = $this->db_query($SQLCmdM);
								if($rsM)
								{
									$returnArr["rt_cd"] = "0000";//查詢成功
									$returnArr['rt_msg'] = "成功";
									$returnArr['br_01'] = substr($rsM[0]['user_id'], -8);
									$returnArr['br_02'] = $rsM[0]['name'];
								}
								else
								{
									$returnArr["rt_cd"] = "0005";//查无会员资料
									$returnArr['rt_msg'] = "查无会员资料";
								}
							}
						}
						else
						{
							$returnArr["rt_cd"] = "0005";//查无会员资料
							$returnArr['rt_msg'] = "查无会员资料";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0004";//查詢不到付款紀錄
						$returnArr['rt_msg'] = "查询不到付款纪录";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0004";//租借站token有誤
					$returnArr['rt_msg'] = "租借站token有誤";
				}
						

			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式錯誤
			$returnArr['rt_msg'] = "格式錯誤";
		}

		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);

		// if($returnArr["rt_cd"] == "0000" && $take_payment_sn!="")
		// {
		// 	$dataArr = array();
		// 	$whereStr = "take_payment_sn = {$take_payment_sn}";
		// 	$dataArr['notify'] = "Y";
		// 	$dataArr['notify_date'] = $upload_date;
		// 	$this->db_update("take_payment_Info",$dataArr,$whereStr);
		// }
	}

	//掃碼確認查詢API
	public function quiniryScan(){
		//計算執行時間
		$time_start = microtime(true);
		//寫入收到的資料到歷史紀錄
		$receive_data = $this->input->post();
		$api_name = 'e9b229f6';
		$api_chinese_name = '掃碼確認查詢';
		$api_log_sn = $this->model_common->insert_api_log($receive_data,$api_name,$api_chinese_name);

		$postjson = $this->input->post("JSONData");
		$take_payment_sn = "";
		//先判斷是否為Json格式
		if($this->is_json($postjson))
		{
			$returnArr = array();
			//收到空資料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//資料為空
				$returnArr['rt_msg'] = "資料為空";
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$upload_date = addslashes($data_json_de['qs01']);//交換站查詢日期
				$bss_token_id = addslashes($data_json_de['qs02']);//BSS Token ID
				$user_id = addslashes($data_json_de['qs03']);//userID
				if($user_id!="")
					$User_ID = str_pad(addslashes($user_id),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
				else
					$User_ID = "";
				$transtoken = addslashes($data_json_de['qs04']);//交易token

				if($bss_token_id!="")
				{
					$SQLCmdBSS = "SELECT s_num FROM tbl_battery_swap_station WHERE bss_token = '{$bss_token_id}'";
					$rsBSS = $this->db_query($SQLCmdBSS);
					if(count($rsBSS))
					{
						$SQLCmdLR = "SELECT scanQR FROM log_battery_leave_return WHERE identifier_code = '{$transtoken}' AND user_id = '{$User_ID}' AND leave_date IS NOT NULL";
						$rsLR = $this->db_query($SQLCmdLR);
						if($rsLR)
						{
							if($rsLR[0]['scanQR']=="Y")
							{
								$returnArr["rt_cd"] = "0000";//成功
								$returnArr['rt_msg'] = "成功";
							}
							else
							{
								$returnArr["rt_cd"] = "0004";//查询不到掃碼纪录
								$returnArr['rt_msg'] = "查询不到掃碼纪录";
							}
						}
						else
						{
							$returnArr["rt_cd"] = "0006";//查詢不到交易紀錄
							$returnArr['rt_msg'] = "查询不到交易纪录";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0005";//租借站token有誤
						$returnArr['rt_msg'] = "租借站token有誤";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//租借站token有誤
					$returnArr['rt_msg'] = "租借站token有誤";
				}
						

			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式錯誤
			$returnArr['rt_msg'] = "格式錯誤";
		}

		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);

		// if($returnArr["rt_cd"] == "0000" && $take_payment_sn!="")
		// {
		// 	$dataArr = array();
		// 	$whereStr = "take_payment_sn = {$take_payment_sn}";
		// 	$dataArr['notify'] = "Y";
		// 	$dataArr['notify_date'] = $upload_date;
		// 	$this->db_update("take_payment_Info",$dataArr,$whereStr);
		// }
	}

	//取得BSS广告网址
	public function get_ad_url(){
		//計算執行時間
		$time_start = microtime(true);
		//寫入收到的資料到歷史紀錄
		$receive_data = $this->input->post();
		$api_name = 'e87dd345';
		$api_chinese_name = '取得BSS广告网址';
		$api_log_sn = $this->model_common->insert_api_log($receive_data,$api_name,$api_chinese_name);
		// $output = implode(', ', array_map(
		//     function ($v, $k) { return sprintf("%s='%s'", $k, $v); },
		//     $this->input->post(),
		//     array_keys($this->input->post())
		// ));
		// $dataArr = array();
		// date_default_timezone_set('Asia/Taipei');
		// $logdataArr['api_name'] = 'bss04';
		// $logdataArr['receive_data'] = $output;
		// $logdataArr['receive_ip'] = $_SERVER['REMOTE_ADDR'];
		// $logdataArr['receive_date'] = "now()";
		// $this->db_insert("log_api_history",$logdataArr);

		$postjson = $this->input->post("JSONData");
		//先判斷是否為Json格式
		if($this->is_json($postjson))
		{
			$returnArr = array();
			//收到空資料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//資料為空
				$returnArr['rt_msg'] = "資料為空";
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$send_date = addslashes($data_json_de['as01']);//交換站查詢日期
				$bss_token_id = addslashes($data_json_de['as02']);//BSS Token ID

				$SQLCmdB = "SELECT s_num FROM tbl_battery_swap_station WHERE bss_token = '{$bss_token_id}' AND status <> 'D'";
				$rsB = $this->db_query($SQLCmdB);
				if($rsB)
				{
					$returnArr["rt_cd"] = "0000";//查詢成功
					$returnArr['rt_msg'] = "成功";
					$sb_num = $rsB[0]['s_num'];
					$now_date = date("Y-m-d");
					$SQLCmdA = "SELECT tba.advertising_address,tbam.s_num,tbam.notify_date   
								FROM tbl_bss_advertising tba 
								LEFT JOIN tbl_bss_advertising_match tbam ON tba.s_num = tbam.advertising_num 
								WHERE tbam.sb_num = {$sb_num} AND '{$now_date}' >= tba.advertising_updating_date 
								ORDER BY tba.s_num DESC";
					$rsA = $this->db_query($SQLCmdA);
					if($rsA)
					{
						$returnArr['ar_01'] = $rsA[0]['advertising_address'];
						if($rsA[0]['notify_date']=="")
						{
							$match_num = $rsA[0]['s_num'];
							//更新通知時間
							$whereStrA = " s_num = {$match_num}";
							$dataArrA['notify_date'] = $send_date;
							$this->db_update("tbl_bss_advertising_match",$dataArrA,$whereStrA);
						}
					}
					else
					{
						$returnArr['ar_01'] = "";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0004";//租借站token有誤
					$returnArr['rt_msg'] = "租借站token有誤";

				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式錯誤
			$returnArr['rt_msg'] = "格式錯誤";
		}

		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//查詢預約電池
	public function searchReserve()
	{
		//計算執行時間
		$time_start = microtime(true);
		//寫入收到的資料到歷史紀錄
		$receive_data = $this->input->post();
		$api_name = 'b2d60436';
		$api_chinese_name = '查询预约电池';
		$api_log_sn = $this->model_common->insert_api_log($receive_data,$api_name,$api_chinese_name);
		// $output = implode(', ', array_map(
		//     function ($v, $k) { return sprintf("%s='%s'", $k, $v); },
		//     $this->input->post(),
		//     array_keys($this->input->post())
		// ));
		// $dataArr = array();
		// date_default_timezone_set('Asia/Taipei');
		// $logdataArr['api_name'] = 'bss04';
		// $logdataArr['receive_data'] = $output;
		// $logdataArr['receive_ip'] = $_SERVER['REMOTE_ADDR'];
		// $logdataArr['receive_date'] = "now()";
		// $this->db_insert("log_api_history",$logdataArr);

		$postjson = $this->input->post("JSONData");
		//先判斷是否為Json格式
		if($this->is_json($postjson))
		{
			$returnArr = array();
			//收到空資料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//資料為空
				$returnArr['rt_msg'] = "資料為空";
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$send_date = addslashes($data_json_de['sr01']);//交換站查詢日期
				$bss_id = addslashes($data_json_de['sr02']);//BSS ID

				$SQLCmdB = "SELECT s_num FROM tbl_battery_swap_station WHERE bss_id = '{$bss_id}' AND status <> 'D'";
				$rsB = $this->db_query($SQLCmdB);
				if($rsB)
				{
					$SQLCmdS = "SELECT config_set FROM sys_config WHERE config_code = 'reserve_battery_time' AND status = '1'";
					$rsS = $this->db_query($SQLCmdS);
					$reserve_min = 10;
					if($rsS)
						$reserve_min = $rsS[0]['config_set'];
					$sb_num = $rsB[0]['s_num'];
					$SQLCmd = "SELECT tb.battery_id,tb.reserveUserID,({$reserve_min}-TIMESTAMPDIFF(MINUTE,tb.reserveDate,now())) as remaining_time   
								FROM tbl_battery_swap_station tbss  
								LEFT JOIN tbl_battery_swap_track tbst ON tbst.sb_num = tbss.s_num 
								LEFT JOIN tbl_battery tb ON tb.battery_id = tbst.battery_id 
								WHERE tbss.s_num = {$sb_num} AND tbst.battery_id !='' AND TIMESTAMPDIFF(MINUTE,tb.reserveDate,now())<={$reserve_min} AND tb.reserveUserID!='' AND tb.reserveUserID IS NOT NULL";
					$rs = $this->db_query($SQLCmd);
					if($rs)
					{
						$returnArr["rt_cd"] = "0000";//查詢成功
						$returnArr['rt_msg'] = "成功";
						foreach($rs as $key => $value)
						{
							$returnArr["em_info"][$key]['battery_id'] = $value['battery_id'];//預約電池序號
							$returnArr["em_info"][$key]['reserveUserID'] = substr($value['reserveUserID'], -8);//預約userID
							$returnArr["em_info"][$key]['remaining_time'] = $value['remaining_time'];//預約剩餘時間
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0005";//查無預約電池
						$returnArr['rt_msg'] = "查无预约电池";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0004";//租借站token有誤
					$returnArr['rt_msg'] = "租借站序号有误";

				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式錯誤
			$returnArr['rt_msg'] = "格式錯誤";
		}

		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//確認驗證碼
	public function check_verificationcode()
	{
		//計算執行時間
		$time_start = microtime(true);
		//寫入收到的資料到歷史紀錄
		$receive_data = $this->input->post();
		$api_name = 'd8f2e5432';
		$api_chinese_name = '確認驗證碼';
		$api_log_sn = $this->model_common->insert_api_log($receive_data,$api_name,$api_chinese_name);

		$postjson = $this->input->post("JSONData");
		//先判斷是否為Json格式
		if($this->is_json($postjson))
		{
			$returnArr = array();
			//收到空資料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//資料為空
				$returnArr['rt_msg'] = "資料為空";
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$send_date = addslashes($data_json_de['cv01']);//確認日期
				$bss_id = addslashes($data_json_de['cv02']);//BSS ID
				$verification_code = addslashes($data_json_de['cv03']);//驗證碼

				$SQLCmdB = "SELECT s_num FROM tbl_battery_swap_station WHERE bss_id = '{$bss_id}' AND status <> 'D'";
				$rsB = $this->db_query($SQLCmdB);
				if($rsB)
				{
					$SQLCmdV = "SELECT lvc.user_id,tm.name  
								FROM log_verification_code lvc 
								LEFT JOIN tbl_member tm ON lvc.user_id = tm.user_id 
								WHERE tm.status <> 'D' AND lvc.bss_id = '{$bss_id}' AND lvc.verificateCode = '{$verification_code}' AND lvc.notification = '0' ORDER by lvc.s_num";
					$rsV = $this->db_query($SQLCmdV);
					if($rsV)
					{
						$whereStr = " bss_id = '{$bss_id}' AND verificateCode = '{$verification_code}' AND notification = '0'";
						$dataArr['notification'] = '1';
						$dataArr['notification_datetime'] = $send_date;
						if($this->db_update('log_verification_code',$dataArr,$whereStr))
						{
							$returnArr["rt_cd"] = "0000";//查詢成功
							$returnArr['rt_msg'] = "成功";
							$returnArr['br_01'] = substr($rsV[0]['user_id'], -8);//user ID
							$returnArr['br_02'] = $rsV[0]['name'];//user Name
						}
						else
						{
							$returnArr["rt_cd"] = "0005";//DB寫入失敗
							$returnArr["rt_msg"] = "DB寫入失敗";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0006";//驗證碼錯誤
						$returnArr['rt_msg'] = "驗證碼錯誤";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0004";//租借站token有誤
					$returnArr['rt_msg'] = "租借站序号有误";

				}
			}	
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式錯誤
			$returnArr['rt_msg'] = "格式錯誤";
		}

		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

}

/* End of file Model_bss02.php */
