<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_bss03 extends MY_Model {
	
	const x_PI  = 52.35987755982988;
    const PI  = 3.1415926535897932384626;
    const a = 6378245.0;
    const ee = 0.00669342162296594323;

    function __construct() 
	{
        parent::__construct();
		// echo "in Model_column";
		define('PI',3.1415926535898);
		define('EARTH_RADIUS',6378.137);
	}

	public function get_return_qrcode(){
		//計算執行時間
		$time_start = microtime(true);
		//寫入收到的資料到歷史紀錄
		$receive_data = $this->input->post();
		$api_name = 'c6cbfe3b';
		$api_chinese_name = '還電取碼';
		$api_log_sn = $this->model_common->insert_api_log($receive_data,$api_name,$api_chinese_name);

		// $output = implode(', ', array_map(
		//     function ($v, $k) { return sprintf("%s='%s'", $k, $v); },
		//     $this->input->post(),
		//     array_keys($this->input->post())
		// ));
		// $dataArr = array();
		// date_default_timezone_set('Asia/Taipei');
		// $logdataArr['api_name'] = 'bss03';
		// $logdataArr['receive_data'] = $output;
		// $logdataArr['receive_ip'] = $_SERVER['REMOTE_ADDR'];
		// $logdataArr['receive_date'] = "now()";
		// $this->db_insert("log_api_history",$logdataArr);

		$postjson = $this->input->post("JSONData");
		//先判斷是否為Json格式
		if($this->is_json($postjson))
		{
			$returnArr = array();
			//收到空資料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//資料為空
				$returnArr["rt_msg"] = "資料為空";//回應訊息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$return_requeast_date = addslashes($data_json_de['qr01']);//還電取碼請求日期
				$bss_token = addslashes($data_json_de['qr02']);//電池交換站Token ID
				$dataArr['bss_request_date'] = $return_requeast_date;
				$dataArr['bss_token'] = $bss_token;
				$this->db_insert('log_get_return_qrcode',$dataArr);
				if($this->model_common->checkdata($api_name,$return_requeast_date))
				{
					exit();
				}

				$SQLCmdB = "SELECT bss_id FROM tbl_battery_swap_station WHERE bss_token = '{$bss_token}' AND status <> 'D'";
				$rsB = $this->db_query($SQLCmdB);
				if($rsB)
				{
					//取得微信的網址參數
					$wechat_get_qrcode_url = "";
					$wechat_qrcode_url = "";
					$SQLCmdC = "SELECT config_code,config_set FROM sys_config WHERE config_code in ('wechat_get_qrcode_url','wechat_qrcode_url') AND status = 1";
					$rsC = $this->db_query($SQLCmdC);
					if($rsC)
					{
						foreach($rsC as $keyC => $valueC)
						{
							if($valueC['config_code'] == 'wechat_get_qrcode_url')
							{
								$wechat_get_qrcode_url = $valueC['config_set'];
							}

							if($valueC['config_code'] == 'wechat_qrcode_url')
							{
								$wechat_qrcode_url = $valueC['config_set'];
							}
						}
					}

					$bss_id = $rsB[0]['bss_id'];
					$mdtoken = $this->generatorToken();

					$returnArr["rt_cd"] = "0000";//成功
					$returnArr["rt_msg"] = "成功";
					$qrcode_url = $wechat_qrcode_url.$bss_id.".png";
					$returnArr["rt_01"] = $qrcode_url;//QRCode url
					$returnArr["rt_02"] = $mdtoken;//魔動token
					$returnArr["rt_03"] = "actionGetcheckUserQrCode&{$bss_id}&8laXaMYfM8";//QRCode內容

					$whereStr = " bss_request_date = '{$return_requeast_date}' AND bss_token = '{$bss_token}'";
					$dataArrR['bss_id'] = $bss_id;
					$dataArrR['mdtoken'] = $mdtoken;
					$dataArrR['qrcode_url'] = $qrcode_url;
					$this->db_update('log_get_return_qrcode',$dataArrR,$whereStr);
					// $url = $wechat_get_qrcode_url.$bss_id."&mdToken={$mdtoken}";
 
					// $ch = curl_init();
					 
					// curl_setopt($ch, CURLOPT_URL, $url);
					// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					// $response = curl_exec($ch);
					// $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
					// curl_close($ch);
					// $response = trim($response);
					// if($http_code!='200')
					// {
					// 	$response = "{$http_code} wechat error";
					// }
					// else if(!$this->is_json($response))
					// {
					// 	$response = "格式錯誤";
					// }
					// // $responseStr = implode(', ', array_map(
					// //     function ($v, $k) { return sprintf("%s='%s'", $k, $v); },
					// //     $response,
					// //     array_keys($response)
					// // ));
					
					// $whereStr = " bss_request_date = '{$return_requeast_date}' AND bss_token = '{$bss_token}'";
					// $dataArrR = array();
					// $dataArrR['bss_id'] = $bss_id;
					// $dataArrR['mdtoken'] = $mdtoken;
					// $dataArrR['request_url'] = $url;
					// $dataArrR['wechat_reponse_date'] = "now()";
					// $dataArrR['wechat_response_content'] = $response;
					// if($this->is_json($response))
					// {
					// 	$response_de = json_decode($response,true);
					// 	$dataArrR['response_status'] = $response_de['status'];
					// 	$dataArrR['response_code'] = $response_de['code'];
					// 	$dataArrR['response_msg'] = $response_de['msg'];
					// 	$dataArrR['response_data'] = $response_de['data'];
					// 	$dataArrR['response_qr_content'] = $response_de['str'];
					// 	if($response_de['status']=='ok')
					// 	{
					// 		$returnArr["rt_cd"] = "0000";//成功
					// 		$returnArr["rt_msg"] = "成功";
					// 		$urlArr = explode("/",$response_de['data']);
					// 		$qrcode_url = $wechat_qrcode_url.$urlArr[count($urlArr)-1];
					// 		$dataArrR['qrcode_url'] = $qrcode_url;
					// 		$returnArr["rt_01"] = $qrcode_url;//QRCode url
					// 		$returnArr["rt_02"] = $mdtoken;//魔動token
					// 		$returnArr["rt_03"] = $response_de['str'];//QRCode內容
					// 	}
					// 	else
					// 	{
					// 		// $returnArr["rt_cd"] = $response_de['code'];//微信後台回應之錯誤
					// 		// $returnArr["rt_msg"] = $response_de['msg'];
					// 		$returnArr["rt_cd"] = "1002";//微信後台回應錯誤
					// 		$returnArr["rt_msg"] = "微信後台回應錯誤";
					// 	}
					// 	$dataArrR['wechat_response_content'] = $response;
					// }	
					// else
					// {
					// 	$returnArr["rt_cd"] = "1002";//微信後台回應錯誤
					// 	$returnArr["rt_msg"] = "微信後台回應錯誤";
					// 	if($response!="")
					// 	{
					// 		$dataArrR['wechat_response_content'] = $response;
					// 	}
					// 	else
					// 	{
					// 		$dataArrR['wechat_response_content'] = "錯誤格式";
					// 	}
					// }
					// $this->db_update('log_get_return_qrcode',$dataArrR,$whereStr);
				}
				else
				{
					$returnArr["rt_cd"] = "0007";//借电站token有误
					$returnArr["rt_msg"] = "借电站token有误";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式錯誤
			$returnArr["rt_msg"] = "格式錯誤";//格式錯誤
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	public function return_battery(){
		//計算執行時間
		$time_start = microtime(true);
		//寫入收到的資料到歷史紀錄
		$receive_data = $this->input->post();
		$api_name = 'f6d48830';
		$api_chinese_name = '还电回报';
		$api_log_sn = $this->model_common->insert_api_log($receive_data,$api_name,$api_chinese_name);

		// $output = implode(', ', array_map(
		//     function ($v, $k) { return sprintf("%s='%s'", $k, $v); },
		//     $this->input->post(),
		//     array_keys($this->input->post())
		// ));
		// $dataArr = array();
		// date_default_timezone_set('Asia/Taipei');
		// $logdataArr['api_name'] = 'bss03';
		// $logdataArr['receive_data'] = $output;
		// $logdataArr['receive_ip'] = $_SERVER['REMOTE_ADDR'];
		// $logdataArr['receive_date'] = "now()";
		// $this->db_insert("log_api_history",$logdataArr);

		$postjson = $this->input->post("JSONData");
		//先判斷是否為Json格式
		if($this->is_json($postjson))
		{
			$returnArr = array();
			//收到空資料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//資料為空
				$returnArr["rt_msg"] = "资料为空";//回應訊息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$return_date = addslashes($data_json_de['br01']);//借電站電池歸還日期
				$bss_token = addslashes($data_json_de['br02']);//借電站Token ID
				if($data_json_de['br03']!="")
					$User_ID = str_pad(addslashes($data_json_de['br03']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
				else
					$User_ID = "";
				$trace_no = addslashes($data_json_de['br04']);//歸還電池的軌道編號
				$battery_id = addslashes($data_json_de['br05']);//電池序號
				$identifier_code = addslashes($data_json_de['br06']);//交換站電池借電唯一交易序號
				if(isset($data_json_de['br07']))
					$return_status = addslashes($data_json_de['br07']);//借電狀態（0:借電成功；1:機櫃還電門未打開；2:用戶未放入電池；3:放入非借出電池；4:取電門未打開；5:用戶未取出電池;6:讀不到電池)
				else
					$return_status = "";
				if(isset($data_json_de['br08']))
					$return_battery_capacity = addslashes($data_json_de['br08']);//還電電池電量
				else
					$return_battery_capacity = "";
				$orderSn = NULL;
				$SQLCmdRL = "SELECT leave_date,usage_time FROM log_battery_leave_return WHERE return_date = '{$return_date}' AND battery_user_id = '{$User_ID}'";
				$rsRL = $this->db_query($SQLCmdRL);
				if(count($rsRL)>0)
				{
					$returnArr["rt_cd"] = "0000";//回應碼
					$returnArr["rt_msg"] = "成功";//回應訊息
					$returnArr["rt_01"] = $rsRL[0]['leave_date'];//電池租借日期
					$returnArr["rt_02"] = $return_date;//電池歸還日期
					$returnArr["rt_03"] = $rsRL[0]['usage_time'];//此次租借時間
					// $returnArr["rt_04"] = $dataArrS['store_balance_before'];//扣款前儲值餘額
					// $returnArr["rt_05"] = $charge_amount;//扣款金額
					// $returnArr["rt_06"] = $dataArrS['store_balance_after'];//扣款後儲值餘額
					// $returnArr["rt_07"] = $dataArrS['store_balance_before'];//扣款前剩餘騎乘時間
					// $returnArr["rt_08"] = $dataArrS['store_balance_after'];//扣款後剩餘騎乘時間
					$returnArr["rt_04"] = "";//扣款前儲值餘額
					$returnArr["rt_05"] = "";//扣款金額
					$returnArr["rt_06"] = "";//扣款後儲值餘額
					$returnArr["rt_07"] = "";//扣款前剩餘騎乘時間
					$returnArr["rt_08"] = "";//扣款後剩餘騎乘時間
					$returnArr["rt_09"] = "";//租借地點
					echo json_encode($returnArr);
					$time_end = microtime(true);
					$time = $time_end - $time_start;
					$otherdataArr['user_id'] = $User_ID;
					$otherdataArr['battery_id'] = $battery_id;
					$otherdataArr['sys_spend_time'] = $time;
					$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
					return;
				}
				$SQLCmdO = "SELECT orderSn FROM take_payment_info WHERE user_id = '{$User_ID}' ORDER BY take_payment_sn DESC limit 1";
				$rsO = $this->db_query($SQLCmdO);
				if($rsO)
				{
					$orderSn = $rsO[0]['orderSn'];
					$SQLCmdLO = "SELECT count(*) cnt FROM log_battery_leave_return WHERE wechat_orderSn = '{$orderSn}'";
					$rsLO = $this->db_query($SQLCmdLO);
					if($rsLO[0]['cnt']==0)
					{
						$dataArr['wechat_orderSn'] = $orderSn;
					}
				}
				$otherdataArr['user_id'] = $User_ID;
				$otherdataArr['battery_id'] = $battery_id;
				$SQLCmd = "SELECT DorO_flag,do_num FROM tbl_battery WHERE battery_id = '{$battery_id}' AND status <> 'D'";
				$rs = $this->db_query($SQLCmd);
				if($rs)
				{
					$dataArr['return_DorO_flag'] = $rs[0]['DorO_flag'];
					$dataArr['return_do_num'] = $rs[0]['do_num'];
				}
				// else
				// {
				// 	$returnArr["rt_cd"] = "0004";//無法對應到電池資料
				// 	$returnArr["rt_msg"] = "無法對應到電池資料";
				// 	echo json_encode($returnArr);
				// 	$time_end = microtime(true);
				// 	$time = $time_end - $time_start;
				// 	$otherdataArr['sys_spend_time'] = $time;
				// 	$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
				// 	return;
				// }

				if($bss_token!="")
				{
					$SQLCmdB = "SELECT s_num,bss_id,location,latitude,longitude FROM tbl_battery_swap_station WHERE bss_token = '{$bss_token}' AND status = 'Y'";
					$rsB = $this->db_query($SQLCmdB);
					if($rsB)
					{
						$dataArr['return_sb_num'] = $rsB[0]['s_num'];
						$battery_swap_location = $rsB[0]['location'];
						$return_gps = $rsB[0]['latitude'].",".$rsB[0]['longitude'];
						$return_bss_id = $rsB[0]['bss_id'];
					}
					else
					{
						$returnArr["rt_cd"] = "0005";//借电站token有误
						$returnArr["rt_msg"] = "借电站token有误";
						echo json_encode($returnArr);
						$time_end = microtime(true);
						$time = $time_end - $time_start;
						$otherdataArr['sys_spend_time'] = $time;
						$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
						return;
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//借电站token有误
					$returnArr["rt_msg"] = "借电站token有误";
					echo json_encode($returnArr);
					$time_end = microtime(true);
					$time = $time_end - $time_start;
					$otherdataArr['sys_spend_time'] = $time;
					$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
					return;
				}

				$SQLCmdM = "SELECT * FROM tbl_member WHERE user_id = '{$User_ID}' AND status = 'Y'";
				$rsM = $this->db_query($SQLCmdM);
				$whereStrBA = "";
				if($rsM)
				{
					if($rsM[0]['member_type']=="F")
					{
						$whereStrBA = "AND lblr.leave_battery_id = '{$battery_id}'";
					}
				}
				
				$SQLCmdL = "SELECT lblr.s_num as lblr_num,lblr.leave_date,lblr.leave_sb_num,lblr.leave_track_no,lblr.leave_battery_id,lblr.wechat_orderSn,lblr.identifier_code,lblr.gps_path_track,lblr.distance       
							FROM log_battery_leave_return lblr 
							WHERE lblr.battery_user_id = '{$User_ID}' 
							AND lblr.leave_date IS NOT NULL 
							AND lblr.return_date IS NULL 
							AND lblr.identifier_code = '{$identifier_code}' 
							{$whereStrBA}
							ORDER BY lblr.s_num DESC 
							limit 1";
				$rsL = $this->db_query($SQLCmdL);

				if($return_status=="" || $return_status==0)
				{
					if($rsL && $identifier_code!="")
					{
						$leave_date = $rsL[0]['leave_date'];
						$leave_sb_num = $rsL[0]['leave_sb_num'];
						$leave_track_no = $rsL[0]['leave_track_no'];
						$leave_battery_id = $rsL[0]['leave_battery_id'];
					}
					else
					{
						$SQLCmdL1 = "SELECT lblr.s_num as lblr_num,lblr.leave_date,lblr.leave_sb_num,lblr.leave_track_no,lblr.leave_battery_id,lblr.wechat_orderSn,lblr.identifier_code,lblr.gps_path_track,lblr.distance 
									FROM log_battery_leave_return lblr 
									WHERE lblr.battery_user_id = '{$User_ID}' 
									AND lblr.leave_date IS NOT NULL 
									AND lblr.return_date IS NULL 
									{$whereStrBA}
									ORDER BY lblr.s_num DESC";
						$rsL1 = $this->db_query($SQLCmdL1);
						if(count($rsL1)>=1)
						{
							$leave_date = $rsL1[0]['leave_date'];
							$leave_battery_id = $rsL1[0]['leave_battery_id'];
							if($rsM)
							{
									//寫入儲值與扣款紀錄
									// $dataArrS['blr_num'] = $rsL[0]['lblr_num'];
									// $dataArrS['tm_num'] = $rsM[0]['s_num'];
									// // $dataArrS['type'] = 'C';
									// $dataArrS['type'] = 'M';//租借分鐘數
									// $dataArrS['exchange_date'] = "now()";
									// // $dataArrS['charge_amount'] = $charge_amount;
									// // $dataArrS['store_balance_before'] = $rsM[0]['stored_value'];
									// // $dataArrS['store_balance_after'] = $rsM[0]['stored_value']-$charge_amount;
									// $dataArrS['charge_amount'] = $usage_time;
									// $dataArrS['store_balance_before'] = $rsM[0]['remaining_time'];
									// $dataArrS['store_balance_after'] = $rsM[0]['remaining_time']-$usage_time;
									// $this->db_insert('log_store_balance_charge',$dataArrS);

									//更新會員儲值餘額欄位
									$whereStrM = "s_num = {$rsM[0]['s_num']}";
									// $dataArrM['stored_value'] = $dataArrS['store_balance_after'];
									// $dataArrM['remaining_time'] = $dataArrS['store_balance_after'];
									$dataArrM['lease_status'] = 'N';
									$this->db_update('tbl_member',$dataArrM,$whereStrM);
							}
							else
							{
									$returnArr["rt_cd"] = "0006";//更新失敗
									$returnArr["rt_msg"] = "会员ID有误";//
									// $dataArrM['member_type'] = 'B';
									// $dataArrM['member_register'] = 'P';
									// $dataArrM['name'] = '系統自動寫入';
									// $dataArrM['mobile'] = $this->randString(10,'0123456789');
									// $key = md5('ebike'.$dataArrM['mobile']);
									// $key = substr($key, 0, 24);
									// $string = $this->randString(10);
									// //加密
									// $dataArrM['personal_id'] = $this->encrypt($key, $string);
									// $dataArrM['user_id'] = $User_ID;
									// $dataArrM['gender'] = $this->randString(1,'MF');
									// $dataArrM['nationality'] = '158';
									// $dataArrM['pay_type'] = 'W';
									// $dataArrM['stored_type'] = 'N';
									// $dataArrM['lease_status'] = 'N';
									// $dataArrM['status'] = 'Y';
									// $dataArrM['create_user'] = '1';
									// $dataArrM['create_date'] = 'now()';
									// $dataArrM['create_ip'] = '127.0.0.1';
									// $this->db_insert('tbl_member',$dataArrM);
									echo json_encode($returnArr);
									$time_end = microtime(true);
									$time = $time_end - $time_start;
									$otherdataArr['sys_spend_time'] = $time;
									$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
									exit;
							}

							$tm_num = $rsM[0]['s_num'];
							$whereStrB = "battery_id = '{$battery_id}' AND status <> 'D'";
							$dataArrB['sv_num'] = NULL;
							$dataArrB['position'] = 'B';
							$this->db_update('tbl_battery',$dataArrB,$whereStrB);

							$dataArr['return_request_date'] = $return_date;
							$dataArr['return_date'] = $return_date;
							$dataArr['return_track_no'] = $trace_no;
							$dataArr['return_battery_id'] = $battery_id;
							$dataArr['return_battery_capacity'] = $return_battery_capacity;
							$dataArr['return_status'] = $return_status;
							$dataArr['usage_time'] = $this->shi_jian_cha($return_date,$leave_date);
							$SQLCmdLBG = "SELECT gps FROM log_battery_gps WHERE lblr_num = {$rsL1[0]['lblr_num']}";
							$rsLBG = $this->db_query($SQLCmdLBG);
							if($rsLBG)
							{
								$dataArr['gps_path_track'] = $rsL1[0]['gps_path_track'];
								$distance = 0;
								foreach ($rsLBG as $keyLBG => $valueLBG) {
									if($dataArr['gps_path_track']=="")
									{
										$dataArr['gps_path_track'] = $valueLBG['gps'];
									}
									else
									{
										$gpsArr = explode(";",$dataArr['gps_path_track']);
										$gpsArr2 = explode(",",$gpsArr[count($gpsArr)-1]);
										if(isset($gpsArr2[1]))
										{
											$lat1 = $gpsArr2[0];
											$lng1 = $gpsArr2[1];
											$nowgpsArr = explode(",",$valueLBG['gps']);
											$lat2 = $nowgpsArr[0];
											$lng2 = $nowgpsArr[1];
											if($lat1!="" && $lng1!="" && $lat2!="" && $lng2!="")
											{
												$distance = $distance+$this->GetDistance($lat1,$lng1,$lat2,$lng2);
											}
										}
										$dataArr['gps_path_track'] .= ";".$valueLBG['gps'];
									}
								}
								$dataArr['distance'] = $rsL1[0]['distance']+$distance;
							}
							$dataArr['return_battery_capacity'] = $return_battery_capacity;
							$whereStrL = " s_num = {$rsL1[0]['lblr_num']}";
							if($this->db_update("log_battery_leave_return",$dataArr,$whereStrL))
							{
								$SQLCmdD = "DELETE FROM log_battery_gps WHERE lblr_num = {$rsL1[0]['lblr_num']}";
								$this->db_query($SQLCmdD);
								// $postArr = array();
								// $postArr['action'] = "actionPostCabinetCallback";
								// $postArr['rentTime'] = $leave_date;
								// $postArr['returnTime'] = $return_date;
								// $postArr['location'] = "";
								// $postArr['returnLocation'] = $battery_swap_location;
								// $postArr['export'] = "";
								// $postArr['access'] = $trace_no;
								// $postArr['rentBatteryNumber'] = $leave_battery_id;
								// $postArr['returnBatteryNumber'] = $battery_id;
								// $postArr['cabinetNumber'] = $return_bss_id;
								// $postArr['userId'] = ltrim($User_ID,"0");
								// $this->wechat_callback($postArr);

								$returnArr["rt_cd"] = "0000";//回應碼
								$returnArr["rt_msg"] = "成功";//回應訊息
								$returnArr["rt_01"] = $leave_date;//電池租借日期
								$returnArr["rt_02"] = $return_date;//電池歸還日期
								$returnArr["rt_03"] = $dataArr['usage_time'];//此次租借時間
								// $returnArr["rt_04"] = $dataArrS['store_balance_before'];//扣款前儲值餘額
								// $returnArr["rt_05"] = $charge_amount;//扣款金額
								// $returnArr["rt_06"] = $dataArrS['store_balance_after'];//扣款後儲值餘額
								// $returnArr["rt_07"] = $dataArrS['store_balance_before'];//扣款前剩餘騎乘時間
								// $returnArr["rt_08"] = $dataArrS['store_balance_after'];//扣款後剩餘騎乘時間
								$returnArr["rt_04"] = "";//扣款前儲值餘額
								$returnArr["rt_05"] = "";//扣款金額
								$returnArr["rt_06"] = "";//扣款後儲值餘額
								$returnArr["rt_07"] = "";//扣款前剩餘騎乘時間
								$returnArr["rt_08"] = "";//扣款後剩餘騎乘時間
								$returnArr["rt_09"] = $battery_swap_location;//租借地點
							}
							else
							{
								$returnArr["rt_cd"] = "0002";//更新失敗
								$returnArr["rt_msg"] = "更新失败";//更新失敗
							}
							echo json_encode($returnArr);
							$time_end = microtime(true);
							$time = $time_end - $time_start;
							$otherdataArr['sys_spend_time'] = $time;
							$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
							exit;
						}
						else
						{
							if($rsM)
							{
								//寫入儲值與扣款紀錄
								// $dataArrS['blr_num'] = $rsL[0]['lblr_num'];
								// $dataArrS['tm_num'] = $rsM[0]['s_num'];
								// // $dataArrS['type'] = 'C';
								// $dataArrS['type'] = 'M';//租借分鐘數
								// $dataArrS['exchange_date'] = "now()";
								// // $dataArrS['charge_amount'] = $charge_amount;
								// // $dataArrS['store_balance_before'] = $rsM[0]['stored_value'];
								// // $dataArrS['store_balance_after'] = $rsM[0]['stored_value']-$charge_amount;
								// $dataArrS['charge_amount'] = $usage_time;
								// $dataArrS['store_balance_before'] = $rsM[0]['remaining_time'];
								// $dataArrS['store_balance_after'] = $rsM[0]['remaining_time']-$usage_time;
								// $this->db_insert('log_store_balance_charge',$dataArrS);

								//更新會員儲值餘額欄位
								$whereStrM = "s_num = {$rsM[0]['s_num']}";
								// $dataArrM['stored_value'] = $dataArrS['store_balance_after'];
								// $dataArrM['remaining_time'] = $dataArrS['store_balance_after'];
								$dataArrM['lease_status'] = 'N';
								$this->db_update('tbl_member',$dataArrM,$whereStrM);
							}
							else
							{
								$returnArr["rt_cd"] = "0006";//更新失敗
								$returnArr["rt_msg"] = "会员ID有误";//
								// $dataArrM['member_type'] = 'B';
								// $dataArrM['member_register'] = 'P';
								// $dataArrM['name'] = '系統自動寫入';
								// $dataArrM['mobile'] = $this->randString(10,'0123456789');
								// $key = md5('ebike'.$dataArrM['mobile']);
								// $key = substr($key, 0, 24);
								// $string = $this->randString(10);
								// //加密
								// $dataArrM['personal_id'] = $this->encrypt($key, $string);
								// $dataArrM['user_id'] = $User_ID;
								// $dataArrM['gender'] = $this->randString(1,'MF');
								// $dataArrM['nationality'] = '158';
								// $dataArrM['pay_type'] = 'W';
								// $dataArrM['stored_type'] = 'N';
								// $dataArrM['lease_status'] = 'N';
								// $dataArrM['status'] = 'Y';
								// $dataArrM['create_user'] = '1';
								// $dataArrM['create_date'] = 'now()';
								// $dataArrM['create_ip'] = '127.0.0.1';
								// $this->db_insert('tbl_member',$dataArrM);
								echo json_encode($returnArr);
								$time_end = microtime(true);
								$time = $time_end - $time_start;
								$otherdataArr['sys_spend_time'] = $time;
								$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
								exit;
							}

							$SQLCmdM = "SELECT s_num FROM tbl_member WHERE user_id = '{$User_ID}' AND status = 'Y'";
							$rsM = $this->db_query($SQLCmdM);
							$tm_num = $rsM[0]['s_num'];
							$whereStrB = "battery_id = '{$battery_id}' AND status <> 'D'";
							$dataArrB['sv_num'] = NULL;
							$dataArrB['position'] = 'B';
							$this->db_update('tbl_battery',$dataArrB,$whereStrB);

							$dataArrL['tm_num'] = $tm_num;
							$dataArrL['system_log_date'] = "now()";
							$dataArrL['battery_user_id'] = $User_ID;
							$dataArrL['return_sb_num'] = $dataArr['return_sb_num'];
							$dataArrL['return_request_date'] = $return_date;
							$dataArrL['return_date'] = $return_date;
							$dataArrL['return_track_no'] = $trace_no;
							$dataArrL['return_battery_id'] = $battery_id;
							$dataArrL['return_battery_capacity'] = $return_battery_capacity;
							$dataArrL['return_status'] = $return_status;
							$dataArrL['usage_time'] = "";

							// $returnArr["rt_cd"] = "0005";//無法對應到電池資料
							// $returnArr["rt_msg"] = "無法取得租借紀錄";
							// echo json_encode($returnArr);
							// $time_end = microtime(true);
							// $time = $time_end - $time_start;
							// $otherdataArr['sys_spend_time'] = $time;
							// $this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
							// return;
							$dataArrL['wechat_orderSn'] = $orderSn;
							if($this->db_insert('log_battery_leave_return',$dataArrL))
							{
								// $postArr = array();
								// $postArr['action'] = "actionPostCabinetCallback";
								// $postArr['rentTime'] = "";
								// $postArr['returnTime'] = $return_date;
								// $postArr['location'] = "";
								// $postArr['returnLocation'] = $battery_swap_location;
								// $postArr['export'] = $leave_track_no;
								// $postArr['access'] = $trace_no;
								// $postArr['rentBatteryNumber'] = "";
								// $postArr['returnBatteryNumber'] = $battery_id;
								// $postArr['cabinetNumber'] = $return_bss_id;
								// $postArr['userId'] = ltrim($User_ID,"0");
								// $this->wechat_callback($postArr);

								$returnArr["rt_cd"] = "0000";//回應碼
								$returnArr["rt_msg"] = "成功";//回應訊息
								$returnArr["rt_01"] = "";//電池租借日期
								$returnArr["rt_02"] = $return_date;//電池歸還日期
								$returnArr["rt_03"] = "";//此次租借時間
								// $returnArr["rt_04"] = $dataArrS['store_balance_before'];//扣款前儲值餘額
								// $returnArr["rt_05"] = $charge_amount;//扣款金額
								// $returnArr["rt_06"] = $dataArrS['store_balance_after'];//扣款後儲值餘額
								// $returnArr["rt_07"] = $dataArrS['store_balance_before'];//扣款前剩餘騎乘時間
								// $returnArr["rt_08"] = $dataArrS['store_balance_after'];//扣款後剩餘騎乘時間
								$returnArr["rt_04"] = "";//扣款前儲值餘額
								$returnArr["rt_05"] = "";//扣款金額
								$returnArr["rt_06"] = "";//扣款後儲值餘額
								$returnArr["rt_07"] = "";//扣款前剩餘騎乘時間
								$returnArr["rt_08"] = "";//扣款後剩餘騎乘時間
								$returnArr["rt_09"] = $battery_swap_location;//租借地點
							}
							else
							{
								$returnArr["rt_cd"] = "0002";//更新失敗
								$returnArr["rt_msg"] = "更新失败";//更新失敗
							}

							echo json_encode($returnArr);
							$time_end = microtime(true);
							$time = $time_end - $time_start;
							$otherdataArr['sys_spend_time'] = $time;
							$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
							return;
						}
						
					}
				}
				else
				{
					$bss_id = $rsB[0]['bss_id'];
					$leave_location = $rsB[0]['location'];
					$SQLCmdM = "SELECT s_num FROM tbl_member WHERE user_id = '{$User_ID}' AND status = 'Y'";
					$rsM = $this->db_query($SQLCmdM);
					$tm_num = $rsM[0]['s_num'];
					$dataArr['tm_num'] = $tm_num;
					$dataArr['battery_user_id'] = $User_ID;
					$dataArr['return_track_no'] = $trace_no;
					$dataArr['return_battery_id'] = $battery_id;
					$dataArr['return_battery_capacity'] = $return_battery_capacity;
					$dataArr['return_status'] = $return_status;
					$dataArr['usage_time'] = "";
					$orderSn = "";
					if($rsL)
					{
						$orderSn = $rsL[0]['wechat_orderSn'];
						$whereStr = "s_num = {$rsL[0]['lblr_num']}";
						$SQLCmdLBG = "SELECT gps FROM log_battery_gps WHERE lblr_num = {$rsL[0]['lblr_num']}";
						$rsLBG = $this->db_query($SQLCmdLBG);
						if($rsLBG)
						{
							$dataArr['gps_path_track'] = $rsL[0]['gps_path_track'];
							$distance = 0;
							foreach ($rsLBG as $keyLBG => $valueLBG) {
								if($dataArr['gps_path_track']=="")
								{
									$dataArr['gps_path_track'] = $valueLBG['gps'];
								}
								else
								{
									$gpsArr = explode(";",$dataArr['gps_path_track']);
									$gpsArr2 = explode(",",$gpsArr[count($gpsArr)-1]);
									if(isset($gpsArr2[1]))
									{
										$lat1 = $gpsArr2[0];
										$lng1 = $gpsArr2[1];
										$nowgpsArr = explode(",",$valueLBG['gps']);
										$lat2 = $nowgpsArr[0];
										$lng2 = $nowgpsArr[1];
										if($lat1!="" && $lng1!="" && $lat2!="" && $lng2!="")
											$distance = $distance+$this->GetDistance($lat1,$lng1,$lat2,$lng2);
									}
									$dataArr['gps_path_track'] .= ";".$valueLBG['gps'];
								}
							}
							$dataArr['distance'] = $rsL[0]['distance']+$distance;
						}
						//更新電池租借歸還記錄表
						if($this->db_update('log_battery_leave_return',$dataArr,$whereStr))
						{
							$SQLCmdD = "DELETE FROM log_battery_gps WHERE lblr_num = {$rsL[0]['lblr_num']}";
							$this->db_query($SQLCmdD);
							$returnArr["rt_cd"] = "0000";//成功
							$returnArr["rt_msg"] = "成功";//成功
						}
						else
						{
							$returnArr["rt_cd"] = "0002";//更新失敗
							$returnArr["rt_msg"] = "更新失败";//更新失敗
						}
					}
					else
					{
						$insert_return_flag = false;
						// if($identifier_code=="")
						// {
							$SQLCmdL1 = "SELECT lblr.s_num as lblr_num,lblr.leave_date,lblr.leave_sb_num,lblr.leave_track_no,lblr.leave_battery_id,lblr.identifier_code,lblr.return_status,lblr.wechat_orderSn,lblr.gps_path_track,lblr.distance   
									FROM log_battery_leave_return lblr 
									WHERE lblr.battery_user_id = '{$User_ID}' 
									AND lblr.leave_date IS NOT NULL 
									AND lblr.return_date IS NULL 
									{$whereStrBA} 
									ORDER BY lblr.s_num DESC";
							$rsL1 = $this->db_query($SQLCmdL1);
							if($rsL1)
							{
								if($rsL1[0]['return_status']!==0 || $identifier_code=="")
								{
									$SQLCmdLBG = "SELECT gps FROM log_battery_gps WHERE lblr_num = {$rsL1[0]['lblr_num']}";
									$rsLBG = $this->db_query($SQLCmdLBG);
									if($rsLBG)
									{
										$dataArr['gps_path_track'] = $rsL1[0]['gps_path_track'];
										$distance = 0;
										foreach ($rsLBG as $keyLBG => $valueLBG) {
											if($dataArr['gps_path_track']=="")
											{
												$dataArr['gps_path_track'] = $valueLBG['gps'];
											}
											else
											{
												$gpsArr = explode(";",$dataArr['gps_path_track']);
												$gpsArr2 = explode(",",$gpsArr[count($gpsArr)-1]);
												if(isset($gpsArr2[1]))
												{
													$lat1 = $gpsArr2[0];
													$lng1 = $gpsArr2[1];
													$nowgpsArr = explode(",",$valueLBG['gps']);
													$lat2 = $nowgpsArr[0];
													$lng2 = $nowgpsArr[1];
													if($lat1!="" && $lng1!="" && $lat2!="" && $lng2!="")
														$distance = $distance+$this->GetDistance($lat1,$lng1,$lat2,$lng2);
												}
												$dataArr['gps_path_track'] .= ";".$valueLBG['gps'];
											}
										}
										$dataArr['distance'] = $rsL1[0]['distance']+$distance;
									}
									$orderSn = $rsL1[0]['wechat_orderSn'];
									$whereStr = "s_num = {$rsL1[0]['lblr_num']}";
									//更新電池租借歸還記錄表
									if($this->db_update('log_battery_leave_return',$dataArr,$whereStr))
									{
										$SQLCmdD = "DELETE FROM log_battery_gps WHERE lblr_num = {$rsL1[0]['lblr_num']}";
										$this->db_query($SQLCmdD);
										$returnArr["rt_cd"] = "0000";//成功
										$returnArr["rt_msg"] = "成功";//成功
									}
									else
									{
										$returnArr["rt_cd"] = "0002";//更新失敗
										$returnArr["rt_msg"] = "更新失败";//更新失敗
									}
								}
								else
								{
									$insert_return_flag = true;
								}
							}
							else
							{
								$insert_return_flag = true;
							}
						// }
						// else
						// {
						// 	$insert_return_flag = true;
						// }

						if($insert_return_flag)
						{
							$dataArr['system_log_date'] = "now()";
							$dataArr['wechat_orderSn'] = $orderSn;
							if($this->db_insert('log_battery_leave_return',$dataArr))
							{
								$returnArr["rt_cd"] = "0000";//成功
								$returnArr["rt_msg"] = "成功";//成功
							}
							else
							{
								$returnArr["rt_cd"] = "0002";//更新失敗
								$returnArr["rt_msg"] = "更新失败";//更新失敗
							}
						}
					}
					$postArr = array();
					$postArr['result'] = (int)$leave_status;
					$postArr['msg'] = "还电失败";
					$postArr['sn_old'] = $battery_id;
					$postArr['sn_new'] = "";
					date_default_timezone_set('UTC');
					$date = new DateTime();
					$postArr['ts'] = $date->getTimestamp();
					$this->app_callback($postArr,"异常回调接口");

					echo json_encode($returnArr);
					$time_end = microtime(true);
					$time = $time_end - $time_start;
					$otherdataArr['sys_spend_time'] = $time;
					$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
					exit;
				}

				$usage_time = $this->minDiff($leave_date,$return_date);//使用時間
				// $charge_amount = ceil($usage_time/30)*2.5;//扣款金額

				if($rsM)
				{
					$dataArrM = array();
					//寫入儲值與扣款紀錄
					// $dataArrS['blr_num'] = $rsL[0]['lblr_num'];
					// $dataArrS['tm_num'] = $rsM[0]['s_num'];
					// // $dataArrS['type'] = 'C';
					// $dataArrS['type'] = 'M';//租借分鐘數
					// $dataArrS['exchange_date'] = "now()";
					// // $dataArrS['charge_amount'] = $charge_amount;
					// // $dataArrS['store_balance_before'] = $rsM[0]['stored_value'];
					// // $dataArrS['store_balance_after'] = $rsM[0]['stored_value']-$charge_amount;
					// $dataArrS['charge_amount'] = $usage_time;
					// $dataArrS['store_balance_before'] = $rsM[0]['remaining_time'];
					// $dataArrS['store_balance_after'] = $rsM[0]['remaining_time']-$usage_time;
					// $this->db_insert('log_store_balance_charge',$dataArrS);

					//更新會員儲值餘額欄位
					$whereStrM = "s_num = {$rsM[0]['s_num']}";
					// $dataArrM['stored_value'] = $dataArrS['store_balance_after'];
					// $dataArrM['remaining_time'] = $dataArrS['store_balance_after'];
					$dataArrM['lease_status'] = 'N';
					$this->db_update('tbl_member',$dataArrM,$whereStrM);

					$whereStrB = "battery_id = '{$battery_id}' AND status <> 'D'";
					$dataArrB['sv_num'] = NULL;
					$dataArrB['position'] = 'B';
					$this->db_update('tbl_battery',$dataArrB,$whereStrB);
				}
				else
				{
					$returnArr["rt_cd"] = "0006";//更新失敗
					$returnArr["rt_msg"] = "会员ID有误";//
					echo json_encode($returnArr);
					$time_end = microtime(true);
					$time = $time_end - $time_start;
					$otherdataArr['sys_spend_time'] = $time;
					$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
					return;
				}
				

				if($return_status=="" || $return_status==0)
				{
					$dataArr['return_request_date'] = $return_date;
					$dataArr['return_date'] = $return_date;
				}
				$dataArr['return_track_no'] = $trace_no;
				$dataArr['return_battery_id'] = $battery_id;
				$dataArr['return_battery_capacity'] = $return_battery_capacity;
				$dataArr['return_status'] = $return_status;
				$dataArr['usage_time'] = $this->shi_jian_cha($return_date,$leave_date);
				$SQLCmdLBG = "SELECT gps FROM log_battery_gps WHERE lblr_num = {$rsL[0]['lblr_num']}";
				$rsLBG = $this->db_query($SQLCmdLBG);
				if($rsLBG)
				{
					$dataArr['gps_path_track'] = $rsL[0]['gps_path_track'];
					$distance = 0;
					foreach ($rsLBG as $keyLBG => $valueLBG) {
						if($dataArr['gps_path_track']=="")
						{
							$dataArr['gps_path_track'] = $valueLBG['gps'];
						}
						else
						{
							$gpsArr = explode(";",$dataArr['gps_path_track']);
							$gpsArr2 = explode(",",$gpsArr[count($gpsArr)-1]);
							if(isset($gpsArr2[1]))
							{
								$lat1 = $gpsArr2[0];
								$lng1 = $gpsArr2[1];
								$nowgpsArr = explode(",",$valueLBG['gps']);
								$lat2 = $nowgpsArr[0];
								$lng2 = $nowgpsArr[1];
								if($lat1!="" && $lng1!="" && $lat2!="" && $lng2!="")
									$distance = $distance+$this->GetDistance($lat1,$lng1,$lat2,$lng2);
							}
							$dataArr['gps_path_track'] .= ";".$valueLBG['gps'];
						}
					}
					$dataArr['distance'] = $rsL[0]['distance'] + $distance;
				}
				// $dataArr['charge_amount'] = $charge_amount;
				$whereStr = "s_num = {$rsL[0]['lblr_num']}";

				//更新電池租借歸還記錄表
				if($this->db_update('log_battery_leave_return',$dataArr,$whereStr))
				{
					$SQLCmdD = "DELETE FROM log_battery_gps WHERE lblr_num = {$rsL[0]['lblr_num']}";
					$this->db_query($SQLCmdD);
					// $SQLCmdBS = "SELECT latitude,longitude,location FROM tbl_battery_swap_station WHERE s_num = {$leave_sb_num} AND status <> 'D'";
					// $rsBS = $this->db_query($SQLCmdBS);
					// $leave_gps = "";
					// if($rsBS)
					// {
					// 	$leave_gps = $rsBS[0]['latitude'].",".$rsBS[0]['longitude'];
					// 	$leave_location = $rsBS[0]['location'];
					// }
					// $postArr = array();
					// $postArr['action'] = "actionPostCabinetCallback";
					// $postArr['rentTime'] = $leave_date;
					// $postArr['returnTime'] = $return_date;
					// $postArr['location'] = $leave_location;
					// $postArr['returnLocation'] = $battery_swap_location;
					// $postArr['export'] = $leave_track_no;
					// $postArr['access'] = $trace_no;
					// $postArr['rentBatteryNumber'] = $leave_battery_id;
					// $postArr['returnBatteryNumber'] = $battery_id;
					// $postArr['cabinetNumber'] = $return_bss_id;
					// $postArr['userId'] = ltrim($User_ID,"0");
					// $this->wechat_callback($postArr);

					$returnArr["rt_cd"] = "0000";//回應碼
					$returnArr["rt_msg"] = "成功";//回應訊息
					$returnArr["rt_01"] = $leave_date;//電池租借日期
					$returnArr["rt_02"] = $return_date;//電池歸還日期
					$returnArr["rt_03"] = $dataArr['usage_time'];//此次租借時間
					// $returnArr["rt_04"] = $dataArrS['store_balance_before'];//扣款前儲值餘額
					// $returnArr["rt_05"] = $charge_amount;//扣款金額
					// $returnArr["rt_06"] = $dataArrS['store_balance_after'];//扣款後儲值餘額
					// $returnArr["rt_07"] = $dataArrS['store_balance_before'];//扣款前剩餘騎乘時間
					// $returnArr["rt_08"] = $dataArrS['store_balance_after'];//扣款後剩餘騎乘時間
					$returnArr["rt_04"] = "";//扣款前儲值餘額
					$returnArr["rt_05"] = "";//扣款金額
					$returnArr["rt_06"] = "";//扣款後儲值餘額
					$returnArr["rt_07"] = "";//扣款前剩餘騎乘時間
					$returnArr["rt_08"] = "";//扣款後剩餘騎乘時間
					$returnArr["rt_09"] = $battery_swap_location;//租借地點
				}
				else
				{
					$returnArr["rt_cd"] = "0002";//更新失敗
					$returnArr["rt_msg"] = "更新失败";//更新失敗
				}

			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式錯誤
			$returnArr["rt_msg"] = "格式错误";//格式錯誤
		}
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		echo json_encode($returnArr);
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
		if($time>5 && $returnArr["rt_cd"]="0000")
		{
			$rsRL = $this->db_query($SQLCmdRL);
			if(count($rsRL)>=2)
			{
				$dataArr = arraty();
				$dataArr['return_DorO_flag'] = NULL;
				$dataArr['return_do_num'] = NULL;
				$dataArr['return_sb_num'] = NULL;
				$dataArr['return_request_date'] = NULL;
				$dataArr['return_date'] = NULL;
				$dataArr['return_track_no'] = NULL;
				$dataArr['return_battery_id'] = NULL;
				$dataArr['return_battery_capacity'] = NULL;
				$dataArr['return_status'] = NULL;
				$dataArr['usage_time'] = NULL;
				$this->db_update("log_battery_leave_return",$dataArr,$whereStr);
			}
		}
	}

	//計算時間差
	public function minDiff($startTime, $endTime) {
	    $start = strtotime($startTime);
	    $end = strtotime($endTime);
	    $timeDiff = $end - $start;
	    return ceil($timeDiff / 60);
	    // return floor($timeDiff / 60);
	}

	//計算時間差
	public function shi_jian_cha($d,$d1)
	{ 
		$time = strtotime($d) - strtotime($d1); 
		$n_time = str_pad(floor($time%(24*3600)/3600),2,0,STR_PAD_LEFT ).":".str_pad(floor($time%3600/60),2,0,STR_PAD_LEFT).":".str_pad($time%3600%60, 2, 0, STR_PAD_LEFT)."秒";
		return $n_time;
	}

	public function wechat_callback($postArr=array(),$api_chinese_name="")
	{
		//取得微信的網址參數
		$SQLCmdC = "SELECT config_set FROM sys_config WHERE config_code ='wechat_callback_url' AND status = 1";
		$rsC = $this->db_query($SQLCmdC);
		if($rsC)
		{
			$url = $rsC[0]['config_set']."cabinetcallback";
		}
		else
		{
			$url = "";
		}

		$postStr = http_build_query($postArr);

		$dataArrLog = array();
		$dataArrLog['api_name'] = $postArr['action'];
		$dataArrLog['api_chinese_name'] = $api_chinese_name;
		if(isset($postArr['returnTime']))
			$dataArrLog['send_date'] = $postArr['returnTime'];
		else
			$dataArrLog['send_date'] = "now()";
		$dataArrLog['send_data'] = $postStr;

		$this->db_insert("log_send_wechat_api",$dataArrLog);
		$SQLCmdLa = "select LAST_INSERT_ID() as log_sn";
		$rsLa = $this->db_query($SQLCmdLa);
		$log_sn = $rsLa[0]['log_sn'];
		$whereStrLog = " s_num = {$log_sn}";
		// $postStr = http_build_query($postArr);
		// $postStr = http_build_query(array("Book"=>1));
		// $postStr = '{"Book":"1"}';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postStr); 
		$output = curl_exec($ch);
		curl_close($ch);
		$output = trim($output);
		if(@strpos($output,"502 Bad Gateway")!==false)
		{
			$output = "502 Bad Gateway";
		}
		else if(!$this->is_json($output))
		{
			$output = "格式錯誤";
		}
		
		$dataArrLog = array();
		$dataArrLog['receive_date'] = "now()";
		$dataArrLog['receive_data'] = $output;
		$this->db_update("log_send_wechat_api",$dataArrLog,$whereStrLog);
	}

	public function app_callback($postArr=array(),$api_chinese_name="")
	{
		// //取得微信的網址參數
		// $SQLCmdC = "SELECT config_set FROM sys_config WHERE config_code ='wechat_callback_url' AND status = 1";
		// $rsC = $this->db_query($SQLCmdC);
		// if($rsC)
		// {
		// 	$url = $rsC[0]['config_set']."cabinetcallback";
		// }
		// else
		// {
		// 	$url = "";
		// }
		date_default_timezone_set('Asia/Taipei');
		$url = "https://api.d.bluesharkmotor.com/webhook/battswap/swapresult";
		$postStr = http_build_query($postArr);

		$dataArrLog = array();
		$dataArrLog['api_name'] = $postArr['action'];
		$dataArrLog['api_chinese_name'] = $api_chinese_name;
		if(isset($postArr['returnTime']))
			$dataArrLog['send_date'] = $postArr['returnTime'];
		else
			$dataArrLog['send_date'] = "now()";
		$dataArrLog['send_data'] = $postStr;

		$this->db_insert("log_send_wechat_api",$dataArrLog);
		$SQLCmdLa = "select LAST_INSERT_ID() as log_sn";
		$rsLa = $this->db_query($SQLCmdLa);
		$log_sn = $rsLa[0]['log_sn'];
		$whereStrLog = " s_num = {$log_sn}";
		// $postStr = http_build_query($postArr);
		// $postStr = http_build_query(array("Book"=>1));
		// $postStr = '{"Book":"1"}';
		$postStr = json_encode($postStr);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postStr); 
		// 這裡略過檢查 SSL 憑證有效性
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$output = curl_exec($ch);
		curl_close($ch);
		$output = trim($output);
		if(@strpos($output,"502 Bad Gateway")!==false)
		{
			$output = "502 Bad Gateway";
		}
		else if(!$this->is_json($output))
		{
			$output = "格式錯誤";
		}
		
		$dataArrLog = array();
		$dataArrLog['receive_date'] = "now()";
		$dataArrLog['receive_data'] = $output;
		$this->db_update("log_send_wechat_api",$dataArrLog,$whereStrLog);
	}

	//乱数产生token
	function generatorToken()
	{
	    $password_len = 8;
	    $password = '';

	    // remove o,0,1,l
	    $word = 'abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ23456789';
	    $len = strlen($word);

	    for ($i = 0; $i < $password_len; $i++) {
	        $password .= $word[rand() % $len];
	    }

	    return $password;
	}

	/**
	* 計算兩組經緯度座標 之間的距離
	* params ：lat1 緯度1； lng1 經度1； lat2 緯度2； lng2 經度2； len_type （1:m or 2:km);
	* return m or km
	*/
	public function GetDistance($lat1, $lng1, $lat2, $lng2, $len_type = 2, $decimal = 2) {
	    $radLat1 = $lat1 * PI / 180.0;
	    $radLat2 = $lat2 * PI / 180.0;
	    $a = $radLat1 - $radLat2;
	    $b = ($lng1 * PI / 180.0) - ($lng2 * PI / 180.0);
	    $s = 2 * asin(sqrt(pow(sin($a/2),2) + cos($radLat1) * cos($radLat2) * pow(sin($b/2),2)));
	    $s = $s * EARTH_RADIUS;
	    $s = round($s * 1000);
	    if ($len_type > 1)
	    {
	    $s /= 1000;
	    }
	    return round($s, $decimal);
	}
}

/* End of file Model_bss03.php */
