<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_bss07 extends MY_Model {
	
    function __construct() 
	{
        parent::__construct();
		// echo "in Model_column";
	}

	public function return_battery(){
		//計算執行時間
		$time_start = microtime(true);
		//寫入收到的資料到歷史紀錄
		$receive_data = $this->input->post();
		$api_name = 'c05d76e4';
		$api_chinese_name = '新还电回报';
		$api_log_sn = $this->model_common->insert_api_log($receive_data,$api_name,$api_chinese_name);

		$postjson = $this->input->post("JSONData");
		//先判斷是否為Json格式
		if($this->is_json($postjson))
		{
			$returnArr = array();
			//收到空資料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//資料為空
				$returnArr["rt_msg"] = "资料为空";//回應訊息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$return_date = addslashes($data_json_de['br01']);//借電站電池歸還日期
				$bss_token = addslashes($data_json_de['br02']);//借電站Token ID
				if($data_json_de['br03']!="")
					$User_ID = str_pad(addslashes($data_json_de['br03']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
				else
					$User_ID = "";
				$trace_no = addslashes($data_json_de['br04']);//歸還電池的軌道編號
				$battery_id = addslashes($data_json_de['br05']);//電池序號
				$transToken = addslashes($data_json_de['br06']);//transToken
				if(isset($data_json_de['br07']))
					$return_status = addslashes($data_json_de['br07']);//借電狀態（0:借電成功；1:機櫃還電門未打開；2:用戶未放入電池；3:放入非借出電池；4:取電門未打開；5:用戶未取出電池;6:讀不到電池)
				else
					$return_status = "";
				$one_trans = $data_json_de['br08'];//是否為第一次交易(Y:是,N:否)
				$otherdataArr['user_id'] = $User_ID;
				$otherdataArr['battery_id'] = $battery_id;
				$SQLCmd = "SELECT DorO_flag,do_num FROM tbl_battery WHERE battery_id = '{$battery_id}' AND status <> 'D'";
				$rs = $this->db_query($SQLCmd);
				if($rs)
				{
					$dataArr['return_DorO_flag'] = $rs[0]['DorO_flag'];
					$dataArr['return_do_num'] = $rs[0]['do_num'];
				}
				// else
				// {
				// 	$returnArr["rt_cd"] = "0004";//無法對應到電池資料
				// 	$returnArr["rt_msg"] = "無法對應到電池資料";
				// 	echo json_encode($returnArr);
				// 	$time_end = microtime(true);
				// 	$time = $time_end - $time_start;
				// 	$otherdataArr['sys_spend_time'] = $time;
				// 	$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
				// 	return;
				// }

				if($bss_token!="")
				{
					$SQLCmdB = "SELECT s_num,bss_id,location,latitude,longitude FROM tbl_battery_swap_station WHERE bss_token = '{$bss_token}' AND status = 'Y'";
					$rsB = $this->db_query($SQLCmdB);
					if($rsB)
					{
						$dataArr['return_sb_num'] = $rsB[0]['s_num'];
						$battery_swap_location = $rsB[0]['location'];
						$return_gps = $rsB[0]['latitude'].",".$rsB[0]['longitude'];
						$return_bss_id = $rsB[0]['bss_id'];
					}
					else
					{
						$returnArr["rt_cd"] = "0007";//借电站token有误
						$returnArr["rt_msg"] = "借电站token有误";
						echo json_encode($returnArr);
						$time_end = microtime(true);
						$time = $time_end - $time_start;
						$otherdataArr['sys_spend_time'] = $time;
						$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
						return;
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0007";//借电站token有误
					$returnArr["rt_msg"] = "借电站token有误";
					echo json_encode($returnArr);
					$time_end = microtime(true);
					$time = $time_end - $time_start;
					$otherdataArr['sys_spend_time'] = $time;
					$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
					return;
				}

				$SQLCmdM = "SELECT * FROM tbl_member WHERE user_id = '{$User_ID}' AND status = 'Y'";
				$rsM = $this->db_query($SQLCmdM);
				$whereStrBA = "";
				if($rsM)
				{
					if($rsM[0]['member_type']=="F")
					{
						$whereStrBA = "AND lblr.leave_battery_id = '{$battery_id}'";
					}
				}
				if($one_trans=="Y")
				{
					$SQLCmdL = "SELECT lblr.s_num as lblr_num,lblr.battery_user_id,lblr.leave_date,lblr.leave_sb_num,lblr.leave_track_no,lblr.leave_battery_id,lblr.wechat_orderSn      
							FROM log_battery_leave_return lblr 
							WHERE lblr.leave_battery_id = '{$battery_id}' 
							AND lblr.leave_date IS NOT NULL 
							AND lblr.return_date IS NULL 
							ORDER BY lblr.s_num DESC 
							limit 1";
					$rsL = $this->db_query($SQLCmdL);
					if($rsL)
					{
						$User_ID = $rsL[0]['battery_user_id'];
						$returnArr["rt_10"] = substr($User_ID, -10);//成功
						$SQLCmdM = "SELECT * FROM tbl_member WHERE user_id = '{$User_ID}' AND status = 'Y'";
						$rsM = $this->db_query($SQLCmdM);
					}
				}
				else
				{
					$SQLCmdL = "SELECT lblr.s_num as lblr_num,lblr.leave_date,lblr.leave_sb_num,lblr.leave_track_no,lblr.leave_battery_id,lblr.wechat_orderSn      
							FROM log_battery_leave_return lblr 
							WHERE lblr.battery_user_id = '{$User_ID}' 
							AND lblr.leave_date IS NOT NULL 
							AND lblr.return_date IS NULL 
							AND lblr.identifier_code = '{$transToken}' 
							{$whereStrBA}
							ORDER BY lblr.s_num DESC 
							limit 1";
					$rsL = $this->db_query($SQLCmdL);
				}

				if($return_status==0)
				{
					if($transToken!="")
					{
						if($rsL)
						{
							$leave_date = $rsL[0]['leave_date'];
							$leave_sb_num = $rsL[0]['leave_sb_num'];
							$leave_track_no = $rsL[0]['leave_track_no'];
							$leave_battery_id = $rsL[0]['leave_battery_id'];
						}
						else
						{
							$returnArr["rt_cd"] = "0005";//成功
							$returnArr["rt_msg"] = "無法取得租借紀錄";//成功
							echo json_encode($returnArr);
							$time_end = microtime(true);
							$time = $time_end - $time_start;
							$otherdataArr['sys_spend_time'] = $time;
							$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
							exit;
						}	
					}
					else
					{
						$returnArr["rt_cd"] = "0008";//成功
						$returnArr["rt_msg"] = "交易Token不得為空";//成功
						echo json_encode($returnArr);
						$time_end = microtime(true);
						$time = $time_end - $time_start;
						$otherdataArr['sys_spend_time'] = $time;
						$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
						exit;
					}
				}
				else
				{
					$bss_id = $rsB[0]['bss_id'];
					$leave_location = $rsB[0]['location'];
					$SQLCmdM = "SELECT s_num FROM tbl_member WHERE user_id = '{$User_ID}' AND status = 'Y'";
					$rsM = $this->db_query($SQLCmdM);
					$tm_num = $rsM[0]['s_num'];
					$dataArr['tm_num'] = $tm_num;
					$dataArr['battery_user_id'] = $User_ID;
					$dataArr['return_track_no'] = $trace_no;
					$dataArr['return_battery_id'] = $battery_id;
					$dataArr['return_status'] = $return_status;
					$dataArr['usage_time'] = "";
					$orderSn = "";
					if($rsL)
					{
						$whereStr = "s_num = {$rsL[0]['lblr_num']}";
						//更新電池租借歸還記錄表
						if($this->db_update('log_battery_leave_return',$dataArr,$whereStr))
						{
							$returnArr["rt_cd"] = "0000";//成功
							$returnArr["rt_msg"] = "成功";//成功
						}
						else
						{
							$returnArr["rt_cd"] = "0002";//更新失敗
							$returnArr["rt_msg"] = "更新失败";//更新失敗
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0005";//成功
						$returnArr["rt_msg"] = "無法取得租借紀錄";//成功
						echo json_encode($returnArr);
						$time_end = microtime(true);
						$time = $time_end - $time_start;
						$otherdataArr['sys_spend_time'] = $time;
						$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
						exit;
					}
					echo json_encode($returnArr);
					$time_end = microtime(true);
					$time = $time_end - $time_start;
					$otherdataArr['sys_spend_time'] = $time;
					$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
					exit;
				}

				$usage_time = $this->minDiff($leave_date,$return_date);//使用時間
				// $charge_amount = ceil($usage_time/30)*2.5;//扣款金額

				if($rsM)
				{
					$dataArrM = array();

					//更新會員儲值餘額欄位
					$whereStrM = "s_num = {$rsM[0]['s_num']}";
					// $dataArrM['stored_value'] = $dataArrS['store_balance_after'];
					// $dataArrM['remaining_time'] = $dataArrS['store_balance_after'];
					$SQLCmdL = "SELECT count(*) cnt FROM log_battery_leave_return WHERE user_id = '{$User_ID}' AND return_date IS NULL";
					$rsL = $this->db_query($SQLCmdL);
					if($rsL[0]['cnt']==0)
					{
						$dataArrM['lease_status'] = 'N';
					}
					$this->db_update('tbl_member',$dataArrM,$whereStrM);

					$whereStrB = "battery_id = '{$battery_id}' AND status <> 'D'";
					$dataArrB['sv_num'] = NULL;
					$dataArrB['position'] = 'B';
					$this->db_update('tbl_battery',$dataArrB,$whereStrB);
				}
				else
				{
					$returnArr["rt_cd"] = "0006";//更新失敗
					$returnArr["rt_msg"] = "会员ID有误";//
					echo json_encode($returnArr);
					$time_end = microtime(true);
					$time = $time_end - $time_start;
					$otherdataArr['sys_spend_time'] = $time;
					$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
					return;
				}
				

				if($return_status=="" || $return_status==0)
				{
					$dataArr['return_request_date'] = $return_date;
					$dataArr['return_date'] = $return_date;
				}
				$dataArr['return_track_no'] = $trace_no;
				$dataArr['return_battery_id'] = $battery_id;
				$dataArr['return_status'] = $return_status;
				$dataArr['usage_time'] = $this->shi_jian_cha($return_date,$leave_date);
				// $dataArr['charge_amount'] = $charge_amount;
				$whereStr = "s_num = {$rsL[0]['lblr_num']}";

				//更新電池租借歸還記錄表
				if($this->db_update('log_battery_leave_return',$dataArr,$whereStr))
				{
					$returnArr["rt_cd"] = "0000";//回應碼
					$returnArr["rt_msg"] = "成功";//回應訊息
					$returnArr["rt_01"] = $leave_date;//電池租借日期
					$returnArr["rt_02"] = $return_date;//電池歸還日期
					$returnArr["rt_03"] = $dataArr['usage_time'];//此次租借時間
					$returnArr["rt_09"] = $battery_swap_location;//租借地點
				}
				else
				{
					$returnArr["rt_cd"] = "0002";//更新失敗
					$returnArr["rt_msg"] = "更新失败";//更新失敗
				}

			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式錯誤
			$returnArr["rt_msg"] = "格式错误";//格式錯誤
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//計算時間差
	public function minDiff($startTime, $endTime) {
	    $start = strtotime($startTime);
	    $end = strtotime($endTime);
	    $timeDiff = $end - $start;
	    return ceil($timeDiff / 60);
	    // return floor($timeDiff / 60);
	}

	//計算時間差
	public function shi_jian_cha($d,$d1)
	{ 
		$time = strtotime($d) - strtotime($d1); 
		$n_time = str_pad(floor($time%(24*3600)/3600),2,0,STR_PAD_LEFT ).":".str_pad(floor($time%3600/60),2,0,STR_PAD_LEFT).":".str_pad($time%3600%60, 2, 0, STR_PAD_LEFT)."秒";
		return $n_time;
	}

	//乱数产生token
	function generatorToken()
	{
	    $password_len = 8;
	    $password = '';

	    // remove o,0,1,l
	    $word = 'abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ23456789';
	    $len = strlen($word);

	    for ($i = 0; $i < $password_len; $i++) {
	        $password .= $word[rand() % $len];
	    }

	    return $password;
	}

}

/* End of file Model_bss02.php */
