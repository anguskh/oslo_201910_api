<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_bss02 extends MY_Model {
	
    function __construct() 
	{
        parent::__construct();
		// echo "in Model_column";
	}

	public function borrow_battery_success(){
		//計算執行時間
		$time_start = microtime(true);
		//寫入收到的資料到歷史紀錄
		$receive_data = $this->input->post();
		$api_name = 'c2af4999';
		$api_chinese_name = '借电回报';
		$api_log_sn = $this->model_common->insert_api_log($receive_data,$api_name,$api_chinese_name);
		// $output = implode(', ', array_map(
		//     function ($v, $k) { return sprintf("%s='%s'", $k, $v); },
		//     $this->input->post(),
		//     array_keys($this->input->post())
		// ));
		// $dataArr = array();
		// date_default_timezone_set('Asia/Taipei');
		// $logdataArr['api_name'] = 'bss02';
		// $logdataArr['receive_data'] = $output;
		// $logdataArr['receive_ip'] = $_SERVER['REMOTE_ADDR'];
		// $logdataArr['receive_date'] = "now()";
		// $this->db_insert("log_api_history",$logdataArr);

		$postjson = $this->input->post("JSONData");
		//先判斷是否為Json格式
		if($this->is_json($postjson))
		{
			$returnArr = array();
			//收到空資料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//資料為空
				$returnArr["rt_msg"] = "资料为空";//資料為空
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$brrow_date = addslashes($data_json_de['bs01']);//借電站電池借出日期
				$bss_tokenID = addslashes($data_json_de['bs02']);//借電站tokenID
				if($data_json_de['bs03']!="")
					$User_ID = str_pad(addslashes($data_json_de['bs03']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
				else
					$User_ID = "";
				$trace_no = addslashes($data_json_de['bs04']);//取出電池的軌道編號
				$battery_id = addslashes($data_json_de['bs05']);//電池序號
				$identifier_code = addslashes($data_json_de['bs06']);//交換站電池借電唯一交易序號
				if(isset($data_json_de['bs07']))
					$leave_status = addslashes($data_json_de['bs07']);//借電狀態（0:借電成功；1:機櫃還電門未打開；2:用戶未放入電池；3:放入非借出電池；4:取電門未打開；5:用戶未取出電池)
				else
					$leave_status = "";
				if(isset($data_json_de['bs08']))
					$leave_battery_capacity = addslashes($data_json_de['bs08']);//電池電量
				else
					$leave_battery_capacity = "";

				if($this->model_common->checkdata($api_name,$brrow_date))
				{
					exit();
				}

				$otherdataArr['user_id'] = $User_ID;
				$otherdataArr['battery_id'] = $battery_id;
				
				$SQLCmdM = "SELECT * FROM tbl_member WHERE user_id = '{$User_ID}'";
				$rsM = $this->db_query($SQLCmdM);
				if(count($rsM) == 0)
				{
						$returnArr["rt_cd"] = "0006";//更新失敗
						$returnArr["rt_msg"] = "会员ID有误";//
						// $dataArrM['member_type'] = 'B';
						// $dataArrM['member_register'] = 'P';
						// $dataArrM['name'] = '系統自動寫入';
						// $dataArrM['mobile'] = $this->randString(10,'0123456789');
						// $key = md5('ebike'.$dataArrM['mobile']);
						// $key = substr($key, 0, 24);
						// $string = $this->randString(10);
						// //加密
						// $dataArrM['personal_id'] = $this->encrypt($key, $string);
						// $dataArrM['user_id'] = $User_ID;
						// $dataArrM['gender'] = $this->randString(1,'MF');
						// $dataArrM['nationality'] = '158';
						// $dataArrM['pay_type'] = 'W';
						// $dataArrM['stored_type'] = 'N';
						// $dataArrM['lease_status'] = 'N';
						// $dataArrM['status'] = 'Y';
						// $dataArrM['create_user'] = '1';
						// $dataArrM['create_date'] = 'now()';
						// $dataArrM['create_ip'] = '127.0.0.1';
						// $this->db_insert('tbl_member',$dataArrM);
						echo json_encode($returnArr);
						$time_end = microtime(true);
						$time = $time_end - $time_start;
						$otherdataArr['sys_spend_time'] = $time;
						$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
						exit;
				}
				
				$return_date = "";
				$return_location = "";
				$return_track_no = "";
				$return_battery_id = "";
				$return_gps = "";
				$battery_capacity = "";
				$return_battery_capacity = "";
				$return_battery_health = "";
				$leave_battery_health = "";
				if($rsM[0]['member_type']=="F")
				{
					$SQLCmdL = "SELECT return_date,return_sb_num,return_track_no,return_battery_id FROM log_battery_leave_return WHERE battery_user_id ='{$User_ID}' ORDER BY return_date DESC limit 1";
					$rsL = $this->db_query($SQLCmdL);
					if($rsL)
					{
						$return_date = $rsL[0]['return_date'];
						$return_track_no = $rsL[0]['return_track_no'];
						$return_battery_id = $rsL[0]['return_battery_id'];
						$return_sb_num = $rsL[0]['return_sb_num'];
						if($return_sb_num!="")
						{
							$SQLCmdBS = "SELECT latitude,longitude,location FROM tbl_battery_swap_station WHERE s_num = {$return_sb_num} AND status <> 'D'";
							$rsBS = $this->db_query($SQLCmdBS);
							$return_gps = "";
							if($rsBS)
							{
								$return_gps = $rsBS[0]['latitude'].",".$rsBS[0]['longitude'];
								$return_location = $rsBS[0]['location'];
							}
						}
					}
				}
				else
				{
					$SQLCmdL = "SELECT return_date,return_sb_num,return_track_no,return_battery_id,return_battery_capacity FROM log_battery_leave_return WHERE battery_user_id ='{$User_ID}' ORDER BY return_date DESC limit 1";
					$rsL = $this->db_query($SQLCmdL);
					if(count($rsL)==1)
					{
						$return_date = $rsL[0]['return_date'];
						$return_track_no = $rsL[0]['return_track_no'];
						$return_battery_id = $rsL[0]['return_battery_id'];
						$return_sb_num = $rsL[0]['return_sb_num'];
						if($return_sb_num!="")
						{
							$SQLCmdBS = "SELECT latitude,longitude,location FROM tbl_battery_swap_station WHERE s_num = {$return_sb_num} AND status <> 'D'";
							$rsBS = $this->db_query($SQLCmdBS);
							$return_gps = "";
							if($rsBS)
							{
								$return_gps = $rsBS[0]['latitude'].",".$rsBS[0]['longitude'];
								$return_location = $rsBS[0]['location'];
							}
						}

						$return_battery_capacity = $rsL[0]['return_battery_capacity'];

						if($return_battery_id!="")
						{
							$SQLCmdRB = "SELECT battery_capacity,battery_health FROM tbl_battery WHERE battery_id = '{$return_battery_id}'";
							$rsRB = $this->db_query($SQLCmdRB);
							if($rsRB)
							{
								if($return_battery_capacity=="")
									$return_battery_capacity = $rsRB[0]['battery_capacity'];
								$return_battery_health = $rsRB[0]['battery_health'];
							}
						}
					}
				}

				$battery_capacity = $leave_battery_capacity;

				if($leave_status=="" || $leave_status==0)
				{	
					$SQLCmd = "SELECT DorO_flag,do_num,battery_capacity,battery_health FROM tbl_battery WHERE battery_id = '{$battery_id}'";
					$rs = $this->db_query($SQLCmd);
					if($rs)
					{
						if($leave_battery_capacity=="")
							$battery_capacity = $rs[0]['battery_capacity'];
						$leave_battery_health = $rs[0]['battery_health'];
						$dataArr['leave_DorO_flag'] = $rs[0]['DorO_flag'];
						$dataArr['leave_do_num'] = $rs[0]['do_num'];
					}
					else
					{
						$returnArr["rt_cd"] = "0004";//無法對應到電池資料
						$returnArr["rt_msg"] = "无法对应到电池资料";
					}

					$SQLCmdS = "SELECT s_num,so_num,bss_id,location FROM tbl_battery_swap_station WHERE bss_token = '{$bss_tokenID}' AND status <> 'D'";
					$rsS = $this->db_query($SQLCmdS);
					if($rsS)
					{
						$bss_id = $rsS[0]['bss_id'];
						$leave_location = $rsS[0]['location'];
						$SQLCmdM = "SELECT s_num,to_num,member_type FROM tbl_member WHERE user_id = '{$User_ID}' AND status = 'Y'";
						$rsM = $this->db_query($SQLCmdM);
						$sb_num = $rsS[0]['s_num'];
						$tm_num = $rsM[0]['s_num'];
						$dataArr['tm_num'] = $tm_num;
						$dataArr['system_log_date'] = "now()";
						$dataArr['identifier_code'] = $identifier_code;
						$dataArr['leave_sb_num'] = $sb_num;
						$dataArr['battery_user_id'] = $User_ID;
						$dataArr['leave_date'] = $brrow_date;
						$dataArr['leave_track_no'] = $trace_no;
						$dataArr['leave_battery_id'] = $battery_id;
						$dataArr['leave_battery_capacity'] = $leave_battery_capacity;
						$dataArr['leave_status'] = $leave_status;
						// $whereStr = "s_num = ( SELECT a.s_num FROM 
						// 			(SELECT lblr.s_num 
						// 			FROM log_battery_leave_return lblr 
						// 			LEFT JOIN tbl_battery_swap_station tbss ON lblr.leave_sb_num = tbss.s_num 
						// 			WHERE tbss.bss_token = '{$bss_tokenID}' 
						// 			AND lblr.battery_user_id = '{$User_ID}' 
						// 			AND leave_date IS NULL 
						// 			ORDER BY lblr.s_num DESC 
						// 			limit 1) a)";
						if($this->db_insert('log_battery_leave_return',$dataArr))
						{
							$update_to_num = "";
							if($rsM[0]['to_num']=="" || $rsM[0]['to_num']=="0")
							{
								$update_to_num = "to_num = '{$rsS[0]['so_num']}',";
							}
							$SQLCmdM = "UPDATE tbl_member SET {$update_to_num}lease_status = 'L',change_count = change_count+1 WHERE user_id = '{$User_ID}' AND status <> 'D'";
							$this->db_query($SQLCmdM); 
							if($rs)
							{
								$SQLCmdB = "UPDATE tbl_battery SET position = 'V',exchange_count = exchange_count+1 WHERE battery_id = '{$battery_id}' AND status <> 'D'";
								$this->db_query($SQLCmdB); 
							}
							$SQLCmdBS = "UPDATE tbl_battery_swap_station SET exchange_num = exchange_num+1 WHERE s_num = '{$sb_num}' AND status <> 'D'";
							$this->db_query($SQLCmdBS); 
							$returnArr["rt_cd"] = "0000";//成功
							$returnArr["rt_msg"] = "成功";//成功
							
							$postArr = array();
							// $postArr['action'] = "actionPostCabinetCallback";
							$postArr['result'] = (int)$leave_status;
							$postArr['msg'] = "成功";
							$postArr['sn_old'] = $return_battery_id;
							$postArr['sn_new'] = $battery_id;
							date_default_timezone_set('UTC');
							$date = new DateTime();
							$postArr['ts'] = $date->getTimestamp();
							$postArr['sid'] = $bss_id;
							$postArr['sname'] = $leave_location;
							$this->app_callback($postArr,"机柜回调接口");
						}
						else
						{
							$returnArr["rt_cd"] = "0002";//更新失敗
							$returnArr["rt_msg"] = "更新失败";//更新失敗
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0005";//借電站token有誤
						$returnArr["rt_msg"] = "借电站token有误";
					}
				}
				else
				{
					$SQLCmdS = "SELECT s_num,bss_id,location FROM tbl_battery_swap_station WHERE bss_token = '{$bss_tokenID}' AND status <> 'D'";
					$rsS = $this->db_query($SQLCmdS);
					if($rsS)
					{
						$bss_id = $rsS[0]['bss_id'];
						$leave_location = $rsS[0]['location'];
						$SQLCmdM = "SELECT s_num FROM tbl_member WHERE user_id = '{$User_ID}' AND status = 'Y'";
						$rsM = $this->db_query($SQLCmdM);
						$sb_num = $rsS[0]['s_num'];
						$tm_num = $rsM[0]['s_num'];
						$dataArr['tm_num'] = $tm_num;
						$dataArr['system_log_date'] = "now()";
						// $dataArr['identifier_code'] = $identifier_code;
						$dataArr['leave_sb_num'] = $sb_num;
						$dataArr['battery_user_id'] = $User_ID;
						$dataArr['leave_track_no'] = $trace_no;
						$dataArr['leave_battery_id'] = $battery_id;
						$dataArr['leave_battery_capacity'] = $leave_battery_capacity;
						$dataArr['leave_status'] = $leave_status;
						if($this->db_insert('log_battery_leave_return',$dataArr))
						{
							$returnArr["rt_cd"] = "0000";//成功
							$returnArr["rt_msg"] = "成功";//成功
						}
						else
						{
							$returnArr["rt_cd"] = "0002";//更新失敗
							$returnArr["rt_msg"] = "更新失败";//更新失敗
						}
						$postArr = array();
						$postArr['action'] = "actionPostBadCallback";
						$postArr['orderSn'] = "";
						$postArr['status'] = $leave_status;
						$postArr['location'] = $leave_location;
						$postArr['cabinetNumber'] = $bss_id;
						$postArr['userId'] = ltrim($User_ID,"0");
						if($leave_status == 4 || $leave_status==5)
							$postArr['returnBatteryNumber'] = $return_battery_id;
						else
							$postArr['returnBatteryNumber'] = "";
						$this->wechat_callback($postArr,"异常回调接口");

						// $postArr = array();
						// // $postArr['action'] = "actionPostBadCallback";
						// $postArr['result'] = (int)$leave_status;
						// $postArr['msg'] = "借电失败";
						// if($leave_status == 4 || $leave_status==5)
						// 	$postArr['sn_old'] = $return_battery_id;
						// else
						// 	$postArr['sn_old'] = "";					
						// $postArr['sn_new'] = $battery_id;
						// $postArr['sid'] = $bss_id;
						// $postArr['sname'] = $leave_location;
						// date_default_timezone_set('UTC');
						// $date = new DateTime();
						// $postArr['ts'] = $date->getTimestamp();
						// $this->app_callback($postArr,"异常回调接口");
					}
					else
					{
						$returnArr["rt_cd"] = "0005";//借電站token有誤
						$returnArr["rt_msg"] = "借电站token有误";
					}
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式錯誤
			$returnArr["rt_msg"] = "格式错误";//格式錯誤
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	public function encrypt($source,$toencrypt){  
		//加密用的key   

		$key = $source;  

		//使用3DES方法加密   

		$encryptMethod = MCRYPT_TRIPLEDES; 

		//初始化向量来增加安全性

		$iv = mcrypt_create_iv(mcrypt_get_iv_size($encryptMethod,MCRYPT_MODE_ECB), MCRYPT_RAND);  

		//使用mcrypt_encrypt函数加密，MCRYPT_MODE_ECB表示使用ECB模式

		$encrypted_toencrypt = mcrypt_encrypt($encryptMethod, $key, $toencrypt, MCRYPT_MODE_ECB,$iv);   

		//回传解密后字串

		return base64_encode($encrypted_toencrypt);  

	}

	function randString($length, $charset='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789')
	{
		$str = '';
		$count = strlen($charset);
		while ($length--) {
			$str .= $charset[mt_rand(0, $count-1)];
		}
		return $str;
	}

	//比对借电电池ID API
	public function compare_leave_battery_id()
	{
		//計算執行時間
		$time_start = microtime(true);
		//寫入收到的資料到歷史紀錄
		$receive_data = $this->input->post();
		$api_name = 'c6be119b';
		$api_chinese_name = '比对借电电池ID';
		$api_log_sn = $this->model_common->insert_api_log($receive_data,$api_name,$api_chinese_name);
		// $output = implode(', ', array_map(
		//     function ($v, $k) { return sprintf("%s='%s'", $k, $v); },
		//     $this->input->post(),
		//     array_keys($this->input->post())
		// ));
		// $dataArr = array();
		// date_default_timezone_set('Asia/Taipei');
		// $logdataArr['api_name'] = 'bss02';
		// $logdataArr['receive_data'] = $output;
		// $logdataArr['receive_ip'] = $_SERVER['REMOTE_ADDR'];
		// $logdataArr['receive_date'] = "now()";
		// $this->db_insert("log_api_history",$logdataArr);

		$postjson = $this->input->post("JSONData");
		$returnArr = array();
		//先判斷是否為Json格式
		if($this->is_json($postjson))
		{
			//收到空資料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//資料為空
				$returnArr["rt_msg"] = "資料為空";//資料為空
			}
			else
			{
				$data_json_de = json_decode($postjson,true);
				$bss_send_date = $data_json_de['cb01'];//租借站發送時間
				if($this->model_common->checkdata($api_name,$bss_send_date))
				{
					exit();
				}
				$SQLCmdS = "SELECT config_set FROM sys_config WHERE config_code = 'compare_battery_id' AND status <> 'D'";
				$rsS = $this->db_query($SQLCmdS);
				if($rsS)
				{
					//若比對電池開關是關就完全不比對,直接回比對成功
					if($rsS[0]['config_set']=="N")
					{
						$returnArr["rt_cd"] = "0000";//成功
						$returnArr["rt_msg"] = "成功";//成功
						$returnArr["rt_01"] = "Y";//電池對應正確
						echo json_encode($returnArr);
						$time_end = microtime(true);
						$time = $time_end - $time_start;
						$otherdataArr['sys_spend_time'] = $time;
						$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
						exit;
					}
				}
				
				$dataArr = array();
				$bss_id = $data_json_de['cb02'];//bss序號
				if($data_json_de['cb03']!="")
					$user_id = str_pad($data_json_de['cb03'],32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
				else
					$user_id = "";
				$battery_id = $data_json_de['cb04'];//電池序號
				$otherdataArr['user_id'] = $user_id;
				$otherdataArr['battery_id'] = $battery_id;
				$SQLCmdBS = "SELECT count(*) cnt FROM tbl_battery_swap_station WHERE bss_id = '{$bss_id}' AND status <> 'D'";
				$rsBs = $this->db_query($SQLCmdBS);
				if($rsBs[0]['cnt']!=0)
				{
					$SQLCmdM = "SELECT eng_staff,member_type,check_wrong_time FROM tbl_member WHERE user_id = '{$user_id}' AND status = 'Y'";
					$rsM = $this->db_query($SQLCmdM);
					if(count($rsM)!= 0)
					{
						$SQLCmdB = "SELECT locked_flag FROM tbl_battery WHERE battery_id = '$battery_id' AND status <> 'D'";
						$rsB = $this->db_query($SQLCmdB);
						if(count($rsB)!= 0)
						{
							if($rsB[0]['locked_flag']==0)
							{
								$whereStr = "";
								if($rsM[0]['member_type']!="")
									$whereStr = "AND leave_battery_id = '{$battery_id}'";
								$SQLCmd = "SELECT * FROM log_battery_leave_return WHERE battery_user_id = '{$user_id}' {$whereStr} ORDER BY system_log_date DESC limit 1";
								$rs = $this->db_query($SQLCmd);
								if($rs)
								{
									if($rs[0]['return_date']!="")
									{
										//工程人員回覆E
										if($rsM[0]['eng_staff']==1)
										{
											$returnArr["rt_cd"] = "0000";//无法取得租借纪录
											$returnArr["rt_msg"] = "成功";
											$returnArr["rt_01"] = "E";
										}
										else
										{
											$returnArr["rt_cd"] = "0000";//无法取得租借纪录
											$returnArr["rt_msg"] = "成功";
											$returnArr["rt_01"] = "N";
										}
										
									}
									else
									{
										$leave_battery_id = $rs[0]['leave_battery_id'];
										if($leave_battery_id==$battery_id)
										{
											$returnArr["rt_cd"] = "0000";//成功
											$returnArr["rt_msg"] = "成功";//成功
											$returnArr["rt_01"] = "Y";//電池對應正確
											//比對錯誤次數歸0
											$dataArr['check_wrong_time'] = 0;
											$whereStr = "user_id = '{$user_id}' AND status = 'Y'";
											$this->db_update('tbl_member',$dataArr,$whereStr);
										}
										else
										{
											if($rsM[0]['check_wrong_time']<=3)
											{
												$returnArr["rt_cd"] = "0000";//成功
												$returnArr["rt_msg"] = "成功";//成功
												$returnArr["rt_01"] = "Y";//電池對應正確
												//比對錯誤次數+1
												$dataArr['check_wrong_time'] = $rsM[0]['check_wrong_time']+1;
												$whereStr = "user_id = '{$user_id}' AND status = 'Y'";
												$this->db_update('tbl_member',$dataArr,$whereStr);
											}
											else
											{
												$returnArr["rt_cd"] = "0000";//电池对应错误
												$returnArr["rt_msg"] = "成功";
												$returnArr["rt_01"] = "N";//電池對應錯誤
											}
											
										}
									}
								}
								else
								{
									$returnArr["rt_cd"] = "0000";//比對失敗
									$returnArr["rt_msg"] = "成功";
									$returnArr["rt_01"] = "N";
								}
							}
							else
							{
								$returnArr["rt_cd"] = "0000";//电池已被锁定
								$returnArr["rt_msg"] = "成功";
								$returnArr["rt_01"] = "L";
							}
						} 
						else
						{
							$returnArr["rt_cd"] = "0005";//电池序号有误
							$returnArr["rt_msg"] = "电池序号有误";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0004";//借电站bssid有误
					$returnArr["rt_msg"] = "借电站bssid有误";
				}
				
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式錯誤
			$returnArr["rt_msg"] = "格式錯誤";//格式錯誤
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	public function wechat_callback($postArr=array(),$api_chinese_name="")
	{
		//取得微信的網址參數
		$SQLCmdC = "SELECT config_set FROM sys_config WHERE config_code ='wechat_callback_url' AND status = 1";
		$rsC = $this->db_query($SQLCmdC);
		if($rsC)
		{
			$url = $rsC[0]['config_set']."cabinetcallback";
		}
		else
		{
			$url = "";
		}

		$postStr = http_build_query($postArr);

		date_default_timezone_set("Asia/Taipei");
		$dataArrLog = array();
		$dataArrLog['sys_date'] = date("Y-m-d H:i:s");
		$dataArrLog['api_name'] = $postArr['action'];
		$dataArrLog['api_chinese_name'] = $api_chinese_name;
		if(isset($postArr['rentTime']))
			$dataArrLog['send_date'] = $postArr['rentTime'];
		else
			$dataArrLog['send_date'] = "now()";
		$dataArrLog['send_data'] = $postStr;

		$this->db_insert("log_send_wechat_api",$dataArrLog);
		// $SQLCmdLa = "select LAST_INSERT_ID() as log_sn FROM log_send_wechat_api";
		$SQLCmdLa = "select s_num as log_sn FROM log_send_wechat_api WHERE sys_date = '{$dataArrLog['sys_date']}'";
		$rsLa = $this->db_query($SQLCmdLa);
		$log_sn = $rsLa[0]['log_sn'];
		// $log_sn = mysql_insert_id();
		$whereStrLog = " s_num = {$log_sn}";
		// $postStr = http_build_query($postArr);
		// $postStr = http_build_query(array("Book"=>1));
		// $postStr = '{"Book":"1"}';
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postStr); 
		$response = curl_exec($ch);
		$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		$response = trim($response);
		if($http_code!='200')
		{
			$response = "{$http_code} wechat error";
		}
		else if(!$this->is_json($response))
		{
			$response = "格式錯誤";
		}

		// $output = curl_exec($ch);
		// curl_close($ch);
		
		// $output = trim($output);
		// if(@strpos($output,"502 Bad Gateway")!==false)
		// {
		// 	$output = "502 Bad Gateway";
		// }
		// else if(!$this->is_json($output))
		// {
		// 	$output = "格式錯誤";
		// }

		$dataArrLog = array();
		$dataArrLog['receive_date'] = "now()";
		$dataArrLog['receive_data'] = $response;
		$this->db_update("log_send_wechat_api",$dataArrLog,$whereStrLog);
	}

	public function app_callback($postArr=array(),$api_chinese_name="")
	{
		// //取得微信的網址參數
		// $SQLCmdC = "SELECT config_set FROM sys_config WHERE config_code ='wechat_callback_url' AND status = 1";
		// $rsC = $this->db_query($SQLCmdC);
		// if($rsC)
		// {
		// 	$url = $rsC[0]['config_set']."cabinetcallback";
		// }
		// else
		// {
		// 	$url = "";
		// }
		date_default_timezone_set('Asia/Taipei');
		$url = "https://api.d.bluesharkmotor.com/webhook/battswap/swapresult";
		$postStr = http_build_query($postArr);

		$dataArrLog = array();
		$dataArrLog['api_name'] = 'actionPostCabinetCallback';
		$dataArrLog['api_chinese_name'] = $api_chinese_name;
		if(isset($postArr['returnTime']))
			$dataArrLog['send_date'] = $postArr['returnTime'];
		else
			$dataArrLog['send_date'] = "now()";
		$dataArrLog['send_data'] = $postStr;

		$this->db_insert("log_send_wechat_api",$dataArrLog);
		$SQLCmdLa = "select LAST_INSERT_ID() as log_sn";
		$rsLa = $this->db_query($SQLCmdLa);
		$log_sn = $rsLa[0]['log_sn'];
		$whereStrLog = " s_num = {$log_sn}";
		// $postStr = http_build_query($postArr);
		// $postStr = http_build_query(array("Book"=>1));
		// $postStr = '{"Book":"1"}';
		$postStr = json_encode($postStr);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postStr); 
		// 這裡略過檢查 SSL 憑證有效性
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$output = curl_exec($ch);
		curl_close($ch);
		$output = trim($output);
		if(@strpos($output,"502 Bad Gateway")!==false)
		{
			$output = "502 Bad Gateway";
		}
		else if(!$this->is_json($output))
		{
			$output = "格式錯誤";
		}
		
		$dataArrLog = array();
		$dataArrLog['receive_date'] = "now()";
		$dataArrLog['receive_data'] = $output;
		$this->db_update("log_send_wechat_api",$dataArrLog,$whereStrLog);
	}
}

/* End of file Model_bss02.php */
