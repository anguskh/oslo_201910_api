<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_bss05 extends MY_Model {
	
    function __construct() 
	{
        parent::__construct();
		// echo "in Model_column";
	}

	public function timely_warring(){
		//計算執行時間
		$time_start = microtime(true);
		//寫入收到的資料到歷史紀錄
		$receive_data = $this->input->post();
		$api_name = 'f1ace353';
		$api_chinese_name = '即時告警';
		$api_log_sn = $this->model_common->insert_api_log($receive_data,$api_name,$api_chinese_name);

		$postjson = $this->input->post("JSONData");
		//先判斷是否為Json格式
		if($this->is_json($postjson))
		{
			$returnArr = array();
			//收到空資料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//資料為空
				$returnArr["rt_msg"] = "資料為空";//資料為空
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				if(isset($data_json_de['rw01']) && isset($data_json_de['rw02']) && isset($data_json_de['rw03']))
				{
					if($data_json_de['rw01']!="" && $data_json_de['rw02']!="" && $data_json_de['rw03']!="" )
					{
						$alarm_date = $data_json_de['rw01'];//告警日期
						$bss_tokenID = $data_json_de['rw02'];//借電站tokenID
						$alarm_type = $data_json_de['rw03'];//告警類別
						$SQLCmdA = "SELECT count(*) cnt FROM log_alarm_online WHERE bss_token_id = '{$bss_tokenID}' AND type = '{$alarm_type}' AND status != '3'";
						$rsA = $this->db_query($SQLCmdA);
						$SQLCmdB = "SELECT s_num,so_num,bss_id FROM tbl_battery_swap_station WHERE bss_token = '{$bss_tokenID}' AND status <> 'D'";
						$rsB = $this->db_query($SQLCmdB);
						if($rsB)
						{
							if($rsA[0]['cnt']==0)
							{
								$dataArr = array();
								$dataArr['so_num'] = $rsB[0]['so_num'];
								$dataArr['sb_num'] = $rsB[0]['s_num'];
								$dataArr['log_date'] = $alarm_date;
								$dataArr['bss_token_id'] = $bss_tokenID;
								$dataArr['type'] = $alarm_type;
								$dataArr['status'] = 1;//處理狀態預設為未檢視
								$this->db_insert("log_alarm_online",$dataArr);
								switch ($alarm_type) {
									case '1':
										$type_name = "火災";
										break;
									case '2':
										$type_name = "淹水";
										break;
									default:
										$type_name = "不明災害";
										break;
								}
								// $this->sendmail($rsB[0]['bss_id'],$type_name);
								//即時傳送訊息給正在線上的使用者
								// $this->model_websocket->send_WmsgtoWeb("機櫃{$rsB[0]['bss_id']}發生{$type_name},請緊急處理!");
							}
							$returnArr["rt_cd"] = "0000";//成功
							$returnArr["rt_msg"] = "成功";
						}
						else
						{
							$returnArr["rt_cd"] = "0004";//借电站token有误
							$returnArr["rt_msg"] = "借电站token有误";
						}
						
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//資料為空
						$returnArr["rt_msg"] = "JSON欄位資料不得為空";//資料為空
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0001";//格式錯誤
					$returnArr["rt_msg"] = "格式錯誤";//格式錯誤
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式錯誤
			$returnArr["rt_msg"] = "格式錯誤";//格式錯誤
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//传送mail
	public function sendmail($sb_name="",$w_type)
	{

		ini_set('date.timezone',"Asia/Taipei");
		$web = $this->config->item('base_url');
		$this->load->library('email');

		$SQLCmd = "SELECT * FROM sys_config WHERE status = 1";
		$rs = $this->db_query($SQLCmd);
		if(count($rs)>0)
		{
			foreach($rs as $key => $value)
			{
				switch($value['config_code']){
				  	case 'smtp_host':
					    $smtp_host = $value['config_set'];
					  	break;
				 	case 'smtp_user':
					    $smtp_user = $value['config_set'];
					 	break;
					case 'smtp_pass':
					    $smtp_pass = $value['config_set'];
					 	break;
					case 'smtp_port':
					    $smtp_port = $value['config_set'];
					 	break;
					case 'sb_warring_mail':
						$toemail = $value['config_set'];
				  	default:
				}
			}
		}
		if($smtp_host == "" || $smtp_user == "" || $smtp_pass == "")
		{
			return "{$this->lang->line('login_email_error1')} {$system} {$this->lang->line('login_email_error2')}";
			exit;
		}

		$config['protocol'] = "smtp";
		$config['smtp_host'] = $smtp_host;
		$config['smtp_port'] = $smtp_port;
		$config['smtp_user'] = $smtp_user; 
		$config['smtp_pass'] = $smtp_pass;
		$config['charset'] = "utf-8";
		$config['mailtype'] = "html";
		$config['newline'] = "\r\n";
		
		$this->email->initialize($config);

		$this->email->from('ecmo@ecmo.com.tw','系统管理员');
		$this->email->to($toemail); 
		// $this->email->cc('another@another-example.com'); 
		// $this->email->bcc('them@their-example.com'); 

		$this->email->subject("借电站告警");

		$Body = "借電站{$sb_name}發生{$w_type}請您儘速處理!";

		$this->email->message($Body); 
		
		return $this->email->send();
		
		// echo $this->email->print_debugger();
	}
}

/* End of file Model_bss05.php */
