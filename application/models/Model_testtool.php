<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_testtool extends CI_Model {
	
    function __construct() 
	{
        parent::__construct();
		// echo "in Model_column";
	}

	public function bss01Rank()
	{
			date_default_timezone_set("Asia/Taipei");


			$rs_arr = array();
			$rs_arr[0] = 'Y';
			$start_arr = array('1','2','3','4','5','6','7','50');

			for($s=0; $s<8; $s++){
				$i = $start_arr[$s];
				$dataArr = array();
				$f_track_no = rand(1,6);
				$f_track_status = rand(1,9);
				$f_status_led = rand(1,3);
				$f_status_photo_sensor = rand(1,3);
				$f_status_bcu = rand(1,3);
				$f_status_battery_in_track = rand(0,1);
				$f_track_enable = rand(0,1);
				$f_battery_cell_status = rand(0,1);
				$f_battery_status = rand(0,1);

				$up_year = date("Y");
				$up_month = rand(1,date("m"));
				$rank_day = date("t",mktime(0,0,0,$up_month,1,$up_year));
				$up_day = rand(1,$rank_day);
				$f_upload_date = date("Y-m-d",mktime(0,0,0,$up_month,$rank_day,$up_year));
				$f_upload_date = ($f_upload_date > date("Y-m-d"))?date("Y-m-d"):$f_upload_date;

				$f_battery_voltage_1 = rand(100,999);
				$f_battery_voltage_2 = rand(10,99);
				$f_battery_voltage = $f_battery_voltage_1.'.'.$f_battery_voltage_2;
				$f_battery_amps_1 = rand(100,999);
				$f_battery_amps_2 = rand(10,99);
				$f_battery_amps = $f_battery_amps_1.'.'.$f_battery_amps_2;
				$f_battery_temperature= rand(1,100).'.'.rand(1,99);
				$f_battery_capacity= rand(1,100);
				$f_charge_cycles = rand(1,99);
				$f_electrify_time = rand(100,9999999);

				$up_year = date("Y");
				$up_month = rand(1,date("m"));
				$rank_day = date("t",mktime(0,0,0,$up_month,1,$up_year));
				$up_day = rand(1,$rank_day);
				$f_blacklist_ver = date("Ymd",mktime(0,0,0,$up_month,$rank_day,$up_year));
				$f_blacklist_ver = ($f_blacklist_ver > date("Ymd"))?date("Ymd"):$f_blacklist_ver.rand(1,2).rand(1,9).rand(1,5).rand(1,9);

				$batteryswaps = $this->Model_testtool->getbatteryswapstationList();
				$f_bss_id = '';
				$f_bss_id_num = rand(1, $batteryswaps->num_rows()) -1;
				if($batteryswaps->num_rows() > 0){
					$batteryswaps_arr = $batteryswaps->result_array();
					$f_bss_id = $batteryswaps_arr[$f_bss_id_num]['bss_id'];
				}

				$battery = $this->Model_testtool->getbatterysList();
				$f_battery_id = '';
				$battery_num = rand(1, $battery->num_rows()) -1 ;
				if($battery->num_rows() > 0){
					$battery_arr = $battery->result_array();
					$f_battery_id = $battery_arr[$battery_num]['battery_id'];
				}

				$vehicle= $this->Model_testtool->getvehicleList();
				$f_vehicle_user_id = '';
				$vehicle_num = rand(1, $vehicle->num_rows()) -1;
				if($vehicle->num_rows() > 0){
					$vehicle_arr = $vehicle->result_array();
					$f_vehicle_user_id = $vehicle_arr[$vehicle_num]['vehicle_user_id'];
				}
				$dataArr['f_upload_date'] = $f_upload_date.'T'.rand(0,1).rand(0,9).':'.rand(0,5).rand(0,9);
				$dataArr['f_blacklist_ver'] = $f_blacklist_ver;
				$dataArr['f_bss_id'] = $f_bss_id;
				$dataArr['f_track_no'] = $f_track_no;
				$dataArr['f_track_status'] = $f_track_status;
				$dataArr['f_status_led'] = $f_status_led;
				$dataArr['f_status_photo_sensor'] = $f_status_photo_sensor;
				$dataArr['f_status_bcu'] = $f_status_bcu;
				$dataArr['f_status_battery_in_track'] = $f_status_battery_in_track;
				$dataArr['f_track_enable'] = $f_track_enable;
				$dataArr['f_battery_id'] =$f_battery_id;
				$dataArr['f_vehicle_user_id'] = $f_vehicle_user_id;
				$dataArr['f_battery_voltage'] = $f_battery_voltage;
				$dataArr['f_battery_cell_status'] = $f_battery_cell_status;
				$dataArr['f_battery_amps'] = $f_battery_amps_2;
				$dataArr['f_charge_cycles'] = $f_charge_cycles;
				$dataArr['f_battery_temperature'] = $f_battery_temperature;
				$dataArr['f_electrify_time'] = $f_electrify_time;
				$dataArr['f_battery_capacity'] = $f_battery_capacity;
				$dataArr['f_battery_status'] = $f_battery_status;
				$rs_arr[$i] = $dataArr;
			}
			echo json_encode($rs_arr); 
	}

	public function bss02Rank(){
			date_default_timezone_set("Asia/Taipei");
			$dataArr = array();
			$f_swap_type = rand(0,1);
			$f_track_no1 = rand(1,6);
			$f_track_no2 = rand(1,6);
			
			$up_year = date("Y");
			$up_month = rand(1,date("m"));
			$rank_day = date("t",mktime(0,0,0,$up_month,1,$up_year));
			$up_day = rand(1,$rank_day);
			$f_upload_date = date("Y-m-d",mktime(0,0,0,$up_month,$rank_day,$up_year));
			$f_upload_date = ($f_upload_date > date("Y-m-d"))?date("Y-m-d"):$f_upload_date;

			$up_year = date("Y");
			$up_month = rand(1,date("m"));
			$rank_day = date("t",mktime(0,0,0,$up_month,1,$up_year));
			$up_day = rand(1,$rank_day);
			$f_exchange_date = date("Y-m-d",mktime(0,0,0,$up_month,$rank_day,$up_year));
			$f_exchange_date = ($f_exchange_date > date("Y-m-d"))?date("Y-m-d"):$f_exchange_date;

			$batteryswaps = $this->Model_testtool->getbatteryswapstationList();
			$f_bss_id = '';
			$f_bss_id_num = rand(1, $batteryswaps->num_rows()) - 1 ;
			if($batteryswaps->num_rows() > 0){
				$batteryswaps_arr = $batteryswaps->result_array();
				$f_bss_id = $batteryswaps_arr[$f_bss_id_num]['bss_id'];
			}

			$battery = $this->Model_testtool->getbatterysList();
			$f_battery_id1 = '';
			$f_battery_id2 = '';
			$battery_num = rand(1, $battery->num_rows())  - 1;
			$battery_2num = rand(1, $battery->num_rows()) - 1;

			if($battery->num_rows() > 0){
				$battery_arr = $battery->result_array();
				$f_battery_id1 = $battery_arr[$battery_num]['battery_id'];
				$f_battery_id2 = $battery_arr[$battery_2num]['battery_id'];
			}

			$vehicle= $this->Model_testtool->getvehicleList();
			$f_vehicle_user_id1 = '';
			$f_vehicle_code = '';
			$vehicle_num = rand(1, $vehicle->num_rows()) -1 ;
			$vehicle_code_num = rand(1, $vehicle->num_rows()) -1;
			if($vehicle->num_rows() > 0){
				$vehicle_arr = $vehicle->result_array();
				$f_vehicle_user_id1 = $vehicle_arr[$vehicle_num]['vehicle_user_id'];
				$f_vehicle_code = $vehicle_arr[$vehicle_code_num]['vehicle_code'];
			}
			$dataArr['f_upload_date'] = $f_upload_date.'T'.rand(0,1).rand(0,9).':'.rand(0,5).rand(0,9);
			$dataArr['f_exchange_date'] = $f_exchange_date.'T'.rand(0,1).rand(0,9).':'.rand(0,5).rand(0,9);
			$dataArr['f_swap_type'] = $f_swap_type;
			$dataArr['f_track_no1'] = $f_track_no1;
			$dataArr['f_track_no2'] = $f_track_no2;
			$dataArr['f_bss_id'] = $f_bss_id;
			$dataArr['f_battery_id1'] = $f_battery_id1;
			$dataArr['f_battery_id2'] = $f_battery_id2;
			$dataArr['f_vehicle_user_id1'] = $f_vehicle_user_id1;
			$dataArr['f_vehicle_code'] = $f_vehicle_code;
			$rs_arr = array();
			$rs_arr[0] = 'Y';
			$rs_arr[1] = $dataArr;

			echo json_encode($rs_arr); 
	}

	public function ecu01Rank(){
			date_default_timezone_set("Asia/Taipei");
			$dataArr = array();
			
			$up_year = date("Y");
			$up_month = rand(1,date("m"));
			$rank_day = date("t",mktime(0,0,0,$up_month,1,$up_year));
			$up_day = rand(1,$rank_day);
			$f_driving_date = date("Y-m-d",mktime(0,0,0,$up_month,$rank_day,$up_year));
			$f_driving_date = ($f_driving_date > date("Y-m-d"))?date("Y-m-d"):$f_driving_date;

			$up_year = date("Y");
			$up_month = rand(1,date("m"));
			$rank_day = date("t",mktime(0,0,0,$up_month,1,$up_year));
			$up_day = rand(1,$rank_day);
			$f_utc_date = date("dmy",mktime(0,0,0,$up_month,$rank_day,$up_year));
			$f_utc_date = ($f_utc_date > date("dmy"))?date("dmy"):$f_utc_date;

			$vehicle= $this->Model_testtool->getvehicleList();
			$f_unit_id = '';
			$vehicle_num = rand(1, $vehicle->num_rows()) -1 ;
			if($vehicle->num_rows() > 0){
				$vehicle_arr = $vehicle->result_array();
				$f_unit_id = $vehicle_arr[$vehicle_num]['unit_id'];
			}

			$f_speed = rand(10,999).'.'.rand(0,9);
			$f_gps_satellite = rand(0,13);
			$f_return_type = rand(0,7);
			$f_engine_status = rand(0,1);
			$f_battery_amps_1 = rand(100,999);
			$f_battery_amps_2 = rand(10,99);
			$f_battery_amps = $f_battery_amps_1.'.'.$f_battery_amps_2;
			$f_environment_temperature = rand(10,100);
			$f_power_bank_voltage_1 = rand(100,999);
			$f_power_bank_voltage_2 = rand(10,99);
			$f_power_bank_voltage = $f_power_bank_voltage_1.'.'.$f_power_bank_voltage_2;
			$f_power_bank_voltage_2 = rand(10,99);
			$f_power_bank_voltage_2 = rand(1,2).rand(0,4).rand(10,99);
			$f_utc_time = date('His',rand(1000000000,9999999999));
			$f_angle = rand(0,359).'.'.rand(1,9);
			$f_battery_voltage_1 = rand(100,999);
			$f_battery_voltage_2 = rand(10,99);
			$f_battery_voltage = $f_battery_voltage_1.'.'.$f_battery_voltage_2;
			$f_battery_temperature = rand(0.1,100);
			$f_battery_capacity = rand(1,100);
			$f_latitude = rand(-89,89).'.'.rand(1111,9999);
			$f_longitude = rand(-179,179).'.'.rand(1111,9999);;
			$dataArr['f_driving_date'] = $f_driving_date.'T'.rand(0,1).rand(0,9).':'.rand(0,5).rand(0,9);
			$dataArr['f_utc_date'] = $f_utc_date;
			$dataArr['f_unit_id'] = $f_unit_id;
			$dataArr['f_speed'] = $f_speed;
			$dataArr['f_gps_satellite'] = $f_gps_satellite;
			$dataArr['f_return_type'] = $f_return_type;
			$dataArr['f_engine_status'] = $f_engine_status;
			$dataArr['f_battery_amps'] = $f_battery_amps;
			$dataArr['f_environment_temperature'] = $f_environment_temperature;
			$dataArr['f_power_bank_voltage'] = $f_power_bank_voltage;
			$dataArr['f_utc_time'] = $f_utc_time;
			$dataArr['f_angle'] = $f_angle;
			$dataArr['f_battery_voltage'] = $f_battery_voltage;
			$dataArr['f_battery_temperature'] = $f_battery_temperature;
			$dataArr['f_battery_capacity'] = $f_battery_capacity;
			$dataArr['f_latitude'] = $f_latitude;
			$dataArr['f_longitude'] = $f_longitude;
			$unit_arr = array('A','V');
			$f_unit_status = $unit_arr[rand(0,1)];
			$dataArr['f_unit_status'] = $f_unit_status;
			$lease_arr = array('S','R','F');
			$f_lease_status = $lease_arr[rand(0,2)];
			$dataArr['f_lease_status'] = $f_lease_status;
			$dataArr['io_x'] = rand(0,1);
			$dataArr['io_y'] = rand(0,1);
			$dataArr['io_z'] = rand(0,1);
			$event_arr = array('20','22','40','90','A0','F1','F2','F3','F4');
			$f_event_id = $event_arr[rand(0,8)];
			$dataArr['f_event_id'] = $f_event_id;

			$rs_arr = array();
			$rs_arr[0] = 'Y';
			$rs_arr[1] = $dataArr;

			echo json_encode($rs_arr); 
	}

	public function ecu02Rank(){
			date_default_timezone_set("Asia/Taipei");
			$dataArr = array();
			
			$up_year = date("Y");
			$up_month = rand(1,date("m"));
			$rank_day = date("t",mktime(0,0,0,$up_month,1,$up_year));
			$up_day = rand(1,$rank_day);
			$f_driving_date = date("Y-m-d",mktime(0,0,0,$up_month,$rank_day,$up_year));
			$f_driving_date = ($f_driving_date > date("Y-m-d"))?date("Y-m-d"):$f_driving_date;

			$up_year = date("Y");
			$up_month = rand(1,date("m"));
			$rank_day = date("t",mktime(0,0,0,$up_month,1,$up_year));
			$up_day = rand(1,$rank_day);
			$f_utc_date = date("dmy",mktime(0,0,0,$up_month,$rank_day,$up_year));
			$f_utc_date = ($f_utc_date > date("dmy"))?date("dmy"):$f_utc_date;

			$vehicle= $this->Model_testtool->getvehicleList();
			$f_unit_id = '';
			$vehicle_num = rand(1, $vehicle->num_rows()) -1 ;
			if($vehicle->num_rows() > 0){
				$vehicle_arr = $vehicle->result_array();
				$f_unit_id = $vehicle_arr[$vehicle_num]['unit_id'];
			}

			$f_speed = rand(10,999).'.'.rand(0,9);
			$f_gps_satellite = rand(0,13);
			$f_return_type = rand(0,7);
			$f_engine_status = rand(0,1);
			$f_battery_amps_1 = rand(100,999);
			$f_battery_amps_2 = rand(10,99);
			$f_battery_amps = $f_battery_amps_1.'.'.$f_battery_amps_2;
			$f_environment_temperature = rand(10,100);
			$f_power_bank_voltage_1 = rand(100,999);
			$f_power_bank_voltage_2 = rand(10,99);
			$f_power_bank_voltage = $f_power_bank_voltage_1.'.'.$f_power_bank_voltage_2;
			$f_power_bank_voltage_2 = rand(10,99);
			$f_power_bank_voltage_2 = rand(1,2).rand(0,4).rand(10,99);
			$f_utc_time = date('His',rand(1000000000,9999999999));
			$f_angle = rand(0,359).'.'.rand(1,9);
			$f_battery_voltage_1 = rand(10,99);
			$f_battery_voltage_2 = rand(10,99);
			$f_battery_voltage = $f_battery_voltage_1.'.'.$f_battery_voltage_2;
			$f_battery_temperature = rand(0.1,100);
			$f_battery_temperature = rand(10000000,99999999).$f_battery_temperature;
			$f_battery_capacity = str_pad(rand(1,100),3,'0',STR_PAD_LEFT);

			$f_latitude = rand(-89,89).'.'.rand(1111,9999);
			$f_longitude = rand(-179,179).'.'.rand(1111,9999);;
			$dataArr['f_driving_date'] = $f_driving_date.'T'.rand(0,1).rand(0,9).':'.rand(0,5).rand(0,9);
			$dataArr['f_utc_date'] = $f_utc_date;
			$dataArr['f_unit_id'] = $f_unit_id;
			$dataArr['f_speed'] = $f_speed;
			$dataArr['f_gps_satellite'] = $f_gps_satellite;
			$dataArr['f_return_type'] = $f_return_type;
			$dataArr['f_engine_status'] = $f_engine_status;
			$dataArr['f_battery_amps'] = $f_battery_amps;
			$dataArr['f_environment_temperature'] = $f_environment_temperature;
			// $dataArr['f_power_bank_voltage'] = $f_power_bank_voltage;
			$dataArr['f_utc_time'] = $f_utc_time;
			$dataArr['f_angle'] = $f_angle;
			$dataArr['f_battery_voltage'] = $f_battery_voltage;
			$dataArr['f_battery_temperature'] = $f_battery_temperature;
			$dataArr['f_battery_capacity'] = $f_battery_capacity;
			$dataArr['f_latitude'] = $f_latitude;
			$dataArr['f_longitude'] = $f_longitude;
			$unit_arr = array('A','V');
			$f_unit_status = $unit_arr[rand(0,1)];
			$dataArr['f_unit_status'] = $f_unit_status;
			$lease_arr = array('S','R','F');
			$f_lease_status = $lease_arr[rand(0,2)];
			$dataArr['f_lease_status'] = $f_lease_status;
			$dataArr['io_x'] = rand(0,1);
			$dataArr['io_y'] = rand(0,1);
			$dataArr['io_z'] = rand(0,1);
			$event_arr = array('01','02','03');
			$f_event_id = $event_arr[rand(0,2)];
			$dataArr['f_event_id'] = $f_event_id;

			$battery_amps_type_arr = array('1','0');
			$f_battery_amps_type = $battery_amps_type_arr[rand(0,1)];
			$dataArr['f_battery_amps_type'] = $f_battery_amps_type;

			$rs_arr = array();
			$rs_arr[0] = 'Y';
			$rs_arr[1] = $dataArr;

			echo json_encode($rs_arr); 
	}

	//取得電池交換站下拉選單資料
	public function getbatteryswapstationList(){
		$SQLCmd = "SELECT tbss.s_num, tbss.note, tbss.bss_id, tbss.bss_token, top.top01
  										FROM tbl_battery_swap_station tbss 
										LEFT JOIN tbl_operator top ON top.s_num = tbss.so_num and top.status = 'Y'
										where tbss.status = 'Y'";
		$rs = $this->db->query($SQLCmd);
		return $rs;	
	}

	//取得電池序號
	public function getbatterysList(){
		$SQLCmd = "SELECT s_num,battery_id, manufacture_date 
  						FROM tbl_battery where status = 0";
		$rs = $this->db->query($SQLCmd);
		return $rs;	
	}

	//取得車輛
	public function getvehicleList(){
		$SQLCmd = "SELECT s_num,unit_id, vehicle_user_id,vehicle_code 
  						FROM tbl_vehicle";
		$rs = $this->db->query($SQLCmd);
		return $rs;	
	}
}

/* End of file Model_testtool.php */
