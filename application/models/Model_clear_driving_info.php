<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_clear_driving_info extends MY_Model {
	
    function __construct() 
	{
        parent::__construct();
		// echo "in Model_column";
	}

	public function cleardata()
	{
		$SQLCmd = "SELECT config_set FROM sys_config WHERE config_code = 'clear_api_history_day'";
		$rs = $this->db_query($SQLCmd);

		if($rs)
		{
			$clear_day = $rs[0]['config_set'];
			$SQLCmdC = "DELETE FROM log_driving_info WHERE datediff(curdate(), system_log_date)>={$clear_day}";
			$this->db_query($SQLCmdC);

			$SQLCmdC = "DELETE FROM log_battery_gps2 WHERE datediff(curdate(), system_log_date)>={$clear_day}";
			$this->db_query($SQLCmdC);

		}
	}

}

/* End of file Model_clear_leavereturn_data.php */
