<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Battery extends CI_Controller {
	
	/**
	 * 建構式
	 * 預先載入Ecu01的物件
	 */
    function __construct() 
    {
        parent::__construct();
        $this->load->model("common/model_common", "model_common") ;
        $this->load->model("battery/model_ecu03", "model_ecu03") ;
    }

	//電池回報
	public function c516a8f6(){
		$this->model_ecu03->savedrivingInfo();
	}
	
	//新電池回報
	public function a9cf7940(){
		// $this->model_ecu03->savenewbatteryInfo();
		$this->model_ecu03->savenewbatteryInfo2();
	}

	//新電池回報2
	public function d78d03d2(){
		$this->model_ecu03->savenewbatteryInfo2();
	}

	//新電池回報3
	public function d0787444(){
		$this->model_ecu03->savenewbatteryInfo3();
	}
}

/* End of file Ecu01.php */
/* Location: ./application/controllers/api/Ecu01.php */