<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mal01 extends CI_Controller {
	
	/**
	 * 建構式
	 * 預先載入Mal01的物件
	 */
    function __construct() 
    {
        parent::__construct();
        $this->load->model("common/model_common", "model_common") ;
        $this->load->model("mal/model_mal01", "model_mal01") ;
    }
	
	public function index()
	{
		$this->model_mal01->save_mal_order();
	}
}

/* End of file Mal01.php */
/* Location: ./application/controllers/api/Mal01.php */