<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Show_driver_info extends CI_Controller {
	
	/**
	 * 建構式
	 * 預先載入Show_driver_info的物件
	 */
    function __construct() 
    {
        parent::__construct();
		$this->load->database();
		$this->load->model("model_driver_info", "model_driver_info") ;
    }
	
	public function index($page_num = 0)
	{
		$data['page_num'] = $page_num;
		$data['driverfield'] = $this->model_driver_info->getdriverfield();
		$data['driverInfo'] = $this->model_driver_info->getdriverinfo($page_num);

		$this->load->view( "driver_info_form", $data) ;
	}

	public function search_battery()
	{
		$battery_id = $this->input->post("battery_id");
		$comparefun = $this->input->post("comparefun");
		$this->session->set_userdata("battery_id",$battery_id);
		$this->session->set_userdata("comparefun",$comparefun);
		$this->index();
	}

}

/* End of file Show_driver_info.php */
/* Location: ./application/controllers/Show_driver_info.php */