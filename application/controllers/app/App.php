<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class App extends CI_Controller {
	
	/**
	 * 建构式
	 * 预先载入Wechat的物件
	 */
    function __construct() 
    {
        parent::__construct();
        $this->load->model("wechat/Model_order_wechat", "Model_order_wechat") ;
        $this->load->model("wechat/model_search", "model_search") ;
        $this->load->model("wechat/model_register", "model_register") ;
        $this->load->model("wechat/Model_exception", "model_exception") ;
		$this->load->library("MY_WxPay") ;
		$this->load->model("common/model_common", "model_common") ;

    }

	//微信支付通知
	public function df57ab54()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'df57ab54','微信支付通知');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				if(isset($data_json_de['wp01']))
					$notify_date = addslashes($data_json_de['wp01']);//微信支付通知日期
				else
					$notify_date = "";
				if(isset($data_json_de['wp02']))
					$bss_id = addslashes($data_json_de['wp02']);//電池交換站Token ID序號
				else
					$bss_id = "";

				if(isset($data_json_de['wp03']))
				{
					if($data_json_de['wp03']!="")
						$user_id = str_pad(addslashes($data_json_de['wp03']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
					else
						$user_id = "";
				}
				else
					$user_id = "";

				if(isset($data_json_de['wp04']))
					$amount = addslashes($data_json_de['wp04']);//微信支付扣款金額
				else
					$amount = "";

				if(isset($data_json_de['wp05']))
					$qrcode_url = addslashes($data_json_de['wp05']);//QR Code URL
				else
					$qrcode_url = "";

				if(isset($data_json_de['wp06']))
					$gps_location = addslashes($data_json_de['wp06']);//微信支付時的GPS座標
				else
					$gps_location = "";

				if(isset($data_json_de['wp07']))
					$orderSn = addslashes($data_json_de['wp07']);//订单号orderSn
				else
					$orderSn = "";

				if(isset($data_json_de['wp08']))
					$wechat_token = addslashes($data_json_de['wp08']);//token
				else
					$wechat_token = "";

				if(isset($data_json_de['wp09']))
					$mdtoken = addslashes($data_json_de['wp09']);//魔力token
				else
					$mdtoken = "";

				if(isset($data_json_de['wp10']))
					$order_status = addslashes($data_json_de['wp10']);//訂單狀態
				else
					$order_status = "";

				if(isset($data_json_de['wp11']))
					$scan_date = addslashes($data_json_de['wp11']);//掃碼時間
				else
					$scan_date = NULL;

				if($user_id!="" && $notify_date!="" && $bss_id!="" && $amount!="" && $qrcode_url!="" && $gps_location!="" && $orderSn!="" && $wechat_token!="" && $mdtoken!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						$bss_info = $this->model_search->get_bssinfo($bss_id);
						if($bss_info)
						{
							$dataArr['payment_date'] = $notify_date;
							$dataArr['user_id'] = $user_id;
							$dataArr['bss_token'] = $bss_info[0]['bss_token'];
							$dataArr['url_Info'] = $qrcode_url;
							$dataArr['tm_num'] = $member_info[0]['s_num'];
							$dataArr['amount'] = $amount;
							$dataArr['gps_location'] = $gps_location;
							$dataArr['notify'] = 0;
							$dataArr['orderSn'] = $orderSn;
							$dataArr['wechat_token'] = $wechat_token;							
							$dataArr['mdtoken'] = $mdtoken;
							$dataArr['order_status'] = $order_status;
							$dataArr['scan_date'] = $scan_date;
							//回调验证接口
							$wechaturl = $this->model_search->getwechaturl();
							if($wechaturl!="")
							{
								$url = $wechaturl."checkcabinetdata";
							}
							else
							{
								$url = "";
							}

							$logArr = $this->model_search->getleavebatterylog($user_id);
							$postArr = array();
							$postArr['action'] = "actionPostCheckData";
							$postArr['userId'] = $data_json_de['wp03'];
							$postArr['token'] = $wechat_token;
							$postArr['orderSn'] = $orderSn;
							$postArr['cabinetNumber'] = $bss_id;
							$postStr = http_build_query($postArr);

							$dataArrLog = array();
							$dataArrLog['api_name'] = "actionPostCheckData";
							$dataArrLog['api_chinese_name'] = "回调验证接口";
							$dataArrLog['send_date'] = $notify_date;
							$dataArrLog['send_data'] = $postStr;
							$log_sn = $this->Model_order_wechat->inserwechatlog($dataArrLog);
							// $postStr = http_build_query($postArr);
							// $postStr = http_build_query(array("Book"=>1));
							// $postStr = '{"Book":"1"}';
							$ch = curl_init();
							curl_setopt($ch, CURLOPT_URL, $url);
							curl_setopt($ch, CURLOPT_POST, true);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
							curl_setopt($ch, CURLOPT_TIMEOUT, 20); 
							curl_setopt($ch, CURLOPT_POSTFIELDS, $postStr); 
							$output = curl_exec($ch);
							curl_close($ch);

							$dataArrLog = array();
							$dataArrLog['receive_date'] = "now()";
							$dataArrLog['receive_data'] = $output;

							$this->Model_order_wechat->updatewechatlog($log_sn,$dataArrLog);
							if($this->model_common->is_json($output))
							{
								if($output!="")
								{
									$output_de = json_decode($output,true);
									if($output_de['status']=='ok')
										$this->Model_order_wechat->insert_payment($dataArr);
								}	
							}

							$returnArr["rt_cd"] = "0000";//成功
							$returnArr["rt_msg"] = "成功";
						}
						else
						{
							$returnArr["rt_cd"] = "0004";//查无会员资料
							$returnArr["rt_msg"] = "查无借电站";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//会员ID错误
					$returnArr["rt_msg"] = "资料有误";
					// $returnArr["rt_msg"] = "会员User ID不得為空";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//查询电池剩余电量API
	public function db05d9c9()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'db05d9c9','剩余电量查询');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson);
				$quiry_date = addslashes($data_json_de->bc01);//查询日期
				if($data_json_de->bc02!="")
					$user_id = str_pad(addslashes($data_json_de->bc02),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
				else
					$user_id = "";
				$coordinate = addslashes($data_json_de->bc03);//查询时纬度经度
				if($user_id!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						$batterypowerInfo = $this->model_search->getbatterypower($user_id);
						if($batterypowerInfo)
						{
							$returnArr["rt_cd"] = "0000";//成功
							$returnArr["rt_msg"] = "成功";
							$returnArr["rt_01"] = $batterypowerInfo[0]['battery_capacity'];//电池电量
							$returnArr["rt_02"] = $batterypowerInfo[0]['latitude'];//电池GPS緯度
							$returnArr["rt_03"] = $batterypowerInfo[0]['longitude'];//电池GPS經度
							foreach($batterypowerInfo as $key => $value)
							{
								$returnArr['em_info'][$key]['battery_id'] = $value['leave_battery_id'];
								$returnArr['em_info'][$key]['battery_capacity'] = $value['battery_capacity'];
								$returnArr['em_info'][$key]['latitude'] = $value['latitude'];
								$returnArr['em_info'][$key]['longitude'] = $value['longitude'];
							}
						}
						else
						{
							$returnArr["rt_cd"] = "0006";//查无电池电量
							$returnArr["rt_msg"] = "查无电池电量";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//会员ID错误
					$returnArr["rt_msg"] = "会员ID错误";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//微信会员注册通知
	public function d31387a9()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'d31387a9','微信会员注册通知');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				if(isset($data_json_de['mw01']))
					$wechatInfoArr['access_token'] = addslashes($data_json_de['mw01']);//網頁授權接口調用憑證
				else
					$wechatInfoArr['access_token'] = "";
				if(isset($data_json_de['mw02']))
					$wechatInfoArr['openid'] = addslashes($data_json_de['mw02']);//用戶唯一標識
				else
					$wechatInfoArr['openid'] = "";
				if(isset($data_json_de['mw03']))
					$wechatInfoArr['scope'] = addslashes($data_json_de['mw03']);//用戶授權的作用域
				else
					$wechatInfoArr['scope'] = "";
				if(isset($data_json_de['mw05']))
					$wechatInfoArr['sex'] = addslashes($data_json_de['mw05']);//用戶性別
				else
					$wechatInfoArr['sex'] = "";
				if(isset($data_json_de['mw06']))
					$wechatInfoArr['province'] = addslashes($data_json_de['mw06']);//用戶省份
				else
					$wechatInfoArr['province'] = "";
				if(isset($data_json_de['mw07']))
					$wechatInfoArr['city'] = addslashes($data_json_de['mw07']);//用戶城市
				else
					$wechatInfoArr['city'] = "";
				if(isset($data_json_de['mw08']))
					$wechatInfoArr['country'] = addslashes($data_json_de['mw08']);//用戶國家
				else
					$wechatInfoArr['country'] = "";
				if(isset($data_json_de['mw09']))
					$wechatInfoArr['headimgurl'] = addslashes($data_json_de['mw09']);//用戶頭像
				else
					$wechatInfoArr['headimgurl'] = "";
				if(isset($data_json_de['mw10']))
					$wechatInfoArr['privilege'] = addslashes($data_json_de['mw10']);//用戶特權信息
				else
					$wechatInfoArr['privilege'] = "";
				if(isset($data_json_de['mw11']))
					$wechatInfoArr['unionid'] = addslashes($data_json_de['mw11']);//UnionID機制
				else
					$wechatInfoArr['unionid'] = "";

				if(isset($data_json_de['mw12']) && $data_json_de['mw12']!="")
					$wechatInfoArr['user_id'] = str_pad(addslashes($data_json_de['mw12']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
				else
					$wechatInfoArr['user_id'] = "";

				if(isset($data_json_de['mw16']))
					$wechatInfoArr['deposit'] = addslashes($data_json_de['mw16']);//押金金額
				else
					$wechatInfoArr['deposit'] = "";

				if(isset($data_json_de['mw17']))
					$wechatInfoArr['deposit_status'] = addslashes($data_json_de['mw17']);//押金狀態（0=未支付；1=已支付）
				else
					$wechatInfoArr['deposit_status'] = "";

				if(isset($data_json_de['mw18']))
					$wechatInfoArr['member_type'] = addslashes($data_json_de['mw18']);//會員類別
				else
					$wechatInfoArr['member_type'] = "";

				if($data_json_de['mw04']!="" && $data_json_de['mw13']!="" && $data_json_de['mw14']!="" && $data_json_de['mw15']!="")
				{
					$wechatInfoArr['nickname'] = addslashes($data_json_de['mw04']);//用戶暱稱
					$wechatInfoArr['create_date'] = addslashes($data_json_de['mw13']);//註冊時間
					$wechatInfoArr['mobile'] = addslashes($data_json_de['mw14']);//手機號
					$wechatInfoArr['personal_id'] = addslashes($data_json_de['mw15']);//身分證號
					if($wechatInfoArr['user_id']!="")
					{
						$change_status = $this->model_register->register_member($wechatInfoArr);
						if($change_status === true)
						{
							$returnArr["rt_cd"] = "0000";//成功
							$returnArr["rt_msg"] = "成功";
							// $returnArr["rt_01"] = $wechatInfoArr['openid'];
						}
						else
						{
							$returnArr["rt_cd"] = "0010";//資料有誤
							$returnArr["rt_msg"] = $change_status;
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0005";//会员ID错误
						$returnArr["rt_msg"] = "会员User ID不得為空";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0010";//資料有誤
					$returnArr["rt_msg"] = "会员注册失败";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//電池交換站查詢
	public function f8c39d7d()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'f8c39d7d','电池交换站查询');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['bl01']);//電池交換站查詢日期
				if($data_json_de['bl02']!="")
					$user_id = str_pad(addslashes($data_json_de['bl02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
				else
					$user_id = "";
				$coordinate = addslashes($data_json_de['bl03']);//查询时纬度经度
				$bss_id = addslashes($data_json_de['bl04']);//電池交換站編號

				if($user_id!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						$stationInfo = $this->model_search->get_bssinfo($bss_id);
						if($stationInfo)
						{
							$returnArr["rt_cd"] = "0000";//成功
							$returnArr["rt_msg"] = "成功";
							$returnArr["rt_01"] = $stationInfo[0]['latitude'].",".$stationInfo[0]['longitude'];//電池交換站的GPS座標
							$returnArr["rt_02"] = $stationInfo[0]['canuse_battery_num'];//可交換電池數量
						}
						else
						{
							$returnArr["rt_cd"] = "0006";//查无借电站资料
							$returnArr["rt_msg"] = "查无借电站资料";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//会员ID错误
					$returnArr["rt_msg"] = "会员User ID不得為空";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		// print_r($returnArr);
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//電池綁定通知
	public function ba191a51()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'ba191a51','电池绑定通知');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				if(isset($data_json_de['bp01']))
					$dataArr['leave_date'] = addslashes($data_json_de['bp01']);//電池綁定日期
				else
					$dataArr['leave_date'] = "";
				if(isset($data_json_de['bp02']))
					$dataArr['battery_user_id'] = str_pad(addslashes($data_json_de['bp02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
				else
					$dataArr['battery_user_id'] = "";
				if(isset($data_json_de['bp03']))
					$dataArr['leave_battery_id'] = $data_json_de['bp03'];
				else
					$dataArr['leave_battery_id'] = "";

				if(isset($data_json_de['bp04']))
					$dataArr['leave_coordinate'] = $data_json_de['bp04'];
				else
					$dataArr['leave_coordinate'] = "";

				if(isset($data_json_de['bp05']))
					$bss_id = $data_json_de['bp05'];
				else
					$bss_id = "";

				if(isset($data_json_de['bp06']))
					$bind_type = $data_json_de['bp06'];
				else
					$bind_type = "";

				if(isset($data_json_de['bp07']))
					$return_battery = $data_json_de['bp07'];
				else
					$return_battery = "";

				$dataArr['leave_sb_num'] = "";

				if($dataArr['leave_date']!="" && $dataArr['battery_user_id']!="" && $dataArr['leave_battery_id']!="" && $dataArr['leave_coordinate']!="")
				{
					$member_info = $this->model_search->get_member_info1($dataArr['battery_user_id']);
					if($member_info)
					{
						$dataArr['tm_num'] = $member_info[0]['s_num'];
						$dataArr['member_type'] = $member_info[0]['member_type'];
						$batteryInfo = $this->model_search->getbatteryInfo($dataArr['leave_battery_id']);
						if(count($batteryInfo)!=0)
						{
							if($batteryInfo[0]['status']!=0 && $batteryInfo[0]['status']!=1)
							{
								$returnArr["rt_cd"] = "0008";//電池故障
								$returnArr["rt_msg"] = "电池故障";
							}
							else
							{
								if($bss_id!="")
								{
									$stationInfo = $this->model_search->get_bssinfo($bss_id);
									$dataArr['leave_sb_num'] = $stationInfo[0]['s_num'];
								}

								$coutinue = true;
								if($return_battery!="")
								{
									$batteryInfoR = $this->model_search->getbatteryInfo($return_battery);
									if(count($batteryInfoR)==0)
									{
										$returnArr["rt_cd"] = "0006";//查无借电站资料
										$returnArr["rt_msg"] = "查无电池资料";
										$coutinue = false;
									}
								}
									
								if($coutinue)
								{
									$bindbattery = $this->model_register->bind_battery($dataArr,$bss_id,$bind_type,$return_battery);

									$returnArr["rt_cd"] = $bindbattery['rt_cd'];//成功
									$returnArr["rt_msg"] = $bindbattery['rt_msg'];
								}							}
						}
						else
						{
							$returnArr["rt_cd"] = "0006";//查无借电站资料
							$returnArr["rt_msg"] = "查无电池资料";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0004";//会员ID错误
					$returnArr["rt_msg"] = "栏位不得为空";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//微信会员批次注册通知
	public function c9a4b934d()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'c9a4b934d','微信会员批次注册通知');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				if(isset($data_json_de['em_info']))
					$em_infoArr = $data_json_de['em_info'];
				else
					$em_infoArr = "";
				if($em_infoArr != "")
				{
					$batch_insert_lag = true;
					$rt_msg = "";
					foreach($em_infoArr as $key => $data_json_de1)
					{
						$wechatInfoArr = array();
						if(isset($data_json_de1['mw01']))
							$wechatInfoArr['access_token'] = addslashes($data_json_de1['mw01']);//網頁授權接口調用憑證
						else
							$wechatInfoArr['access_token'] = "";
						if(isset($data_json_de1['mw02']))
							$wechatInfoArr['openid'] = addslashes($data_json_de1['mw02']);//用戶唯一標識
						else
							$wechatInfoArr['openid'] = "";
						if(isset($data_json_de1['mw03']))
							$wechatInfoArr['scope'] = addslashes($data_json_de1['mw03']);//用戶授權的作用域
						else
							$wechatInfoArr['scope'] = "";
						if(isset($data_json_de1['mw05']))
							$wechatInfoArr['sex'] = addslashes($data_json_de1['mw05']);//用戶性別
						else
							$wechatInfoArr['sex'] = "";
						if(isset($data_json_de1['mw06']))
							$wechatInfoArr['province'] = addslashes($data_json_de1['mw06']);//用戶省份
						else
							$wechatInfoArr['province'] = "";
						if(isset($data_json_de1['mw07']))
							$wechatInfoArr['city'] = addslashes($data_json_de1['mw07']);//用戶城市
						else
							$wechatInfoArr['city'] = "";
						if(isset($data_json_de1['mw08']))
							$wechatInfoArr['country'] = addslashes($data_json_de1['mw08']);//用戶國家
						else
							$wechatInfoArr['country'] = "";
						if(isset($data_json_de1['mw09']))
							$wechatInfoArr['headimgurl'] = addslashes($data_json_de1['mw09']);//用戶頭像
						else
							$wechatInfoArr['headimgurl'] = "";
						if(isset($data_json_de1['mw10']))
							$wechatInfoArr['privilege'] = addslashes($data_json_de1['mw10']);//用戶特權信息
						else
							$wechatInfoArr['privilege'] = "";
						if(isset($data_json_de1['mw11']))
							$wechatInfoArr['unionid'] = addslashes($data_json_de1['mw11']);//UnionID機制
						else
							$wechatInfoArr['unionid'] = "";

						if(isset($data_json_de1['mw12']) && $data_json_de1['mw12']!="")
							$wechatInfoArr['user_id'] = str_pad(addslashes($data_json_de1['mw12']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
						else
							$wechatInfoArr['user_id'] = "";

						if(isset($data_json_de1['mw16']))
							$wechatInfoArr['deposit'] = addslashes($data_json_de1['mw16']);//押金金額
						else
							$wechatInfoArr['deposit'] = "";

						if(isset($data_json_de1['mw17']))
							$wechatInfoArr['deposit_status'] = addslashes($data_json_de1['mw17']);//押金狀態（0=未支付；1=已支付）
						else
							$wechatInfoArr['deposit_status'] = "";

						if(isset($data_json_de['mw18']))
							$wechatInfoArr['member_type'] = addslashes($data_json_de['mw18']);//會員類別
						else
							$wechatInfoArr['member_type'] = "";
						
						if($data_json_de1['mw04']!="" && $data_json_de1['mw13']!="" && $data_json_de1['mw14']!="" && $data_json_de1['mw15']!="")
						{
							$wechatInfoArr['nickname'] = addslashes($data_json_de1['mw04']);//用戶暱稱
							$wechatInfoArr['create_date'] = addslashes($data_json_de1['mw13']);//註冊時間
							$wechatInfoArr['mobile'] = addslashes($data_json_de1['mw14']);//手機號
							$wechatInfoArr['personal_id'] = addslashes($data_json_de1['mw15']);//身分證號
							if($wechatInfoArr['user_id']!="")
							{
								$change_status = $this->model_register->register_member($wechatInfoArr);
								if(!$change_status)
								{
									$batch_insert_lag = false;
									$rt_msg .= $wechatInfoArr['user_id']."注册失敗,錯誤為:".$change_status." ";
									// $returnArr["rt_cd"] = "0000";//成功
									// $returnArr["rt_msg"] = "成功";
									// $returnArr["rt_01"] = $wechatInfoArr['openid'];
								}
								// else
								// {
								// 	$returnArr["rt_cd"] = "0010";//資料有誤
								// 	$returnArr["rt_msg"] = $change_status;
								// }
							}
							else
							{
								$batch_insert_lag = false;
								$rt_msg .= "会员User ID不得為空 ";
								// $returnArr["rt_cd"] = "0005";//会员ID错误
								// $returnArr["rt_msg"] = "会员User ID不得為空";
							}
						}
						else
						{
							$batch_insert_lag = false;
							$rt_msg .= "会员注册失败 ";
							// $returnArr["rt_cd"] = "0010";//資料有誤
							// $returnArr["rt_msg"] = "会员注册失败";
						}

					}

					if($batch_insert_lag===true)
					{
						$returnArr["rt_cd"] = "0000";//成功
						$returnArr["rt_msg"] = "成功";
					}
					else
					{
						$returnArr["rt_cd"] = "0010";//資料有誤
						$returnArr["rt_msg"] = $rt_msg;
					}

				}
				else
				{
					$returnArr["rt_cd"] = "0003";//资料为空
					$returnArr["rt_msg"] = "资料为空";//回应讯息
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//查詢使用者目前的電池序號
	public function f74cf60e()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'f74cf60e','查询使用者目前的电池序号');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['su01']);//查詢日期
				if($data_json_de['su02']!="")
					$user_id = str_pad(addslashes($data_json_de['su02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
				else
					$user_id = "";
				$coordinate = addslashes($data_json_de['su03']);//查询时纬度经度

				if($user_id!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						$userbatteryID = $this->model_search->getleavebatterylog2($user_id);
						if($userbatteryID)
						{
							if($userbatteryID[0]['return_date']=="")
							{
								$returnArr["rt_cd"] = "0000";//成功
								$returnArr["rt_msg"] = "成功";
								$returnArr["rt_01"] = $userbatteryID[0]['leave_battery_id'];//電池序號
								foreach($userbatteryID as $key => $value)
								{
									$returnArr['em_info'][$key]['battery_id'] = $value['leave_battery_id'];
								}
							}
							else
							{
								$returnArr["rt_cd"] = "0006";//查无借电站资料
								$returnArr["rt_msg"] = "查无使用者目前的电池序号";
							}
						}
						else
						{
							$returnArr["rt_cd"] = "0006";//查无借电站资料
							$returnArr["rt_msg"] = "查无使用者目前的电池序号";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//会员ID错误
					$returnArr["rt_msg"] = "会员User ID不得為空";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		// print_r($returnArr);
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//解除电池绑定
	public function d8bdf6c3()
	{
		$this->model_exception->return_battery();
	}

	//查詢滿電電池軌道
	public function e64a69d6()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'e64a69d6','查询满电电池轨道');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['bt01']);//查询日期
				if(isset($data_json_de['bt02']))
				{
					if($data_json_de['bt02']!="")
						$user_id = str_pad(addslashes($data_json_de['bt02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
					else
						$user_id = "";
				}
				else
					$user_id = "";
				if(isset($data_json_de['bt03']))
					$bss_id = addslashes($data_json_de['bt03']);//電池交換站序號(BSS ID)
				else
					$bss_id = "";
				// $coordinate = addslashes($data_json_de['bt04']);//查询时纬度经度
				if($user_id!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						$fullbatterytrackInfo = $this->model_search->getfullbatterytrack($bss_id);
						if($fullbatterytrackInfo)
						{
							$returnArr["rt_cd"] = "0000";//成功
							$returnArr["rt_msg"] = "成功";
							$trackStr = "";
							foreach($fullbatterytrackInfo as $key=> $value)
							{
								if($trackStr == "")
									$trackStr = $value["track_no"];
								else
									$trackStr .= ",".$value["track_no"];
							}
							$returnArr["rt_01"] = $trackStr;//滿電電池軌道(軌道以逗號隔開)
						}
						else
						{
							$returnArr["rt_cd"] = "0006";//查无电池电量
							$returnArr["rt_msg"] = "查无满电轨道";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//会员ID错误
					$returnArr["rt_msg"] = "会员ID错误";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//批次查询电池是否绑定
	public function bbcadb96()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'bbcadb96','批次查询电池是否绑定');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['bb01']);//查询日期
				if(isset($data_json_de['bb02']))
				{
					if($data_json_de['bb02']!="")
						$user_id = str_pad(addslashes($data_json_de['bb02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
					else
						$user_id = "";
				}
				else
					$user_id = "";
				if(isset($data_json_de['bb03']))
					$batteryIDArr = $data_json_de['bb03'];//電池序號(battery ID)
				else
					$batteryIDArr = "";
				// $coordinate = addslashes($data_json_de['bt04']);//查询时纬度经度
				if($user_id!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						$returnArr["rt_cd"] = "0000";//成功
						$returnArr["rt_msg"] = "成功";
						foreach($batteryIDArr as $key => $value)
						{
							$batteryInfo = $this->model_search->getbatteryInfo($value['battery_id']);
							if($batteryInfo)
							{
								switch($batteryInfo[0]['status'])
								{
									case 2:
									{
										$returnArr["em_info"][$key]['battery_id'] = $value['battery_id'];
										$returnArr["em_info"][$key]['b_cd'] = "0007";
										$returnArr["em_info"][$key]['b_msg'] = "电池故障";
									}	
									break;
									case 3:
									{
										$returnArr["em_info"][$key]['battery_id'] = $value['battery_id'];
										$returnArr["em_info"][$key]['b_cd'] = "0008";
										$returnArr["em_info"][$key]['b_msg'] = "电池维修";
									}	
									break;
									case 4:
									{
										$returnArr["em_info"][$key]['battery_id'] = $value['battery_id'];
										$returnArr["em_info"][$key]['b_cd'] = "0009";
										$returnArr["em_info"][$key]['b_msg'] = "电池报废";
									}	
									break;
									case 5:
									{
										$returnArr["em_info"][$key]['battery_id'] = $value['battery_id'];
										$returnArr["em_info"][$key]['b_cd'] = "0010";
										$returnArr["em_info"][$key]['b_msg'] = "电池失窃";
									}
									break;
									default:
									{
										$batterybindstatus = $this->model_search->getbatterystatus($value['battery_id']);
										if($batterybindstatus=="N")
										{
											$returnArr["em_info"][$key]['battery_id'] = $value['battery_id'];
											$returnArr["em_info"][$key]['b_cd'] = "0000";
											$returnArr["em_info"][$key]['b_msg'] = "电池无绑定";
										}
										else if($batterybindstatus=="Y")
										{
											$returnArr["em_info"][$key]['battery_id'] = $value['battery_id'];
											$returnArr["em_info"][$key]['b_cd'] = "0012";
											$returnArr["em_info"][$key]['b_msg'] = "电池已被绑定";
										}
										else
										{
											$returnArr["em_info"][$key]['battery_id'] = $value['battery_id'];
											$returnArr["em_info"][$key]['b_cd'] = "0011";
											$returnArr["em_info"][$key]['b_msg'] = "电池查无绑定状态";
										}
									}
									break;
								}
								
							}
							else
							{
								$returnArr["em_info"][$key]['battery_id'] = $value['battery_id'];
								$returnArr["em_info"][$key]['b_cd'] = "0006";
								$returnArr["em_info"][$key]['b_msg'] = "此电池尚未建档";
							}
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//会员ID错误
					$returnArr["rt_msg"] = "会员ID错误";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//查询电池定位與軌跡API
	public function d812c768()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'d812c768','查询电池定位與軌跡');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['bl01']);//查询日期
				if(isset($data_json_de['bl02']))
				{
					if($data_json_de['bl02']!="")
						$user_id = str_pad(addslashes($data_json_de['bl02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
					else
						$user_id = "";
				}
				else
					$user_id = "";
				if(isset($data_json_de['bl03']))
					$battery_id = addslashes($data_json_de['bl03']);//電池序號(battery ID)
				else
					$battery_id = "";

				// $coordinate = addslashes($data_json_de->bc03);//查询时纬度经度
				if($user_id!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						if($battery_id!="")
						{
							$batteryLocation = $this->model_search->getbatteryLocation($user_id,$battery_id);
							if($batteryLocation)
							{
								$returnArr["rt_cd"] = "0000";//成功
								$returnArr["rt_msg"] = "成功";
								$returnArr["rt_01"] = $batteryLocation[0]['latitude'];//电池GPS緯度
								$returnArr["rt_02"] = $batteryLocation[0]['longitude'];//电池GPS經度
								$returnArr["rt_03"] = $batteryLocation[0]['gps_path_track'];//电池軌跡
							}
							else
							{
								$returnArr["rt_cd"] = "0006";//查无电池电量
								$returnArr["rt_msg"] = "电池序号有误";
							}
						}
						else
						{
							$returnArr["rt_cd"] = "0004";//電池不得為空
							$returnArr["rt_msg"] = "电池ID不得为空";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//会员ID错误
					$returnArr["rt_msg"] = "会员ID错误";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//查詢借電站可預約之電池
	public function d4696729()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'d4696729','查询借电站可预约之电池');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['sr01']);//查询日期
				if(isset($data_json_de['sr02']))
				{
					if($data_json_de['sr02']!="")
						$user_id = str_pad(addslashes($data_json_de['sr02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
					else
						$user_id = "";
				}
				else
					$user_id = "";
				if(isset($data_json_de['sr03']))
					$bss_id = addslashes($data_json_de['sr03']);//電池交換站序號(BSS ID)
				else
					$bss_id = "";
				// $coordinate = addslashes($data_json_de['bt04']);//查询时纬度经度
				if($user_id!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						$reservebatteryInfo = $this->model_search->getfullPowerBattery($bss_id);
						if($reservebatteryInfo)
						{
							$returnArr["rt_cd"] = "0000";//成功
							$returnArr["rt_msg"] = "成功";
							$batteryStr = "";
							foreach($reservebatteryInfo as $key=> $value)
							{
								if($batteryStr == "")
									$batteryStr = $value["battery_id"];
								else
									$batteryStr .= ",".$value["battery_id"];
							}
							$returnArr["rt_01"] = $batteryStr;//可預約之電池ID(電池ID以逗號隔開)
						}
						else
						{
							$returnArr["rt_cd"] = "0006";//查无可预约电池
							$returnArr["rt_msg"] = "查无可预约电池";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//会员ID错误
					$returnArr["rt_msg"] = "会员ID错误";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//預約電池API
	public function be3a034e()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'be3a034e','预约电池');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['rb01']);//查询日期
				if(isset($data_json_de['rb02']))
				{
					if($data_json_de['rb02']!="")
						$user_id = str_pad(addslashes($data_json_de['rb02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
					else
						$user_id = "";
				}
				else
					$user_id = "";
				if(isset($data_json_de['rb03']))
					$battery_id = addslashes($data_json_de['rb03']);//電池序號(battery ID)
				else
					$battery_id = "";

				// $coordinate = addslashes($data_json_de->bc03);//查询时纬度经度
				if($user_id!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						$checkreserveUser = $this->model_search->checkReserveUser($user_id);
						if($checkreserveUser == 0)
						{
							if($battery_id!="")
							{
								$batteryInfo = $this->model_search->getbatteryInfo($battery_id);
								if($batteryInfo)
								{
									$checkbattery = $this->model_search->checkReserveBattery($battery_id);
									if($checkbattery)
									{
										$ReBattery = $this->model_register->reserverBattery($user_id,$battery_id);
										if($ReBattery)
										{
											$returnArr["rt_cd"] = "0000";//成功
											$returnArr["rt_msg"] = "成功";
										}
										else
										{
											$returnArr["rt_cd"] = "0009";//预约失败
											$returnArr["rt_msg"] = "预约失败";
										}
									}
									else
									{
										$returnArr["rt_cd"] = "0007";//查无电池电量
										$returnArr["rt_msg"] = "此电池已被预约或借出,请换别颗电池预约";
									}
								}
								else
								{
									$returnArr["rt_cd"] = "0006";//查无电池电量
									$returnArr["rt_msg"] = "电池序号有误";
								}
							}
							else
							{
								$returnArr["rt_cd"] = "0004";//查无电池电量
								$returnArr["rt_msg"] = "电池ID不得为空";
							}
						}
						else
						{
							$returnArr["rt_cd"] = "0008";//此会员已经预约过
							$returnArr["rt_msg"] = "此会员已经预约过";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//会员ID错误
					$returnArr["rt_msg"] = "会员ID错误";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//会员基本资料-魔力3
	public function bd332f49()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'bd332f49','会员基本资料-魔力3');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['mq01']);//会员资料查询日期
				if(isset($data_json_de['mq02']))
				{
					if($data_json_de['mq02']!="")
						$user_id = str_pad(addslashes($data_json_de['mq02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
					else
						$user_id = "";
				}
				else
					$user_id = "";
				$coordinate = addslashes($data_json_de['mq03']);//查询时纬度经度
				if($user_id!="")
				{
					$member_info = $this->model_search->get_member_info_detail($user_id);
					if($member_info)
					{
						if($member_info[0]['status'] == 'Y')
						{
							$returnArr["rt_cd"] = "0000";//成功
							$returnArr["rt_msg"] = "成功";
							$returnArr["rt_01"] = $member_info[0]['name'];//會員姓名
							if($member_info[0]['gender']=='M')
								$sex = "先生";
							else if($member_info[0]['gender']=='F')
								$sex = "小姐";
							else
								$sex = "";
							$returnArr["rt_02"] = $sex;//用户稱謂
							$returnArr["rt_03"] = $member_info[0]['province'];//用户省份
							$returnArr["rt_04"] = $member_info[0]['city'];//用户縣市
							$returnArr["rt_05"] = $member_info[0]['district'];//用戶區
							$returnArr["rt_06"] = "http://47.100.19.81/DEMODogWood/member/memberpic/getImageSrc/{$member_info[0]['s_num']}/pic_face";//用户头像 
							$returnArr["rt_07"] = $member_info[0]['change_count'];//租借次數
							if($member_info[0]['payment_plan']!="M")
								$member_info[0]['payment_plan'] = "O";
							$returnArr["rt_08"] = $member_info[0]['payment_plan'];//會員付費類別
							$returnArr["rt_09"] = $member_info[0]['nick_name'];//會員暱稱
							$returnArr["rt_10"] = $member_info[0]['mobile'];//行動電話
							$returnArr["rt_11"] = $member_info[0]['birthday'];//生日
							$leave_battery_id = "";
							$leaveInfo = $this->model_search->getleavebatterylog2($user_id);
							if($leaveInfo)
							{
								foreach($leaveInfo as $keyL => $valueL)
								{
									if($leave_battery_id=="")
										$leave_battery_id = $valueL['leave_battery_id'];
									else
										$leave_battery_id .= ",".$valueL['leave_battery_id'];
								}
							}
							$returnArr["rt_12"] = $leave_battery_id;//會員目前綁定之電池編號
							if($member_info[0]['member_type']=="F")
								$returnArr["rt_13"] = 'Y';//可榜定多顆
							else
								$returnArr["rt_13"] = 'N';//不可榜定多顆
							$returnArr["rt_14"] = $member_info[0]['lease_status'];//借電狀態(L:借出中,N:尚未借電)
							if($member_info[0]['top12']=="")
								$member_info[0]['top12'] = 'N';
							$returnArr["rt_15"] = $member_info[0]['top12'];//是否為車電一體配合廠商(Y:是,N:否)

						}
						else if($member_info[0]['status'] == 'R')
						{
							$returnArr["rt_cd"] = "0006";//審核中
							$returnArr["rt_msg"] = "您的会员资格尚在审核中,请等待您审核通过后,再次执行此APP";
							
						}
						else
						{
							$returnArr["rt_cd"] = "0002";//查无会员资料
							$returnArr["rt_msg"] = "查无会员资料";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//会员ID错误
					$returnArr["rt_msg"] = "会员ID错误";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//查询钱包API(for 魔力3.0)
	public function bcbd1a79()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'bcbd1a79','钱包資訊-3');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['cf01']);//查询日期
				if(isset($data_json_de['cf02']))
				{
					if($data_json_de['cf02']!="")
						$user_id = str_pad(addslashes($data_json_de['cf02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
					else
						$user_id = "";
				}
				else
					$user_id = "";
				$coordinate = addslashes($data_json_de['cf03']);//查询时纬度经度
				if($user_id!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						//写入查询记录
						$this->model_search->insert_wallet_query_log($member_info[0]['s_num'],$quiry_date,$coordinate);
						$returnArr["rt_cd"] = "0000";//成功
						$returnArr["rt_msg"] = "成功";
						$returnArr["rt_01"] = $member_info[0]['deposit'];//押金
						if($member_info[0]['member_type']=='M')
						{
							$returnArr["rt_02"] = $member_info[0]['month_expiry_date'];//月費到期日
						}
						else
						{
							$returnArr["rt_02"] = "";//月費到期日
						}
						$returnArr["rt_03"] = $member_info[0]['stored_value'];//剩餘儲值金額
						if($member_info[0]['deposit_status']=="")
							$member_info[0]['deposit_status'] = 0;
						$returnArr["rt_04"] = $member_info[0]['deposit_status'];//是否已繳交押金(0:未支付,1:已支付)
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//会员ID错误
					$returnArr["rt_msg"] = "会员ID错误";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//借电站查询API（for魔動)
	public function eb029535()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'eb029535','借电站查询API（for魔動)');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['bl01']);//查询日期
				if(isset($data_json_de['bl02']))
				{
					if($data_json_de['bl02']!="")
						$user_id = str_pad(addslashes($data_json_de['bl02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
					else
						$user_id = "";
				}
				$coordinate = $data_json_de['bl03'];//查询时纬度经度
				if($user_id!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						$stationInfo = $this->model_search->getstationfordom($coordinate);
						if($stationInfo)
						{
							$returnArr["rt_cd"] = "0000";//成功
							$returnArr["rt_msg"] = "成功";
							foreach($stationInfo as $key => $value)
							{
								$returnArr['holy_info'][$key]['rt01'] = $value['bss_id'];//借電站序號
								$returnArr['holy_info'][$key]['rt02'] = $value['latitude'].",".$value['longitude'];//租借纬度經度
								$returnArr['holy_info'][$key]['rt03'] = $value['canuse_battery_num'];//可租借电池数
								$returnArr['holy_info'][$key]['rt04'] = $value['location'];//借電站名稱
							}
							//写入查询记录
							$this->model_search->insert_bss_query_log($member_info[0]['s_num'],$quiry_date,$coordinate);
						}
						else
						{
							$returnArr["rt_cd"] = "0006";//查无借电站资料
							$returnArr["rt_msg"] = "查无借电站资料";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//会员ID错误
					$returnArr["rt_msg"] = "会员ID错误";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		// print_r($returnArr);
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//电池查询API(for魔動)
	public function de09fec0()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'de09fec0','电池查询API(for魔動)');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['bt01']);//查询日期
				if(isset($data_json_de['bt02']))
				{
					if($data_json_de['bt02']!="")
						$user_id = str_pad(addslashes($data_json_de['bt02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
					else
						$user_id = "";
				}
				$coordinate = addslashes($data_json_de['bt03']);//查询时纬度经度
				if($user_id!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						$stationInfo = $this->model_search->getbatteryfordom($coordinate);
						if($stationInfo)
						{
							$returnArr["rt_cd"] = "0000";//成功
							$returnArr["rt_msg"] = "成功";
							foreach($stationInfo as $key => $value)
							{
								$returnArr['holy_info'][$key]['rt01'] = $value['battery_id'];//電池序號
								$returnArr['holy_info'][$key]['rt02'] = $value['latitude'].",".$value['longitude'];//租借纬度經度
								$returnArr['holy_info'][$key]['rt03'] = $value['user_id'];//User ID
								$returnArr['holy_info'][$key]['rt04'] = $value['battery_capacity'];//電池電量
							}
							//写入查询记录
							$this->model_search->insert_battery_query_log($member_info[0]['s_num'],$quiry_date,$coordinate);
						}
						else
						{
							$returnArr["rt_cd"] = "0006";//查无借电站资料
							$returnArr["rt_msg"] = "查无电池资料";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//会员ID错误
					$returnArr["rt_msg"] = "会员ID错误";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		// print_r($returnArr);
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//查询车辆API
	public function bdcb75fa()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'bdcb75fa','车辆查询');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['cl01']);//查询日期
				if(isset($data_json_de['cl02']))
				{
					if($data_json_de['cl02']!="")
						$user_id = str_pad(addslashes($data_json_de['cl02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
					else
						$user_id = "";
				}
				$coordinate = addslashes($data_json_de['cl03']);//查询时纬度经度
				if($user_id!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						$carInfo = $this->model_search->getcar($coordinate);
						if($carInfo)
						{
							$returnArr["rt_cd"] = "0000";//成功
							$returnArr["rt_msg"] = "成功";
							foreach($carInfo as $key => $value)
							{
								$returnArr['holy_info'][$key]['rt_02'] = $value['latitude'].",".$value['longitude'];//租借纬度精度
								$returnArr['holy_info'][$key]['rt_03'] = $value['unit_name'];//車輛ID
							}
						}
						else
						{
							$returnArr["rt_cd"] = "0006";//查无车辆资料
							$returnArr["rt_msg"] = "查无车辆资料";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//会员ID错误
					$returnArr["rt_msg"] = "会员ID错误";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//查询储值消费历程API
	public function beb5cc1d()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'beb5cc1d','储值消费历程');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['sc01']);//查询日期
				$personal_id = addslashes($data_json_de['sc02']);//身分证字号
				$query_type = addslashes($data_json_de['sc03']);//查询类别
				$coordinate = addslashes($data_json_de['sc04']);//查询时纬度经度
				if($personal_id!="")
				{
					$member_info = $this->model_search->get_member_info($personal_id);
					if($member_info)
					{
						$storechargeInfo = $this->model_search->getstorechargelog($personal_id,$query_type);
						if($storechargeInfo)
						{
							$returnArr["rt_cd"] = "0000";//成功
							$returnArr["rt_msg"] = "成功";
							foreach($storechargeInfo as $key => $value)
							{
								$returnArr['holy_info'][$key]['rt01'] = $value['exchange_date'];//储值日期
								$returnArr['holy_info'][$key]['rt02'] = $value['type'];//储值类别
								$returnArr['holy_info'][$key]['rt03'] = $value['charge_amount'];//此次储值/扣款金额
								$returnArr['holy_info'][$key]['rt04'] = $value['store_balance_before'];//储值/扣款前储值余额
								$returnArr['holy_info'][$key]['rt05'] = $value['store_balance_after'];//储值/扣款后储值余额
							}
						}
						else
						{
							$returnArr["rt_cd"] = "0006";//查无消费资料
							$returnArr["rt_msg"] = "查无消费资料";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//会员ID错误
					$returnArr["rt_msg"] = "会员ID错误";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//查询电池租借历程(魔力3)
	public function bafcb4e3()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'bafcb4e3','电池租借历程(魔力3)');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['sh01']);//查询日期
				if(isset($data_json_de['sh02']))
				{
					if($data_json_de['sh02']!="")
						$user_id = str_pad(addslashes($data_json_de['sh02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
					else
						$user_id = "";
				}
				$coordinate = addslashes($data_json_de['sh03']);//查询时纬度经度
				if($user_id!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						$storechargeInfo = $this->model_search->getbatterylog1($user_id);
						// print_r($storechargeInfo);
						if($storechargeInfo)
						{
							$returnArr["rt_cd"] = "0000";//成功
							$returnArr["rt_msg"] = "成功";
							foreach($storechargeInfo as $key => $value)
							{
								$returnArr['holy_info'][$key]['rt01'] = $value['leave_date'];//租借日期
								$returnArr['holy_info'][$key]['rt02'] = $value['leave_bss_location'];//租借地点
								$returnArr['holy_info'][$key]['rt03'] = $value['leave_bss_id'];//租借借电站BSS ID
								$returnArr['holy_info'][$key]['rt04'] = $value['return_date'];//归还日期
								$returnArr['holy_info'][$key]['rt05'] = $value['return_bss_location'];//归还地点
								$returnArr['holy_info'][$key]['rt06'] = $value['return_bss_id'];//归还借电站BSS ID
								$returnArr['holy_info'][$key]['rt07'] = $value['usage_time'];//租借时间
								$charge_amount = 0;
								if($value['charge_amount']!=0)
									$charge_amount = $value['charge_amount'];
								$returnArr['holy_info'][$key]['rt08'] = $charge_amount;//交易扣款金額
								$returnArr['holy_info'][$key]['rt09'] = $value['stored_value'];//剩餘金額
								$returnArr['holy_info'][$key]['rt10'] = "1";//支付方式
								$returnArr['holy_info'][$key]['rt11'] = $value['leave_battery_id'];//借出電池
								$returnArr['holy_info'][$key]['rt12'] = $value['return_battery_id'];//歸還電池
							}
						}
						else
						{
							$returnArr["rt_cd"] = "0006";//查无租借资料
							$returnArr["rt_msg"] = "查无租借资料";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//会员ID错误
					$returnArr["rt_msg"] = "会员ID错误";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//会员登入
	public function feb836c0()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'feb836c0','会员登入');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$login_date = addslashes($data_json_de['ml01']);//登入日期
				$mobile = addslashes($data_json_de['ml02']);//电话号码
				if($mobile!="")
				{
					$member_info = $this->model_search->get_member_info2($mobile);
					if($member_info)
					{
						$returnArr["rt_cd"] = "0000";
						$returnArr["rt_msg"] = "成功";
						$returnArr["rt_01"] = "N";
						$returnArr["rt_02"] = substr($member_info[0]['user_id'],-8);
						$returnArr["rt_03"] = $member_info[0]['top01'];
						$returnArr["rt_04"] = $member_info[0]['verify_name'];
						$returnArr["rt_05"] = $member_info[0]['lease_status'];
					}
					else
					{
						$returnArr = $this->model_register->register_member1($mobile);
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0002";//请填入电话号码
					$returnArr["rt_msg"] = "请填入电话号码";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//身分验证API
	public function ff36cc21()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'ff36cc21','身分验证');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$verify_date = addslashes($data_json_de['iv01']);//驗證日期
				$user_id = "";
				if(isset($data_json_de['iv02']))
				{
					if($data_json_de['iv02']!="")
						$user_id = str_pad(addslashes($data_json_de['iv02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
				}
				$personal_id = addslashes($data_json_de['iv03']);//身分证字号
				$name = addslashes($data_json_de['iv04']);//姓名
				if($personal_id!="" && $name!="" && $user_id!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						$url = "https://way.jd.com/fegine/idCert?name={$name}&id={$personal_id}&appkey=e796007fc21f318e84310ef4c1fa5fb2";
						$s_num = $this->model_common->insert_person_verify_log($url);
						$ch = curl_init();
		 				
						curl_setopt($ch, CURLOPT_URL, $url);
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
						curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
						$output = curl_exec($ch);
						curl_close($ch);
						$this->model_common->update_person_verify_log($s_num,$output);
						if($output!="")
						{
							if($this->model_common->is_json($output))
							{
								$dataArr = array();
								$dataArr = json_decode($output,true);
								if($dataArr['result']['success'])
								{
									$dataArrm['personal_id'] = $dataArr['result']['id'];
									$dataArrm['name'] = $dataArr['result']['name'];
									if($dataArr['result']['sex']=="男")
										$gender = "M";
									else if(($dataArr['result']['sex']=="女"))
										$gender = "F";
									$areaArr1 = explode("省",$dataArr['result']['area']);
									$areaArr2 = explode("市",$areaArr1[count($areaArr1)-1]);
									$areaArr3 = explode("区",$areaArr2[count($areaArr2)-1]);
									if($areaArr1[0]!="")
										$dataArrm['province'] = $areaArr1[0]."省";
									if($areaArr2[0]!="")
										$dataArrm['city'] = $areaArr2[0]."市";
									if($areaArr3[0]!="")
										$dataArrm['district'] = $areaArr3[0]."区";
									$dataArrm['total_address'] = $dataArr['result']['area'];
									$dataArrm['birthday'] = $dataArr['result']['birthday'];
									$dataArrm['verify_name'] = 'Y';
									$returnArr = $this->model_register->editmember($user_id,$dataArrm);
								}
								else
								{
									$returnArr["rt_cd"] = "0005";//验证失败
									$returnArr["rt_msg"] = "验证失败";
								}
								
							}
							else
							{
								$returnArr["rt_cd"] = "0005";//验证失败
								$returnArr["rt_msg"] = "验证失败";
							}
						}
						else
						{
							$returnArr["rt_cd"] = "0005";//验证失败
							$returnArr["rt_msg"] = "验证失败";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0004";//请填入身分证以及姓名
					$returnArr["rt_msg"] = "请填入身分证以及姓名";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//修改会员资料
	public function ab1145c5()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'ab1145c5','修改会员资料');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$editor_date = addslashes($data_json_de['me01']);//修改日期
				$user_id = "";
				if(isset($data_json_de['me02']))
				{
					if($data_json_de['me02']!="")
						$user_id = str_pad(addslashes($data_json_de['me02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
				}
				$nick_name = addslashes($data_json_de['me03']);//暱稱
				$edit_mobile = addslashes($data_json_de['me04']);//是否修改电话号码
				$mobile = addslashes($data_json_de['me05']);//电话号码
				$edit_pwd = addslashes($data_json_de['me06']);//是否修改密碼
				$old_pwd = addslashes($data_json_de['me07']);//舊密碼
				$new_pwd = addslashes($data_json_de['me08']);//新密碼
				if($user_id!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						$edit_flag = false; 
						if($nick_name!="")
						{
							$edit_flag = true;
							$dataArrm['nick_name'] = $nick_name;
						}
						if($edit_mobile=="Y")
						{
							if($mobile!="")
							{
								$checkmobile = $this->model_search->checkmobile($user_id,$mobile);
								if($checkmobile)
								{
									if($old_pwd!="")
									{
										$canedit = $this->model_search->checkpwd($user_id,$old_pwd);
										if($canedit)
										{
											$edit_flag = true;
											$dataArrm['mobile'] = $mobile;
										}
										else
										{
											$returnArr["rt_cd"] = "0007";//旧密码错误
											$returnArr["rt_msg"] = "旧密码错误";
										}
									}
									else
									{
										$returnArr["rt_cd"] = "0005";//请填入电话号码
										$returnArr["rt_msg"] = "请填入旧密码";
									}
								}
								else
								{
									$returnArr["rt_cd"] = "0010";//电话已被使用
									$returnArr["rt_msg"] = "电话已被使用";
								}
							}
							else
							{
								$returnArr["rt_cd"] = "0004";//请填入电话号码
								$returnArr["rt_msg"] = "请填入电话号码";
							}
						}

						if($edit_pwd=="Y")
						{
							if($old_pwd!="")
							{
								if($new_pwd!="")
								{
									$canedit = $this->model_search->checkpwd($user_id,$old_pwd);
									if($canedit)
									{
										$edit_flag = true;
										$dataArrm['person_pwd'] = $new_pwd;
									}
									else
									{
										$returnArr["rt_cd"] = "0007";//旧密码错误
										$returnArr["rt_msg"] = "旧密码错误";
									}
								}
								else
								{
									$returnArr["rt_cd"] = "0008";//请填入新密码
									$returnArr["rt_msg"] = "请填入新密码";
								}
							}
							else
							{
								if($new_pwd!="")
								{
									$edit_flag = true;
									$dataArrm['person_pwd'] = $new_pwd;
								}
								else
								{
									$returnArr["rt_cd"] = "0008";//请填入新密码
									$returnArr["rt_msg"] = "请填入新密码";
								}
							}	
						}

						if($edit_flag)
						{
							$returnArr = $this->model_register->editmember($user_id,$dataArrm);
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0009";//UserID不可為空
					$returnArr["rt_msg"] = "UserID不可為空";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//取得付費方案 //改撈微信 获取会员方案接口
	public function c3b5f94e()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'c3b5f94e','取得付费方案');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$editor_date = addslashes($data_json_de['gp01']);//請求日期
				$user_id = "";
				if(isset($data_json_de['gp02']))
				{
					if($data_json_de['gp02']!="")
						$user_id = ltrim(addslashes($data_json_de['gp02']),"0");	//去除前面的0
				}
				$latitude = addslashes($data_json_de['gp03']);//請求緯度
				$longitude = addslashes($data_json_de['gp04']);//請求經度

				if($user_id!="")
				{
					
					//取得微信的網址參數
					$SQLCmdC = "SELECT config_set FROM sys_config WHERE config_code ='wechat_callback_url' AND status = 1";
					$rsC = $this->db_query($SQLCmdC);
					if($rsC)
					{
						$url = $rsC[0]['config_set']."synchrouser";
					}
					else
					{
						$url = "";
					}
					$postStr = array("action" => "userScheme", 
							"id" => $user_id,
							"la" => $latitude,
							"ln" => $longitude,
							"type" => 0,
							);                
					$postStr = http_build_query($postStr);
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
					curl_setopt($ch, CURLOPT_TIMEOUT, 20); 
					curl_setopt($ch, CURLOPT_POSTFIELDS, $postStr); 
					$output = curl_exec($ch);
					$output = json_decode($output);
					$return = array();
					foreach($output as $key=>$arr){
						if($key == "data"){
							foreach($arr as $k=>$val){
								$return[$key][$k] = $val;
							}
						}else{
							$return[$key] = $arr;
						}
					}

					if($return['status'] == "ok"){
						/*1.方案編號
						2.強制模式
						3.月費
						4.季費
						5.半年費
						6.年費
						7.可綁定電池數
						8.電池押金(依照可綁定電池數來決定帶下多少個押金,每顆電池對應一個押金)
						9.底拖支付費用
						10.是否能綁定多顆
						11.是否為優惠活動
						12.優惠活動起始日
						13.優惠活動終止日*/
						$returnArr["rt_cd"] = "0000";//成功
						$returnArr["rt_msg"] = "成功";
						$returnArr["rt_01"] = $return['data'][1];//方案編號
						$returnArr["rt_02"] = $return['data'][2];//強制模式
						$returnArr["rt_03"] = $return['data'][3];//月費
						$returnArr["rt_04"] = $return['data'][4];//季費
						$returnArr["rt_05"] = $return['data'][5];//半年費
						$returnArr["rt_06"] = $return['data'][6];//年費
						$returnArr["rt_07"] = $return['data'][7];//可綁定電池數
						$returnArr["rt_08"] = $return['data'][8];//電池押金
						$returnArr["rt_09"] = $return['data'][9];//底拖支付費用
						$returnArr["rt_10"] = $return['data'][10];//允許綁定多顆
						$returnArr["rt_11"] = $return['data'][11];//是否為優惠活動
						$returnArr["rt_12"] = $return['data'][12];//優惠活動起始日
						$returnArr["rt_13"] = $return['data'][13];//優惠活動終止日
					}else{
						$returnArr["rt_cd"] = $return['code'];
						$returnArr["rt_msg"] = $return['msg'];
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0009";//UserID不可為空
					$returnArr["rt_msg"] = "UserID不可為空";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

}	

/* End of file Bss01.php */
/* Location: ./application/controllers/api/Bss01.php */