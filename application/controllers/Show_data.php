<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Show_data extends CI_Controller {
	
	/**
	 * 建構式
	 * 預先載入Show_driver_info的物件
	 */
    function __construct() 
    {
        parent::__construct();
		$this->load->database();
		$this->load->model("common/model_common", "model_common") ;
    }
	
	public function index($page_num = 0)
	{
		$this->model_common->getstatistics();
	}

	public function insertbsslogdata()
	{
		$this->model_common->insertbsslogdata();
	}

	// public function testtoprod()
	// {
	// 	$this->model_common->testtoprod();
	// }

	public function addnewlog()
	{
		$this->model_common->addnewlog();
	}
}

/* End of file Show_driver_info.php */
/* Location: ./application/controllers/Show_driver_info.php */