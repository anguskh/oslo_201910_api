<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wechat extends CI_Controller {
	
	/**
	 * 建构式
	 * 预先载入Wechat的物件
	 */
    function __construct() 
    {
        parent::__construct();
        $this->load->model("wechat/Model_order_wechat", "Model_order_wechat") ;
        $this->load->model("wechat/model_search", "model_search") ;
        $this->load->model("wechat/model_register", "model_register") ;
        $this->load->model("wechat/Model_exception", "model_exception") ;
		$this->load->library("MY_WxPay") ;
		$this->load->model("common/model_common", "model_common") ;

    }

	//微信支付通知
	public function df57ab54()
	{
		//計算執行時間
		$time_start = microtime(true);
		$api_name = 'df57ab54';
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'df57ab54','微信支付通知');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				if(isset($data_json_de['wp01']))
					$notify_date = addslashes($data_json_de['wp01']);//微信支付通知日期
				else
					$notify_date = "";
				if(isset($data_json_de['wp02']))
					$bss_id = addslashes($data_json_de['wp02']);//電池交換站Token ID序號
				else
					$bss_id = "";

				if(isset($data_json_de['wp03']))
				{
					if($data_json_de['wp03']!="")
						$user_id = str_pad(addslashes($data_json_de['wp03']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
					else
						$user_id = "";
				}
				else
					$user_id = "";

				if(isset($data_json_de['wp04']))
					$amount = addslashes($data_json_de['wp04']);//微信支付扣款金額
				else
					$amount = "";

				$wechat_qrcode_url = $this->model_search->getqrconfig();

				$qrcode_url = $wechat_qrcode_url.$bss_id.".png";//QR Code URL

				if(isset($data_json_de['wp06']))
					$gps_location = addslashes($data_json_de['wp06']);//微信支付時的GPS座標
				else
					$gps_location = "";

				// if(isset($data_json_de['wp07']))
				// 	$orderSn = addslashes($data_json_de['wp07']);//订单号orderSn
				// else
				$orderSn = "XO".date("YmdHis").floor(microtime() * 1000);

				$wechat_token = $this->generatorToken();//token

				$mdtoken = $this->model_search->getmdtoken($qrcode_url);//魔力token

				$order_status = 2;//訂單狀態

				if(isset($data_json_de['wp11']))
					$scan_date = addslashes($data_json_de['wp11']);//掃碼時間
				else
					$scan_date = NULL;

				if($this->model_common->checkdata($api_name,$notify_date))
				{
					exit();
				}

				if($user_id!="" && $notify_date!="" && $bss_id!="" && $amount!="" && $qrcode_url!="" && $gps_location!="" && $orderSn!="" && $wechat_token!="" && $mdtoken!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						$bss_info = $this->model_search->get_bssinfo($bss_id);
						if($bss_info)
						{
							$dataArr['payment_date'] = $notify_date;
							$dataArr['user_id'] = $user_id;
							$dataArr['bss_token'] = $bss_info[0]['bss_token'];
							$dataArr['url_Info'] = $qrcode_url;
							$dataArr['tm_num'] = $member_info[0]['s_num'];
							$dataArr['amount'] = $amount;
							$dataArr['gps_location'] = $gps_location;
							$dataArr['notify'] = 0;
							$dataArr['orderSn'] = $orderSn;
							$dataArr['wechat_token'] = $wechat_token;							
							$dataArr['mdtoken'] = $mdtoken;
							$dataArr['order_status'] = $order_status;
							$dataArr['scan_date'] = $scan_date;
							

							$logArr = $this->model_search->getleavebatterylog($user_id);

							$this->Model_order_wechat->insert_payment($dataArr);

							$returnArr["rt_cd"] = "0000";//成功
							$returnArr["rt_msg"] = "成功";
						}
						else
						{
							$returnArr["rt_cd"] = "0004";//查无会员资料
							$returnArr["rt_msg"] = "查无借电站";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//会员ID错误
					$returnArr["rt_msg"] = "资料有误";
					// $returnArr["rt_msg"] = "会员User ID不得為空";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//查询电池剩余电量API
	public function db05d9c9()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'db05d9c9','剩余电量查询');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson);
				$quiry_date = addslashes($data_json_de->bc01);//查询日期
				if($data_json_de->bc02!="")
					$user_id = str_pad(addslashes($data_json_de->bc02),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
				else
					$user_id = "";
				$coordinate = addslashes($data_json_de->bc03);//查询时纬度经度
				if($user_id!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						$batterypowerInfo = $this->model_search->getbatterypower($user_id,$member_info[0]['member_type']);
						if($batterypowerInfo)
						{
							$returnArr["rt_cd"] = "0000";//成功
							$returnArr["rt_msg"] = "成功";
							$returnArr["rt_01"] = $batterypowerInfo[0]['battery_capacity'];//电池电量
							$returnArr["rt_02"] = $batterypowerInfo[0]['latitude'];//电池GPS緯度
							$returnArr["rt_03"] = $batterypowerInfo[0]['longitude'];//电池GPS經度
							if($member_info[0]['member_type']=='F')
							{
								foreach($batterypowerInfo as $key => $value)
								{
									$returnArr['em_info'][$key]['battery_id'] = $value['leave_battery_id'];
									$returnArr['em_info'][$key]['battery_capacity'] = $value['battery_capacity'];
									$returnArr['em_info'][$key]['latitude'] = $value['latitude'];
									$returnArr['em_info'][$key]['longitude'] = $value['longitude'];
								}
							}
							else
							{
								$returnArr['em_info'][0]['battery_id'] = $batterypowerInfo[0]['leave_battery_id'];
								$returnArr['em_info'][0]['battery_capacity'] = $batterypowerInfo[0]['battery_capacity'];
								$returnArr['em_info'][0]['latitude'] = $batterypowerInfo[0]['latitude'];
								$returnArr['em_info'][0]['longitude'] = $batterypowerInfo[0]['longitude'];
							}
						}
						else
						{
							$returnArr["rt_cd"] = "0006";//查无电池电量
							$returnArr["rt_msg"] = "查无电池电量";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//会员ID错误
					$returnArr["rt_msg"] = "会员ID错误";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//微信会员注册通知
	public function d31387a9()
	{
		//計算執行時間
		$time_start = microtime(true);
		$api_name = 'd31387a9';
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'d31387a9','微信会员注册通知');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				if(isset($data_json_de['mw01']))
					$wechatInfoArr['access_token'] = addslashes($data_json_de['mw01']);//網頁授權接口調用憑證
				else
					$wechatInfoArr['access_token'] = "";
				if(isset($data_json_de['mw02']))
					$wechatInfoArr['openid'] = addslashes($data_json_de['mw02']);//用戶唯一標識
				else
					$wechatInfoArr['openid'] = "";
				if(isset($data_json_de['mw03']))
					$wechatInfoArr['scope'] = addslashes($data_json_de['mw03']);//用戶授權的作用域
				else
					$wechatInfoArr['scope'] = "";
				if(isset($data_json_de['mw05']))
					$wechatInfoArr['sex'] = addslashes($data_json_de['mw05']);//用戶性別
				else
					$wechatInfoArr['sex'] = "";
				if(isset($data_json_de['mw06']))
					$wechatInfoArr['province'] = addslashes($data_json_de['mw06']);//用戶省份
				else
					$wechatInfoArr['province'] = "";
				if(isset($data_json_de['mw07']))
					$wechatInfoArr['city'] = addslashes($data_json_de['mw07']);//用戶城市
				else
					$wechatInfoArr['city'] = "";
				if(isset($data_json_de['mw08']))
					$wechatInfoArr['country'] = addslashes($data_json_de['mw08']);//用戶國家
				else
					$wechatInfoArr['country'] = "";
				if(isset($data_json_de['mw09']))
					$wechatInfoArr['headimgurl'] = addslashes($data_json_de['mw09']);//用戶頭像
				else
					$wechatInfoArr['headimgurl'] = "";
				if(isset($data_json_de['mw10']))
					$wechatInfoArr['privilege'] = addslashes($data_json_de['mw10']);//用戶特權信息
				else
					$wechatInfoArr['privilege'] = "";
				if(isset($data_json_de['mw11']))
					$wechatInfoArr['unionid'] = addslashes($data_json_de['mw11']);//UnionID機制
				else
					$wechatInfoArr['unionid'] = "";

				if(isset($data_json_de['mw12']) && $data_json_de['mw12']!="")
					$wechatInfoArr['user_id'] = str_pad(addslashes($data_json_de['mw12']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
				else
					$wechatInfoArr['user_id'] = "";

				if(isset($data_json_de['mw16']))
					$wechatInfoArr['deposit'] = addslashes($data_json_de['mw16']);//押金金額
				else
					$wechatInfoArr['deposit'] = "";

				if(isset($data_json_de['mw17']))
					$wechatInfoArr['deposit_status'] = addslashes($data_json_de['mw17']);//押金狀態（0=未支付；1=已支付）
				else
					$wechatInfoArr['deposit_status'] = "";

				if(isset($data_json_de['mw18']))
					$wechatInfoArr['member_type'] = addslashes($data_json_de['mw18']);//會員類別
				else
					$wechatInfoArr['member_type'] = "";

				if(isset($data_json_de['mw19']))
					$wechatInfoArr['wechat_op_id'] = addslashes($data_json_de['mw19']);//微信企業ID
				else
					$wechatInfoArr['wechat_op_id'] = "";

				if($this->model_common->checkdata($api_name,$wechatInfoArr['access_token']))
				{
					exit();
				}

				if($data_json_de['mw04']!="" && $data_json_de['mw13']!="" && $data_json_de['mw14']!="" && $data_json_de['mw15']!="")
				{
					$wechatInfoArr['nickname'] = addslashes($data_json_de['mw04']);//用戶暱稱
					$wechatInfoArr['create_date'] = addslashes($data_json_de['mw13']);//註冊時間
					$wechatInfoArr['mobile'] = addslashes($data_json_de['mw14']);//手機號
					$wechatInfoArr['personal_id'] = addslashes($data_json_de['mw15']);//身分證號
					if($wechatInfoArr['user_id']!="")
					{
						$change_status = $this->model_register->register_member($wechatInfoArr);
						if($change_status === true)
						{
							$returnArr["rt_cd"] = "0000";//成功
							$returnArr["rt_msg"] = "成功";
							// $returnArr["rt_01"] = $wechatInfoArr['openid'];
						}
						else
						{
							$returnArr["rt_cd"] = "0010";//資料有誤
							$returnArr["rt_msg"] = $change_status;
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0005";//会员ID错误
						$returnArr["rt_msg"] = "会员User ID不得為空";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0010";//資料有誤
					$returnArr["rt_msg"] = "会员注册失败";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//電池交換站查詢
	public function f8c39d7d()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'f8c39d7d','电池交换站查询');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['bl01']);//電池交換站查詢日期
				if($data_json_de['bl02']!="")
					$user_id = str_pad(addslashes($data_json_de['bl02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
				else
					$user_id = "";
				$coordinate = addslashes($data_json_de['bl03']);//查询时纬度经度
				// $bss_id = addslashes($data_json_de['bl04']);//電池交換站編號

				if($user_id!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						// $stationInfo = $this->model_search->get_bssinfo($bss_id);
						$stationInfo = $this->model_search->getstation($coordinate);
						if($stationInfo)
						{
							$returnArr["rt_cd"] = "0000";//成功
							$returnArr["rt_msg"] = "成功";
							foreach($stationInfo as $key => $value)
							{
								$returnArr["em_info"][$key]['bss_id'] = $value['bss_id'];//機櫃序號
								$returnArr["em_info"][$key]['bss_name'] = $value['location'];//機櫃名稱
								$returnArr["em_info"][$key]['gps'] = $value['latitude'].",".$value['longitude'];//GPS經緯度
								$returnArr["em_info"][$key]['tracks'] = $value['track_quantity'];//機櫃軌道數量
								$returnArr["em_info"][$key]['batt_tatal'] = $value['battery_count'];//機櫃電池總數
								$returnArr["em_info"][$key]['batt_full'] = $value['full_battery_count'];//飽電電池總數
								$returnArr["em_info"][$key]['batt_charging'] = (string)$value['battery_count'] - $value['full_battery_count'];//充電中電池數量
							}
							// $returnArr["rt_01"] = $stationInfo[0]['latitude'].",".$stationInfo[0]['longitude'];//電池交換站的GPS座標
							// $returnArr["rt_02"] = $stationInfo[0]['canuse_battery_num'];//可交換電池數量
						}
						else
						{
							$returnArr["rt_cd"] = "0006";//查无借电站资料
							$returnArr["rt_msg"] = "查无借电站资料";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//会员ID错误
					$returnArr["rt_msg"] = "会员User ID不得為空";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		// print_r($returnArr);
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//電池綁定通知
	public function ba191a51()
	{
		//計算執行時間
		$time_start = microtime(true);
		$api_name = 'ba191a51';
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'ba191a51','电池绑定通知');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				if(isset($data_json_de['bp01']))
					$dataArr['leave_date'] = addslashes($data_json_de['bp01']);//電池綁定日期
				else
					$dataArr['leave_date'] = "";
				if(isset($data_json_de['bp02']))
					$dataArr['battery_user_id'] = str_pad(addslashes($data_json_de['bp02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
				else
					$dataArr['battery_user_id'] = "";
				if(isset($data_json_de['bp03']))
					$dataArr['leave_battery_id'] = $data_json_de['bp03'];
				else
					$dataArr['leave_battery_id'] = "";

				if(isset($data_json_de['bp04']))
					$dataArr['leave_coordinate'] = $data_json_de['bp04'];
				else
					$dataArr['leave_coordinate'] = "";

				if(isset($data_json_de['bp05']))
					$bss_id = $data_json_de['bp05'];
				else
					$bss_id = "";

				if(isset($data_json_de['bp06']))
					$bind_type = $data_json_de['bp06'];
				else
					$bind_type = "";

				if(isset($data_json_de['bp07']))
					$return_battery = $data_json_de['bp07'];
				else
					$return_battery = "";

				if($this->model_common->checkdata($api_name,$dataArr['leave_date']))
				{
					exit();
				}

				$dataArr['leave_sb_num'] = "";

				if($dataArr['leave_date']!="" && $dataArr['battery_user_id']!="" && $dataArr['leave_battery_id']!="" && $dataArr['leave_coordinate']!="")
				{
					$member_info = $this->model_search->get_member_info1($dataArr['battery_user_id']);
					if($member_info)
					{
						$dataArr['tm_num'] = $member_info[0]['s_num'];
						$dataArr['member_type'] = $member_info[0]['member_type'];
						$batteryInfo = $this->model_search->getbatteryInfo($dataArr['leave_battery_id']);
						if(count($batteryInfo)!=0)
						{
							if($batteryInfo[0]['status']!=0 && $batteryInfo[0]['status']!=1 && $batteryInfo[0]['status']!=6)
							{
								$returnArr["rt_cd"] = "0008";//電池故障
								$returnArr["rt_msg"] = "电池故障";
							}
							else
							{
								if($bss_id!="")
								{
									$stationInfo = $this->model_search->get_bssinfo($bss_id);
									$dataArr['leave_sb_num'] = $stationInfo[0]['s_num'];
								}

								$coutinue = true;
								if($return_battery!="")
								{
									$batteryInfoR = $this->model_search->getbatteryInfo($return_battery);
									if(count($batteryInfoR)==0)
									{
										$returnArr["rt_cd"] = "0006";//查无借电站资料
										$returnArr["rt_msg"] = "查无电池资料";
										$coutinue = false;
									}
								}
									
								if($coutinue)
								{
									$bindbattery = $this->model_register->bind_battery($dataArr,$bss_id,$bind_type,$return_battery);

									$returnArr["rt_cd"] = $bindbattery['rt_cd'];//成功
									$returnArr["rt_msg"] = $bindbattery['rt_msg'];
								}							}
						}
						else
						{
							$returnArr["rt_cd"] = "0006";//查无借电站资料
							$returnArr["rt_msg"] = "查无电池资料";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0004";//会员ID错误
					$returnArr["rt_msg"] = "栏位不得为空";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//微信会员批次注册通知
	public function c9a4b934d()
	{
		//計算執行時間
		$time_start = microtime(true);
		$api_name = 'c9a4b934d';
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'c9a4b934d','微信会员批次注册通知');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				if(isset($data_json_de['em_info']))
					$em_infoArr = $data_json_de['em_info'];
				else
					$em_infoArr = "";
				if($em_infoArr != "")
				{
					$batch_insert_lag = true;
					$rt_msg = "";
					foreach($em_infoArr as $key => $data_json_de1)
					{
						$wechatInfoArr = array();
						if(isset($data_json_de1['mw01']))
							$wechatInfoArr['access_token'] = addslashes($data_json_de1['mw01']);//網頁授權接口調用憑證
						else
							$wechatInfoArr['access_token'] = "";
						if(isset($data_json_de1['mw02']))
							$wechatInfoArr['openid'] = addslashes($data_json_de1['mw02']);//用戶唯一標識
						else
							$wechatInfoArr['openid'] = "";
						if(isset($data_json_de1['mw03']))
							$wechatInfoArr['scope'] = addslashes($data_json_de1['mw03']);//用戶授權的作用域
						else
							$wechatInfoArr['scope'] = "";
						if(isset($data_json_de1['mw05']))
							$wechatInfoArr['sex'] = addslashes($data_json_de1['mw05']);//用戶性別
						else
							$wechatInfoArr['sex'] = "";
						if(isset($data_json_de1['mw06']))
							$wechatInfoArr['province'] = addslashes($data_json_de1['mw06']);//用戶省份
						else
							$wechatInfoArr['province'] = "";
						if(isset($data_json_de1['mw07']))
							$wechatInfoArr['city'] = addslashes($data_json_de1['mw07']);//用戶城市
						else
							$wechatInfoArr['city'] = "";
						if(isset($data_json_de1['mw08']))
							$wechatInfoArr['country'] = addslashes($data_json_de1['mw08']);//用戶國家
						else
							$wechatInfoArr['country'] = "";
						if(isset($data_json_de1['mw09']))
							$wechatInfoArr['headimgurl'] = addslashes($data_json_de1['mw09']);//用戶頭像
						else
							$wechatInfoArr['headimgurl'] = "";
						if(isset($data_json_de1['mw10']))
							$wechatInfoArr['privilege'] = addslashes($data_json_de1['mw10']);//用戶特權信息
						else
							$wechatInfoArr['privilege'] = "";
						if(isset($data_json_de1['mw11']))
							$wechatInfoArr['unionid'] = addslashes($data_json_de1['mw11']);//UnionID機制
						else
							$wechatInfoArr['unionid'] = "";

						if(isset($data_json_de1['mw12']) && $data_json_de1['mw12']!="")
							$wechatInfoArr['user_id'] = str_pad(addslashes($data_json_de1['mw12']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
						else
							$wechatInfoArr['user_id'] = "";

						if(isset($data_json_de1['mw16']))
							$wechatInfoArr['deposit'] = addslashes($data_json_de1['mw16']);//押金金額
						else
							$wechatInfoArr['deposit'] = "";

						if(isset($data_json_de1['mw17']))
							$wechatInfoArr['deposit_status'] = addslashes($data_json_de1['mw17']);//押金狀態（0=未支付；1=已支付）
						else
							$wechatInfoArr['deposit_status'] = "";

						if(isset($data_json_de['mw18']))
							$wechatInfoArr['member_type'] = addslashes($data_json_de['mw18']);//會員類別
						else
							$wechatInfoArr['member_type'] = "";
						
						if(isset($data_json_de['mw19']))
							$wechatInfoArr['wechat_op_id'] = addslashes($data_json_de['mw19']);//微信企業ID
						else
							$wechatInfoArr['wechat_op_id'] = "";
						
						if($this->model_common->checkdata($api_name,$wechatInfoArr['access_token']))
						{
							exit();
						}
						
						if($data_json_de1['mw04']!="" && $data_json_de1['mw13']!="" && $data_json_de1['mw14']!="" && $data_json_de1['mw15']!="")
						{
							$wechatInfoArr['nickname'] = addslashes($data_json_de1['mw04']);//用戶暱稱
							$wechatInfoArr['create_date'] = addslashes($data_json_de1['mw13']);//註冊時間
							$wechatInfoArr['mobile'] = addslashes($data_json_de1['mw14']);//手機號
							$wechatInfoArr['personal_id'] = addslashes($data_json_de1['mw15']);//身分證號
							if($wechatInfoArr['user_id']!="")
							{
								$change_status = $this->model_register->register_member($wechatInfoArr);
								if(!$change_status)
								{
									$batch_insert_lag = false;
									$rt_msg .= $wechatInfoArr['user_id']."注册失敗,錯誤為:".$change_status." ";
									// $returnArr["rt_cd"] = "0000";//成功
									// $returnArr["rt_msg"] = "成功";
									// $returnArr["rt_01"] = $wechatInfoArr['openid'];
								}
								// else
								// {
								// 	$returnArr["rt_cd"] = "0010";//資料有誤
								// 	$returnArr["rt_msg"] = $change_status;
								// }
							}
							else
							{
								$batch_insert_lag = false;
								$rt_msg .= "会员User ID不得為空 ";
								// $returnArr["rt_cd"] = "0005";//会员ID错误
								// $returnArr["rt_msg"] = "会员User ID不得為空";
							}
						}
						else
						{
							$batch_insert_lag = false;
							$rt_msg .= "会员注册失败 ";
							// $returnArr["rt_cd"] = "0010";//資料有誤
							// $returnArr["rt_msg"] = "会员注册失败";
						}

					}

					if($batch_insert_lag===true)
					{
						$returnArr["rt_cd"] = "0000";//成功
						$returnArr["rt_msg"] = "成功";
					}
					else
					{
						$returnArr["rt_cd"] = "0010";//資料有誤
						$returnArr["rt_msg"] = $rt_msg;
					}

				}
				else
				{
					$returnArr["rt_cd"] = "0003";//资料为空
					$returnArr["rt_msg"] = "资料为空";//回应讯息
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//查詢使用者目前的電池序號
	public function f74cf60e()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'f74cf60e','查询使用者目前的电池序号');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['su01']);//查詢日期
				if($data_json_de['su02']!="")
					$user_id = str_pad(addslashes($data_json_de['su02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
				else
					$user_id = "";
				$coordinate = addslashes($data_json_de['su03']);//查询时纬度经度

				if($user_id!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						$userbatteryID = $this->model_search->getleavebatterylog2($user_id,$member_info[0]['member_type']);
						if($userbatteryID)
						{
							if($userbatteryID[0]['return_date']=="")
							{
								$returnArr["rt_cd"] = "0000";//成功
								$returnArr["rt_msg"] = "成功";
								$returnArr["rt_01"] = $userbatteryID[0]['leave_battery_id'];//電池序號
								if($member_info[0]['member_type']=='F')
								{
									foreach($userbatteryID as $key => $value)
									{
										$returnArr['em_info'][$key]['battery_id'] = $value['leave_battery_id'];
									}
								}
								else
								{
									$returnArr['em_info'][0]['battery_id'] = $userbatteryID[0]['leave_battery_id'];//電池序號
								}
							}
							else
							{
								$returnArr["rt_cd"] = "0006";//查无借电站资料
								$returnArr["rt_msg"] = "查无使用者目前的电池序号";
							}
						}
						else
						{
							$returnArr["rt_cd"] = "0006";//查无借电站资料
							$returnArr["rt_msg"] = "查无使用者目前的电池序号";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//会员ID错误
					$returnArr["rt_msg"] = "会员User ID不得為空";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		// print_r($returnArr);
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//解除电池绑定
	public function d8bdf6c3()
	{
		$this->model_exception->return_battery();
	}

	//查詢滿電電池軌道
	public function e64a69d6()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'e64a69d6','查询满电电池轨道');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['bt01']);//查询日期
				if(isset($data_json_de['bt02']))
				{
					if($data_json_de['bt02']!="")
						$user_id = str_pad(addslashes($data_json_de['bt02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
					else
						$user_id = "";
				}
				else
					$user_id = "";
				if(isset($data_json_de['bt03']))
					$bss_id = addslashes($data_json_de['bt03']);//電池交換站序號(BSS ID)
				else
					$bss_id = "";
				// $coordinate = addslashes($data_json_de['bt04']);//查询时纬度经度
				if($user_id!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						// if($fullbatterytrackInfo)
						// {
						$returnArr["rt_cd"] = "0000";//成功
						$returnArr["rt_msg"] = "成功";
						$fulltrackStr = "";
						$fullbatterytrackInfo = $this->model_search->getfullbatterytrack($bss_id);
						if($fullbatterytrackInfo)
						{
							foreach($fullbatterytrackInfo as $key=> $value)
							{
								if($fulltrackStr == "")
									$fulltrackStr = $value["track_no"];
								else
									$fulltrackStr .= ",".$value["track_no"];
							}
						}
						
						$returnArr["rt_01"] = $fulltrackStr;//滿電電池軌道(軌道以逗號隔開)

						$chargingtrackStr = "";
						$chargingbatterytrackInfo = $this->model_search->getchargingbatterytrack($bss_id);
						if($chargingbatterytrackInfo)
						{
							foreach($chargingbatterytrackInfo as $key=> $value)
							{
								if($chargingtrackStr == "")
									$chargingtrackStr = $value["track_no"].":".$value["battery_capacity"];
								else
									$chargingtrackStr .= ",".$value["track_no"].":".$value["battery_capacity"];
							}
						}
						
						$returnArr["rt_02"] = $chargingtrackStr;//充電中電池軌道(軌道以逗號隔開)

						$emptytrackStr = "";
						$emptybatterytrackInfo = $this->model_search->getemptybatterytrack($bss_id);
						if($emptybatterytrackInfo)
						{
							foreach($emptybatterytrackInfo as $key=> $value)
							{
								if($emptytrackStr == "")
									$emptytrackStr = $value["track_no"];
								else
									$emptytrackStr .= ",".$value["track_no"];
							}
						}
						
						$returnArr["rt_03"] = $emptytrackStr;//空軌道(軌道以逗號隔開)
						// }
						// else
						// {
						// 	$returnArr["rt_cd"] = "0006";//查无电池电量
						// 	$returnArr["rt_msg"] = "查无满电轨道";
						// }
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//会员ID错误
					$returnArr["rt_msg"] = "会员ID错误";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//批次查询电池是否绑定
	public function bbcadb96()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'bbcadb96','批次查询电池是否绑定');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['bb01']);//查询日期
				if(isset($data_json_de['bb02']))
				{
					if($data_json_de['bb02']!="")
						$user_id = str_pad(addslashes($data_json_de['bb02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
					else
						$user_id = "";
				}
				else
					$user_id = "";
				if(isset($data_json_de['bb03']))
					$batteryIDArr = $data_json_de['bb03'];//電池序號(battery ID)
				else
					$batteryIDArr = "";
				// $coordinate = addslashes($data_json_de['bt04']);//查询时纬度经度
				if($user_id!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						$returnArr["rt_cd"] = "0000";//成功
						$returnArr["rt_msg"] = "成功";
						foreach($batteryIDArr as $key => $value)
						{
							$batteryInfo = $this->model_search->getbatteryInfo($value['battery_id']);
							if($batteryInfo)
							{
								switch($batteryInfo[0]['status'])
								{
									case 2:
									{
										$returnArr["em_info"][$key]['battery_id'] = $value['battery_id'];
										$returnArr["em_info"][$key]['b_cd'] = "0007";
										$returnArr["em_info"][$key]['b_msg'] = "电池故障";
									}	
									break;
									case 3:
									{
										$returnArr["em_info"][$key]['battery_id'] = $value['battery_id'];
										$returnArr["em_info"][$key]['b_cd'] = "0008";
										$returnArr["em_info"][$key]['b_msg'] = "电池维修";
									}	
									break;
									case 4:
									{
										$returnArr["em_info"][$key]['battery_id'] = $value['battery_id'];
										$returnArr["em_info"][$key]['b_cd'] = "0009";
										$returnArr["em_info"][$key]['b_msg'] = "电池报废";
									}	
									break;
									case 5:
									{
										$returnArr["em_info"][$key]['battery_id'] = $value['battery_id'];
										$returnArr["em_info"][$key]['b_cd'] = "0010";
										$returnArr["em_info"][$key]['b_msg'] = "电池失窃";
									}
									break;
									default:
									{
										$batterybindstatus = $this->model_search->getbatterystatus($value['battery_id']);
										if($batterybindstatus=="N")
										{
											$returnArr["em_info"][$key]['battery_id'] = $value['battery_id'];
											$returnArr["em_info"][$key]['b_cd'] = "0000";
											$returnArr["em_info"][$key]['b_msg'] = "电池无绑定";
										}
										else if($batterybindstatus=="Y")
										{
											$returnArr["em_info"][$key]['battery_id'] = $value['battery_id'];
											$returnArr["em_info"][$key]['b_cd'] = "0012";
											$returnArr["em_info"][$key]['b_msg'] = "电池已被绑定";
										}
										else
										{
											$returnArr["em_info"][$key]['battery_id'] = $value['battery_id'];
											$returnArr["em_info"][$key]['b_cd'] = "0011";
											$returnArr["em_info"][$key]['b_msg'] = "电池查无绑定状态";
										}
									}
									break;
								}
								
							}
							else
							{
								$returnArr["em_info"][$key]['battery_id'] = $value['battery_id'];
								$returnArr["em_info"][$key]['b_cd'] = "0006";
								$returnArr["em_info"][$key]['b_msg'] = "此电池尚未建档";
							}
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//会员ID错误
					$returnArr["rt_msg"] = "会员ID错误";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//查询电池定位與軌跡API
	public function d812c768()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'d812c768','查询电池定位與軌跡');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['bl01']);//查询日期
				if(isset($data_json_de['bl02']))
				{
					if($data_json_de['bl02']!="")
						$user_id = str_pad(addslashes($data_json_de['bl02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
					else
						$user_id = "";
				}
				else
					$user_id = "";
				if(isset($data_json_de['bl03']))
					$battery_id = addslashes($data_json_de['bl03']);//電池序號(battery ID)
				else
					$battery_id = "";

				// $coordinate = addslashes($data_json_de->bc03);//查询时纬度经度
				if($user_id!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						if($battery_id!="")
						{
							$batteryLocation = $this->model_search->getbatteryLocation($user_id,$battery_id);
							if($batteryLocation)
							{
								$returnArr["rt_cd"] = "0000";//成功
								$returnArr["rt_msg"] = "成功";
								$returnArr["rt_01"] = $batteryLocation[0]['latitude'];//电池GPS緯度
								$returnArr["rt_02"] = $batteryLocation[0]['longitude'];//电池GPS經度
								$returnArr["rt_03"] = $batteryLocation[0]['gps_path_track'];//电池軌跡
							}
							else
							{
								$returnArr["rt_cd"] = "0006";//查无电池电量
								$returnArr["rt_msg"] = "电池序号有误";
							}
						}
						else
						{
							$returnArr["rt_cd"] = "0004";//電池不得為空
							$returnArr["rt_msg"] = "电池ID不得为空";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//会员ID错误
					$returnArr["rt_msg"] = "会员ID错误";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//查詢借電站可預約之電池
	public function d4696729()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'d4696729','查询借电站可预约之电池');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['sr01']);//查询日期
				if(isset($data_json_de['sr02']))
				{
					if($data_json_de['sr02']!="")
						$user_id = str_pad(addslashes($data_json_de['sr02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
					else
						$user_id = "";
				}
				else
					$user_id = "";
				if(isset($data_json_de['sr03']))
					$bss_id = addslashes($data_json_de['sr03']);//電池交換站序號(BSS ID)
				else
					$bss_id = "";
				// $coordinate = addslashes($data_json_de['bt04']);//查询时纬度经度
				if($user_id!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						$reservebatteryInfo = $this->model_search->getfullPowerBattery($bss_id);
						if($reservebatteryInfo)
						{
							$returnArr["rt_cd"] = "0000";//成功
							$returnArr["rt_msg"] = "成功";
							$batteryStr = "";
							foreach($reservebatteryInfo as $key=> $value)
							{
								if($batteryStr == "")
									$batteryStr = $value["battery_id"];
								else
									$batteryStr .= ",".$value["battery_id"];
							}
							$returnArr["rt_01"] = $batteryStr;//可預約之電池ID(電池ID以逗號隔開)
						}
						else
						{
							$returnArr["rt_cd"] = "0006";//查无可预约电池
							$returnArr["rt_msg"] = "查无可预约电池";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//会员ID错误
					$returnArr["rt_msg"] = "会员ID错误";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//預約電池API
	public function be3a034e()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'be3a034e','预约电池');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['rb01']);//查询日期
				if(isset($data_json_de['rb02']))
				{
					if($data_json_de['rb02']!="")
						$user_id = str_pad(addslashes($data_json_de['rb02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
					else
						$user_id = "";
				}
				else
					$user_id = "";
				if(isset($data_json_de['rb03']))
					$battery_id = addslashes($data_json_de['rb03']);//電池序號(battery ID)
				else
					$battery_id = "";

				// $coordinate = addslashes($data_json_de->bc03);//查询时纬度经度
				if($user_id!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						$checkreserveUser = $this->model_search->checkReserveUser($user_id);
						if($checkreserveUser == 0)
						{
							if($battery_id!="")
							{
								$batteryInfo = $this->model_search->getbatteryInfo($battery_id);
								if($batteryInfo)
								{
									$checkbattery = $this->model_search->checkReserveBattery($battery_id);
									if($checkbattery)
									{
										$ReBattery = $this->model_register->reserverBattery($user_id,$battery_id);
										if($ReBattery)
										{
											$returnArr["rt_cd"] = "0000";//成功
											$returnArr["rt_msg"] = "成功";
										}
										else
										{
											$returnArr["rt_cd"] = "0009";//预约失败
											$returnArr["rt_msg"] = "预约失败";
										}
									}
									else
									{
										$returnArr["rt_cd"] = "0007";//查无电池电量
										$returnArr["rt_msg"] = "此电池已被预约或借出,请换别颗电池预约";
									}
								}
								else
								{
									$returnArr["rt_cd"] = "0006";//查无电池电量
									$returnArr["rt_msg"] = "电池序号有误";
								}
							}
							else
							{
								$returnArr["rt_cd"] = "0004";//查无电池电量
								$returnArr["rt_msg"] = "电池ID不得为空";
							}
						}
						else
						{
							$returnArr["rt_cd"] = "0008";//此会员已经预约过
							$returnArr["rt_msg"] = "此会员已经预约过";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//会员ID错误
					$returnArr["rt_msg"] = "会员ID错误";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//锁定电池
	public function c6359082()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'c6359082','锁定电池');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['lb01']);//電池鎖定時間
				if($data_json_de['lb02']!="")
					$user_id = str_pad(addslashes($data_json_de['lb02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
				else
					$user_id = "";
				$battery_id = addslashes($data_json_de['lb03']);//電池序號
				$locked_status = addslashes($data_json_de['lb04']);//鎖定狀態,0:未鎖定,1:已鎖定
				$otherdataArr['user_id'] = $user_id;
				$otherdataArr['battery_id'] = $battery_id;
				if($battery_id!="")
				{
					if($user_id!="")
					{
						$member_info = $this->model_search->get_member_info1($user_id);
						if($member_info)
						{
							if($this->model_register->lockbattery($battery_id,$locked_status))
							{
								$returnArr["rt_cd"] = "0000";//成功
								$returnArr["rt_msg"] = "成功";
							}
							else
							{
								$returnArr["rt_cd"] = "0006";//鎖定狀態更改失敗
								$returnArr["rt_msg"] = "锁定状态更改失败";
							}
						}
						else
						{
							$returnArr["rt_cd"] = "0002";//查无会员资料
							$returnArr["rt_msg"] = "查无会员资料";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0005";//会员ID错误
						$returnArr["rt_msg"] = "会员User ID不得為空";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0004";//电池序号不得为空
					$returnArr["rt_msg"] = "电池序号不得为空";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		// print_r($returnArr);
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//同步營運商
	public function eab3ce16()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'eab3ce16','同步营运商');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['op01']);//營運商同步時間
				$operatorArr = $data_json_de['em_info'];//營運商資料
				$returnArr = $this->model_register->syncop($operatorArr);
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		// print_r($returnArr);
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//同步付費方案
	public function a52c6ce6()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'a52c6ce6','同步付费方案');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['lp01']);//付費方案同步時間
				$leaseplanArr = $data_json_de['em_info'];//付費方案資料
				$returnArr = $this->model_register->synclp_new($leaseplanArr);
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		// print_r($returnArr);
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//查詢電池座標API
	public function da736966()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'da736966','查询电池座标');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['qb01']);//查询日期
				if(isset($data_json_de['eminfo']))
					$battery_idArr = $data_json_de['eminfo'];//電池序號(battery ID)
				else
					$battery_idArr = "";


				if($battery_idArr!="")
				{
					$returnArr["rt_cd"] = "0000";//成功
					$returnArr["rt_msg"] = "成功";
					$batteryIDSet = "";
					foreach($battery_idArr as $key => $data_json_de1)
					{
						if($data_json_de1)
						{
							if($batteryIDSet=="")
								$batteryIDSet = "'{$data_json_de1['bq02']}'";
							else
								$batteryIDSet .= ",'{$data_json_de1['bq02']}'";
						}
					}
					$batteryLocation = $this->model_search->getbatteryInfo($batteryIDSet);
					foreach($batteryLocation as $keyb => $valueb)
					{
						foreach($battery_idArr as $key => $data_json_de1)
						{
							$battery_id = $data_json_de1['bq02'];
							if($valueb && $valueb['battery_id']==$data_json_de1['bq02'])
							{
								$returnArr["em_info"][$keyb]["rt_01"] = $valueb['latitude'];//电池GPS緯度
								$returnArr["em_info"][$keyb]["rt_02"] = $valueb['longitude'];//电池GPS經度
								$returnArr["em_info"][$keyb]["rt_03"] = $valueb['battery_id'];//電池序號(battery ID)
							}
						}
						if(!isset($returnArr["em_info"][$keyb]["rt_03"]))
						{
							$returnArr["em_info"][$keyb]["rt_01"] = "";//电池GPS緯度
							$returnArr["em_info"][$keyb]["rt_02"] = "";//电池GPS經度
							$returnArr["em_info"][$keyb]["rt_03"] = $valueb['battery_id'];//電池序號(battery ID)
						}
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0004";//電池不得為空
					$returnArr["rt_msg"] = "电池ID不得为空";
				}

			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//查询时间区间电池座标API
	public function b60338c3()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'b60338c3','查询时间区间电池座标');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = addslashes($data_json_de['qd01']);//查询日期
				if(isset($data_json_de['eminfo']))
					$battery_idArr = $data_json_de['eminfo'];
				else
					$battery_idArr = "";
				// if(isset($data_json_de['qd03']))
				// 	$startdate = addslashes($data_json_de['qd03']);//查詢起始時間
				// else
				// 	$startdate = "";
				// if(isset($data_json_de['qd04']))
				// 	$enddate = addslashes($data_json_de['qd04']);//查詢結束時間
				// else
				// 	$enddate = "";


				if($battery_idArr)
				{
					$returnArr["rt_cd"] = "0000";//成功
					$returnArr["rt_msg"] = "成功";
					foreach($battery_idArr as $keyb => $data_json_de1)
					{
						if($data_json_de1['qd02']!=""&&$data_json_de1['qd03']!=""&&$data_json_de1['qd04']!="")
						{
							$battery_id = $data_json_de1['qd02'];//電池序號(battery ID)
							$startdate = $data_json_de1['qd03'];//查詢起始時間
							$enddate = $data_json_de1['qd04'];//查詢結束時間
							$batteryLocation = $this->model_search->batterydategps($battery_id,$startdate,$enddate);
							$gps_path_track = "";
							if($batteryLocation)
							{
								foreach($batteryLocation as $key => $value)
								{
									if($gps_path_track=="")
										$gps_path_track = $value['gps'];
									else
										$gps_path_track .= ";".$value['gps'];
								}
							}
							$returnArr["em_info"][$keyb]["rt_01"] = $gps_path_track;//時間區間內的电池GPS座標(以分號隔開)
							$returnArr["em_info"][$keyb]["rt_02"] = $battery_id;//電池序號(battery ID)
						}
					}

				}
				else
				{
					$returnArr["rt_cd"] = "0004";//資料不得為空
					$returnArr["rt_msg"] = "资料不得为空";
				}

			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//乱数产生token
	function generatorToken()
	{
	    $password_len = 8;
	    $password = '';

	    // remove o,0,1,l
	    $word = 'abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ23456789';
	    $len = strlen($word);

	    for ($i = 0; $i < $password_len; $i++) {
	        $password .= $word[rand() % $len];
	    }

	    return $password;
	}

	//查詢驗證碼
	public function e0376432()
	{
		//計算執行時間
		$time_start = microtime(true);
		//写入API收到的资料到历史纪录
		$api_log_sn = $this->model_common->insert_api_log($this->input->post(),'e0376432','查詢驗證碼');
		$postjson = $this->input->post("JSONData");
		if($this->model_common->is_json($postjson))
		{
			$returnArr = array();
			//收到空资料
			if($postjson == "")
			{
				$returnArr["rt_cd"] = "0003";//资料为空
				$returnArr["rt_msg"] = "资料为空";//回应讯息
			}
			else
			{
				$dataArr = array();
				$data_json_de = json_decode($postjson,true);
				$quiry_date = "";
				$user_id = "";
				$bss_id = "";
				$coordinate = "";
				if(isset($data_json_de['vs01']))
					$quiry_date = addslashes($data_json_de['vs01']);//查询日期
				if(isset($data_json_de['vs02']))
				{
					if($data_json_de['vs02']!="")
						$user_id = str_pad(addslashes($data_json_de['vs02']),32,"0",STR_PAD_LEFT);//user_id,左補0補滿32碼
					else
						$user_id = "";
				}
				if(isset($data_json_de['vs03']))
					$bss_id = addslashes($data_json_de['vs03']);//BSS ID
				if(isset($data_json_de['vs04']))
					$coordinate = addslashes($data_json_de['vs04']);//查询时纬度经度
				if($quiry_date!="" && $user_id!="" && $bss_id!="")
				{
					$member_info = $this->model_search->get_member_info1($user_id);
					if($member_info)
					{
						$bssInfo = $this->model_search->get_bssinfo($bss_id);
						if($bssInfo)
						{
							$verificateCode = $this->generateVerificateCode();
							if($this->model_register->saveVerificateCode($user_id,$bss_id,$verificateCode,$quiry_date))
							{
								$returnArr["rt_cd"] = "0000";//成功
								$returnArr["rt_msg"] = "成功";
								$returnArr["rt_01"] = $verificateCode;
							}
							else
							{
								$returnArr["rt_cd"] = "0006";//DB寫入失敗
								$returnArr["rt_msg"] = "DB寫入失敗";
							}
							
						}
						else
						{
							$returnArr["rt_cd"] = "0004";//查无機櫃資料
							$returnArr["rt_msg"] = "查无機櫃資料";
						}
					}
					else
					{
						$returnArr["rt_cd"] = "0002";//查无会员资料
						$returnArr["rt_msg"] = "查无会员资料";
					}
				}
				else
				{
					$returnArr["rt_cd"] = "0005";//資料错误
					$returnArr["rt_msg"] = "資料错误";
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0001";//格式错误
			$returnArr["rt_msg"] = "格式错误";//格式错误
		}
		echo json_encode($returnArr);
		$time_end = microtime(true);
		$time = $time_end - $time_start;
		$otherdataArr['sys_spend_time'] = $time;
		$this->model_common->update_api_log($api_log_sn,json_encode($returnArr,JSON_UNESCAPED_UNICODE),$otherdataArr);
	}

	//乱数产生驗證碼
	function generateVerificateCode()
	{
	    $password_len = 4;
	    $password = '';

	    // remove o,0,1,l
	    $word = '1234567890';
	    $len = strlen($word);

	    for ($i = 0; $i < $password_len; $i++) {
	        $password .= $word[rand() % $len];
	    }

	    return $password;
	}
}	

/* End of file Bss01.php */
/* Location: ./application/controllers/api/Bss01.php */