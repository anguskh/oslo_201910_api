<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bss3 extends CI_Controller {
	
	/**
	 * 建構式
	 * 預先載入Bssapi的物件
	 */
    function __construct() 
    {
        parent::__construct();
        $this->load->model("common/model_common", "model_common") ;
        $this->load->model("bss/model_bss01", "model_bss01") ;
        $this->load->model("bss/model_bss02", "model_bss02") ;
        $this->load->model("bss/model_bss03", "model_bss03") ;
        $this->load->model("bss/model_bss04", "model_bss04") ;
        $this->load->model("bss/model_bss05", "model_bss05") ;
        $this->load->model("bss/model_bss06", "model_bss06") ;
        $this->load->model("bss/model_bss07", "model_bss07") ;
        $this->load->model("bss/model_bss08", "model_bss08") ;
        // $this->load->model("common/model_websocket","model_websocket");
    }
	
	//監控回報
	public function b61eefbd()
	{
		$this->model_bss01->save_exchange_monitor_log();
	}

	//借電成功
	public function c2af4999()
	{
		$this->model_bss02->borrow_battery_success();
	}

	//還電取碼
	public function c6cbfe3b()
	{
		$this->model_bss03->get_return_qrcode();
	}

	//還電成功
	public function f6d48830()
	{
		$this->model_bss03->return_battery();
	}

	//付款確認查詢
	public function d6f6d2bc()
	{
		$this->model_bss04->save_battery_exchange_log2();
	}

	//即時告警
	public function f1ace353()
	{
		$this->model_bss05->timely_warring();
	}

	//可租借電池數量
	public function cc7c34fd()
	{
		$this->model_bss01->report_battery_num();
	}

	//比对借电电池ID API
	public function c6be119b()
	{
		$this->model_bss02->compare_leave_battery_id();
	}

	//取得BSS广告网址
	public function e87dd345()
	{
		$this->model_bss04->get_ad_url();
	}

	//查询预约电池
	public function b2d60436()
	{
		$this->model_bss04->searchReserve();
	}

	//新借电回报
	public function ee4451d1()
	{
		$this->model_bss06->borrow_battery_report();
	}

	//新还电回报
	public function c05d76e4()
	{
		$this->model_bss07->return_battery();
	}

	//建立交易
	public function c2c77a9c()
	{
		$this->model_bss08->create_transaction();
	}

	//掃碼確認查詢
	public function e9b229f6()
	{
		$this->model_bss04->quiniryScan();
	}

}

/* End of file Bssapi.php */
/* Location: ./application/controllers/api/Bssapi.php */