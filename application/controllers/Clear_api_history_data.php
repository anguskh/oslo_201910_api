<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clear_api_history_data extends CI_Controller {
	
	/**
	 * 建構式
	 * 預先載入Clear_api_history_data的物件
	 */
    function __construct() 
    {
        parent::__construct();
		$this->load->model("model_clear_api_history_data", "model_clear_api_history_data") ;
    }

    public function index()
    {
    	$this->model_clear_api_history_data->cleardata();
    }
    
}

/* End of file Clear_api_history_data.php */
/* Location: ./application/controllers/Clear_api_history_data.php */