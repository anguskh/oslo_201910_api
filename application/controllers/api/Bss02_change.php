<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bss02 extends CI_Controller {
	
	/**
	 * 建構式
	 * 預先載入Bss02的物件
	 */
    function __construct() 
    {
        parent::__construct();
        $this->load->model("common/model_common", "model_common") ;
        $this->load->model("api/model_bss02", "model_bss02") ;
    }
	
	public function index()
	{
		$this->model_bss02->save_battery_exchange_log();
	}
	
}

/* End of file Bss02.php */
/* Location: ./application/controllers/api/Bss02.php */