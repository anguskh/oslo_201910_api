<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bssapi extends CI_Controller {
	
	/**
	 * 建構式
	 * 預先載入Bssapi的物件
	 */
    function __construct() 
    {
        parent::__construct();
        $this->load->model("common/model_common", "model_common") ;
        $this->load->model("api/model_bss01", "model_bss01") ;
        $this->load->model("api/model_bss02", "model_bss02") ;
        $this->load->model("api/model_bss03", "model_bss03") ;
        $this->load->model("api/model_bss04", "model_bss04") ;
        $this->load->model("api/model_bss05", "model_bss05") ;
        // $this->load->model("Model_log_api_leave", "Model_log_api_leave") ;
        // $this->load->model("api/model_insert_battery_info_new", "model_insert_battery_info_new") ;
    }
	
	//監控
	public function e9b2ed77()
	{
		$this->model_bss01->save_exchange_monitor_log();
	}
	
	//借電成功
	public function c2af4999()
	{
		$this->model_bss02->borrow_battery_success();
	}

	//還電計費
	public function b4496c8a()
	{
		$this->model_bss03->return_battery();
	}

	//借電請求查詢
	public function d7be6c29()
	{
		$this->model_bss04->save_battery_exchange_log();
	}

	//卡片次數查詢
	public function e94335f6()
	{
		$this->model_bss04->get_card_lave_num();
	}

	//即時告警
	public function f1ace353()
	{
		$this->model_bss05->timely_warring();
	}

	//可租借電池數量
	public function cc7c34fd()
	{
		$this->model_bss01->report_baatery_num();
	}

	//比对借电电池ID API
	public function c6be119b()
	{
		$this->model_bss02->compare_leave_battery_id();
	}

	//取得BSS广告网址
	public function e87dd345()
	{
		$this->model_bss04->get_ad_url();
	}
	// public function insertfromlog()
	// {
	// 	$this->model_insert_battery_info_new->insert_battery_log();
	// }

	// public function showdata()
	// {
	// 	$this->model_insert_battery_info_new->showdata();
	// }

	// public function updateleave()
	// {
	// 	$this->Model_log_api_leave->get_log();
	// }

	// public function updatebatterylog($limit1="",$limit2="")
	// {
	// 	ini_set("memory_limit","1G");
	// 	set_time_limit(0);
	// 	$this->Model_log_api_leave->fill_log($limit1,$limit2);
	// }

	// public function checkdistance($s_num = "")
	// {
	// 	$this->Model_log_api_leave->checkdistance($s_num);
	// }
}

/* End of file Bssapi.php */
/* Location: ./application/controllers/api/Bssapi.php */