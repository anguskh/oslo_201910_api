<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ecu03 extends CI_Controller {
	
	/**
	 * 建構式
	 * 預先載入Ecu01的物件
	 */
    function __construct() 
    {
        parent::__construct();
        $this->load->model("common/model_common", "model_common") ;
        $this->load->model("api/model_ecu03", "model_ecu03") ;
    }
	
	public function index()
	{
		//$this->model_ecu03->savedrivingInfo();
	}

	//電池回報
	public function c516a8f6(){
		$this->model_ecu03->savedrivingInfo();
	}
	
}

/* End of file Ecu01.php */
/* Location: ./application/controllers/api/Ecu01.php */