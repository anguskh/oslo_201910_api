<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bss03 extends CI_Controller {
	
	/**
	 * 建構式
	 * 預先載入Bss03的物件
	 */
    function __construct() 
    {
        parent::__construct();
        $this->load->model("common/model_common", "model_common") ;
        $this->load->model("api/model_bss03", "model_bss03") ;
    }
	
	public function index()
	{
		$this->model_bss03->return_battery();
	}

}

/* End of file Bss03.php */
/* Location: ./application/controllers/api/Bss03.php */