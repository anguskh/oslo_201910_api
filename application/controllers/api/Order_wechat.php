<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_wechat extends CI_Controller {
	
	/**
	 * 建構式
	 * 預先載入Bss01的物件
	 */
    function __construct() 
    {
        parent::__construct();
        $this->load->model("api/Model_order_wechat", "Model_order_wechat") ;
		$this->load->library("MY_WxPay") ;

    }
	
	public function index()
	{
		ini_set('date.timezone','Asia/Taipei');
		$out_trade_no = WxPayConfig::MCHID.date("YmdHis");
		$pay_type = $this->input->post('pay_type');
		$pay_type = isset($pay_type)?$pay_type:'';
		
		$show_name = '';
		//儲值S / 支付P/ 押金D
		if($pay_type == 'D'){
			//押金 預設金額
			$price = 0.01;
			$show_name = '押金';
		}else{
			$show_name = (($pay_type=='S')?'儲值':'支付');

			$price = $this->input->post('price');
			$price = isset($price)?$price:0;
		}
		$total_fee = $price * 100;
		$personal_id = $this->input->post('personal_id');
		$personal_id = isset($personal_id)?$personal_id:'';
		$user_ip = $this->input->post('user_ip');
		$user_ip = isset($user_ip)?$user_ip:'';
		$user_ip = $_SERVER['REMOTE_ADDR'];
		$web = $this->config->item('base_url');
		$notify_url = $web."api/Order_wechat/return_wechat";
		$par_arr = array();
		$pay_date_start = date("YmdHis");  
		$pay_date_end = date("YmdHis", time() + 60*10);  
		$payorder_sum = 0;
		$payorder_arr = array();
		if($personal_id != ''){
			// 准备微信支付参数数组
			$params = array(
				'out_trade_no'		=> $out_trade_no,
				'body'				=> '微信支付測試-'.$show_name,
				'total_fee'			=> $total_fee,
				'trade_type'		=> 'APP',
				'pay_date_start'    => $pay_date_start,
				'pay_date_end'		=> $pay_date_end,
				'notify_url'		=> $notify_url
			);

			$payorder_arr = array(
				'user_ip'			=> $user_ip,
				'pay_type'			=> $pay_type,
				'personal_id'		=> $personal_id,
				'out_trade_no'		=> $out_trade_no,
				'price'				=> $price,
				'total_fee'			=> $total_fee,
				'pay_date_start'    => $pay_date_start,
				'pay_date_end'		=> $pay_date_end,
				'pay_status'		=> 0
			);
			//INSERT至tbl_payorder
			$payorder_sum = $this->Model_order_wechat->insert_payorder($payorder_arr );
			$par_arr = $this->my_wxpay->getUnifiedOrderInput($params);
		}
		$this->Model_order_wechat->install_wechat($par_arr, $payorder_sum);
	}

	public function return_wechat(){
		$xmlData = file_get_contents('php://input');
		libxml_disable_entity_loader(true);
		$data = json_decode(json_encode(simplexml_load_string($xmlData, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
		if(count($data)>1){
			ksort($data);
			$buff = '';
			foreach ($data as $k => $v){
				if($k != 'sign'){
					$buff .= $k . '=' . $v . '&';
				}
			}

			$stringSignTemp = $buff . 'key='.WxPayConfig::KEY;//key为证书密钥
			$sign = strtoupper(md5($stringSignTemp));

			if($sign == $data['sign']){
				//insert 內容到 log_payment

				//檢查有沒有送過了
				$pay_status = $this->Model_order_wechat->Get_logpayment($data['out_trade_no']);
				if($pay_status == '0'){
					$this->Model_order_wechat->insert_log_payment($data);
					
					$update_arr = array();
					//查out_trade_no在
					$payorder_arr = $this->Model_order_wechat->get_payorder($data['out_trade_no']);
					if($data['result_code'] == 'SUCCESS' && $data['return_code'] == 'SUCCESS'){
						//更新tbl_payorder;
						$update_arr['pay_status'] = 1;
						$update_arr['pay_ok_time'] = substr($data['time_end'],0,4).'-'.substr($data['time_end'],4,2).'-'.substr($data['time_end'],6,2).' '.substr($data['time_end'],8,2).':'.substr($data['time_end'],10,2).':'.substr($data['time_end'],12,2);
					}else{
						$update_arr['pay_status'] = 2;
					}
					$update_arr['update_date'] = 'now()';
					$this->Model_order_wechat->update_payorder($update_arr, $payorder_arr);
				}
			}

			$fp = fopen('./xml/log_wechat.txt',"a+");
			fwrite($fp, $xmlData."\r\n");
			fwrite($fp, $buff."\r\n");
			fwrite($fp, $sign."\r\n");
			fwrite($fp, $data['sign']."\r\n");
			fclose($fp);
		}
	}
	
	public function hello(){
		echo 'hello';
	}
	public function hihi(){
		$web = $this->config->item('base_url');
		$url = $web."api/Order_wechat/hello";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array("price"=>"1", "personal_id"=>"999999", "user_ip"=>"192.168.1.137"))); 
		$output = curl_exec($ch); 
		curl_close($ch);
		echo $output;	
	}

	public function hihi3(){
		$web = $this->config->item('base_url');

		$filename = "./xml/file001.xml"; 
		$handle = fopen($filename, "r"); 
		$XPost = fread($handle, filesize($filename)); 
		fclose($handle); 
		$url = $web."api/Order_wechat/return_wechat";
		$ch = curl_init(); // initialize curl handle 
		curl_setopt($ch, CURLOPT_VERBOSE, 1); // set url to post to 
		curl_setopt($ch, CURLOPT_URL, $url); // set url to post to 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return into a variable 
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml")); 
		curl_setopt($ch, CURLOPT_HEADER, 1); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 40); // times out after 4s 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $XPost); // add POST fields 
		curl_setopt($ch, CURLOPT_POST, 1); 

		$result = curl_exec($ch); // run the whole process 
		print_r($result);
	}

	public function hihi2(){
		$web = $this->config->item('base_url');
		$url = $web."api/Order_wechat";
		 
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array("pay_type"=>"D", "price"=>"1", "personal_id"=>"999999", "user_ip"=>"192.168.1.137"))); 
		$output = curl_exec($ch); 
		curl_close($ch);
		 
		echo $output;	
	}
}

/* End of file Bss01.php */
/* Location: ./application/controllers/api/Bss01.php */