<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ecu02 extends CI_Controller {
	
	/**
	 * 建構式
	 * 預先載入Ecu01的物件
	 */
    function __construct() 
    {
        parent::__construct();
        $this->load->model("common/model_common", "model_common") ;
        $this->load->model("api/model_ecu02", "model_ecu02") ;
    }
	
	public function index()
	{
		$this->model_ecu02->savedrivingInfo();
	}
	
	public function insertcar()
	{
		$this->model_ecu02->insertcar();
	}
}

/* End of file Ecu01.php */
/* Location: ./application/controllers/api/Ecu01.php */