<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bss01 extends CI_Controller {
	
	/**
	 * 建構式
	 * 預先載入Bss01的物件
	 */
    function __construct() 
    {
        parent::__construct();
        $this->load->model("common/model_common", "model_common") ;
        $this->load->model("api/model_bss01", "model_bss01") ;
    }
	
	public function index()
	{
		$this->model_bss01->save_exchange_monitor_log();
	}

	public function insertbattery()
	{
		$this->model_bss01->insertbattery();
	}
	
}

/* End of file Bss01.php */
/* Location: ./application/controllers/api/Bss01.php */