<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Testtool extends CI_Controller {
	
	/**
	 * 建構式
	 * 預先載入Testtool的物件
	 */
    function __construct() 
    {
        parent::__construct();
		$this->load->database();
		$this->load->model("Model_testtool", "Model_testtool") ;
    }
	
	public function index()
	{
		$data["batteryswaps"] = $this->Model_testtool->getbatteryswapstationList();
		$data["battery"] = $this->Model_testtool->getbatterysList();
		$data["vehicle"] = $this->Model_testtool->getvehicleList();

		$this->load->view( "common/header", $data) ;
		$this->load->view( "bss01test_form", $data) ;
	}

	public function testbss02()
	{
		$data["batteryswaps"] = $this->Model_testtool->getbatteryswapstationList();
		$data["battery"] = $this->Model_testtool->getbatterysList();
		$data["vehicle"] = $this->Model_testtool->getvehicleList();

		$this->load->view( "common/header", $data) ;
		$this->load->view( "bss02test_form", $data) ;
	}

	public function testbss03()
	{
		$data["batteryswaps"] = $this->Model_testtool->getbatteryswapstationList();
		$data["battery"] = $this->Model_testtool->getbatterysList();
		$data["vehicle"] = $this->Model_testtool->getvehicleList();

		$this->load->view( "common/header", $data) ;
		$this->load->view( "bss03test_form", $data) ;
	}

	public function testbss04()
	{
		$data["batteryswaps"] = $this->Model_testtool->getbatteryswapstationList();
		$data["battery"] = $this->Model_testtool->getbatterysList();
		$data["vehicle"] = $this->Model_testtool->getvehicleList();

		$this->load->view( "common/header", $data) ;
		$this->load->view( "bss04test_form", $data) ;
	}

	public function testecu01()
	{
		$data["vehicle"] = $this->Model_testtool->getvehicleList();

		$this->load->view( "common/header", $data) ;
		$this->load->view( "ecu01test_form", $data) ;
	}

	public function testecu02()
	{
		$data["vehicle"] = $this->Model_testtool->getvehicleList();

		$this->load->view( "common/header", $data) ;
		$this->load->view( "ecu02test_form", $data) ;
	}

	public function bss01Rank(){
		$this->Model_testtool->bss01Rank();
	}

	public function bss02Rank(){
		$this->Model_testtool->bss02Rank();
	}

	public function ecu01Rank(){
		$this->Model_testtool->ecu01Rank();
	}

	public function ecu02Rank(){
		$this->Model_testtool->ecu02Rank();
	}
}

/* End of file Testtool.php */
/* Location: ./application/controllers/api/Testtool.php */