<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clear_leavereturn_data extends CI_Controller {
	
	/**
	 * 建構式
	 * 預先載入Clear_leavereturn_data的物件
	 */
    function __construct() 
    {
        parent::__construct();
		$this->load->model("model_clear_leavereturn_data", "model_clear_leavereturn_data") ;
    }

    public function index()
    {
    	$data['memberInfo'] = $this->model_clear_leavereturn_data->getmemberdata();
    	$this->load->view( "clear_leavereturn_data_form",$data) ;
    }
    
    public function cleardataanydata()
   	{
   		$this->model_clear_leavereturn_data->cleardataanydata();
   	}
   	
	public function cleardata()
	{
		echo $this->model_clear_leavereturn_data->cleardata();
	}

	public function clearTeodata()
	{
		echo $this->model_clear_leavereturn_data->clearTeodata();
	}
}

/* End of file Clear_leavereturn_data.php */
/* Location: ./application/controllers/Clear_leavereturn_data.php */