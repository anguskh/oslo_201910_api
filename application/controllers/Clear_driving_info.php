<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clear_driving_info extends CI_Controller {
	
	/**
	 * 建構式
	 * 預先載入Clear_driving_info的物件
	 */
    function __construct() 
    {
        parent::__construct();
		$this->load->model("model_clear_driving_info", "model_clear_driving_info") ;
    }

    public function index()
    {
    	$this->model_clear_driving_info->cleardata();
    }
    
}

/* End of file Clear_api_history_data.php */
/* Location: ./application/controllers/Clear_api_history_data.php */