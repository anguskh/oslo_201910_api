<?php 
	
	function readfile1($PATH){
	  $dataArr = array();
	  if(file_exists($PATH))
	  {
	   $txt = fopen($PATH,"r");
	   $filecontent = fread($txt,10240);
	   $filecontent = iconv("BIG5","UTF-8",$filecontent);
	   $dataArr = explode("\r\n",$filecontent);
	  }
	  return $dataArr;
	}

	if(isset($_POST['JSONData']))
	{
		$now_time = date("Y-m-d H:i:s");
		$postjson = $_POST['JSONData'];
		$dataArr = array();
		$data_json_de = json_decode($postjson,true);
		$requestdate = addslashes($data_json_de['rq01']);//查詢日期
		$bss_id = addslashes($data_json_de['rq02']);//借電站BSS ID

		if($requestdate!="" && $bss_id!="")
		{
			$filepath = "./tmpfile/cfc777c9.txt";
			$startTime = "";
			$endTime = "";
			$returnArr["rt_cd"] = "0000";//成功
			$returnArr["rt_msg"] = "成功";//成功
			if(!file_exists($filepath)){
				$returnArr["rt_01"] = "N";//不需自毀
			}
			else
			{
				$dataArr1 = readfile1($filepath);
				if($dataArr1!="")
				{
					$timeArr = explode(",",$dataArr1[0]);
					if(count($timeArr)==2)
					{
						$startTime = $timeArr[0];
						$endTime = $timeArr[1];
						if($now_time<$endTime && $now_time>$startTime)
						{
							$returnArr["rt_01"] = "Y";//需自毀
						}
						else
						{
							$returnArr["rt_01"] = "N";//不需自毀
						}
					}
					else
					{
						$returnArr["rt_01"] = "Y";//需自毀
					}
				}
				else
				{
					$returnArr["rt_01"] = "N";//不需自毀
				}
			}
		}
		else
		{
			$returnArr["rt_cd"] = "0003";//資料為空
			$returnArr["rt_msg"] = "资料为空";//資料為空
		}
	}
	else
	{
		$returnArr["rt_cd"] = "0003";//資料為空
		$returnArr["rt_msg"] = "资料为空";//資料為空
	}
	echo json_encode($returnArr);
?>