<?
	session_start();

	header("Content-Type:text/html; charset=utf-8");
	date_default_timezone_set("Asia/Taipei");

	if (!empty($_SERVER["HTTP_CLIENT_IP"])){
		$ip = $_SERVER["HTTP_CLIENT_IP"];
	}elseif(!empty($_SERVER["HTTP_X_FORWARDED_FOR"])){
		$ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
	}else{
		$ip = $_SERVER["REMOTE_ADDR"];
	}
	$now_time = date("Y-m-d H:i:s");

	$res = 'N';
	$res_content = "";
	// if(isset($_SESSION['key_value']) && $_SESSION['key_value'] == $_SESSION['getkey']){
		if($_POST['c_type']  == '1'){
			//輕度
			//檢查檔案存不存在 
			if(!file_exists("./tmpfile/d1c2485bf.txt")){
				$file = fopen("./tmpfile/d1c2485bf.txt","w");
				fputs($file,"Y");
				fclose($file);
			}

			$api_arr = array(
				"c6be119b",
				"c6cbfe3b",
				"c2af4999",
				"df57ab54",
				"ba191a5",
				"d31387a9",
				"c9a4b934d",
				"b61eefbd"
			);

			$PATH = "./tmpfile/";
			$show_cut = array();
			foreach($api_arr as $k=>$api_name){
				if(file_exists("./tmpfile/".$api_name)){
					unlink('./tmpfile/'.$api_name);//將檔案刪除
				}

			}

			$res = 'Y';

		}else if($_POST['c_type'] == '2'){
			//中度
			//檢查檔案存不存在 
			if(file_exists("./tmpfile/test.zip")){
				//先做刪除再重產
				exec('rm -f /var/www/html/DogWood_api/tmpfile/test.zip', $out, $return_var);
			}

			exec('zip -P 12345 ./tmpfile/test.zip ./application/config/database.php', $out, $return_var);
			exec('sh break.sh', $out, $return_var);
			//var_dump($out);
			//var_dump($return_var);
			$res = 'Y';
		}else if($_POST['c_type'] == '3'){
			//重度

			$res = 'Y';
		}else if($_POST['c_type'] == '4'){
			//解除
			$res = 'Y';
			exec('unzip -o -P 12345 ./tmpfile/test.zip', $out, $return_var);
			//exec('chown -r apache.dogwood /var/www/html/DogWood_api/application', $out, $return_var);
			//exec('chown apache.dogwood /var/www/html/DogWood_api/application/config/database.php', $out, $return_var);
			//將txt檔刪除
			if(file_exists("./tmpfile/d1c2485bf.txt")){
				unlink('./tmpfile/d1c2485bf.txt');//將檔案刪除
			}
		}

		$log_txt = $_POST['c_name'].','.$now_time.','.$ip."\r\n";
		//記log
		$file2 = fopen("./tmpfile/d1c2485bf_log.txt","a+");
		fputs($file2, $log_txt);
		fclose($file2);
	// }
	$rs_arr[0] = $res;
	$rs_arr[1] = $res_content;
	echo json_encode($rs_arr);   
?>

